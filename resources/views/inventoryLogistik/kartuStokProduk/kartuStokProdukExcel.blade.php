<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Kartu Stok Produk " . date('d-m-Y') . ".xls");
?>

<h1 align="center">KARTU STOK PRODUK</h1>
<table width="100%">
    <tbody>
    <tr>
        <td width="33%" align="center">Nama Kategori Produk : {{ $nama_kategori_produk }}</td>
        <td width="33%" align="center">Nama Produk : {{ $nama_produk }}</td>
        <td width="33%" align="center">Tanggal : {{ $date_start }} s/d {{ $date_end }}</td>
        <td width="33%" align="center">Lokasi : {{ $nama_lokasi }}</td>
    </tr>
    </tbody>
</table>
<br/>
<table width="100%" border="1" class="table-data">
    <caption>

    </caption>
    <thead>
    <tr>
        <th colspan="<?php echo $stok_masuk_col ?>" class="text-center" width="50%">STOK MASUK</th>
        <th colspan="<?php echo $stok_keluar_col ?>" class="text-center" width="50%">STOK KELUAR</th>
    </tr>
    <tr>
        <th>Tanggal</th>
        @if($view_produk == 'all')
            <th>Produk</th>
        @endif
        <th>Kode Produksi</th>
        <th>Masuk</th>
        @if($view_lokasi == 'all')
            <th>Lokasi</th>
        @endif
        <th>Keterangan</th>
        <th>Tanggal</th>
        @if($view_produk == 'all')
            <th>Produk</th>
        @endif
        <th>Kode Produksi</th>
        <th>Keluar</th>
        @if($view_lokasi == 'all')
            <th>Lokasi</th>
        @endif
        <th>Penerima/Keterangan</th>
        <th>Stok Akhir</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $tgl => $value)

        @foreach($value as $r_arus)
            @php
                $status = $r_arus->stok_in == 0 ? 'out' : 'in';
                    $stok_akhir = $r_arus->last_stok;
            @endphp

            @if($status == 'in')
                <tr>
                    <td>{{ Main::format_date($tgl) }}</td>
                    @if($view_produk == 'all')
                        <td>{{ $r_arus->kode_produk.' '.$r_arus->nama_produk }}</td>
                    @endif
                    <td>{{ $r_arus->no_seri_produk }}</td>
                    <td>{{ Main::format_number($r_arus->stok_in) }}</td>
                    @if($view_lokasi == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    <td>{{ $r_arus->arus_stok_produk_keterangan }}</td>
                    <td></td>
                    @if($view_produk == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    @if($view_lokasi == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td></td>
                    <td>{{ Main::format_number($stok_akhir) }}</td>
                </tr>
            @else
                <tr>
                    <td></td>
                    @if($view_produk == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td></td>
                    @if($view_lokasi == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td>{{ Main::format_date($tgl) }}</td>
                    @if($view_produk == 'all')
                        <td>{{ $r_arus->kode_produk.' '.$r_arus->nama_produk }}</td>
                    @endif
                    <td>{{ $r_arus->no_seri_produk }}</td>
                    <td>{{ Main::format_number($r_arus->stok_out) }}</td>
                    @if($view_lokasi == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    <td>{{ $r_arus->arus_stok_produk_keterangan }}</td>
                    <td>{{ Main::format_number($stok_akhir) }}</td>
                </tr>
            @endif
        @endforeach
    @endforeach

    </tbody>
</table>