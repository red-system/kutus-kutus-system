@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>

@endsection

@section('body')

    @include('dashboard.modal_detail')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="row">
                <div class="col-lg-7">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Daftar Hutang
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <table class="table m-table table-bordered m-table--head-bg-success datatable-no-order">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode Piutang</th>
                                    <th>Faktur Jual</th>
                                    <th>Tanggal</th>
                                    <th>Jatuh Tempo</th>
                                    <th>Jumlah</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($list_piutang_pelanggan as $r)
                                    @php
                                        $piutang_total += $r->pp_amount;
                                    @endphp
                                    <tr>
                                        <td>{{ $no_piutang++ }}.</td>
                                        <td>{{ $r->no_piutang_pelanggan }}</td>
                                        <td>{{ $r->pp_no_faktur }}</td>
                                        <td>{{ Main::format_date($r->tanggal_piutang) }}</td>
                                        <td>{{ Main::format_date($r->pp_jatuh_tempo) }}</td>
                                        <td align="right">{{ Main::format_money($r->pp_amount) }}</td>
                                        <td>
                                            <button type="button"
                                                    class="btn-detail m-btn btn btn-primary m-btn--pill btn-sm"
                                                    data-route="{{ route('piutangPelangganDetail', ['id'=>Main::encrypt($r->id)]) }}">
                                                <i class="la la-eye"></i> Detail
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                @foreach($list_piutang_lain as $r)
                                    @php
                                        $piutang_total += $r->pl_amount;
                                    @endphp
                                    <tr>
                                        <td>{{ $no_piutang++ }}.</td>
                                        <td>{{ $r->pl_invoice }}</td>
                                        <td>{{ '-' }}</td>
                                        <td>{{ Main::format_date($r->pl_tanggal) }}</td>
                                        <td>{{ Main::format_date($r->pl_jatuh_tempo) }}</td>
                                        <td align="right">{{ Main::format_money($r->pl_amount) }}</td>
                                        <td>
                                            <button type="button"
                                                    class="btn-detail m-btn btn btn-primary m-btn--pill btn-sm"
                                                    data-route="{{ route('piutangLainDetail', ['id'=>Main::encrypt($r->id)]) }}">
                                                <i class="la la-eye"></i> Detail
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="5" class="text-center"><strong>Total</strong></th>
                                    <th class="text-right">{{ Main::format_money($piutang_total) }}</th>
                                    <th></th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5">
                    <div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        History Belanja
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <table class="table m-table table-bordered m-table--head-bg-success datatable-no-order">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($list_penjualan as $r)
                                    @php
                                        $history_total += $r->grand_total;
                                    @endphp
                                    <tr>
                                        <td>{{ $no_history++ }}.</td>
                                        <td>{{ $r->no_faktur }}</td>
                                        <td>{{ Main::format_date($r->tanggal) }}</td>
                                        <td align="right">{{ Main::format_money($r->grand_total) }}</td>
                                        <td>
                                            <button type="button"
                                                    class="btn-detail m-btn btn btn-primary m-btn--pill btn-sm"
                                                    data-route="{{ route('historyBelanjaDetail', ['id'=>Main::encrypt($r->id)]) }}">
                                                <i class="la la-eye"></i> Detail
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="3" class="text-center"><strong>Total</strong></th>
                                    <th class="text-right">{{ Main::format_money($history_total) }}</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
