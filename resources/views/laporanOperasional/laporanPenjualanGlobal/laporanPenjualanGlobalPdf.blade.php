<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>Laporan Penjualan Global</h3></td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <table width="100%" class="table-header">
                <tbody>
                <tr>
                    <td>Tanggal Penjualan: {{ $date_start.' s/d '.$date_end }}</td>
                </tr>
                </tbody>
            </table>
            <br/>
            <div id="table">
                <table>
                    <tr class="tabletitle">
                        <th class="item">No Faktur</th>
                        <th class="item">Tanggal</th>
                        <th class="item">Jenis Transaksi</th>
                        <th class="item">Distributor</th>
                        <th class="item">Total</th>
                        <th class="item">Biaya Tambahan</th>
                        <th class="item">Potongan</th>
                        <th class="item">PPN</th>
                        <th class="item">Grand Total</th>
                    </tr>
                    @foreach($list as $r)
                        @php
                            $total_grand_total += $r->grand_total;
                            $total_biaya_tambahan += $r->biaya_tambahan;
                            $total_potongan += $r->total_potongan;
                            $total_ppn_nominal += $r->total_ppn_nominal;
                        @endphp
                        <tr class="service">
                            <td class="tableitem"><p class="itemtext">{{ $r->no_faktur }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_date($r->tanggal) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ ucwords($r->jenis_transaksi) }}</p></td>
                            <td class="tableitem"><p
                                        class="itemtext">{{ '('.$r->distributor->kode_distributor.') '.$r->distributor->nama_distributor }}</p>
                            </td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->total) }}</p></td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->biaya_tambahan) }}</p>
                            </td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->total_potongan) }}</p></td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->total_ppn_potongan) }}</p></td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->grand_total) }}</p>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5" align="center" class="font-weight-bold">
                            Total
                        </td>
                        <td align="right" class="font-weight-bold">{{ Main::format_number($total_biaya_tambahan) }}</td>
                        <td align="right" class="font-weight-bold">{{ Main::format_number($total_potongan) }}</td>
                        <td align="right" class="font-weight-bold">{{ Main::format_number($total_ppn_nominal) }}</td>
                        <td align="right" class="font-weight-bold">{{ Main::format_number($total_grand_total) }}</td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>