@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('inventoryLogistik/komposisiProduk/komposisiCreate')
    @include('inventoryLogistik/komposisiProduk/komposisiEdit')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("komposisiProdukPage") }}"
                       class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Komposisi Produk</span>
                            </span>
                    </a>
                    {!! "&nbsp" !!}
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                       data-toggle="modal" data-target="#modal-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Bahan</th>
                            <th>Nama Bahan</th>
                            <th>Satuan</th>
                            <th>Qty</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)

                            @php
                                $total += $r->qty
                            @endphp
                        <tr>
                            <td align="center">{{ $no++ }}.</td>
                            <td>{{ $r->bahan['kode_bahan'] }}</td>
                            <td>{{ $r->bahan['nama_bahan'] }}</td>
                            <td>{{ $r->bahan['satuan']['satuan'] }}</td>
                            <td align="right">{{ Main::format_number($r->qty) }}</td>
                            <td align="center">
                                <textarea class="row-data hidden">@json($r)</textarea>
                                <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                    <button type="button"
                                            class="m-btn btn btn-success btn-edit"
                                            data-route="{{ route('komposisiUpdate', ['id'=>$r->id]) }}"
                                            data-redirect="?table_value={{ $no-1 }}.&table_index=0">
                                        <i class="la la-edit"></i> Edit
                                    </button>
                                    <button type="button"
                                            class="m-btn btn btn-danger btn-hapus"
                                            data-route='{{ route('komposisiDelete', ['id'=>$r->id]) }}'>
                                        <i class="la la-remove"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" align="center">
                                    <strong>TOTAL</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_number($total) }}</strong>
                                </td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection