<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mProduk extends Model
{

    protected $table = 'tb_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_produk',
        'nama_produk',
        'id_kategori_produk',
        'id_lokasi',
        'harga_eceran',
        'harga_grosir',
        'harga_kosinyasi',
        'minimal_stok',
        'disc_persen',
        'disc_nominal'
    ];

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function kategori_produk() {
        return $this->belongsTo(mKategoriProduk::class, 'id_kategori_produk');
    }

    function stok_produk() {
        return $this->hasMany(mStokProduk::class, 'id_produk', 'id');
    }

    function target_produksi() {
        return $this->belongsTo(mTargetProduksi::class, 'id', 'id_produk');
    }

    function komposisi_produk() {
        return $this->hasMany(mKomposisiProduk::class, 'id_produk');
    }

    function pengemas_produk() {
        return $this->hasMany(mPengemasProduk::class, 'id_produk');
    }

    function detail_produksi() {
        return $this->hasMany(mDetailProduksi::class, 'id_produk');
    }

    function penjualan_produk() {
        return $this->hasMany(mPenjualanProduk::class, 'id_produk');
    }

    function progress_produksi() {
        return $this->hasMany(mProgressProduksi::class, 'id_produk');
    }
}
