<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPoBahanPembelian extends Model
{

    protected $table = 'tb_po_bahan_pembelian';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_po_bahan',
        'urutan',
        'no_pembelian',
        'tanggal'
    ];

    function po_bahan() {
        return $this->belongsTo(mPoBahan::class, 'id_po_bahan');
    }
}
