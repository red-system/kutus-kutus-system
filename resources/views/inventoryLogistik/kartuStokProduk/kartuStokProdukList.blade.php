@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/input-mask.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">


                        <div class="form-group m-form__group row">

                            <label class="col-lg-2 col-form-label">Tanggal Mulai</label>
                            <div class="col-lg-3">
                                <div class="input-group date">
                                    <input type="text"
                                           class="form-control m-input m_datepicker"
                                           readonly
                                           name="date_start"
                                           value="{{ $date_start }}"/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>

                            <label class="col-lg-2 col-form-label">Tanggal Selesai</label>
                            <div class="col-lg-3">
                                <div class="input-group date">
                                    <input type="text"
                                           class="form-control m-input m_datepicker"
                                           readonly
                                           name="date_end"
                                           value="{{ $date_end }}"/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Kategori Produk</label>
                            <div class="col-lg-3">
                                <select class="form-control m-select2" name="id_kategori_produk">
                                    <option value="all">Semua Kategori Produk</option>
                                    @foreach($kategori_produk as $r)
                                        <option value="{{ $r->id }}" {{ $id_kategori_produk == $r->id ? 'selected':'' }}>{{ $r->kategori_produk }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="col-lg-2 col-form-label">Produk</label>
                            <div class="col-lg-3">
                                <select class="form-control m-select2" name="id_produk">
                                    <option value="all">Semua Produk</option>
                                    @foreach($produk as $r)
                                        <option value="{{ $r->id }}" {{ $id_produk == $r->id ? 'selected':'' }}>{{ '('.$r->kode_produk.') '.$r->nama_produk }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-group m-form__group row">

                            <label class="col-lg-2 col-form-label">Gudang</label>
                            <div class="col-lg-3">
                                <select class="form-control m-select2" name="id_lokasi">
                                    <option value="all">Semua Gudang</option>
                                    @foreach($gudang as $r)
                                        <option value="{{ $r->id }}" {{ $id_lokasi == $r->id ? 'selected':'' }}>{{ $r->lokasi }}</option>
                                    @endforeach
                                </select>
                            </div>


                        </div>


                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('kartuStokProdukPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('kartuStokProdukExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover datatable-no-order">
                        <thead>
                        <tr>
                            <th colspan="5" class="text-center" width="50%">STOK MASUK</th>
                            <th colspan="6" class="text-center" width="50%">STOK KELUAR</th>
                        </tr>
                        <tr>
                            <th width="130">Tanggal</th>
                            <th>Produk</th>
                            <th>Kode Produksi</th>
                            <th>Masuk</th>
                            <th>Lokasi</th>
                            <th>Keterangan</th>
                            <th width="130">Tanggal</th>
                            <th>Produk</th>
                            <th>Kode Produksi</th>
                            <th>Keluar</th>
                            <th>Penerima/Keterangan</th>
                            <th>Stok Akhir</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $tgl => $value)

                            @foreach($value as $r_arus)
                                @php
                                    $status = $r_arus->stok_in == 0 ? 'out' : 'in';
                                    $stok_akhir = $r_arus->last_stok;
                                @endphp



                                @if($status == 'in')
                                    <tr>
                                        <td>{{ Main::format_date($tgl) }}</td>
                                        <td>{{ '('.$r_arus->kode_produk.') '.$r_arus->nama_produk }}</td>
                                        <td>{{ $r_arus->no_seri_produk }}</td>
                                        <td>{{ Main::format_number($r_arus->stok_in) }}</td>
                                        <td>{{ $r_arus->lokasi }}</td>
                                        <td>{{ $r_arus->arus_stok_produk_keterangan }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ Main::format_number($stok_akhir) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ Main::format_date($tgl) }}</td>
                                        <td>{{ '('.$r_arus->kode_produk.') '.$r_arus->nama_produk }}</td>
                                        <td>{{ $r_arus->no_seri_produk }}</td>
                                        <td>{{ Main::format_number($r_arus->stok_out) }}</td>
                                        <td>{{ $r_arus->keterangan }}</td>
                                        <td>{{ Main::format_number($stok_akhir) }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="m-portlet m-portlet--mobile hidden">

                <div class="m-portlet__body">

                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tanggal</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>No Seri Produk</th>
                            <th>Qty In</th>
                            <th>Qty Out</th>
                            <th>Gudang</th>
                            <th>Penerima/Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- @foreach($list as $r)
                             <tr>
                                 <td align="center">{{ $no++ }}.</td>
                                 <td>{{ $r->tgl }}</td>
                                 <td>{{ $r->produk['kode_produk'] }}</td>
                                 <td>{{ $r->produk['nama_produk'] }}</td>
                                 <td>{{ $r->stok_produk['no_seri_produk'] }}</td>
                                 <td>{{ $r->stok_in == 0 ? '-' : Main::format_number($r->stok_in) }}</td>
                                 <td>{{ $r->stok_out == 0 ? '-' : Main::format_number($r->stok_out) }}</td>
                                 <td>{{ $r->stok_produk['lokasi']['lokasi'] }}</td>
                                 <td>{{ $r->arus_stok_produk_keterangan }}</td>
                             </tr>
                         @endforeach--}}
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection