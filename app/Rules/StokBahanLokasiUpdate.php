<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;
use app\Models\mStokBahan;
use app\Models\mBahan;
use app\Models\mLokasi;

class StokBahanLokasiUpdate implements Rule
{
    private $id_stok_bahan;
    private $id_lokasi;
    private $id_bahan;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_stok_bahan, $id_lokasi)
    {
        $this->id_stok_bahan = $id_stok_bahan;
        $this->id_lokasi = $id_lokasi;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id_lokasi_update = $value;
        $id_lokasi_awal = mStokBahan::select('id_lokasi')->where('id', $this->id_stok_bahan)->first()->id_lokasi;
        $id_bahan = mStokBahan::select('id_bahan')->where('id', $this->id_stok_bahan)->first()->id_bahan;
        $this->id_bahan = $id_bahan;

        //echo $id_lokasi_awal.' = '.$id_lokasi_update.' = '.$this->id_stok_produk ;

        if($id_lokasi_awal == $id_lokasi_update) {
            return TRUE;
        } else {
            $check = mStokBahan
                ::where('id_lokasi', '!=', $id_lokasi_awal)
                ->where([
                    'id_lokasi'=>$id_lokasi_update,
                    'id_bahan'=>$id_bahan
                ])
                ->count();

            if($check == 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $bahan = mBahan::find($this->id_bahan);
        $lokasi = mLokasi::find($this->id_lokasi);
        return 'Bahan <strong>'.$bahan->nama_bahan.'</strong> sudah ada pada Gudang <strong>'.$lokasi->lokasi.'</strong>';
    }
}
