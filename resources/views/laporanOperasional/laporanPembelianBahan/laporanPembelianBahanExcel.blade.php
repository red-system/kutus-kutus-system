<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Pembelian BahanZ " . date('d-m-Y') . ".xls");
?>

<h3 align="center">LAPORAN PEMBELIAN BAHAN</h3>
<br/>
Tanggal : {{ $date_start.' s/d '.$date_end }}
<br/>
@foreach($po_bahan as $r_po_bahan)

    @php
        $id_bahan_arr = [];
        foreach($r_po_bahan->po_bahan_detail as $r_po_bahan_detail) {
            $id_bahan_arr[] = $r_po_bahan_detail->id_bahan;
        }

        if($id_bahan == 'all') {
            $bahan_exist = TRUE;
        } else {
            $bahan_exist = FALSE;
            $bahan_exist = in_array($id_bahan, $id_bahan_arr) ? TRUE: FALSE;
        }

    @endphp

    @if($bahan_exist)


        <table width="100%">
            <tr>
                <td style="border: 1px solid black; padding: 2px;" align="left">
                    <h4 align="left" style="margin: 0; padding: 0;">{{ $no_card++ }}</h4>
                    <table width="100%" class="table-header" border="1">
                        <tbody>
                        <tr>
                            <td></td>
                            <td width="20%">No Pembelian</td>
                            <td width="30%">{{ $r_po_bahan->no_faktur }}</td>
                            <td width="20%">Total</td>
                            <td width="30%">{{ Main::format_money($r_po_bahan->total) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Tanggal</td>
                            <td>{{ $r_po_bahan->tanggal }}</td>
                            <td>Biaya Tambahan</td>
                            <td>{{ Main::format_money($r_po_bahan->biaya_tambahan) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="20%">Pembayaran</td>
                            <td>{{ ucwords($r_po_bahan->pembayaran) }}</td>
                            <td>Potongan</td>
                            <td>{{ Main::format_money($r_po_bahan->potongan) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Pengiriman</td>
                            <td>{{ ucwords($r_po_bahan->pengiriman) }}</td>
                            <td>Grand Total</td>
                            <td>{{ Main::format_money($r_po_bahan->grand_total) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Supplier</td>
                            <td>{{ '('.$r_po_bahan->supplier->kode_supplier.') '.$r_po_bahan->supplier->nama_supplier }}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    <table class="table table-striped table-bordered table-hover" border="1">
                        <thead>
                        <tr class="tabletitle">
                            <th class="item" width="10">No</th>
                            <th class="item">Kode Bahan</th>
                            <th class="item">Nama Bahan</th>
                            <th class="item">Harga</th>
                            <th class="item">PPN</th>
                            <th class="item">Harga Net</th>
                            <th class="item">Qty</th>
                            <th class="item">Sub Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $no = 1;
                            $harga = 0;
                            $ppn_nominal = 0;
                            $harga_net = 0;
                            $qty = 0;
                            $sub_total = 0;
                        @endphp
                        @foreach($r_po_bahan->po_bahan_detail as $r_po_bahan_detail)
                            @php
                                $harga += $r_po_bahan_detail->harga;
                                $ppn_nominal += $r_po_bahan_detail->ppn_nominal;
                                $harga_net += $r_po_bahan_detail->harga_net;
                                $qty += $r_po_bahan_detail->qty;
                                $sub_total += $r_po_bahan_detail->sub_total;
                            @endphp
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext">{{ $no++ }}</p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">{{ $r_po_bahan_detail->bahan->kode_bahan }}</p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">{{ $r_po_bahan_detail->bahan->nama_bahan }}</p>
                                </td>
                                <td class="tableitem" align="right">
                                    <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->harga) }}</p>
                                </td>
                                <td class="tableitem" align="right">
                                    <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->ppn_nominal) }}</p>
                                </td>
                                <td class="tableitem" align="right">
                                    <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->harga_net) }}</p>
                                </td>
                                <td class="tableitem" align="right">
                                    <p class="itemtext">{{ Main::format_number($r_po_bahan_detail->qty) }}</p>
                                </td>
                                <td class="tableitem" align="right">
                                    <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->sub_total) }}</p>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td class="tableitem" colspan="3" align="right">
                                <p class="itemtext">
                                    <strong>Total</strong>
                                </p>
                            </td>
                            <td class="tableitem" align="right">
                                <p class="itemtext">
                                    <strong>{{ Main::format_money($harga) }}</strong>
                                </p>
                            </td>
                            <td class="tableitem" align="right">
                                <p class="itemtext">
                                    <strong>{{ Main::format_money($ppn_nominal) }}</strong>
                                </p>
                            </td>
                            <td class="tableitem" align="right">
                                <p class="itemtext">
                                    <strong>{{ Main::format_money($harga_net) }}</strong>
                                </p>
                            </td>
                            <td class="tableitem" align="right">
                                <p class="itemtext">
                                    <strong>{{ Main::format_number($qty) }}</strong>
                                </p>
                            </td>
                            <td class="tableitem" align="right">
                                <p class="itemtext">
                                    <strong>{{ Main::format_money($sub_total) }}</strong>
                                </p>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>
        <br/>

    @endif

@endforeach