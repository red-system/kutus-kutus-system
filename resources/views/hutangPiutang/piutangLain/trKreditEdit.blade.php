@foreach($kredit as $r)
    <tr>
        <td class="td-master-id">
            <select class="form-control select2-kredit" name="master_id_kredit[]" style="width: 100%">
                <option value="">Pilih Pembayaran</option>
                @foreach($master as $r_master)
                    <option value="{{ $r_master->master_id }}" {{ $r_master->master_id == $r->master_id ? 'selected': '' }}>{{ $r_master->mst_kode_rekening.' - '.$r_master->mst_nama_rekening }}</option>
                @endforeach
            </select>
        </td>
        <td class="td-jumlah">
            <input type="text"
                   name="kredit[]"
                   class="form-control touchspin-kredit"
                   value="{{ $r->trs_kredit }}"
                   style="width: 80px">
        </td>
        <td>
            <button type="button" class="btn-delete-row-kredit btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
@endforeach