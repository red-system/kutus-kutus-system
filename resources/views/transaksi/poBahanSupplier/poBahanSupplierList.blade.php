@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-switch.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/po_bahan_supplier.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('transaksi.poBahanSupplier.modalEditPassword')
    @include('transaksi.poBahanSupplier.modalDetail')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('poBahanSupplierCreate') }}"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Buat PO</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tanggal PO</th>
                            <th>No PO</th>
                            <th>Supplier</th>
                            <th>Grand Total</th>
                            <th width="190">Kedatangan Bahan</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)

                            @php
                                $sum_grand_total += $r->grand_total;
                            @endphp

                            <tr>
                                <td>{{ $no++ }}.</td>
                                <td>{{ Main::format_date($r->tanggal) }}</td>
                                <td>{{ $r->no_faktur }}</td>
                                <td>{{ '('.$r->supplier->kode_supplier.') '.$r->supplier->nama_supplier }}</td>
                                <td align="right">{{ Main::format_money($r->grand_total) }}</td>
                                <td>
                                    @if($r->status_bahan_datang == 'arrive')
                                        <span class="m-badge m-badge--success m-badge--wide">
                                            <i class="la la-check"></i> Sudah Datang
                                        </span>
                                    @else
                                        <span class="m-badge m-badge--warning m-badge--wide">
                                            <i class="la la-close"></i> Belum Datang
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    <textarea class="row-data hidden">@json([])</textarea>
                                    <div class="btn-group">
                                        <button class="btn btn-accent btn-sm  dropdown-toggle" type="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i> Menu
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item akses-cetak_po"
                                               href="{{ route('poBahanSupplierCetak', ['id'=>Main::encrypt($r->id)]) }}">
                                                <i class="la la-print"></i> Cetak PO
                                            </a>
                                            <a class="dropdown-item btn-detail akses_detail"
                                               href="#"
                                               data-route="{{ route('poBahanSupplierDetail', ['id'=>$r->id]) }}">
                                                <i class="la la-eye"></i> Detail
                                            </a>
                                            @if($r->status_pembelian == 'not_yet')
                                                <a class="dropdown-item btn-edit akses-edit"
                                                   href="#"
                                                   data-route="{{ route('poBahanSupplierUserCheck') }}"
                                                   data-redirect="{{ route('poBahanSupplierEdit', ['id'=>Main::encrypt($r->id)]) }}">
                                                    <i class="la la-edit"></i> Edit
                                                </a>
                                                <div class="dropdown-divider"></div>
                                            @endif
                                            @if($r->status_pembelian == 'not_yet')
                                                <a class="dropdown-item akses-pembelian"
                                                   href="{{ route('poBahanSupplierPembelian', ['id'=>Main::encrypt($r->id)]) }}">
                                                    <i class="la la-shopping-cart"></i> Pembelian
                                                </a>
                                            @endif
                                            @if($r->status_bahan_datang == 'not_yet')
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item btn-bahan-datang"
                                                   data-id-po-bahan="{{ $r->id }}"
                                                   data-status-pembelian="{{ $r->status_pembelian }}"
                                                   href="#">
                                                    <i class="la la-check"></i> Bahan Sudah Datang
                                                </a>
                                            @endif
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item btn-hapus m--font-danger"
                                               data-route="{{ route('poBahanSupplierDelete', ['id'=>$r->id]) }}"
                                               data-message="Yakin hapus data ini ?<br />
                                                             Karena akan mempengaruhi Stok Bahan dan Akunting."
                                               href="#">
                                                <i class="la la-remove m--font-danger"></i> Hapus
                                            </a>
                                            <a class="dropdown-item btn-hapus btn-danger m--font-light"
                                               data-route="{{ route('poBahanSupplierDeleteForce', ['id'=>$r->id]) }}"
                                               data-message="Yakin hapus data ini ?<br />
                                                             Karena akan menghapus permanen <strong>PO Bahan</strong>, <strong>Pembayaran PO Bahan</strong>, <strong>Pengurangan Stok Bahan</strong>, <strong>Hutang Supplier</strong>, <strong>Pembayaran Hutang Supplier</strong> dan <strong>Jurnal Umum</strong>"
                                               href="#">
                                                <i class="la la-remove m--font-light"></i> Hapus Paksa
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4"></th>
                            <th class="text-right">{{ Main::format_money($sum_grand_total) }}</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection