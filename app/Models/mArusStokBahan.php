<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mArusStokBahan extends Model
{

    protected $table = 'tb_arus_stok_bahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tgl',
        'table_id',
        'table_name',
        'id_stok_bahan',
        'id_po_bahan_detail',
        'id_po_bahan',
        'id_bahan',
        'stok_in',
        'stok_out',
        'last_stok',
        'last_stok_total',
        'keterangan',
        'method'
    ];

    function bahan() {
        return $this->belongsTo(mBahan::class, 'id_bahan');
    }

    function stok_bahan() {
        return $this->belongsTo(mStokBahan::class, 'id_stok_bahan');
    }

    function po_bahan_detail() {
        return $this->belongsTo(mPoBahanDetail::class, 'id_po_bahan_detail');
    }

    function po_bahan() {
        return $this->belongsTo(mPoBahan::class, 'id_po_bahan');
    }
}
