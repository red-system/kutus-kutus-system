<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Neraca " . date('d-m-Y') . ".xls");
?>

<h3 align="center">NERACA</h3>
<br/>
Tanggal : {{ $date_start.' s/d '.$date_end }}
<br/>

<table border="1" width="100%">
    <thead>
    <tr class="success">
        <th width="25%" class="text-center">ASET</th>
        <th width="25%" class="text-center">LIABILITAS</th>
        <th width="25%" class="text-center">EKUITAS</th>
        <th width="25%" class="text-center">STATUS</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td align="center">{{ Main::format_number($total_asset) }}</td>
        <td align="center">{{ Main::format_number($total_liabilitas) }}</td>
        <td align="center">{{ Main::format_number($total_ekuitas) }}</td>
        @if($status=='BALANCE')
            <td align="center"><font color="green"><strong>{{$status}}
                        : {{ Main::format_number($selisih) }}</strong></font></td>
        @else
            <td align="center"><font color="red"><strong>{{$status}}
                        : {{ Main::format_number($selisih) }}</strong></font></td>
        @endif
    </tr>
    </tbody>
</table>


<table border="1" width="100%">
    <thead>
    <tr>
        <th colspan="2" class="text-center">Activa</th>
    </tr>
    <tr>
        <th class="text-center">Description</th>
        <th class="text-center">Jumlah</th>
    </tr>
    </thead>
    <tbody>

    @php($activa = 0)
    @foreach($detail_perkiraan as $detail)
        @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening != 'AKTIVA TETAP')
            <tr>
                <td class="font-weight-bold">
                    {{ $detail->mst_kode_rekening }} {{ $detail->mst_nama_rekening }}
                </td>
                @php($total_assets_last = $total_assets_last + $asset[$detail->master_id])
                <td align="right" class="font-weight-bold">
                    {{ Main::format_number($asset[$detail->master_id]) }}
                </td>
            </tr>
            @php($activa++)
            @foreach($detail->childs as $det_child)
                <tr>
                    <td>
                        {!! $space1 !!}
                        {{ $det_child->mst_kode_rekening }} {{ $det_child->mst_nama_rekening }}
                    </td>
                    @php($total_assets_last = $total_assets_last + $asset[$det_child->master_id])
                    <td align="right">{{ Main::format_number($asset[$det_child->master_id]) }}</td>
                </tr>
                @php($activa++)
            @endforeach
        @endif
    @endforeach

    @php($total_aktiva = 0)

    @foreach($detail_perkiraan as $detail)
        @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening == 'AKTIVA TETAP')
            <tr>
                <td class="font-weight-bold">
                    {{ $detail->mst_kode_rekening }} {{ $detail->mst_nama_rekening }}
                </td>
                <td align="right">
                    <strong>{{ Main::format_number($asset[$detail->master_id]) }}</strong>
                </td>
            </tr>

            @php($activa++)
            @foreach($detail->childs as $det_child)
                <tr>
                    <td class="font-weight-bold">
                        {!!$space1!!}
                        {{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}
                    </td>
                    <td align="right" class="font-weight-bold">
                        {{ Main::format_number($asset[$det_child->master_id])}}
                    </td>
                </tr>

                @php($activa++)
                @foreach($det_child->childs as $child)
                    <tr>
                        <td>
                            {!!$space2!!}
                            {{ $child->mst_kode_rekening }} {{ $child->mst_nama_rekening }}
                        </td>
                        <td align="right">{{ Main::format_number($asset[$child->master_id])}}</td>
                    </tr>
                    @php($activa++)
                @endforeach
            @endforeach
        @endif
    @endforeach
    </tbody>
    <tfoot>
    <tr class="m-table__row--success">
        <th class="font-weight-bold">Total Aktiva</th>
        <th class="text-right font-weight-bold">{{ Main::format_number($total_asset)}}</th>
    </tr>
    </tfoot>
</table>

<br/><br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th colspan="2" class="text-center">Pasiva</th>
    </tr>
    <tr>
        <th class="text-center">Description</th>
        <th class="text-center">Jumlah</th>
    </tr>
    </thead>
    <tbody>
    @php($passiva = 0)
    @foreach($detail_perkiraan as $detail)
        @if($detail->mst_neraca_tipe == 'liabilitas' && $detail->mst_master_id == '0')
            <tr>
                <td class="font-weight-bold">
                    {{ $detail->mst_kode_rekening }} {{ $detail->mst_nama_rekening }}
                </td>
                <td align="right" class="font-weight-bold">
                    {{ Main::format_number($liabilitas[$detail->master_id]) }}
                </td>
            </tr>
            @php($passiva++)
            @foreach($detail->childs as $det_child)
                <tr>
                    <td>
                        {!!$space1!!}
                        {{ $det_child->mst_kode_rekening }} {{ $det_child->mst_nama_rekening }}
                    </td>
                    <td align="right">{{ Main::format_number($liabilitas[$det_child->master_id]) }}</td>
                </tr>
                @php($passiva++)
            @endforeach
        @endif
    @endforeach

    @foreach($detail_perkiraan as $detail)
        @if($detail->mst_neraca_tipe == 'ekuitas' && $detail->mst_master_id == '0')
            <tr>
                <td class="font-weight-bold">{{ $detail->mst_kode_rekening }} {{ $detail->mst_nama_rekening }}</td>
                <td align="right" class="font-weight-bold">
                    {{ Main::format_number($ekuitas[$detail->master_id])}}
                </td>
            </tr>
            @php($passiva++)
            @foreach($detail->childs as $det_child)
                <tr>
                    <td>
                        {!!$space1!!}
                        {{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}
                    </td>

                    <td align="right">
                        {{ Main::format_number($ekuitas[$det_child->master_id]) }}
                    </td>
                </tr>
                @php($passiva++)
            @endforeach
        @endif
    @endforeach

    @php($selisih = $activa - $passiva)

    </tbody>
    <tfoot>
    <tr class="m-table__row--success">
        <th class="font-weight-bold">Total Pasiva</th>
        <th class="text-right font-weight-bold">{{ Main::format_number($hitung) }}</th>
    </tr>
    </tfoot>
</table>