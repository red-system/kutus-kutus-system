@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('transaksi/penjualanProduk/modalDistributor')
    @include('transaksi/penjualanProduk/modalProduk')
    @include('transaksi/penjualanProduk/modalProdukStok')

    @include('transaksi/penjualanProduk/trProduk')
    @include('transaksi/penjualanProduk/trPembayaran')

    <form action="{{ route('penjualanProdukUpdate', ['id'=>Main::encrypt($penjualan->id)]) }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('penjualanProdukPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        @include('transaksi/penjualanProduk/modalPembayaranEdit')

        {{ csrf_field() }}
        <input type="hidden" name="urutan" value="{{ $penjualan->urutan }}">
        <input type="hidden" name="id_distributor" value="{{ $penjualan->id_distributor }}">
        <input type="hidden" name="kode_distributor" value="{{ $penjualan->distributor->kode_distributor }}">
        <input type="hidden" name="total" value="{{ $penjualan->total }}">
        <input type="hidden" name="grand_total" value="{{ $penjualan->grand_total }}">
        <input type="hidden" name="terbayar" value="{{ $penjualan->terbayar }}">
        <input type="hidden" name="sisa_pembayaran" value="{{ $penjualan->sisa_pembayaran }}">
        <input type="hidden" name="kembalian" value="{{ $penjualan->kembalian }}">
        <input type="hidden" name="no_faktur" value="{{ $penjualan->no_faktur }}">
        <input type="hidden" name="id_penjualan_produk_delete">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">


                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal" class="form-control m-input m_datepicker"
                                               readonly="" value="{{ date('d-m-Y', strtotime($penjualan->tanggal)) }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Distributor</label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="nama_distributor"
                                               placeholder="Nama Distributor..." disabled value="{{ '('.$penjualan_distributor->kode_distributor.') '.$penjualan_distributor->nama_distributor }}">
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-distributor">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <div class="col-9 col-form-label alamat-distributor">
                                    {{ $penjualan_distributor->alamat_distributor }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Telepon</label>
                                <div class="col-9 col-form-label telepon-distributor">
                                    {{ $penjualan_distributor->telp_distributor }}
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
                                <div class="col-9 col-form-label no_faktur">
                                    {{ $penjualan->no_faktur }}
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jenis Transaksi</label>
                                <div class="col-4">
                                    <select class="form-control" name="jenis_transaksi">
                                        <option value="retail" {{ $penjualan->jenis_transaksi == 'detail' ? 'selected':'' }}>Retail</option>
                                        <option value="distributor" {{ $penjualan->jenis_transaksi == 'distributor' ? 'selected':'' }}>Distributor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pengiriman</label>
                                <div class="col-3">
                                    <div class="m-radio-inline">
                                        <label class="m-radio">
                                            <input type="radio" name="pengiriman" value="yes" {{ $penjualan->pengiriman == 'yes' ? 'checked':'' }}> Ya
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="pengiriman" value="no" {{ $penjualan->pengiriman == 'no' ? 'checked':'' }}> Tidak
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <label for="example-text-input" class="col-2 col-form-label wrapper-ekspedisi">Ekspedisi</label>
                                <div class="col-4 wrapper-ekspedisi">
                                    <select class="form-control m-select2 " name="id_ekspedisi">
                                        @foreach($ekspedisi as $r)
                                            <option value="{{ $r->id }}" {{ $penjualan->id_ekspedisi == $r->id ? 'selected': '' }}>{{ $r->nama_ekspedisi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-produk btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Produk
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Gudang</th>
                                <th>No Seri Produk</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Potongan</th>
                                <th>PPN({{ $ppnPersen*100 }}%)</th>
                                <th>Harga Net</th>
                                <th>Sub Total</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($penjualan_produk as $r)
                                    @php
                                        $stok_produk = \app\Models\mStokProduk
                                                            ::where(['id_lokasi' => $r->id_lokasi, 'id_produk' => $r->id_produk])
                                                            ->orderBy('no_seri_produk')
                                                            ->get(['id', 'no_seri_produk', 'qty']);
                                        $gudang = \app\Models\mStokProduk
                                                    ::with('lokasi')
                                                    ->distinct()
                                                    ->where([
                                                        'id_produk' => $r->id_produk,
                                                    ])
                                                    ->where('qty', '>', 0)
                                                    ->get(['id_lokasi']);

                                    @endphp
                                    <tr data-index="{{ $no++ }}"
                                        data-id-penjualan-produk="{{ $r->id }}">
                                        <td class="m--hide data">
                                            <input type="hidden" class="id_produk" name="id_produk[{{ $r->id }}]" value="{{ $r->id_produk }}">
                                            <input type="hidden" class="harga" name="harga[{{ $r->id }}]" value="{{ $r->harga }}">
{{--                                            <input type="hidden" class="ppn_nominal" name="ppn_nominal[{{ $r->id }}]" value="{{ $r->ppn_nominal }}">--}}
                                            <input type="hidden" class="harga_net" name="harga_net[{{ $r->id }}]" value="{{ $r->harga_net }}">
                                            <input type="hidden" class="sub_total" name="sub_total[{{ $r->id }}]" value="{{ $r->sub_total }}">
                                        </td>
                                        <td>
                                            <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                                                    type="button">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </td>
                                        <td class="produk-nama">
                                            {{ $r->produk->kode_produk.' '.$r->produk->nama_produk }}
                                        </td>
                                        <td>
                                            <select class="form-control m-select2 id_lokasi" name="id_lokasi[{{ $r->id }}]" style="width: 140px">
                                                <option value="">Pilih Gudang</option>
                                                @foreach($gudang as $r_gudang)
                                                    <option value="{{ $r_gudang->id_lokasi }}" {{ $r_gudang->id_lokasi == $r->id_lokasi ? 'selected':'' }}>{{ $r_gudang->lokasi->lokasi }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control m-select2 id_stok_produk" name="id_stok_produk[{{ $r->id }}]" style="width: 140px">
                                                @foreach($stok_produk as $r_stok_produk)
                                                    <option value="{{ $r_stok_produk->id }}" {{ $r_stok_produk->id == $r->id_stok_produk ? 'selected':'' }}>{{ $r_stok_produk->no_seri_produk.': Jumlah Stok = '.Main::format_number($r_stok_produk->qty) }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="td-qty">
                                            <input type="text" name="qty[{{ $r->id }}]" class="touchspin-number-decimal qty" value="{{ $r->qty }}" style="width: 70px">
                                        </td>
                                        <td class="td-harga">
                                            {{ Main::format_number($r->harga) }}
                                        </td>
                                        <td class="td-potongan">
                                            <input type="text" name="potongan[{{ $r->id }}]" class="touchspin-number-decimal potongan" value="{{ $r->potongan }}" style="width: 60px">
                                        </td>
                                        <td class="td-ppn">
                                            <input type="text" name="ppn_nominal[{{ $r->id }}]" class="touchspin-number-decimal ppn_nominal" value="{{ $r->ppn_nominal }}" style="width: 60px">
                                        </td>
                                        <td class="td-harga-net">
                                            {{ Main::format_number($r->harga_net) }}
                                        </td>
                                        <td class="td-sub-total">
                                            {{ Main::format_number($r->sub_total) }}
                                        </td>
                                        <td>
                                            <button type="button"
                                                    class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                                                    data-confirm="false">
                                                <i class="la la-remove"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label">
                                            <strong><span class="total">{{ Main::format_number($penjualan->total) }}</span></strong>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number" type="text"
                                                   name="biaya_tambahan" value="{{ $penjualan->biaya_tambahan }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number" type="text"
                                                   name="potongan_akhir" value="{{ $penjualan->potongan_akhir }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label">
                                            <strong><span class="grand-total">{{ Main::format_number($penjualan->grand_total) }}</span></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


        <div class="target-produksi-buttons">
            <button type="button"
                    class="btn-selanjutnya btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                    data-toggle="modal"
                    data-target="#modal-pembayaran"
                    data-backdrop="static"
                    data-keyboard="false">
                <span>
                    <i class="la la-arrow-right"></i>
                    <span>Proses Selanjutnya</span>
                </span>
            </button>
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Simpan Penjualan</span>
                </span>
            </button>
            <a href="{{ route("penjualanProdukPage") }}"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>


@endsection