<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=History PenyesuaianStok Bahan " . date('d-m-Y') . ".xls");
?>

<h3 align="center">History Penyesuaian Stok Bahan</h3>
Tanggal History: {{ $date_start }} s/d {{ $date_end }}
<br />
<table width="100%" border="1">
    <thead>
    <tr class="tabletitle">
        <th class="item" width="10">No</th>
        <th class="item">Tanggal</th>
        <th class="item">Kode Bahan</th>
        <th class="item">Nama Bahan</th>
        <th class="item">Kategori Bahan</th>
        <th class="item">Lokasi</th>
        <th class="item">Qty Awal</th>
        <th class="item">Qty Akhir</th>
        <th class="item">Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
        $total = 0;
    @endphp
    @foreach($list as $r)

        <tr class="service">
            <td class="tableitem"><p class="itemtext">{{ $no++ }}.</p></td>
            <td class="tableitem"><p class="itemtext">{{ Main::format_date($r->tgl) }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->kode_bahan }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->nama_bahan }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->kategori_bahan }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->lokasi }}</p></td>
            <td class="tableitem"><p class="itemtext" align="right">{{ Main::format_number($r->qty_awal) }}</p></td>
            <td class="tableitem"><p class="itemtext" align="right">{{ Main::format_number($r->qty_akhir) }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->keterangan }}</p></td>
        </tr>
    @endforeach
    </tbody>
</table>