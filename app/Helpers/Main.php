<?php

namespace app\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Config;

use app\Models\mBahan;
use app\Models\mProduk;
use app\Models\mLokasi;
use app\Models\mKategoriProduk;
use app\Models\mStokProduk;


class Main
{

    public static function data($breadcrumb = array(), $menuActive = '')
    {
        $notifAlertBahan = Main::notifAlertBahan();
        $notifAlertProduk = Main::notifAlertProduk();
        $notifications = [
            $notifAlertBahan,
            $notifAlertProduk
        ];
        $totalNotification = Main::totalNotification($notifications);
        $badgeNotification = Main::badgeNotification($totalNotification);
        $level = Session::get('level');
        $user = Session::get('user');
        $user_foto = '';
        $user_nama = '';
        $user_email = '';

        if ($level == 'administrator') {
            $user_foto = $user->karyawan->foto_karyawan;
            $user_nama = $user->karyawan->nama_karyawan;
            $user_email = $user->karyawan->email_karyawan;
        } elseif ($level == 'distributor') {
            $user_foto = $user->foto_distributor;
            $user_nama = $user->nama_distributor;
            $user_email = $user->email_distributor;
        }


        $data['menu'] = Main::generateTopMenu($menuActive);
        $data['menuAction'] = Main::menuActionData($menuActive);
        $data['footer'] = Main::footer();
        $data['metaTitle'] = Main::metaTitle($breadcrumb);
        $data['pageTitle'] = Main::pageTitle($breadcrumb);
        $data['breadcrumb'] = Main::breadcrumb($breadcrumb);
        $data['user'] = Session::get('user');
        $data['user_role_data'] = json_decode(Session::get('user.user_role.role_akses'), TRUE);
        $data['user_foto'] = $user_foto;
        $data['user_nama'] = $user_nama;
        $data['user_email'] = $user_email;
        $data['level'] = $level;
        $data['pageMethod'] = '';
        $data['no'] = 1;
        $data['imgWidth'] = 100;
        $data['decimalStep'] = '.01';
        $data['roundDecimal'] = 2;
        $data['prefixNoSeriProduk'] = 'SPO-';
        $data['prefixNoSeriBahan'] = 'SBH-';
        $data['prefixProduksi'] = 'MKK-';
        $data['prefixNoPenjualan'] = 'PJL-';
        $data['prefixNoPoBahan'] = 'PBH-';
        $data['prefixNoPoBahanPembelian'] = 'PBL-';
        $data['prefixNoDistributorOrder'] = 'DOR-';
        $data['prefixNoDistribusi'] = 'DST-';
        $data['kodeHutangSupplier'] = 'HS';
        $data['labelBalance'] = '<span class="m-badge m-badge--success m-badge--wide">BALANCE</span>';
        $data['labelBalanceNot'] = '<span class="m-badge m-badge--danger m-badge--wide">NOT BALANCE</span>';
        $data['ppnPersen'] = Config::get('constants.ppnPersen');
        $data['methodProses'] = '';
        $data['tableValue'] = isset($_GET['table_value']) ? $_GET['table_value'] : '';
        $data['tableIndex'] = isset($_GET['table_index']) ? $_GET['table_index'] : '';

        $data['cons'] = Config::get('constans');

        $data['notifAlertBahan'] = $notifAlertBahan;
        $data['notifAlertProduk'] = $notifAlertProduk;
        $data['totalNotification'] = $totalNotification;
        $data['badgeNotification'] = $badgeNotification;


        return $data;
    }

    public static function companyInfo()
    {
        $data = [
            'bankType' => Config::get('constants.bankType'),
            'bankRekening' => Config::get('constants.bankRekening'),
            'bankAtasNama' => Config::get('constants.bankAtasNama'),
            'companyName' => Config::get('constants.companyName'),
            'companyAddress' => Config::get('constants.companyAddress'),
            'companyPhone' => Config::get('constants.companyPhone'),
            'companyTelp' => Config::get('constants.companyTelp'),
            'companyEmail' => Config::get('constants.companyEmail'),
            'companyBendahara' => Config::get('constants.companyBendahara'),
            'companyTuju' => Config::get('constants.companyTuju'),
        ];

        $data = (object)$data;

        return $data;
    }

    public static function totalNotification($notifications)
    {
        return count($notifications);
    }

    public static function badgeNotification($totalNotification)
    {
        if ($totalNotification > 0) {
            return '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot m-badge--danger"></span>';
        }
        return '';
    }

    public static function totalAlertBahan()
    {
        $list = mBahan
            ::with(
                'stok_bahan:id_bahan,qty'
            )
            ->get();
        $totalAlert = 0;

        foreach ($list as $r) {
            $countQty = 0;
            foreach ($r->stok_bahan as $r2) {
                $countQty += $r2->qty;
            }

            if ($r->minimal_stok > $countQty) {
                $totalAlert++;
            }
        }

        return $totalAlert;
    }

    public static function totalAlertProduk()
    {
        $list = mProduk
            ::with(
                'stok_produk:id_produk,qty'
            )
            ->get();
        $totalAlert = 0;

        foreach ($list as $r) {
            $countQty = 0;
            foreach ($r->stok_produk as $r2) {
                $countQty += $r2->qty;
            }

            if ($r->minimal_stok > $countQty) {
                $totalAlert++;
            }
        }


        return $totalAlert;
    }

    public static function badgeAlertProduk($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertProduk();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function badgeAlertBahan($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertBahan();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function bagdeInventory($totalAlert = '')
    {
        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function notifAlertBahan()
    {
        $totalAlert = Main::totalAlertBahan();
        if ($totalAlert > 0) {
            return '
                <a href="' . route('stokAlertBahan') . '" class="m-list-timeline__item">
                    <span class="m-list-timeline__badge"></span>
                    <span href="" class="m-list-timeline__text">Stok Bahan kurang dari Minimal Stok, segera setarakan. <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
                </a>
            ';
        }
        return '';
    }

    public static function notifAlertProduk()
    {
        $totalAlert = Main::totalAlertProduk();
        if ($totalAlert > 0) {
            return '
                <a href="' . route('stokAlertProduk') . '" class="m-list-timeline__item">
                    <span class="m-list-timeline__badge"></span>
                    <span href="" class="m-list-timeline__text">Stok Produk kurang dari Minimal Stok, segera setarakan. 
                    <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
                </a>
            ';
        }
        return '';
    }

    public static function generateTopMenu($menuActive)
    {
        $totalAlertBahan = Main::totalAlertBahan();
        $totalAlertProduk = Main::totalAlertProduk();

        $data['routeName'] = Route::currentRouteName();
        $data['menu'] = Main::menuList();
        $data['menuActive'] = $menuActive;
        $data['consMenu'] = Config::get('constants.topMenu');
        $data['badgeAlertBahan'] = Main::badgeAlertBahan($totalAlertBahan);
        $data['badgeAlertProduk'] = Main::badgeAlertProduk($totalAlertProduk);
        $data['badgeInventory'] = Main::bagdeInventory($totalAlertBahan + $totalAlertProduk);


        return view('component/menu', $data);
    }

    public static function footer()
    {
        $data = [];
        return view('component/footer', $data);
    }

    public static function format_money($number)
    {
        if (Main::check_decimal($number)) {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        } else {
            return 'Rp. ' . number_format($number, 0, '', '.');
        }
    }

    public static function format_number($number)
    {
        if (Main::check_decimal($number)) {
            return number_format($number, 2, ',', '.');
        } else {
            return number_format($number, 0, '', '.');
        }
    }

    public static function format_number_system($number) {
        return number_format($number, 2, ',', '.');
    }

    public static function format_number_db($number) {
        $number = str_replace(['.',','], ['','.'], $number);
        return number_format($number, 2, '.', '');
    }

    public static function format_discount($number)
    {
        return $number . ' %';
    }

    public static function format_date($date)
    {
        return date('d-m-Y', strtotime($date));
    }

    public static function format_date_label($date)
    {
        return date('d F Y', strtotime($date));
    }

    public static function format_date_db($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function convert_money($str)
    {
        $find = array('Rp', '.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function encrypt($id)
    {
        return Crypt::encrypt($id);
    }

    public static function decrypt($id)
    {
        return Crypt::decrypt($id);
    }

    public static function convert_number($str)
    {
        $find = array('.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function convert_discount($str)
    {
        $find = array('%', ' ', '_');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function check_decimal($number)
    {
        if( $number - floor($number) >= 0.1  ) {
            return true;
        } else {
            return false;
        }
    }

    public static function metaTitle($breadcrumb)
    {
        krsort($breadcrumb);
        $title = '';
        foreach ($breadcrumb as $label => $value) {
            $title .= Main::menuAction($value['label']) . ' < ';
        }

        $title .= env("APP_NAME", "Sistem Kutus Kutus");

        return $title;

    }

    public static function pageTitle($breadcrumb = array())
    {
        krsort($breadcrumb);
        $title = isset($breadcrumb[1]) ? Main::menuAction($breadcrumb[1]['label']) : Main::menuAction($breadcrumb[0]['label']);

        return $title;

    }

    public static function menuAction($string)
    {
        $string = str_replace('_', ' ', $string);
        $string = ucwords($string);

        return $string;
    }

    public static function menuStrip($string)
    {
        return str_replace([' ', '/'], '_', strtolower($string));
    }

    public static function menuActionData($menuActive)
    {
        $menuList = Main::menuAdministrator();
        $routeName = Route::currentRouteName();
        $userRole = json_decode(Session::get('user.user_role.role_akses'), TRUE);
        //$menuActive = '';
        $action = [];

        if($menuActive == '') {
            foreach ($menuList as $menu => $val) {
                if($val['route'] == $routeName) {
                    $menuActive = $menu;
                    break;
                } else {
                    if(isset($val['sub'])) {
                        foreach($val['sub'] as $menu_sub => $val_sub) {
                            if($val_sub['route'] == $routeName) {
                                $menuActive = $menu_sub;
                            }
                        }
                    }
                }
            }
        }

        if($userRole) {
            foreach($userRole as $menuName => $menuVal) {
                if($menuName == $menuActive) {
                    $action = $menuVal;
                } else {
                    foreach($menuVal as $menuSubName => $menuSubVal) {
                        if($menuSubName == $menuActive) {
                            $action = $menuSubVal;
                        }
                    }
                }
            }
        }

        return $action;

    }

    public static function breadcrumb($breadcrumb_extend = array())
    {
        $cons = Config::get('constants.topMenu');

        $breadcrumb[] = [
            'label' => $cons['dashboard'],
            'route' => 'dashboardPage'
        ];

        $data['breadcrumb'] = array_merge($breadcrumb, $breadcrumb_extend);
        return view('component.breadcrumb', $data);

    }

    public static function no_seri_produk($month, $year, $id_produk, $id_lokasi, $urutan)
    {
        $id_kategori_produk = mProduk::select('id_kategori_produk')->where('id', $id_produk)->first()->id_kategori_produk;
        $kode_kategori_produk = mKategoriProduk::select('kode_kategori_produk')->where('id', $id_kategori_produk)->first()->kode_kategori_produk;
        $kode_lokasi = mLokasi::select('kode_lokasi')->where('id', $id_lokasi)->first()->kode_lokasi;

        return $month . $year . $kode_kategori_produk . $kode_lokasi . $urutan;
    }

    public static function urutan_produk($id_produk, $id_lokasi, $month, $year)
    {
        $urutan = 1;
        $where = [
            'id_produk' => $id_produk,
            'id_lokasi' => $id_lokasi,
            'month' => $month,
            'year' => $year
        ];

        $stok_urutan = mStokProduk::where($where);

        if ($stok_urutan->count() > 0) {
            $urutan_now = $stok_urutan->select('urutan')->orderBy('urutan', 'DESC')->first()->urutan;
            $urutan = $urutan_now + 1;
        }

        return $urutan;
    }

    public static function status($status) {
        switch ($status) {
            case "yes":
                return '<span class="m-badge m-badge--info m-badge--wide">Ya</span>';
                break;
            case "no":
                return '<span class="m-badge m-badge--warning m-badge--wide">Tidak</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function distributor_order_status($status)
    {
        switch ($status) {
            case "order":
                return '<span class="m-badge m-badge--info m-badge--wide">Order</span>';
                break;
            case "konfirm_admin":
                return '<span class="m-badge m-badge--accent m-badge--wide">Konfirmasi Admin</span>';
                break;
            case "konfirm_distributor":
                return '<span class="m-badge m-badge--warning m-badge--wide">Konfirmasi Distributor</span>';
                break;
            case "done":
                return '<span class="m-badge m-badge--success m-badge--wide">Selesai</span>';
                break;
            case "reject":
                return '<span class="m-badge m-badge--danger m-badge--wide">Batal</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function distributor_pengiriman($pengiriman)
    {
        switch ($pengiriman) {
            case "yes":
                return '<span class="m-badge m-badge--info m-badge--wide">Ya</span>';
                break;
            case "no":
                return '<span class="m-badge m-badge--warning m-badge--wide">Tidak</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function hutang_supplier_status($pengiriman)
    {
        switch ($pengiriman) {
            case "lunas":
                return '<span class="m-badge m-badge--info m-badge--wide">Lunas</span>';
                break;
            case "belum_lunas":
                return '<span class="m-badge m-badge--warning m-badge--wide">Belum Lunas</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function hutang_lain_status($pengiriman)
    {
        switch ($pengiriman) {
            case "lunas":
                return '<span class="m-badge m-badge--info m-badge--wide">Lunas</span>';
                break;
            case "belum_bayar":
                return '<span class="m-badge m-badge--warning m-badge--wide">Belum Bayar</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    /**
     * @return array
     *
     * Menu variable
     */
    public static function menuList()
    {

        $level = Session::get('level');

        if ($level == 'administrator') {
            return Main::menuAdministrator();
        } elseif ($level == 'distributor') {
            return Main::menuDistributor();
        }

    }

    public static function menuAdministrator()
    {
        $cons = Config::get('constants.topMenu');
        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage'
            ],
            $cons['masterData'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => '#',
                'sub' => [
                    $cons['master_4'] => [
                        'route' => 'karyawanPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_11'] => [
                        'route' => 'userRolePage',
                        'action' => [
                            'list',
                            'menu_akses',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_5'] => [
                        'route' => 'userPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_1'] => [
                        'route' => 'kategoriProdukPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_2'] => [
                        'route' => 'kategoriBahanPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_10'] => [
                        'route' => 'kategoriAssetPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_3'] => [
                        'route' => 'satuanPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_6'] => [
                        'route' => 'supplierPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_7'] => [
                        'route' => 'distributorPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_8'] => [
                        'route' => 'lokasiPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_9'] => [
                        'route' => 'ekspedisiPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                ]
            ],
            $cons['inventory'] => [
                'icon' => 'flaticon-open-box',
                'route' => '#',
                'sub' => [
                    $cons['inventory_1'] => [
                        'route' => 'bahanPage',
                        'action' => [
                            'list',
                            'stok',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['inventory_2'] => [
                        'route' => 'produkPage',
                        'action' => [
                            'list',
                            'stok',
                            'harga',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['inventory_3'] => [
                        'route' => 'komposisiProdukPage',
                        'action' => [
                            'list',
                            'komposisi_produk_data',
                            'komposisi_packaging_data',
                        ]
                    ],
                    $cons['inventory_4'] => [
                        'route' => 'transferStokBahanPage',
                        'action' => [
                            'list_transfer_stok_bahan',
                            'history_transfer',
                            'stok_bahan',
                            'filter',
                            'pdf',
                            'excel',
                            'list'
                        ]

                    ],
                    $cons['inventory_5'] => [
                        'route' => 'transferStokProdukPage',
                        'action' => [
                            'list_transfer_stok_produk',
                            'history_transfer',
                            'stok_produk',
                            'filter',
                            'pdf',
                            'excel',
                            'list'
                        ]

                    ],
                    $cons['inventory_6'] => [
                        'route' => 'kartuStokBahanPage',
                        'action' => [
                            'filter',
                            'pdf',
                            'excel',
                            'list'
                        ]

                    ],
                    $cons['inventory_7'] => [
                        'route' => 'kartuStokProdukPage',
                        'action' => [
                            'filter',
                            'pdf',
                            'excel',
                            'list'
                        ]
                    ],
                    $cons['inventory_8'] => [
                        'route' => 'penyesuaianStokBahanPage',
                        'action' => [
                            'penyesuaian_stok_bahan',
                            'stok_bahan',
                            'history',
                            'filter',
                            'pdf',
                            'excel',
                            'list'
                        ]
                    ],
                    $cons['inventory_9'] => [
                        'route' => 'penyesuaianStokProdukPage',
                        'action' => [
                            'penyesuaian_stok_produk',
                            'stok_produk',
                            'history',
                            'filter',
                            'pdf',
                            'excel',
                            'list'
                        ]
                    ],
                    $cons['inventory_10'] => [
                        'route' => 'stokAlertBahan',
                        'action' => [
                            'list'
                        ]
                    ],
                    $cons['inventory_11'] => [
                        'route' => 'stokAlertProduk',
                        'action' => [
                            'list'
                        ]
                    ],
                ]
            ],
            $cons['produksi'] => [
                'icon' => 'flaticon-refresh',
                'route' => '#',
                'sub' => [
                    $cons['produksi_2'] => [
                        'route' => 'targetProduksiPage',
                        'action' => [
                            'create',
                            'list',
                            'daftar_target_produksi',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['produksi_4'] => [
                        'route' => 'perkiraanJumlahProduksi',
                        'action' => [
                            'perkiraan_jumlah_produksi',
                            'jumlah_bahan_terpakai',
                            'keperluan_bahan_target',
                            'keperluan_bahan_target_pertahun'
                        ]
                    ],
                    $cons['produksi_1'] => [
                        'route' => 'produksiPage',
                        'action' => [
                            'daftar_produksi',
                            'create',
                            'edit',
                            'delete',
                            'detail',
                            'publish',
                            'hasil_produksi',
                            'history_produksi'
                        ]
                    ],
                    $cons['produksi_3'] => [
                        'route' => 'realisasiTargetProduksiPage',
                        'action' => [
                            'list',
                            'detail_transaksi'
                        ]
                    ]
                ]
            ],
            $cons['transaksi'] => [
                'icon' => 'flaticon-arrows',
                'route' => '#',
                'sub' => [
                    $cons['transaksi_1'] => [
                        'route' => 'penjualanProdukPage',
                        'action' => [
                            'create',
                            'list',
                            'cetak_faktur',
                            'edit',
                            'delete'
                        ]

                    ],
                    $cons['transaksi_5'] => [
                        'route' => 'poBahanSupplierPage',
                        'action' => [
                            'create',
                            'list',
                            'cetak_po',
                            'detail',
                            'edit',
                            'pembelian'
                        ]

                    ],
                    $cons['transaksi_9'] => [
                        'route' => 'distributorOrderOnlinePage',
                        'action' => [
                            'distributor_order_baru',
                            'distributor_konfirmasi_order',
                            'distributor_order_selesai',
                            'delete',
                            'konfirmasi_detail'
                        ]
                    ],
                    /*                    $cons['transaksi_2'] => [
                                            'route' => 'penjualanProdukWholesalePage',

                                        ],*/
                    /*                    $cons['transaksi_4'] => [
                                            'route' => 'pemesananProdukDistributorPage',

                                        ],*/
                    /*                    $cons['transaksi_6'] => [
                                            'route' => 'pembelianBahanSupplierPage',

                                        ],*/
                    /**
                     * Karena biaya operasional akan diinputkan oleh akunting
                     */
                    /*                    $cons['transaksi_7'] => [
                                            'route' => 'biayaOperasionalKantorPage',

                                        ],
                                        $cons['transaksi_8'] => [
                                            'route' => 'biayaOperasionalProduksiPage',

                                        ]*/
                ]
            ],
            $cons['distribusi'] => [
                'icon' => 'flaticon-truck',
                'route' => 'distribusiPage',
                'action' => [
                    'list',
                    'distribusi_qty_produk',
                    'history_distribusi'
                ]

            ],
            $cons['laporanOperasional'] => [
                'icon' => 'flaticon-open-box',
                'route' => '#',
                'sub' => [
                    $cons['laporanO_1'] => [
                        'route' => 'laporanPenjualanGlobalPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ],
                    $cons['laporanO_2'] => [
                        'route' => 'laporanPenjualanDetailPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ],
                    $cons['laporanO_3'] => [
                        'route' => 'laporanProduksiPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ],
                    $cons['laporanO_4'] => [
                        'route' => 'laporanPenggunaanBahanPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ],
                    $cons['laporanO_5'] => [
                        'route' => 'laporanPembelianBahanPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ]
                ]
            ],
            $cons['hutangPiutang'] => [
                'icon' => 'flaticon-piggy-bank',
                'route' => '#',
                'sub' => [
                    $cons['hutang_1'] => [
                        'route' => 'hutangSupplierPage',
                        'action' => [
                            'list',
                            'edit',
                            'pembayaran',
                            'detail',
                        ]

                    ],
                    $cons['hutang_2'] => [
                        'route' => 'hutangLainPage',
                        'action' => [
                            'list',
                            'edit',
                            'pembayaran',
                            'detail',
                            'create',
                        ]

                    ],
                    $cons['hutang_3'] => [
                        'route' => 'piutangPelangganPage',
                        'action' => [
                            'list',
                            'print',
                            'pembayaran',
                            'detail',
                        ]

                    ],
                    $cons['hutang_4'] => [
                        'route' => 'piutangLainPage',
                        'action' => [
                            'list',
                            'edit',
                            'pembayaran',
                            'detail',
                            'create',
                        ]

                    ]
                ]
            ],
            $cons['akunting'] => [
                'icon' => 'flaticon-coins',
                'route' => '#',
                'sub' => [
                    $cons['akunting_1'] => [
                        'route' => 'perkiraanPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]

                    ],
                    $cons['akunting_2'] => [
                        'route' => 'jurnalUmumPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete',
                            'filter',
                            'pdf',
                            'excel'
                        ]

                    ],
                    $cons['akunting_3'] => [
                        'route' => 'arusKasPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ],
                    $cons['akunting_4'] => [
                        'route' => 'labaRugiPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ],
                    $cons['akunting_5'] => [
                        'route' => 'neracaPage',
                        'action' => [
                            'list',
                            'filter',
                            'pdf',
                            'excel',
                        ]

                    ],
                    $cons['akunting_6'] => [
                        'route' => 'assetPage',
                        'action' => [
                            'list',
                            'create',
                            'penyusutan_aset',
                            'delete'
                        ]

                    ],
                    $cons['akunting_7'] => [
                        'route' => 'penyusutanAssetPage',
                        'action' => [
                            'list',
                        ]

                    ]
                ]
            ],
            /*$cons['laporanAkunting'] => [
                'icon' => 'flaticon-coins',
                'route' => '#',
                'sub' => [
                    $cons['laporanA_1'] => [
                        'route' => 'underconstructionPage',

                    ],
                    $cons['laporanA_2'] => [
                        'route' => 'underconstructionPage',

                    ],
                    $cons['laporanA_3'] => [
                        'route' => 'underconstructionPage',

                    ],
                    $cons['laporanA_4'] => [
                        'route' => 'underconstructionPage',

                    ],
                    $cons['laporanA_5'] => [
                        'route' => 'underconstructionPage',

                    ],
                    $cons['laporanA_6'] => [
                        'route' => 'underconstructionPage',

                    ]
                ]
            ]*/
        ];
    }

    public static function menuDistributor()
    {
        $cons = Config::get('constants.topMenu');
        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage',
            ],
            $cons['transaksi_3'] => [
                'icon' => 'flaticon-arrows',
                'route' => 'orderOnlinePage'
            ]
        ];
    }

}
