<?php

namespace app\Http\Controllers\LaporanOperasional;

use app\Helpers\Main;
use app\Http\Controllers\Controller;
use app\Models\mBahan;
use app\Models\mPoBahan;
use app\Models\mProduk;
use app\Models\mProduksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use app\Models\mPenjualan;
use app\Models\mPenjualanProduk;
use PDF;

class LaporanPembelianBahan extends Controller
{

    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['distribusi'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporanOperasional'],
                'route' => ''
            ],
            [
                'label' => $cons['laporanO_5'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $id_bahan = $request->input('id_bahan');
        $date_start_db = mPoBahan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPoBahan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');

        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $id_bahan = $id_bahan ? $id_bahan : 'all';
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $bahan = mBahan
            ::orderBy('nama_bahan', 'ASC')
            ->get([
                'id',
                'kode_bahan',
                'nama_bahan'
            ]);

        $po_bahan = mPoBahan
            ::with([
                'po_bahan_detail.bahan:id,kode_bahan,nama_bahan',
                'supplier',

            ])
            ->whereBetween('tanggal', $where_date)
            ->orderBy('tb_po_bahan.id', 'ASC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_bahan' => $id_bahan
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'id_bahan' => $id_bahan,
            'po_bahan' => $po_bahan,
            'total' => 0,
            'bahan' => $bahan,
            'no_card' => 1
        ]);

        return view('laporanOperasional/laporanPembelianBahan/laporanPembelianBahanList', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $id_bahan = $request->input('id_bahan');
        $date_start_db = mPoBahan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPoBahan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');

        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $id_bahan = $id_bahan ? $id_bahan : 'all';
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $po_bahan = mPoBahan
            ::with([
                'po_bahan_detail.bahan:id,kode_bahan,nama_bahan',
                'supplier',

            ])
            ->whereBetween('tanggal', $where_date)
            ->orderBy('id', 'ASC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_bahan' => $id_bahan
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_bahan' => $id_bahan,
            'params' => $params,
            'po_bahan' => $po_bahan,
            'total' => 0,
            'company' => Main::companyInfo(),
            'no_card' => 1
        ]);

        $pdf = PDF::loadView('laporanOperasional/laporanPembelianBahan/laporanPembelianBahanPdf', $data);

        //return $pdf->stream();

        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Laporan Pembelian Bahan ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $id_bahan = $request->input('id_bahan');
        $date_start_db = mPoBahan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPoBahan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');

        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $id_bahan = $id_bahan ? $id_bahan : 'all';
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $po_bahan = mPoBahan
            ::with([
                'po_bahan_detail.bahan:id,kode_bahan,nama_bahan',
                'supplier',

            ])
            ->whereBetween('tanggal', $where_date)
            ->orderBy('id', 'ASC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_bahan' => $id_bahan,
            'params' => $params,
            'po_bahan' => $po_bahan,
            'total' => 0,
            'no_card' => 1,
            'company' => Main::companyInfo()
        ]);

        return view('laporanOperasional/laporanPembelianBahan/laporanPembelianBahanExcel', $data);
    }

}
