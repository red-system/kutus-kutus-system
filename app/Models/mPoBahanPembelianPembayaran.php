<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPoBahanPembelianPembayaran extends Model
{

    protected $table = 'tb_po_bahan_pembelian_pembayaran';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_po_bahan',
        'id_po_bahan_pembelian',
        'master_id',
        'jumlah',
        'jatuh_tempo',
        'no_check_bg',
        'keterangan',
    ];

    function po_bahan_pembelian() {
        return $this->belongsTo(mPoBahanPembelian::class, 'id_po_bahan_pembelian');
    }
}
