<h3>Detail PO Bahan</h3>
<div class="m-form">
    <div class="m-portlet__body">
        <div class="m-form__section m-form__section--first row data-detail-section">
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Tanggal Dokumen</label>
                    <div class="col-lg-8 col-form-label">
                        {{ Main::format_date($po_bahan->tanggal) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Supplier</label>
                    <div class="col-lg-8 col-form-label">
                        {{ '('.$po_bahan->supplier->kode_supplier.') '.$po_bahan->supplier->nama_supplier }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No Telepon</label>
                    <div class="col-lg-8 col-form-label">
                        {{ $po_bahan->supplier->telp_supplier }}
                    </div>
                    <label class="col-lg-4 col-form-label">Alamat</label>
                    <div class="col-lg-8 col-form-label">
                        {{ $po_bahan->supplier->alamat_supplier }}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">No Pre Order</label>
                    <div class="col-lg-9 col-form-label">
                        {{ $po_bahan->no_faktur }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Pembayaran</label>
                    <div class="col-lg-9 col-form-label">
                        {{ ucwords($po_bahan->pembayaran) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Pengiriman</label>
                    <div class="col-lg-9 col-form-label">
                        {{ ucwords($po_bahan->pengiriman) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr/>
<br/>
<h3>Bahan</h3>
<table class="table m-table m-table--head-separator-primary">
    <thead>
    <tr>
        <th>No</th>
        <th>Kode Bahan</th>
        <th>Nama Bahan</th>
        <th>Harga</th>
        <th>PPN</th>
        <th>Harga Net</th>
        <th>Qty</th>
        <th>Subtotal</th>
    </tr>
    </thead>
    <tbody>
    @php(
        $no = 1
    )
    @foreach($po_bahan_detail as $r)
        <tr>
            <td>{{ $no++ }}.</td>
            <td>{{ $r->bahan->kode_bahan }}</td>
            <td>{{ $r->bahan->nama_bahan }}</td>
            <td>{{ Main::format_number($r->harga) }}</td>
            <td>{{ Main::format_number($r->ppn_nominal) }}</td>
            <td>{{ Main::format_number($r->harga_net) }}</td>
            <td>{{ Main::format_number($r->qty) }}</td>
            <td>{{ Main::format_number($r->sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<hr/>
<br/>
<div class="row">
    <div class="col-xs-12 col-lg-5 offset-lg-7">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-4 col-form-label">Total</label>
            <div class="col-8 col-form-label total">{{ Main::format_number($po_bahan->total) }}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-4 col-form-label">Biaya
                Tambahan</label>
            <div class="col-8 col-form-label total">{{ Main::format_number($po_bahan->biaya_tambahan) }}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
            <div class="col-8 col-form-label total">{{ Main::format_number($po_bahan->potongan) }}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
            <div class="col-8 col-form-label grand-total">{{ Main::format_number($po_bahan->grand_total) }}</div>
        </div>
    </div>
</div>