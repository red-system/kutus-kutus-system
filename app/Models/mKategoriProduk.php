<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mKategoriProduk extends Model
{

    protected $table = 'tb_kategori_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_kategori_produk',
        'kategori_produk',
    ];
}

