@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form method="get">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-gear"></i>
                            </span>
                                <h3 class="m-portlet__head-text">
                                    Filter Data
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ Main::format_date($date_start) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end"
                                           value="{{ Main::format_date($date_end) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('labaRugiPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('labaRugiExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">

                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                        <thead>
                        <tr class="success">
                            <th width="10%">Kode Rekening</th>
                            <th width="60%">Nama Rekening</th>
                            <th width="20%" class="text-right">Nominal</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php($total_pendapatan = 0)
                        @foreach($kode_pendapatan as $pendapatan)
                            @php($total_pendapatan = $total_pendapatan + $biaya[$pendapatan->mst_kode_rekening])
                            <tr>
                                <td class="font-weight-bold">{{$pendapatan->mst_kode_rekening}}</td>
                                <td class="font-weight-bold">{{$pendapatan->mst_nama_rekening}}</td>
                                <td colspan="3"
                                    align="right">{{ Main::format_number($biaya[$pendapatan->mst_kode_rekening])}}
                                </td>
                            </tr>
                            @foreach($pendapatan->childs as $pendapatanUsaha)
                                <?php
                                // $total_pendapatan=$total_pendapatan+$biaya[$pendapatanUsaha->mst_kode_rekening];
                                ?>
                                <tr>
                                    <td>{{$pendapatanUsaha->mst_kode_rekening}}</td>
                                    <td>{!! $space1.$pendapatanUsaha->mst_nama_rekening !!}</td>
                                    <td colspan="3"
                                        align="right">{{ Main::format_number($biaya[$pendapatanUsaha->mst_kode_rekening]) }}
                                    </td>
                                </tr>
                                @foreach($pendapatanUsaha->childs as $penjualan)
                                    <?php
                                    // $total_pendapatan=$total_pendapatan+$biaya[$penjualan->mst_kode_rekening];
                                    ?>
                                    <tr>
                                        <td>{{$penjualan->mst_kode_rekening}}</td>
                                        <td>{!! $space2.$penjualan->mst_nama_rekening !!}</td>
                                        <td colspan="3"
                                            align="right">{{ Main::format_number($biaya[$penjualan->mst_kode_rekening])}}</td>
                                    </tr>
                                    @foreach($penjualan->childs as $penjualanChilds)
                                        <?php
                                        // $total_pendapatan=$total_pendapatan+$biaya[$penjualanChilds->mst_kode_rekening];
                                        ?>
                                        <tr>
                                            <td>{{$penjualanChilds->mst_kode_rekening}}</td>
                                            <td>{!! $space3.$penjualanChilds->mst_nama_rekening  !!}</td>
                                            <td colspan="3"
                                                align="right">
                                                {{ Main::format_number($biaya[$penjualanChilds->mst_kode_rekening]) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        <tr class="bg-info m--font-light">
                            <td colspan="2" class="text-center font-weight-bold">
                                <h5>TOTAL</h5>
                            </td>
                            <td align="right" class="font-weight-bold">
                                <h5>
                                    {{ Main::format_number($total_pendapatan)}}
                                </h5>
                            </td>
                        </tr>
                        @php($total_hpp = 0)
                        @foreach($kode_hpp as $hpp)
                            @php($total_hpp = $total_hpp + $biaya[$hpp->mst_kode_rekening])
                            <tr>
                                <td class="font-weight-bold">{{$hpp->mst_kode_rekening}}</td>
                                <td class="font-weight-bold">{!! $hpp->mst_nama_rekening !!}</td>
                                <td colspan="3" align="right" class="font-weight-bold">
                                    {{ Main::format_number($biaya[$hpp->mst_kode_rekening]) }}
                                </td>
                            </tr>
                            @foreach($hpp->childs as $hppChild)
                                <tr>
                                    <td class="font-weight-bold">{{$hppChild->mst_kode_rekening}}</td>
                                    <td class="font-weight-bold">{!! $space1.$hppChild->mst_nama_rekening !!}</td>
                                    <td colspan="3" align="right" class="font-weight-bold">
                                        {{ Main::format_number($biaya[$hppChild->mst_kode_rekening]) }}
                                    </td>
                                </tr>
                                @foreach($hppChild->childs as $hppChild2)
                                    <tr>
                                        <td>{{$hppChild2->mst_kode_rekening}}</td>
                                        <td>{!! $space2.$hppChild2->mst_nama_rekening !!}</td>
                                        <td colspan="3"
                                            align="right">
                                            {{ Main::format_number($biaya[$hppChild2->mst_kode_rekening]) }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                        <tr class="bg-info m--font-light">
                            <td colspan="2" align="center" class="font-weight-bold">
                                <h5> TOTAL </h5>
                            </td>
                            <td align="right" class="font-weight-bold">
                                <h5>{{ Main::format_number($total_hpp) }} </h5>
                            </td>
                        </tr>
                        <tr class="bg-success m--font-light">
                            <td colspan="2" align="center" class="font-weight-bold">
                                <h4>LABA (RUGI) KOTOR</h4>
                            </td>
                            <td align="right">
                                <h4>
                                    <strong>{{ Main::format_number($total_pendapatan-$total_hpp) }}</strong>
                                </h4>
                            </td>
                        </tr>
                        @php($total_biaya = 0)
                        @foreach($kode_biaya as $biaya2)
                            @php($total_biaya = $total_biaya + $biaya[$biaya2->mst_kode_rekening])
                            <tr>
                                <td class="font-weight-bold">{{$biaya2->mst_kode_rekening}}</td>
                                <td class="font-weight-bold">{!! $biaya2->mst_nama_rekening !!}</td>
                                <td colspan="3"
                                    align="right"
                                    class="font-weight-bold">
                                    {{ Main::format_number($biaya[$biaya2->mst_kode_rekening]) }}
                                </td>
                            </tr>
                            @foreach($biaya2->childs as $biayaChild)
                                <?php
                                // $total_biaya=$total_biaya+$biaya[$biayaChild->mst_kode_rekening];
                                ?>
                                <tr>
                                    <td class="font-weight-bold">
                                        {{ $biayaChild->mst_kode_rekening }}
                                    </td>
                                    <td class="font-weight-bold">{!! $space2.$biayaChild->mst_nama_rekening !!}</td>
                                    <td colspan="3"
                                        align="right">
                                        <strong>{{ Main::format_number($biaya[$biayaChild->mst_kode_rekening]) }}</strong>
                                    </td>
                                </tr>
                                @foreach($biayaChild->childs as $biayaChild2)
                                    <?php
                                    // $total_biaya=$total_biaya+$biaya[$biayaChild2->mst_kode_rekening];
                                    ?>
                                    <tr>
                                        <td>{{$biayaChild2->mst_kode_rekening}}</td>
                                        <td>{!! $space3.$biayaChild2->mst_nama_rekening !!}</td>
                                        <td colspan="3"
                                            align="right">
                                            {{ Main::format_number($biaya[$biayaChild2->mst_kode_rekening]) }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                        <tr class="bg-info m--font-light">
                            <td colspan="2" align="center" class="font-weight-bold"><h4>TOTAL</h4></td>
                            <td align="right" class="font-weight-bold"><h4>{{Main::format_number($total_biaya) }}</h4></td>
                        </tr>
                        @php($total_pendapatan_diluar_usaha = 0)
                        @foreach($kode_pendapatan_diluar_usaha as $pendapatan_diluar_usaha)
                            @php(
                            $total_pendapatan_diluar_usaha = $total_pendapatan_diluar_usaha + $biaya[$pendapatan_diluar_usaha->mst_kode_rekening]
                            )
                            <tr>
                                <td class="font-weight-bold">{{$pendapatan_diluar_usaha->mst_kode_rekening}}</td>
                                <td class="font-weight-bold">{!! $pendapatan_diluar_usaha->mst_nama_rekening !!}</td>
                                <td colspan="3"
                                    align="right">{{ Main::format_number($biaya[$pendapatan_diluar_usaha->mst_kode_rekening]) }}
                                </td>
                            </tr>
                            @foreach($pendapatan_diluar_usaha->childs as $pendapatan_diluar_usahaChild)
                                <tr>
                                    <td>{{ $pendapatan_diluar_usahaChild->mst_kode_rekening }}</td>
                                    <td>{!! $space1.$pendapatan_diluar_usahaChild->mst_nama_rekening !!}</td>
                                    <td colspan="3"
                                        align="right">
                                        {{ Main::format_number($biaya[$pendapatan_diluar_usahaChild->mst_kode_rekening]) }}
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        <tr class="bg-info m--font-light">
                            <td colspan="2" align="center" class="font-weight-bold"><h5> TOTAL </h5></td>
                            <td align="right" class="font-weight-bold">
                                <h5>
                                    {{ Main::format_number($total_pendapatan_diluar_usaha) }}
                                </h5>
                            </td>
                        </tr>
                        <tr class="bg-success m--font-light">
                            <td colspan="2" align="center" class="font-weight-bold"><h4>LABA (RUGI) BERSIH </h4></td>
                            <td align="right" class="font-weight-bold">
                                <h4>
                                    {{ Main::format_number($total_pendapatan-$total_hpp-$total_biaya+$total_pendapatan_diluar_usaha) }} </strong>
                                </h4>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection