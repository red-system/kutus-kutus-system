<?php

namespace app\Http\Controllers\InventoryLogistik;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;

use DB;

use app\Models\mKategoriBahan;
use app\Models\mBahan;
use app\Models\mLokasi;
use app\Models\mSatuan;
use app\Models\mSupplier;
use app\Models\mHistoryPenyesuaianBahan;
use app\Models\mHistoryTransferBahan;
use app\Models\mPoBahanDetail;
use app\Models\mStokBahan;
use app\Models\mBahanProduksi;


class Bahan extends Controller
{

    private $breadcrumb;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['kategoriBahan'] = mKategoriBahan::orderBy('kategori_bahan', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['satuan'] = mSatuan::orderBy('satuan', 'ASC')->get();
        $data['supplier'] = mSupplier::orderBy('nama_supplier', 'ASC')->get();
        $data['total_bahan'] = 0;
        $data['list'] = mBahan
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_bahan:id,kode_kategori_bahan,kategori_bahan',
                'satuan:id,kode_satuan,satuan',
                'supplier:id,kode_supplier,nama_supplier'
            )
            ->withcount([
                'stok_bahan AS qty_total' => function ($query) {
                    return $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        return view('inventoryLogistik/bahan/bahanList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'kode_bahan' => 'required',
            'nama_bahan' => 'required',
            'id_kategori_bahan' => 'required',
            'minimal_stok' => 'required',
            /*            'qty'=>'required',*/
            'harga' => 'required',
//            'id_lokasi'=>'required',
            'id_satuan' => 'required',
            'id_supplier' => 'required'
        ]);

        $data_bahan = $request->except(['_token']);
        //$data_bahan['minimal_stok'] = main::convert_number($data_bahan['minimal_stok']);
        $data_bahan['harga'] = main::convert_money($data_bahan['harga']);
        /*        $data_bahan['qty'] = main::convert_number($data_bahan['qty']);*/
//        $data_bahan['last_stok'] = $data_bahan['qty'];
        $response = mBahan::create($data_bahan);
        $id_bahan = $response->id;

        /*        $data_arus_bahan = array(
                    'tgl'=>date('Y-m-d H:i:s'),
                    'id_bahan'=>$id_bahan,
                    'stok_in'=>$data_bahan['qty'],
                    'stok_out'=>0,
                    'last_stok'=>$data_bahan['qty'],
                    'keterangan'=>$data_bahan['keterangan'],
                    'method'=>'insert'
                );
                mArusStokBahan::create($data_arus_bahan);*/
    }

    function delete($id)
    {
        $id_bahan = $id;
        $where = [
            'id_bahan' => $id_bahan
        ];
        $bahan_produksi = mBahanProduksi::where($where)->count();
        $history_penyesuaian_bahan = mHistoryPenyesuaianBahan::where($where)->count();
        $history_transfer_bahan = mHistoryTransferBahan::where($where)->count();
        $po_bahan_detail = mPoBahanDetail::where($where)->count();
        $stok_bahan = mStokBahan::where($where)->count();
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';

        $list_table = [
            'bahan_produksi',
            'history_penyesuaian_bahan',
            'history_transfer_bahan',
            'po_bahan_detail',
            'stok_bahan'
        ];

        foreach ($list_table as $table) {
            if ($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch ($check_table_problem) {
            case "bahan_produksi":
                $message = 'Tidak bisa dihapus, karena Bahan sudah digunakan di <strong>Bahan Produksi</strong>';
                break;
            case "history_penyesuaian_bahan":
                $message = 'Tidak bisa dihapus, karena Bahan sudah tersimpan di <strong>History Penyesuaian Bahan</strong>';
                break;
            case "history_transfer_bahan":
                $message = 'Tidak bisa dihapus, karena Bahan sudah tersimpan di <strong>History Transfer Bahan</strong>';
                break;
            case "po_bahan_detail":
                $message = 'Tidak bisa dihapus, karena Bahan sudah tersimpan di <strong>PO Bahan</strong>';
                break;
            case "stok_bahan":
                $message = 'Ada <strong>Stok Bahan</strong> pada Bahan ini, sehingga tidak bisa dihapus';
                break;
        }

        if ($check_status) {
            mBahan::where('id', $id)->delete();
        } else {
            $response = [
                "title" => 'Perhatian ...',
                "message" => $message
            ];

            return response($response, 422);
        }
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'kode_bahan' => 'required',
            'nama_bahan' => 'required',
            'id_kategori_bahan' => 'required',
            'minimal_stok' => 'required',
            /*            'qty'=>'required',*/
            'harga' => 'required',
//            'id_lokasi'=>'required',
            'id_satuan' => 'required',
            'id_supplier' => 'required'
        ]);

        $data_bahan = $request->except("_token");
        //$data_bahan['minimal_stok'] = main::convert_number($data_bahan['minimal_stok']);
        $data_bahan['harga'] = main::convert_money($data_bahan['harga']);
        /*        $data_bahan['qty'] = main::convert_number($data_bahan['qty']);*/
        mBahan::where(['id' => $id])->update($data_bahan);

        /*        $data_arus_bahan = array(
                    'tgl'=>date('Y-m-d H:i:s'),
                    'id_bahan'=>$id,
                    'stok_in'=>$data_bahan['qty'],
                    'stok_out'=>0,
                    'last_stok'=>$data_bahan['qty'],
                    'keterangan'=>$data_bahan['keterangan'],
                    'method'=>'update'
                );
                mArusStokBahan::create($data_arus_bahan);*/
    }
}
