@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('.update-stok-bahan').dblclick(function (e) {
                var arus_id = $(this).data('arus-id');
                var last_stok = $(this).data('last-stok');

                $('#modal-stok-bahan').modal('show');
                $('#modal-stok-bahan [name="arus_stok_bahan_id"]').val(arus_id);
                $('#modal-stok-bahan [name="last_stok"]').val(last_stok);
            });
        });
    </script>

    <style type="text/css">
        .update-stok-bahan {
            cursor: pointer;
        }
    </style>
@endsection

@section('body')

    <form action="{{ route('kartuStokBahanPageUpdate') }}" method="post" class="form-send">
        {{ csrf_field() }}
        <div class="modal fade" tabindex="-1" id="modal-stok-bahan" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="arus_stok_bahan_id">
                        <input type="text" name="last_stok">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </form>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">


                        <div class="form-group m-form__group row">

                            <label class="col-lg-2 col-form-label">Tanggal Mulai</label>
                            <div class="col-lg-3">
                                <div class="input-group date">
                                    <input type="text"
                                           class="form-control m-input m_datepicker"
                                           readonly
                                           name="date_start"
                                           value="{{ $date_start }}"/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>

                            <label class="col-lg-2 col-form-label">Tanggal Selesai</label>
                            <div class="col-lg-3">
                                <div class="input-group date">
                                    <input type="text"
                                           class="form-control m-input m_datepicker"
                                           readonly
                                           name="date_end"
                                           value="{{ $date_end }}"/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Kategori Bahan</label>
                            <div class="col-lg-3">
                                <select class="form-control m-select2" name="id_kategori_bahan">
                                    <option value="all">Semua Kategori Bahan</option>
                                    @foreach($kategori_bahan as $r)
                                        <option value="{{ $r->id }}" {{ $id_kategori_bahan == $r->id ? 'selected':'' }}>
                                            {{ $r->kategori_bahan }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="col-lg-2 col-form-label">Bahan</label>
                            <div class="col-lg-3">
                                <select class="form-control m-select2" name="id_bahan">
                                    <option value="all">Semua Bahan</option>
                                    @foreach($bahan as $r)
                                        <option value="{{ $r->id }}" {{ $id_bahan == $r->id ? 'selected':'' }}>{{
                                            '('.$r->kode_bahan.') '.$r->nama_bahan }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-group m-form__group row">

                            <label class="col-lg-2 col-form-label">Gudang</label>
                            <div class="col-lg-3">
                                <select class="form-control m-select2" name="id_lokasi">
                                    <option value="all">Semua Gudang</option>
                                    @foreach($gudang as $r)
                                        <option value="{{ $r->id }}" {{ $id_lokasi == $r->id ? 'selected':'' }}>{{
                                            $r->lokasi }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('kartuStokBahanPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('kartuStokBahanExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover datatable-no-order">
                        <thead>
                        <tr>
                            <th colspan="6" class="text-center" width="50%">STOK MASUK</th>
                            <th colspan="7" class="text-center" width="50%">STOK KELUAR</th>
                        </tr>
                        <tr>
                            <th width="130">Tanggal</th>
                            <th>Bahan</th>
                            <th>Satuan</th>
                            <th>Lokasi</th>
                            <th>Masuk</th>
                            <th>Invoice/Nota</th>
                            <th>Penerima/Keterangan</th>
                            <th width="200">Stok Sementara</th>
                            <th width="100">Tanggal</th>
                            <th>Bahan</th>
                            <th>Satuan</th>
                            <th>Lokasi</th>
                            <th>Keluar</th>
                            <th>Penerima/Keterangan</th>
                            <th width="400">Stok Akhir</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $tgl => $value)

                            @foreach($value as $r_arus)
                                @php
                                    $status = $r_arus->stok_in == 0 ? 'out' : 'in';

                                    $stok_akhir = $r_arus->last_stok;
                                @endphp



                                @if($status == 'in')
                                    <tr id="{{ $r_arus->id }}">
                                        <td>{{ Main::format_date($tgl) }}</td>
                                        <td>{{ '('.$r_arus->bahan['kode_bahan'].') '.$r_arus->bahan['nama_bahan'] }}</td>
                                        <td>{{ $r_arus->bahan['satuan']['satuan'] }}</td>
                                        <td>{{ $r_arus->lokasi }}</td>
                                        <td>{{ Main::format_number($r_arus->stok_in) }}</td>
                                        <td>{{ $r_arus->po_bahan['no_faktur'] }}</td>
                                        <td>{{ $r_arus->keterangan }}</td>
                                        <td align="right">{{ Main::format_number($r_arus->last_stok) }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td align="right" class="update-stok-bahan" data-arus-id="{{ $r_arus->id }}"
                                            data-last-stok="{{ $r_arus->last_stok }}">{{ Main::format_number($stok_akhir) }}</td>
                                    </tr>
                                @else
                                    <tr id="{{ $r_arus->id }}">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ Main::format_date($tgl) }}</td>
                                        <td>{{ '('.$r_arus->bahan['kode_bahan'].') '.$r_arus->bahan['nama_bahan'] }}</td>
                                        <td>{{ $r_arus->bahan['satuan']['satuan'] }}</td>
                                        <td>{{ $r_arus->lokasi }}</td>
                                        <td align="right">{{ Main::format_number($r_arus->stok_out) }}</td>
                                        <td>{{ $r_arus->keterangan }}</td>
                                        <td align="right" class="update-stok-bahan" data-arus-id="{{ $r_arus->id }}"
                                            data-last-stok="{{ $r_arus->last_stok }}">{{ Main::format_number($stok_akhir) }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
