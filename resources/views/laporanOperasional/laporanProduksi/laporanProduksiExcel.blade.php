<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Produksi " . date('d-m-Y') . ".xls");
?>

<h3 align="center">LAPORAN PRODUKSI</h3>
<br />
<table width="100%" class="table-header">
    <tbody>
    <tr>
        <td>Tanggal Produksi: {{ $date_start.' s/d '.$date_end }}</td>
    </tr>
    </tbody>
</table>
<br/>
    <h4>Rangkuman Produksi</h4>
    <table border="1" width="100%">
        <thead>
        <tr class="tabletitle">
            <th class="item" width="10">No</th>
            <th class="item">Kode Produk</th>
            <th class="item">Nama Produk</th>
            <th class="item">Qty Produk</th>
        </tr>
        </thead>
        <tbody>
        @foreach($produk as $r)
            @php
                $total += $r->total_qty
            @endphp
            <tr class="service">
                <td class="tableitem"><p class="itemtext">{{ $no++ }}</p></td>
                <td class="tableitem"><p class="itemtext">{{ $r->kode_produk }}</p></td>
                <td class="tableitem"><p class="itemtext">{{ $r->nama_produk }}</p></td>
                <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->total_qty) }}</p></td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3" align="right">
                <strong>Total</strong>
            </td>
            <td align="right">{{ Main::format_number($total) }}</td>
        </tr>
        </tfoot>
    </table>
    <br /><br />
    <h4>Detail Produksi</h4>
    <table border="1" width="100%">
        <thead>
        <tr class="tabletitle">
            <th class="item">No</th>
            <th colspan="3" class="item">Tanggal / Produk</th>
            @foreach($produk as $r)
                <th class="item">{{ '('.$r->kode_produk.') '.$r->nama_produk }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @php
            $no = 1
        @endphp

        @foreach($produksi as $r)
            <tr class="service">
                <td rowspan="2">{{ $no++ }}</td>
                <td rowspan="2">{{ $r->kode_produksi }}</td>
                <td>Mulai</td>
                <td>{{ Main::format_date($r->tgl_mulai_produksi) }}</td>
                @foreach($produk as $r_produk)
                    @php
                        $qty = \app\Models\mProgressProduksi
                        ::where([
                            'id_produk'=>$r_produk->id,
                            'id_produksi'=>$r->id
                        ])
                        ->value('qty_progress');

                        $no_seri_produk = \app\Models\mStokProduk
                        ::where([
                            'id_produksi'=>$r->id,
                            'id_produk'=>$r_produk->id
                        ])
                        ->value('no_seri_produk')
                    @endphp
                    <td class="tableitem" rowspan="2" align="right">
                        <p class="itemtext">
                            {{ Main::format_number($qty) }}
                        </p>
                    </td>
                @endforeach
            </tr>
            <tr class="service">
                <td></td>Selesai</p></td>
                <td class="tableitem"><p class="itemtext">{{ Main::format_date($r->tgl_selesai_produksi) }}</p></td>
            </tr>
        @endforeach
        </tbody>
    </table>