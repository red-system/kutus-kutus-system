<form action="{{ route('transferStokBahanInsert') }}" method="post" class="m-form form-send m-form--label-align-right">
    {{ csrf_field() }}
    <input type="hidden" name="id">
    <input type="hidden" name="id_lokasi_dari">
    <input type="hidden" name="qty_dari">
    <input type="hidden" name="id_bahan">
    <div class="modal" id="modal-transfer-stok-bahan" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Transfer Stok Bahan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Kode Bahan</label>
                                <div class="col-lg-6 label-data">
                                    <span class="kode_bahan"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Nama Bahan</label>
                                <div class="col-lg-6 label-data">
                                    <span class="nama_bahan"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Dari Lokasi</label>
                                <div class="col-lg-6 label-data">
                                    <span class="lokasi"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Qty</label>
                                <div class="col-lg-6 label-data">
                                    <span class="qty_dari_format"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Tujuan Lokasi </label>
                                <div class="col-lg-6">
                                    <select class="form-control m-input m-select2" name="id_lokasi_tujuan" style="width: 100% !important;">
                                        @foreach($lokasi as $r)
                                            <option value="{{ $r->id }}">{{ $r->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Qty Transfer </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control m-input touchspin-number" name="qty_transfer" value="1">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Penerima</label>
                                <div class="col-lg-6">
                                    <input type="text" name="keterangan" class="form-control m-input">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="la la-truck"></i>  Transfer Bahan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>