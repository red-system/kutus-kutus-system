<form action="" method="post" class="m-form form-send m-form--label-align-right">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label">Kode Produk </label>
                                <div class="col-lg-9 ">
                                    {{ $bahan->kode_bahan }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label">Nama Produk </label>
                                <div class="col-lg-9 ">
                                    {{ $bahan->nama_bahan }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">No Seri Bahan </label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control m-input" name="no_seri_bahan" autofocus>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Qty </label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control m-input touchspin-number-decimal" name="qty">
                                </div>
                                <div class="col-lg-2 form-control">{{ $bahan->satuan->satuan }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Lokasi </label>
                                <div class="col-lg-9">
                                    <select class="form-control m-input  m-select2" name="id_lokasi" style="width: 100% !important;">
                                        @foreach($lokasi as $r)
                                            <option value="{{ $r->id }}">{{ $r->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label">Penerima </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="keterangan">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>