<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mKaryawan extends Model
{
    protected $table = 'tb_karyawan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_karyawan',
        'nama_karyawan',
        'alamat_karyawan',
        'telp_karyawan',
        'posisi_karyawan',
        'email_karyawan',
        'foto_karyawan'
    ];

    public function user() {
        return $this->hasMany(mUser::class, 'id');
    }
}
