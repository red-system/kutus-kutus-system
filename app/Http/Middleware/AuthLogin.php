<?php

namespace app\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class AuthLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $login = Session::get('login');
        $level = Session::get('level');
        if (!$login) {
            return redirect()->route('loginPage');
        }

        $role = strpos($role, '|') ? explode('|', $role) : array($role);
        //echo json_encode([$level, $role]);

        if (!in_array($level, $role)) {
            return redirect()->route('dashboardPage');
        }

        return $next($request);
    }
}
