<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHistoryTransferProduk extends Model
{

    protected $table = 'tb_history_transfer_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tgl',
        'id_produk',
        'id_stok_produk',
        'kode_produk',
        'nama_produk',
        'no_seri_produk',
        'qty_transfer',
        'id_lokasi_dari',
        'id_lokasi_tujuan',
        'dari',
        'tujuan',
        'last_stok'
    ];
}
