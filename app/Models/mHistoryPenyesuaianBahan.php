<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHistoryPenyesuaianBahan extends Model
{

    protected $table = 'tb_history_penyesuaian_bahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tgl',
        'id_bahan',
        'id_kategori_bahan',
        'id_lokasi',
        'id_stok_bahan',
        'kode_bahan',
        'nama_bahan',
        'kategori_bahan',
        'lokasi',
        'qty_awal',
        'qty_akhir',
        'keterangan'
    ];

    function bahan() {
        return $this->belongsTo(mBahan::class, 'id_bahan');
    }

    function kategori_bahan() {
        return $this->belongsTo(mKategoriBahan::class, 'id_kategori_bahan');
    }

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }
}
