@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('akunting.asset.modalPenyusutan')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('assetCreate') }}"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <i class="la la-plus"></i>
                        Tambah Asset
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped- table-bordered table-hover datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Asset</th>
                            <th>Nama</th>
                            <th>Tanggal Perolehan</th>
                            <th>Kategori</th>
                            <th>Jumlah</th>
                            <th>Harga Beli</th>
                            <th>Beban Perbulan</th>
                            <th>Nilai Buku</th>
                            <th>Akumulasi Beban</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->kode_asset }}</td>
                                <td>{{ $r->nama }}</td>
                                <td>{{ Main::format_date_label($r->tanggal_beli) }}</td>
                                <td>{{ $r->kategori_asset->nama}}</td>
                                <td>{{ Main::format_number($r->qty) }}</td>
                                <td>{{ Main::format_money($r->harga_beli) }}</td>
                                <td>{{ Main::format_money($r->beban_perbulan) }}</td>
                                <td>{{ Main::format_money($r->nilai_buku) }}</td>
                                <td>{{ Main::format_money($r->akumulasi_beban) }}</td>
                                <td align="center">
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm akses-penyusutan_aset">
                                        <button type="button"
                                                class="m-btn btn btn-info btn-detail"
                                                data-route="{{ route('assetPenyusutanModal', ['id'=>$r->id]) }}">
                                            <i class="la la-anchor"></i> Penyusutan Asset
                                        </button>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-hapus akses-delete"
                                                data-route='{{ route('assetDelete', ['id'=>$r->id]) }}'
                                                data-message="Yakin hapus data asset ini ?<br />
                                                              Data ini akan menghapus data yang terkait juga.<br />
                                                              Seperti: Penyusutan Asset dan Jurnal Umum">
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
