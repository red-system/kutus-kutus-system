<style type="text/css">
    .table-data {
        border-left: 0.01em solid #ccc;
        border-right: 0;
        border-top: 0.01em solid #ccc;
        border-bottom: 0;
        border-collapse: collapse;
    }
    .table-data td,
    .table-data th {
        border-left: 0;
        border-right: 0.01em solid #ccc;
        border-top: 0;
        border-bottom: 0.01em solid #ccc;
        padding: 2px 4px;
        text-align: center !important;
        font-size: 12px;
    }
</style>

<h1 align="center">KARTU STOK BAHAN</h1>
<table width="100%">
    <tbody>
    <tr>
        <td width="25%">Nama Kategori Bahan : {{ $nama_kategori_bahan }}</td>
        <td width="25%">Nama Bahan : {{ $nama_bahan }}</td>
        <td width="25%">Tanggal : {{ $date_start }} s/d {{ $date_end }}</td>
        <td width="25%">Lokasi : {{ $nama_lokasi }}</td>
    </tr>
    <tr>
        <td>Satuan: {{ $nama_satuan }}</td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<table width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th colspan="{{ $stok_masuk_col }}" width="40%">STOK MASUK</th>
        <th colspan="{{ $stok_keluar_col }}" width="60%">STOK KELUAR</th>
    </tr>
    <tr>
        <th>Tanggal</th>
        @if($view_bahan == 'all')
            <th>Bahan</th>
        @endif
        @if($view_bahan == 'all')
            <th>Satuan</th>
        @endif
        @if($view_lokasi == 'all')
            <th>Lokasi</th>
        @endif
        <th>Masuk</th>
        <th>Invoice/Nota</th>
        <th>Penerima/Keterangan</th>
        <th>Stok Sementara</th>
        <th>Tanggal</th>
        @if($view_bahan == 'all')
            <th>Bahan</th>
        @endif
        @if($view_bahan == 'all')
            <th>Satuan</th>
        @endif
        @if($view_lokasi == 'all')
            <th>Lokasi</th>
        @endif
        <th>Keluar</th>
        <th>Penerima/Keterangan</th>
        <th>Stok Akhir</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $tgl => $value)

        @foreach($value as $r_arus)
            @php
                $status = $r_arus->stok_in == 0 ? 'out' : 'in';
                    $stok_akhir = $r_arus->last_stok;
            @endphp

            @if($status == 'in')
                <tr>
                    <td>{{ Main::format_date($tgl) }}</td>
                    @if($view_bahan == 'all')
                        <td>{{ $r_arus->bahan['nama_bahan'] }}</td>
                    @endif
                    @if($view_lokasi == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    @if($view_satuan == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    <td>{{ Main::format_number($r_arus->stok_in) }}</td>
                    <td>{{ $r_arus->po_bahan['no_faktur'] }}</td>
                    <td>{{ $r_arus->keterangan }}</td>
                    <td align="right">{{ Main::format_number($r_arus->last_stok) }}</td>
                    <td></td>
                    @if($view_bahan == 'all')
                        <td></td>
                    @endif
                    @if($view_lokasi == 'all')
                        <td></td>
                    @endif
                    @if($view_satuan == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td></td>
                    <td align="right">{{ Main::format_number($stok_akhir) }}</td>
                </tr>
            @else
                <tr>
                    <td></td>
                    @if($view_bahan == 'all')
                        <td></td>
                    @endif
                    @if($view_lokasi == 'all')
                        <td></td>
                    @endif
                    @if($view_satuan == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right">{{ Main::format_date($tgl) }}</td>
                    @if($view_bahan == 'all')
                        <td>{{ $r_arus->bahan['nama_bahan'] }}</td>
                    @endif
                    @if($view_lokasi == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    @if($view_satuan == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    <td>{{ Main::format_number($r_arus->stok_out) }}</td>
                    <td>{{ $r_arus->keterangan }}</td>
                    <td align="right">{{ Main::format_number($stok_akhir) }}</td>
                </tr>
            @endif
        @endforeach
    @endforeach

    </tbody>
</table>