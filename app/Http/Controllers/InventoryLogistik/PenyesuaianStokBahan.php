<?php

namespace app\Http\Controllers\InventoryLogistik;

use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;

use app\Models\mBahan;
use app\Models\mStokBahan;
use app\Models\mKategoriBahan;
use app\Models\mlokasi;
use app\Models\mHistoryPenyesuaianBahan;
use Illuminate\Support\Facades\Config;
use app\Models\mArusStokBahan;

use DB,
    PDF;

class PenyesuaianStokBahan extends Controller
{


    private $breadcrumb;
    private $cons;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->datetime = date('Y-m-d H:i:s');

        $this->menuActive = $cons['inventory_8'];
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_8'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mBahan
            ::with(
                'kategori_bahan:id,kode_kategori_bahan,kategori_bahan',
                'lokasi:id,kode_lokasi,lokasi'
            )
            ->withCount([
                'stok_bahan AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(qty) AS total_qty'));
                }
            ])
            ->orderBy('nama_bahan', 'ASC')
            ->get();

        $data['list'] = $list;
        $data['tab'] = 'penyesuaian';

        return view('inventoryLogistik/penyesuaianStokBahan/penyesuaianStokBahanList', $data);
    }

    function stok($idBahan = '')
    {
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_14'],
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $idBahan = Main::decrypt($idBahan);
        $data = Main::data($breadcrumb, $this->menuActive);
        $bahan = mBahan::with('kategori_bahan')->find($idBahan);
        $pageTitle = $bahan->kode_bahan . ' - ' . $bahan->nama_bahan;
        $list = mStokBahan
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'bahan:id,kode_bahan,nama_bahan'
            )
            ->where('id_bahan', $idBahan)
            ->orderBy('id', 'ASC')
            ->get();


        $data['kategoribahan'] = mKategoriBahan::orderBy('kategori_bahan', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['pageTitle'] = $pageTitle;
        $data['list'] = $list;
        $data['bahan'] = $bahan;
        $data['tab'] = 'penyesuaian';


        return view('inventoryLogistik/penyesuaianStokBahan/stokPenyesuaianBahan', $data);
    }

    function history(Request $request)
    {
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_13'],
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryPenyesuaianBahan
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryPenyesuaianBahan
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mHistoryPenyesuaianBahan
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'ASC')
            ->get();
        $tab = 'history';

        $data = array_merge($data, [
            'list' => $list,
            'tab' => $tab,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end
        ]);

        return view('inventoryLogistik/penyesuaianStokBahan/historyPenyesuaianStokBahanList', $data);
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'qty' => 'required',
            'keterangan_penyesuaian' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $qty_stok_bahan_before = $request->input('qty_awal');
            $qty = $request->input('qty');
            $qty_now = $qty;
            $keterangan_penyesuaian = $request->input('keterangan_penyesuaian');
            $id_bahan = $request->input('id_bahan');
            //$bahan = mBahan::where('id', $id_bahan)->first(['kode_bahan','nama_bahan']);
            $id_kategori_bahan = $request->input('id_kategori_bahan');
            $id_lokasi = $request->input('id_lokasi');
            $kode_bahan = $request->input('kode_bahan');
            $nama_bahan = $request->input('nama_bahan');
            $kategori_bahan = $request->input('kategori_bahan');
            $lokasi = $request->input('lokasi');
            $qty_awal = $request->input('qty_awal');
            $tipe_arus_kas = 'Operasi';
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $kode_penyesuaian_stok_bahan = Config::get('constants.kodePenyesuaianStokBahan');
            $harga = mBahan::where('id', $id_bahan)->value('harga');
            $total_hpp = abs($qty_stok_bahan_before - $qty_now) * $harga;

            if ($qty_now > $qty_stok_bahan_before) {
                $stok_in = $qty_now - $qty_stok_bahan_before;
                $stok_out = 0;
            } else {
                $stok_in = 0;
                $stok_out = $qty_stok_bahan_before - $qty_now;
            }


            $data_stok_bahan = [
                'qty' => $qty,
                'keterangan' => $keterangan_penyesuaian
            ];

            mStokBahan::where(['id' => $id])->update($data_stok_bahan);

            $last_stok = mStokBahan::where('id_bahan', $id_bahan)->sum('qty');
            $last_stok_total = mStokBahan::sum('qty');

            $data_penyesuaian = [
                'tgl' => date('Y-m-d H:i:s'),
                'id_bahan' => $id_bahan,
                'id_kategori_bahan' => $id_kategori_bahan,
                'id_lokasi' => $id_lokasi,
                'id_stok_bahan' => $id,
                'kode_bahan' => $kode_bahan,
                'nama_bahan' => $nama_bahan,
                'kategori_bahan' => $kategori_bahan,
                'lokasi' => $lokasi,
                'qty_awal' => $qty_awal,
                'qty_akhir' => $qty,
                'keterangan' => $keterangan_penyesuaian
            ];

            $response = mHistoryPenyesuaianBahan::create($data_penyesuaian);
            $id_history_penyesuaian_bahan = $response->id;
            $no_invoice = $kode_penyesuaian_stok_bahan . '-' . $id_history_penyesuaian_bahan;

            $data_arus_stok_bahan = [
                'tgl' => date('Y-m-d'),
                'table_id' => $id,
                'table_name' => 'tb_stok_bahan',
                'id_stok_bahan' => $id,
                'id_bahan' => $id_bahan,
                'stok_in' => $stok_in,
                'stok_out' => $stok_out,
                'last_stok' => $last_stok,
                'last_stok_total' => $last_stok_total,
                'keterangan' => 'Penyesuaian Stok Bahan: ' . $keterangan_penyesuaian,
                'method' => 'update'
            ];

            mArusStokBahan::create($data_arus_stok_bahan);

            /**
             * Begin Akunting
             */
            $data_jurnal_umum = [
                'no_invoice' => $no_invoice,
                'id_history_penyesuaian_bahan' => $id_history_penyesuaian_bahan,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_keterangan' => 'Penyesuaian Stok Bahan: (' . $kode_bahan . ') - ' . $nama_bahan,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
            ];
            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            $master_id_kredit = [
                192, // Penambahan (Kerugian) Penyesuaian Stok Bahan Baku
                191 // Persediaan bahan baku
            ];

            foreach ($master_id_kredit as $key => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                if ($qty_stok_bahan_before > $qty_now) {
                    if ($master_id == 192) {
                        $debet_nom = $total_hpp;
                        $kredit_nom = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                    } elseif ($master_id == 191) {
                        $debet_nom = 0;
                        $kredit_nom = $total_hpp;
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                    }
                } else {
                    if ($master_id == 192) {
                        $debet_nom = 0;
                        $kredit_nom = $total_hpp;
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                    } elseif ($master_id == 191) {
                        $debet_nom = $total_hpp;
                        $kredit_nom = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                    }
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $debet_nom,
                    'trs_kredit' => $kredit_nom,
                    'user_id' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'tgl_transaksi' => $this->datetime,
                ];
                mAcTransaksi::create($data_transaksi);
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }

    function history_pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryPenyesuaianBahan
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryPenyesuaianBahan
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mHistoryPenyesuaianBahan
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'ASC')
            ->get();
        $tab = 'history';

        $data = array_merge($data, [
            'list' => $list,
            'tab' => $tab,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company'=>Main::companyInfo()
        ]);

        $pdf = PDF::loadView('inventoryLogistik/penyesuaianStokBahan/historyPenyesuaianStokBahanPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('History Penyesuaian Stok Bahan ' . date('d-m-Y') . '.pdf');
    }

    function history_excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryPenyesuaianBahan
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryPenyesuaianBahan
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mHistoryPenyesuaianBahan
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'ASC')
            ->get();
        $tab = 'history';

        $data = array_merge($data, [
            'list' => $list,
            'tab' => $tab,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company'=>Main::companyInfo()
        ]);

        return view('inventoryLogistik/penyesuaianStokBahan/historyPenyesuaianStokBahanExcel', $data);
    }

}
