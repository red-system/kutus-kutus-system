
<input type="hidden" name="id_asset" value="{{$asset->id}}">
<input type="hidden" name="beban_perbulan" value="{{$asset->beban_perbulan}}">
<input type="hidden" name="akumulasi_beban" value="{{$asset->akumulasi_beban}}">
<input type="hidden" name="nilai_buku" value="{{$asset->nilai_buku}}">
<input type="hidden" name="nama" value="{{$asset->nama}}">
<input type="hidden" name="master_id_asset" value="{{$asset->master_id_asset}}">
<input type="hidden" name="master_id_depresiasi" value="{{$asset->master_id_depresiasi}}">
<input type="hidden" name="master_id_akumulasi" value="{{$asset->master_id_akumulasi}}">

<div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12 required">Tanggal Transaksi</label>
    <div class="col-lg-4 col-md-9 col-sm-12">
        <div class="input-group date">
            <input type="text" class="form-control m-input m_datepicker"
                   name="tanggal_transaksi" readonly="" value="{{ date('d-m-Y') }}">
            <div class="input-group-append">
                <span class="input-group-text">
                    <i class="la la-calendar-check-o"></i>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12 required">No Transaksi</label>
    <div class="col-lg-4 col-md-9 col-sm-12">
        <input type="text" name="no_transaksi" class="form-control"
               value="{{ $asset->kode_asset }}" readonly>
    </div>
</div>

<table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
    <thead>
    <tr>
        <th>Kode Perkiraan</th>
        <th>Debet</th>
        <th>Kredit</th>
        <th>Keterangan <span class="required"></span></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            {{ $asset->kodePerkiraanDepresiasi->mst_kode_rekening}}
            -
            {{$asset->kodePerkiraanDepresiasi->mst_nama_rekening}}
        </td>
        <td class="payment">
            {{ Main::format_number($asset->beban_perbulan) }}
        </td>
        <td class="payment_total">
            0
        </td>

        <td>
            <input type="text" name="keterangan_depresiasi" class="form-control">
        </td>
    </tr>
    <tr>
        <td>
            {{$asset->kodePerkiraanAkumulasi->mst_kode_rekening}}
            -
            {{$asset->kodePerkiraanAkumulasi->mst_nama_rekening}}
        </td>
        <td class="payment">
            0
        </td>
        <td class="payment_total">
            {{ Main::format_number($asset->beban_perbulan )}}
        </td>

        <td>
            <input type="text" name="keterangan_akumulasi" class="form-control">
        </td>
    </tr>
    </tbody>
</table>