<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>Laporan Penggunaan Bahan</h3></td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <table width="100%" class="table-header">
                <tbody>
                <tr>
                    <td>Tanggal Produksi: {{ $date_start.' s/d '.$date_end }}</td>
                </tr>
                </tbody>
            </table>
            <br/>
            <div id="table">
                <h4>Rangkuman Penggunaan Bahan</h4>
                <table>
                    <thead>
                    <tr class="tabletitle">
                        <th class="item" width="10">No</th>
                        <th class="item">Kode Produk</th>
                        <th class="item">Nama Produk</th>
                        <th class="item">Qty Produk</th>
                        <th class="item">Total Bahan Digunakan</th>
                    </tr>
                    </thead>
                    <tbody>


                    @php
                        $total_qty_produksi = 0;
                        $total_qty_bahan = 0;
                    @endphp

                    @foreach($produk as $r)
                        @php
                            $total_qty_produksi += $r->total_qty_produksi;
                            $total_qty_bahan += ($r->total_qty_produksi*$r->total_qty_bahan)
                        @endphp
                        <tr class="service">
                            <td class="tableitem"><p class="itemtext">{{ $no++ }}.</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->kode_produk }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->nama_produk }}</p></td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->total_qty_produksi) }}</p></td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->total_qty_produksi*$r->total_qty_bahan) }}</p></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3" align="right">
                            <strong>Total</strong>
                        </td>
                        <td align="right">{{ Main::format_number($total_qty_produksi) }}</td>
                        <td align="right">{{ Main::format_number($total_qty_bahan) }}</td>
                    </tr>
                    </tfoot>
                </table>
                <br />
                <h4>Detail Penggunaan Bahan</h4>
                <table>
                    <thead>
                    <tr class="tabletitle">
                        <th class="item"  width="30">No</th>
                        <th class="item"  width="210">Nama Bahan</th>
                        @foreach($produksi as $r)
                            <th class="item">
                                {{ $r->kode_produksi }}
                            </th>
                        @endforeach
                        <th class="item"  >Total Pemakaian</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($bahan as $r1_bahan)
                        @php
                            $id_bahan = $r1_bahan->id;
                            $nama_bahan = $r1_bahan->nama_bahan;
                            $total_pemakaian = 0;
                        @endphp

                        <tr class="service">>
                            <td class="tableitem"><p class="itemtext">{{ $no++ }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $nama_bahan }}</p></td>
                            @foreach($produksi as $r2_produksi)
                                @php
                                    $id_produksi = $r2_produksi->id;

                                    $qty_komposisi_bahan = \app\Models\mBahanProduksi
                                        ::where([
                                            'id_produksi'=>$id_produksi,
                                            'id_bahan'=>$id_bahan
                                        ])
                                        ->sum('qty_diperlukan');


                                    $qty_bahan_pengemas = \app\Models\mProgressProduksiPengemas
                                        ::where([
                                            'id_produksi'=>$id_produksi,
                                            'id_bahan'=>$id_bahan
                                        ])
                                        ->sum('qty');

                                    $total_qty = $qty_komposisi_bahan + $qty_bahan_pengemas;

                                    $total_pemakaian += $total_qty;

                                @endphp
                                <td class="tableitem"><p class="itemtext">{{ Main::format_number($total_qty) }}</p></td>
                            @endforeach
                            <td align="right" class="tableitem">
                                <p class="itemtext">
                                    {{ Main::format_number($total_pemakaian) }}
                                </p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>

        </div>
    </div>
</div>