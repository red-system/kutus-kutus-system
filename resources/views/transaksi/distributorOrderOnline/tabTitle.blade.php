<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link akses-distributor_order_baru {{ $tab == 'list' ? 'active':''  }}" href="{{ route('distributorOrderOnlinePage') }}">
                    <i class="la la-refresh"></i> Distributor Order Baru
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link akses-distributor_konfirmasi_order {{ $tab == 'konfirmasi' ? 'active':''  }}" href="{{ route('distributorOrderOnlineKonfirmasi') }}">
                    <i class="la la-check"></i> Distributor Konfirmasi Order
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link akses-distributor_order_selesai {{ $tab == 'done' ? 'active':''  }}" href="{{ route('distributorOrderOnlineDone') }}">
                    <i class="la la-history"></i> Distributor Order Selesai
                </a>
            </li>
        </ul>
    </div>
</div>