<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHutangLain extends Model
{
    protected $table = 'tb_hutang_lain';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_supplier',
        'master_id_kredit',
        'master_id_debet',
        'no_hutang_lain',
        'hl_jatuh_tempo',
        'hl_tanggal',
        'hl_amount',
        'hl_sisa_amount',
        'hl_keterangan',
        'hl_status',
        'master_id_akun_debet',
        'akun_debet',
        'ref_kode',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name'
    ];

    function supplier() {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    function master_kredit() {
        return $this->belongsTo(mAcMaster::class, 'master_id_kredit');
    }

    function master_debet() {
        return $this->belongsTo(mAcMaster::class, 'master_id_debet');
    }
}
