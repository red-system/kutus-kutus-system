<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;
use app\Models\mDistributor;

class UsernameDistributorUpdate implements Rule
{
    protected $username;
    protected $id_distributor;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_distributor)
    {
        $this->id_distributor = $id_distributor;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $username_first = mDistributor::select('username')->where('id', $this->id_distributor)->first()->username;
        $username = $value;
        $this->username = $value;

        if($username == $username_first) {
            return TRUE;
        } else {
            $checkSame = mDistributor
                ::where('username', $username)
                ->whereNotIn('username', array($username_first))
                ->count();
            if($checkSame > 0) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Username \"<strong>".$this->username."</strong>\" not available";
    }
}
