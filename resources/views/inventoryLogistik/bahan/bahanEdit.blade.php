<form action="" method="post" class="m-form form-send m-form--label-align-right">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Kode Bahan </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control m-input" name="kode_bahan" autofocus>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Nama Bahan </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control m-input" name="nama_bahan">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Kategori Bahan </label>
                                <div class="col-lg-9">
                                    <select class="form-control m-input  m-select2" name="id_kategori_bahan" style="width: 100% !important;">
                                        @foreach($kategoriBahan as $r)
                                            <option value="{{ $r->id }}">{{ $r->kategori_bahan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Harga </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control m-input touchspin-rp" name="harga" value="0">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Minimal Stok </label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control m-input touchspin-number" name="minimal_stok" value="0">
                                </div>
                            </div>
{{--                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Qty </label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control m-input touchspin-number" name="qty" value="0">
                                </div>
                            </div>--}}
{{--                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Lokasi </label>
                                <div class="col-lg-9">
                                    <select class="form-control m-input  m-select2" name="id_lokasi" style="width: 100% !important;">
                                        @foreach($lokasi as $r)
                                            <option value="{{ $r->id }}">{{ $r->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>--}}
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Satuan </label>
                                <div class="col-lg-9">
                                    <select class="form-control m-input  m-select2" name="id_satuan" style="width: 100% !important;">
                                        @foreach($satuan as $r)
                                            <option value="{{ $r->id }}">{{ $r->satuan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Supplier </label>
                                <div class="col-lg-9">
                                    <select class="form-control m-input  m-select2" name="id_supplier" style="width: 100% !important;">
                                        @foreach($supplier as $r)
                                            <option value="{{ $r->id }}">{{ $r->nama_supplier }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label">Keterangan </label>
                                <div class="col-lg-9">
                                    <textarea name="keterangan" class="form-control m-input" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>