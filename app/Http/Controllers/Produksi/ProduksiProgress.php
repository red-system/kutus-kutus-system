<?php

namespace app\Http\Controllers\produksi;

use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use app\Models\mArusStokProduk;
use app\Models\mPoBahanDetail;
use app\Models\mStokBahan;
use app\Models\mStokProduk;
use Illuminate\View\View;

use app\Helpers\Main;
use app\Helpers\hProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\mDistributor;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

use app\Models\mProduksi;
use app\Models\mLokasi;
use app\Models\mProduk;
use app\Models\mKomposisiProduk;
use app\Models\mPengemasProduk;
use app\Models\mBahan;
use app\Models\mDetailProduksi;
use app\Models\mProgressProduksi;
use app\Models\mArusStokBahan;
use app\Models\mProgressProduksiPengemas;
use app\Models\mBahanProduksi;

use DB;

class ProduksiProgress extends Controller
{

    /**
     * Method insert ada di comment untuk :
     * - karena hasil progress produksi bisa melebihi rencana qty produksi yang diinputkan
     */
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['produksi_1'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['produksi'],
                'route' => ''
            ],
            [
                'label' => $cons['produksi_1'],
                'route' => ''
            ]
        ];
    }

    function index($id_produksi)
    {
        $breadcrumb = [
            [
                'label' => 'Progress',
                'route' => ''
            ]
        ];
        $id_produksi = Main::decrypt($id_produksi);
        $produksi = mProduksi::find($id_produksi);

        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $dataMain = Main::data($breadcrumb, $this->menuActive);
        $dataMain['pageTitle'] = $produksi->kode_produksi;
        //$list = mProgressProduksi::with('produ')->where('id_produksi', $id_produksi)->orderBy('id','DESC')->get();
        $list = mDetailProduksi
            ::with([
                'produk.pengemas_produk.bahan.satuan',
                'produk.pengemas_produk.bahan.stok_bahan.lokasi'
            ])
            ->where('id_produksi', $id_produksi)
            ->get();
        $gudang = mLokasi::where('tipe', 'gudang')->get(['id', 'lokasi']);
        $ketentuan_no_seri_produksi = '
            <strong>PERHATIAN !</strong>
            <br />
            <p align="justify">Jika inputan Nomer Seri Produk dikosongkan,
            Nomer Seri Produk akan di generate otomatis.</p>';

        $data = [
            'produksi' => $produksi,
            'list' => $list,
            'id_produksi' => $id_produksi,
            'tab' => 'progress',
            'gudang' => $gudang,
            'ketentuan_no_seri_produksi' => $ketentuan_no_seri_produksi,
        ];

        $data = array_merge($data, $dataMain);

        return view('produksi/produksiProgress/produksiProgress', $data);
    }

    function history($id_produksi)
    {
        $breadcrumb = [
            [
                'label' => 'Progress History',
                'route' => ''
            ]
        ];
        $id_produksi = Main::decrypt($id_produksi);
        $produksi = mProduksi::find($id_produksi);

        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $dataMain = Main::data($breadcrumb, $this->menuActive);
        $pageTitle = $produksi->kode_produksi;
        $gudang = mLokasi::where('tipe', 'gudang')->get(['id', 'lokasi']);
        $message = 'Yakin data dihapus ? <br />Data akan berpengaruh ke Akunting';
        $total_progress = 0;

        $count_start = mProgressProduksi::where('id_produksi', $id_produksi)->count();
        $tgl_start = date('Y-m-d');
        $tgl_end = date('Y-m-d');
        if ($count_start > 0) {
            $tgl_start = mProgressProduksi
                ::orderBy('tgl_selesai', 'ASC')
                ->where('id_produksi', $id_produksi)
                ->first(['tgl_selesai'])
                ->tgl_selesai;
            $tgl_end = mProgressProduksi
                ::orderBy('tgl_selesai', 'DESC')
                ->where('id_produksi', $id_produksi)
                ->first(['tgl_selesai'])
                ->tgl_selesai;
        }

        $tgl_start = isset($_GET['tgl_start']) ? $_GET['tgl_start'] : date('d-m-Y', strtotime($tgl_start));
        $tgl_start_db = date('Y-m-d', strtotime($tgl_start));
        $tgl_end = isset($_GET['tgl_end']) ? $_GET['tgl_end'] : date('d-m-Y', strtotime($tgl_end));
        $tgl_end_db = date('Y-m-d', strtotime($tgl_end));

        $id_lokasi = isset($_GET['id_lokasi']) ? $_GET['id_lokasi'] : 'all';


        $list = mProgressProduksi
            ::with(['produk:id,kode_produk,nama_produk', 'lokasi:id,lokasi', 'stok_produk'])
            ->where('id_produksi', $id_produksi);
        if ($id_lokasi != 'all') {
            $list = $list->where('id_lokasi', $id_lokasi);
        }
        $list = $list->whereBetween('tgl_selesai', [$tgl_start_db, $tgl_end_db])
            ->orderBy('id', 'DESC')
            ->get();

        $data = [
            'produksi' => $produksi,
            'list' => $list,
            'id_produksi' => $id_produksi,
            'id_lokasi' => $id_lokasi,
            'tab' => 'history',
            'tgl_start' => $tgl_start,
            'tgl_end' => $tgl_end,
            'pageTitle' => $pageTitle,
            'gudang' => $gudang,
            'message' => $message,
            'total_progress' => $total_progress,
        ];

        $data = array_merge($dataMain, $data);

        return view('produksi/produksiProgress/produksiProgressHistory', $data);


    }

    function detail($id_progress_produksi)
    {
        $id_produksi = mProgressProduksi::where('id', $id_progress_produksi)->value('id_produksi');
        $produksi = mProduksi::with('lokasi')->where('id', $id_produksi)->first();
        $detail_produksi = mDetailProduksi::with('produk')->where('id_produksi', $id_produksi)->orderBy('id', 'ASC')->get();
        $bahan_produksi = mBahanProduksi::with('bahan')->where('id_produksi', $id_produksi)->OrderBy('id', 'ASC')->get();
        $progress_produksi_pengemas = mProgressProduksiPengemas
            ::select('id_bahan')
            ->with([
                'bahan:id,nama_bahan,kode_bahan',
            ])
            ->where('id_progress_produksi', $id_progress_produksi)
            ->orderBy('id', 'ASC')
            ->groupBy('id_bahan')
            ->get();

        $data = [
            'id_produksi' => $id_produksi,
            'id_progress_produksi' => $id_progress_produksi,
            'produksi' => $produksi,
            'detail_produksi' => $detail_produksi,
            'bahan_produksi' => $bahan_produksi,
            'progress_produksi_pengemas' => $progress_produksi_pengemas,
        ];

        return view('produksi/produksiProgress/produksiDetailProgressTable', $data);
    }

    function insert(Request $request, $id_produksi)
    {
        $rules = [
            'qty_progress' => 'required',
            'qty_progress.*' => 'required',
            'hpp' => 'required',
            'hpp.*' => 'required',
            'id_lokasi' => 'required',
            'id_lokasi.*' => 'required'
        ];

        $attributes = [
            'qty_progress' => 'Qty Hasil',
            'hpp' => 'HPP',
            'id_lokasi' => 'Gudang',
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['qty_progress.' . $i] = 'QTY Hasil';
            $attr['hpp.' . $i] = 'HPP';
            $attr['id_lokasi.' . $i] = 'Gudang';
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $total_stok_arr = $request->input('total_stok');
        $qty_bahan_digunakan_arr = $request->input('qty_bahan_digunakan');
        $bahan_status_kelebihan = FALSE;

        if ($total_stok_arr) {
            foreach ($total_stok_arr as $index => $total_stok) {
                $qty_bahan_digunakan = $qty_bahan_digunakan_arr[$index];
                if ($qty_bahan_digunakan > $total_stok) {
                    $bahan_status_kelebihan = TRUE;
                }
            }

            if ($bahan_status_kelebihan) {
                return response([
                    'message' => 'Total bahan digunakan, <strong>Kurang Dari</strong> Stok bahan tersedia'
                ], 422);
            }
        }


        DB::beginTransaction();
        try {

            $id_detail_produksi_arr = $request->id_detail_produksi;
            $id_produk = $request->id_produk;
            $id_lokasi = $request->id_lokasi;
            $tgl_selesai = $request->tgl_selesai;
            $qty_produksi = $request->qty_produksi;
            $produksi = mProduksi::find($id_produksi);
            $hpp = $request->hpp;
            $qty_selesai = $request->qty_selesai;
            $qty_progress = $request->qty_progress;
            $total_qty_progress = 0;
            $no_seri_produk = $request->no_seri_produk;
            $deskripsi = $request->deskripsi;
            $qty_bahan_pengemas_arr = $request->input('qty_bahan_pengemas');
            $valid = TRUE;
            $month = date('m');
            $year = date('y');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $persediaan_bahan_baku_pengemas_nominal = $qty_bahan_pengemas_arr ? hProduk::persediaan_bahan_baku_pengemas_nominal($qty_bahan_pengemas_arr) : 0;
            $persediaan_bahan_baku_produksi_nominal = hProduk::persediaan_bahan_baku_produksi_nominal($id_produksi);

            //return response($persediaan_bahan_baku_pengemas_nominal, 422);

            $total_hpp = 0;

            /**
             * Update produksi menjadi finish
             */
            mProduksi::where('id', $id_produksi)->update(['status' => 'finish']);
            $kode_produksi = mProduksi::where('id', $id_produksi)->value('kode_produksi');

            /**
             * Memasukkan produk ke dalam stok produk
             */
            foreach ($id_detail_produksi_arr as $id_detail_produksi) {

                $_id_produk = $id_produk[$id_detail_produksi];
                $_id_lokasi = $id_lokasi[$id_detail_produksi];
                $_tgl_selesai = $tgl_selesai[$id_detail_produksi];
                $_deskripsi = $deskripsi[$id_detail_produksi];
                $_qty_produksi = $qty_produksi[$id_detail_produksi];
                $_qty_progress = $qty_progress[$id_detail_produksi];
                $_hpp = Main::format_number_db($hpp[$id_detail_produksi]);
                $total_qty_progress += $_qty_progress;

                $total_hpp += $_hpp * $_qty_progress;

                /**
                 * Insert Progress Produksi
                 */
                $data_insert = [
                    'id_produksi' => $id_produksi,
                    'id_detail_produksi' => $id_detail_produksi,
                    'id_produk' => $_id_produk,
                    'id_lokasi' => $_id_lokasi,
                    'tgl_selesai' => date('Y-m-d', strtotime($_tgl_selesai)) . ' ' . date('H:i:s'),
                    'deskripsi' => $_deskripsi,
                    'qty_produksi' => $_qty_produksi,
                    'qty_progress' => $_qty_progress
                ];
                $response = mProgressProduksi::create($data_insert);
                $id_progress_produksi = $response->id;

                /**
                 * Insert stok produksi
                 */

                $urutan = hProduk::urutan_produk($month, $year, $_id_produk, $_id_lokasi);
                $no_seri_produk = $no_seri_produk[$id_detail_produksi] ? $no_seri_produk[$id_detail_produksi] : hProduk::no_seri_produk($month, $year, $_id_produk, $_id_lokasi, $id_produksi);

                $data_insert = [
                    'id_produk' => $_id_produk,
                    'id_lokasi' => $_id_lokasi,
                    'id_produksi' => $id_produksi,
                    'id_progress_produksi' => $id_progress_produksi,
                    'month' => $month,
                    'year' => $year,
                    'urutan' => $urutan,
                    'no_seri_produk' => $no_seri_produk,
                    'qty' => $_qty_progress,
                    'hpp' => $_hpp
                ];

                $response = mStokProduk::create($data_insert);
                $id_stok_produk = $response->id;


                if ($_qty_progress > 0) {
                    /**
                     * Insert Arus Stok Produk
                     */

                    $last_stok = mStokProduk::where('id_produk', $_id_produk)->sum('qty');
                    $last_stok_total = mStokProduk::sum('qty');

                    $data_arus_stok_produk = [
                        'tgl' => date('Y-m-d H:i:s'),
                        'table_id' => $id_progress_produksi,
                        'table_name' => 'tb_progress_produksi',
                        'id_produksi' => $id_produksi,
                        'id_progress_produksi' => $id_progress_produksi,
                        'id_stok_produk' => $id_stok_produk,
                        'id_produk' => $_id_produk,
                        'stok_in' => $_qty_progress,
                        'stok_out' => 0,
                        'last_stok' => $last_stok,
                        'last_stok_total' => $last_stok_total,
                        'keterangan' => 'Hasil produksi produk',
                        'method' => 'insert'
                    ];

                    mArusStokProduk::create($data_arus_stok_produk);
                }
            }


            /**
             * Mengurangi Stok Bahan pengemas sesuai jumlah progress produksi
             * Jika produk terdapat bahan pengemas
             */

            if ($qty_bahan_pengemas_arr) {

                foreach ($qty_bahan_pengemas_arr as $qty_pengemas_produk) {
                    $id_stok_bahan = $qty_pengemas_produk['id_stok_bahan'];
                    $id_bahan = mStokBahan::where('id', $id_stok_bahan)->value('id_bahan');
                    $qty = $qty_pengemas_produk['qty'];
                    $id_detail_produksi = $qty_pengemas_produk['id_detail_produksi'];

                    //return response([$id_stok_bahan, $id_bahan, $qty, $id_detail_produksi], 422);

                    $data_pengemas = [
                        'id_produksi' => $id_produksi,
                        'id_progress_produksi' => $id_progress_produksi,
                        'id_detail_produksi' => $id_detail_produksi,
                        'id_stok_bahan' => $id_stok_bahan,
                        'id_bahan' => $id_bahan,
                        'qty' => $qty
                    ];

                    mProgressProduksiPengemas::create($data_pengemas);

                    /**
                     * Karena ada qty pengemas produk yang kosong
                     */
                    if ($qty_pengemas_produk > 0) {

                        $id_bahan = mStokBahan::where('id', $id_stok_bahan)->value('id_bahan');
                        $qty_stok_bahan_now = mStokBahan::where('id', $id_stok_bahan)->value('qty');
                        $last_stok = mStokBahan::where('id_bahan', $id_bahan)->sum('qty');
                        $qty_update = $qty_stok_bahan_now - $qty;
                        $data_update = [
                            'qty' => $qty_update
                        ];
                        mStokBahan::where('id', $id_stok_bahan)->update($data_update);

                        /**
                         * Untuk memasukkan arus stok bahan keluar
                         */
                        //$last_stok = $qty;
                        $last_stok_total = mStokBahan::sum('qty');
                        $stok_out = $qty;
                        $data_arus_bahan = [
                            'tgl' => $this->datetime,
                            'table_id' => $id_produksi,
                            'table_name' => 'tb_produksi',
                            'id_stok_bahan' => $id_stok_bahan,
                            'id_bahan' => $id_bahan,
                            'stok_in' => 0,
                            'stok_out' => $stok_out,
                            'last_stok' => $last_stok,
                            'last_stok_total' => $last_stok_total,
                            'keterangan' => 'Digunakan untuk progress produk ' . $kode_produksi,
                            'method' => 'insert'
                        ];

                        mArusStokBahan::create($data_arus_bahan);

                    }
                }
            }


            /**
             * Begin Akunting
             */
            $jmu_keterangan = 'Progress Produksi ' . $produksi->kode_produksi . ' berjumlah ' . Main::format_number($total_qty_progress);

            $data_jurnal = [
                'id_produksi' => $id_produksi,
                'no_invoice' => $produksi->kode_produksi,
                'id_progress_produksi' => $id_progress_produksi,
                'jmu_keterangan' => $jmu_keterangan,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            $master_id_debet_arr = [52]; // Persediaan Barang Jadi
            $master_id_kredit_arr = [
                194, // Produk Dalam Proses
                191, // Persediaan Bahan Baku

            ];

            /**
             * Transaksi Debet
             */
            foreach ($master_id_debet_arr as $index => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_debet = 0;

                switch ($master_id) {
                    case 52:
                        $trs_debet = $persediaan_bahan_baku_pengemas_nominal + $persediaan_bahan_baku_produksi_nominal;
                        break;
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => '',
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                    'tgl_transaksi' => $this->datetime
                ];

                mAcTransaksi::create($data_transaksi);
            }

            /**
             * Transaksi Kredit
             */
            foreach ($master_id_kredit_arr as $index => $master_id) {
                $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_kredit = 0;

                switch ($master_id) {
                    case 191: // Persedian Bahan Baku
                        $trs_kredit = $persediaan_bahan_baku_pengemas_nominal;
                        break;
                    case 194: // Produk Dalam Proses
                        $trs_kredit = $persediaan_bahan_baku_produksi_nominal;
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet' => 0,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => '',
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                    'tgl_transaksi' => $this->datetime
                ];

                mAcTransaksi::create($data_transaksi);
            }


            DB::commit();

            //return response(['produksi'=>$qty_produksi, 'progress'=>$qty_progress], 422);


        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }


    }

    function delete($id_progress_produksi)
    {
        DB::beginTransaction();
        try {

            $id_produksi = mProgressProduksi
                ::where('id', $id_progress_produksi)
                ->value('id_produksi');
            $jurnal_umum_id = mAcJurnalUmum
                ::where('id_progress_produksi', $id_progress_produksi)
                ->value('jurnal_umum_id');

            /**
             * Pengembalian bahan pengemas ke dalam stok
             */

            $progress_produksi_pengemas = mProgressProduksiPengemas
                ::where('id_progress_produksi', $id_progress_produksi)
                ->get();

            foreach ($progress_produksi_pengemas as $r) {
                $id_stok_bahan = $r->id_stok_bahan;
                $qty_pengemas = $r->qty;
                $qty_now = mStokBahan::where('id', $id_stok_bahan)->value('qty');
                $qty_after = $qty_now + $qty_pengemas;
                $data_update = [
                    'qty' => $qty_after
                ];

                mStokBahan::where('id', $id_stok_bahan)->update($data_update);

                /**
                 * Delete Arus stok bahan, karena bahan sudah dikembalikan ke stok bahan / gudang
                 */
                mArusStokBahan::where('id_stok_bahan', $id_stok_bahan)->delete();
            }


            /**
             * Delete Data
             */
            mStokProduk::where('id_progress_produksi', $id_progress_produksi)->delete();
            mProgressProduksi
                ::where('id', $id_progress_produksi)
                ->delete();
            mAcTransaksi
                ::where('jurnal_umum_id', $jurnal_umum_id)
                ->delete();
            mAcJurnalUmum
                ::where('jurnal_umum_id', $jurnal_umum_id)
                ->delete();
            mProgressProduksiPengemas
                ::where('id_progress_produksi', $id_progress_produksi)
                ->delete();

            /**
             * Change status produksi
             */
            $count_progress = mProgressProduksi
                ::where('id_produksi', $id_produksi)
                ->count();
            if ($count_progress == 0) {
                $data_update = ['status' => 'progress'];
                mProduksi::where('id', $id_produksi)->update($data_update);
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }

    /**
     * Untuk menghitung HPP produk saat progress produksi, yang akan digunakan pada akunting : jurnal umum dan transaksi
     * yang dihitung adalah HPP Bahan Produk & HPP Bahan Pengemas
     *
     * @param Request $request
     * @return float|int|string
     */
    function produk_hpp(Request $request)
    {
        $id_produk = $request->input('id_produk');
        $id_produksi = $request->input('id_produksi');
        $qty_progress = $request->input('qty_progress');
        $bahan_pengemas = $request->input('bahan_pengemas');
        $qty_produksi = mDetailProduksi
            ::where([
                'id_produksi' => $id_produksi,
                'id_produk' => $id_produk
            ])
            ->value('qty');
        $hpp_bahan_produk = [];
        $hpp_bahan_pengemas = [];

        /**
         * 1. Menghitung HPP Bahan Produk
         * - Mencari HPP dari setiap bahan yang ada di komposisi produk
         * - Mengecheck terlebih dahulu, apakah bahan sudah di PO sebelumnya, jika tidak ada,
         *   maka HPP bahan diambil dari HPP Bahan langsung di Menu Bahan
         *   Jika ada, maka HPP Bahan akan dihitung dari rata-rata 3 PO sebelumnya yang sudah ada,
         * - Lalu dikalikan dengan Qty Komposisi dan Qty Produksi
         * - Menghasilkan array HPP dari setiap bahan
         */
        $komposisi_produk = mKomposisiProduk::where('id_produk', $id_produk)->get();
        foreach ($komposisi_produk as $r) {
            $id_bahan = $r->id_bahan;
            $qty_komposisi = $r->qty;
            $hpp_bahan_now = mBahan::where('id', $id_bahan)->value('harga');

            $hpp_bahan_produk[$id_bahan] = ($hpp_bahan_now * $qty_komposisi) * $qty_produksi;
        }

        /**
         * 2. Menghitung HPP Bahan Pengemas Produk
         * - Mencari HPP dari setiap bahan yang ada di pengemas produk
         * - Mengecheck terlebih dahulu, apakah bahan sudah ada di PO sebelumnya, jika tidak ada,
         *   maka HPP bahan diambil dari HPP Bahan langsung di Menu Bahan
         *   Jika ada, maka HPP Bahan akan dibutng dari rata-rata 3 PO sebelumnya yang sudah ada,
         * - Lalu dikalikan dengan Qty Komposisi
         * - Menghasilkan array HPP dari setiap bahan
         */
        //$pengemas_produk = mPengemasProduk::where('id_produk', $id_produk)->get();
        if ($bahan_pengemas) {
            foreach ($bahan_pengemas as $r) {
                $id_bahan = $r['id_bahan'];
                $qty_pengemas = $r['qty_pengemas'];
                $hpp_bahan_now = mBahan::where('id', $id_bahan)->value('harga');

                $hpp_bahan_pengemas[$id_bahan] = ($hpp_bahan_now * $qty_pengemas);
            }
        } else {
            $hpp_bahan_pengemas = [0];
        }

        /**
         * Sum total bahan produk dan bahan pengemas produk
         */
        $hpp_bahan_produk_total = array_sum($hpp_bahan_produk);
        $hpp_pengemas_produk_total = array_sum($hpp_bahan_pengemas);

        /**
         * Kalkulasi HPP produk
         */
        $hpp_produk = ($hpp_bahan_produk_total + $hpp_pengemas_produk_total) / $qty_progress;
        $hpp_produk = Main::format_number_system($hpp_produk);

        return $hpp_produk;
    }


    function bahan_pengemas(Request $request)
    {
        $id_detail_produksi = $request->input('id_detail_produksi');
        $detail_produksi = mDetailProduksi::where('id', $id_detail_produksi)->first(['id_produk', 'id_produksi']);
        $id_produk = $detail_produksi['id_produk'];
        $id_produksi = $detail_produksi['id_produksi'];
        $id_bahan = $request->input('id_bahan');
        $qty_digunakan = $request->input('qty_digunakan');

        $stok_bahan = mStokBahan
            ::with('lokasi')
            ->where('id_bahan', $id_bahan)
            ->get();

        $data = [
            'stok_bahan' => $stok_bahan,
            'qty_digunakan' => $qty_digunakan,
            'id_detail_produksi' => $id_detail_produksi,
            'id_produk' => $id_produk,
            'id_produksi' => $id_produksi
        ];

        return view('produksi/produksiProgress/tablePengemasProdukQty', $data);


    }

}


