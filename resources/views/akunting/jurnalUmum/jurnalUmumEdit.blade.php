@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/jurnal_umum.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('akunting.jurnalUmum.trTransaksi')

    <form method="post"
          action="{{ route('jurnalUmumUpdate', ['jurnal_umum_id'=>Main::encrypt($jurnal_umum->jurnal_umum_id)]) }}"
          class="form-send"
          data-alert-show="true"
          data-alert-field-message="true"
          data-redirect="{{ route('jurnalUmumPage') }}"
          style="width: 100%">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            {{ csrf_field() }}

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12 required">Tanggal Transaksi</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <input type="text"
                                       class="form-control m-input m_datepicker"
                                       name="jmu_tanggal"
                                       autocomplete="off"
                                       value="{{ Main::format_date($jurnal_umum->jmu_tanggal) }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12 required">No Invoice</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <input type="text" name="no_invoice" class="form-control"
                                       value="{{ $jurnal_umum->no_invoice }}" readonly>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12 required">Keterangan</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <textarea class="form-control"
                                          name="jmu_keterangan">{{ $jurnal_umum->jmu_keterangan }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-transaksi btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Data Transaksi
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table table-bordered table-striped table-hover table-transaksi">
                            <thead>
                            <tr>
                                <th>Kode Perkiraan</th>
                                <th>Jenis Transaksi</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>Tipe Arus Kas</th>
                                <th>Catatan</th>
                                <th>Menu</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transaksi as $r_transaksi)

                                @php
                                    $kode_perkiraan_option_edit = \app\Helpers\hAkunting::kode_perkiraan_select_edit($r_transaksi->master_id);
                                    $total_debet += $r_transaksi->trs_debet;
                                    $total_kredit += $r_transaksi->trs_kredit;
                                @endphp
                                <tr>
                                    <td class="m--hide data">

                                    </td>
                                    <td>
                                        <select class="m-select2" name="master_id[]" style="width:100%;">
                                            <option value="">Pilih Kode Perkiraan</option>
                                            {!! $kode_perkiraan_option_edit !!}
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control jenis_transaksi" name="trs_jenis_transaksi[]">
                                            <option value="debet" {{ $r_transaksi->trs_jenis_transaksi == 'debet' ? 'selected': '' }}>
                                                Debet
                                            </option>
                                            <option value="kredit" {{ $r_transaksi->trs_jenis_transaksi == 'kredit' ? 'selected': '' }}>
                                                Kredit
                                            </option>
                                        </select>
                                    </td>
                                    <td class="trs_debet">
                                        <input type="text" class="form-control touchspin-number-decimal trs-debet"
                                               name="trs_debet[]"
                                               value="{{ $r_transaksi->trs_debet }}" {{ $r_transaksi->trs_jenis_transaksi == 'kredit' ? 'readonly':'' }}>
                                    </td>
                                    <td class="trs_kredit">
                                        <input type="text" class="form-control touchspin-number-decimal trs-kredit"
                                               name="trs_kredit[]"
                                               value="{{ $r_transaksi->trs_kredit }}" {{ $r_transaksi->trs_jenis_transaksi == 'debet' ? 'readonly':'' }}>
                                    </td>
                                    <td>
                                        <select name="tipe_arus_kas[]" class="form-control">
                                            <option value="Operasi" {{ $r_transaksi->trs_tipe_arus_kas == 'Operasi' ? 'selected': '' }}>
                                                Operasi
                                            </option>
                                            <option value="Pendanaan" {{ $r_transaksi->trs_tipe_arus_kas == 'Pendanaan' ? 'selected': '' }}>
                                                Pendanaan
                                            </option>
                                            <option value="Investasi" {{ $r_transaksi->trs_tipe_arus_kas == 'Investasi' ? 'selected': '' }}>
                                                Investasi
                                            </option>
                                        </select>
                                    </td>
                                    <td>
                                        <textarea class="form-control"
                                                  name="trs_catatan[]">{{ $r_transaksi->trs_catatan }}</textarea>
                                    </td>
                                    <td>
                                        <button type="button"
                                                class="btn-delete-row-transaksi btn m-btn--pill btn-danger btn-sm"
                                                data-confirm="false">
                                            <i class="la la-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="2" class="font-weight-bold text-center">
                                    STATUS
                                </th>
                                <th colspan="2" class="text-center th-status-label">
                                    @if($total_debet == $total_kredit)
                                        {!! $labelBalance !!}
                                    @else
                                        {!! $labelBalanceNot !!}
                                    @endif
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="target-produksi-buttons">
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Perbarui Jurnal Umum</span>
                </span>
            </button>
            <a href="{{ route("jurnalUmumPage") }}"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>
@endsection