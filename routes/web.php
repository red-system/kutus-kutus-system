<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;

/*Route::get('/bring-back-bahan', function () {
    $bahan_produksi = \app\Models\mBahanProduksi::where('id_produksi', 1)->get(['gudang_qty']);
    foreach ($bahan_produksi as $r) {
        $gudang_qty = json_decode($r->gudang_qty, TRUE);
        foreach($gudang_qty as $id_stok_bahan => $stok) {
            $qty_sekarang = \app\Models\mStokBahan::where('id', $id_stok_bahan)->value('qty');
            $qty_back = $qty_sekarang + $stok;

            //\app\Models\mStokBahan::where('id', $id_stok_bahan)->update(['qty'=>$qty_back]);

            //echo $id_stok_bahan.' '.$qty_sekarang.'+'.$stok.'='.$qty_back.'<br />';
        }
        echo '<br />';
    }
});*/


Route::get('/error-test', function() {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function() {
    Artisan::call('down');
});

Route::get('/production', function() {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::namespace('Akunting')->group(function () {
    Route::get('/cron-penyusutan-asset', "PenyusutanAsset@cron_penyusutan_asset");
});


Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
//        Route::get('/', function() {
//            return '<h1 align="center">Kutus Kutus System sudah pindah ke Server Pabrik,<br /> Harap Hubungi Team IT Kutus Kutus</h1>';
//        });
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin:administrator|distributor', 'prefix' => 'admin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');
        Route::post('/profile/distributor', "Profile@update_distributor")->name('profilDistributorUpdate');
    });
});


// ====================== Area Distributor ======================= //
Route::group(['namespace' => 'Transaksi', 'middleware' => 'authLogin:distributor', 'prefix' => 'distributor'], function () {


    // Distributor Order Via Sistem
    Route::get('/order-online', 'OrderOnline@index')->name('orderOnlinePage');
    Route::get('/order-online/konfirmasi', 'OrderOnline@konfirmasi')->name('orderOnlineKonfirmasi');
    Route::get('/order-online/history', 'OrderOnline@history')->name('orderOnlineHistory');
    Route::get('/order-online/create', 'OrderOnline@create')->name('orderOnlineCreate');
    Route::get('/order-online/edit/{id}', 'OrderOnline@edit')->name('orderOnlineEdit');
    Route::post('/order-online/update/{id}', 'OrderOnline@update')->name('orderOnlineUpdate');
    Route::post('/order-online-produk-stok', 'OrderOnline@produk_stok')->name('orderOnlineProdukStok');
    Route::post('/order-online', 'OrderOnline@insert')->name('orderOnlineInsert');
    Route::get('/order-online/detail-baru/{id}', 'OrderOnline@detail_order_baru')->name('orderOnlineDetailBaru');
    Route::get('/order-online/detail-konfirmasi/{id}', 'OrderOnline@detail_order_konfirmasi')->name('orderOnlineDetailKonfirmasi');
    Route::post('/order-online/konfirmasi-distributor', 'OrderOnline@konfirm_distributor')->name('orderOnlineKonfirmDistributor');
    Route::get('/order-online/batal/{id}', 'OrderOnline@batal')->name('orderOnlineBatal');
});

Route::group(['namespace' => 'General', 'middleware' => 'authLogin:distributor', 'prefix' => 'distributor'], function () {

    // Dashboard Distributor
    Route::get('/piutang-pelanggan-detail/{id}', 'Dashboard@detail_piutang_pelanggan')->name('piutangPelangganDetail');
    Route::get('/piutang-lain-detail/{id}', 'Dashboard@detail_piutang_lain')->name('piutangLainDetail');
    Route::get('/history-belanja-detail/{id}', 'Dashboard@detail_history_belanja')->name('historyBelanjaDetail');

});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Kategori Produk
    Route::get('/kategori-produk', "KategoriProduk@index")->name('kategoriProdukPage');
    Route::post('/kategori-produk', "KategoriProduk@insert")->name('kategoriProdukInsert');
    Route::delete('/kategori-produk/{id}', "KategoriProduk@delete")->name('kategoriProdukDelete');
    Route::post('/kategori-produk/{id}', "KategoriProduk@update")->name('kategoriProdukUpdate');

    // Kategori Bahan
    Route::get('/kategori-bahan', "KategoriBahan@index")->name('kategoriBahanPage');
    Route::post('/kategori-bahan', "KategoriBahan@insert")->name('kategoriBahanInsert');
    Route::delete('/kategori-bahan/{id}', "KategoriBahan@delete")->name('kategoriBahanDelete');
    Route::post('/kategori-bahan/{id}', "KategoriBahan@update")->name('kategoriBahanUpdate');

    // Kategori Asset
    Route::get('/kategori-asset', "KategoriAsset@index")->name('kategoriAssetPage');
    Route::post('/kategori-asset', "KategoriAsset@insert")->name('kategoriAssetInsert');
    Route::delete('/kategori-asset/{id}', "KategoriAsset@delete")->name('kategoriAssetDelete');
    Route::post('/kategori-asset/{id}', "KategoriAsset@update")->name('kategoriAssetUpdate');

    // Satuan
    Route::get('/satuan', "Satuan@index")->name('satuanPage');
    Route::post('/satuan', "Satuan@insert")->name('satuanInsert');
    Route::delete('/satuan/{id}', "Satuan@delete")->name('satuanDelete');
    Route::post('/satuan/{id}', "Satuan@update")->name('satuanUpdate');

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Supplier
    Route::get('/supplier', "Supplier@index")->name('supplierPage');
    Route::post('/supplier', "Supplier@insert")->name('supplierInsert');
    Route::delete('/supplier/{id}', "Supplier@delete")->name('supplierDelete');
    Route::post('/supplier/{id}', "Supplier@update")->name('supplierUpdate');

    // Distributor
    Route::get('/distributor', "Distributor@index")->name('distributorPage');
    Route::post('/distributor', "Distributor@insert")->name('distributorInsert');
    Route::delete('/distributor/{id}', "Distributor@delete")->name('distributorDelete');
    Route::post('/distributor/{id}', "Distributor@update")->name('distributorUpdate');

    // Lokasi
    Route::get('/lokasi', "Lokasi@index")->name('lokasiPage');
    Route::post('/lokasi', "Lokasi@insert")->name('lokasiInsert');
    Route::delete('/lokasi/{id}', "Lokasi@delete")->name('lokasiDelete');
    Route::post('/lokasi/{id}', "Lokasi@update")->name('lokasiUpdate');

    // Ekspedisi
    Route::get('/ekspedisi', "Ekspedisi@index")->name('ekspedisiPage');
    Route::post('/ekspedisi', "Ekspedisi@insert")->name('ekspedisiInsert');
    Route::delete('/ekspedisi/{id}', "Ekspedisi@delete")->name('ekspedisiDelete');
    Route::post('/ekspedisi/{id}', "Ekspedisi@update")->name('ekspedisiUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    // ================== Inventory/Logistik ==================== //

});

// ========= Inventory/Logistik

Route::group(['namespace' => 'InventoryLogistik', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Bahan
    Route::get('/bahan', "Bahan@index")->name('bahanPage');
    Route::post('/bahan', "Bahan@insert")->name('bahanInsert');
    Route::delete('/bahan/{id}', "Bahan@delete")->name('bahanDelete');
    Route::post('/bahan/{id}', "Bahan@update")->name('bahanUpdate');

    // Stok Bahan
    Route::get('/stok-bahan/{idBahan?}', "StokBahan@index")->name('stokBahanPage');
    Route::post('/stok-bahan/{idBahan}', "StokBahan@insert")->name('stokBahanInsert');
    Route::delete('/stok-bahan/{idStok}', "StokBahan@delete")->name('stokBahanDelete');
    Route::post('/stok-bahan/update/{idStok}', "StokBahan@update")->name('stokBahanUpdate');

    // Produk
    Route::get('/produk', "Produk@index")->name('produkPage');
    Route::post('/produk', "Produk@insert")->name('produkInsert');
    Route::delete('/produk/{id}', "Produk@delete")->name('produkDelete');
    Route::post('/produk/{id}', "Produk@update")->name('produkUpdate');
    Route::get('/harga-produk/{idProduk?}', "Produk@harga")->name('hargaProdukPage');
    Route::post('/harga-produk/{idProduk}', "Produk@harga_insert")->name('hargaProdukInsert');

    // Stok Produk
    Route::get('/stok-produk/{idProduk?}', "StokProduk@index")->name('stokPage');
    Route::post('/stok-produk/{idProduk}', "StokProduk@insert")->name('stokInsert');
    Route::delete('/stok-produk/{idStok}', "StokProduk@delete")->name('stokDelete');
    Route::post('/stok-produk/update/{idStok}', "StokProduk@update")->name('stokUpdate');

    // Komposisi Produk
    Route::get('/komposisi-produk', "KomposisiProduk@index")->name('komposisiProdukPage');
    Route::get('/komposisi-produk/{idProduk}', "KomposisiProduk@indexKomposisi")->name('komposisiDaftarPage');
    Route::post('/komposisi-produk', "KomposisiProduk@insert")->name('komposisiInsert');
    Route::delete('/komposisi-produk/{id}', "KomposisiProduk@delete")->name('komposisiDelete');
    Route::post('/komposisi-produk/{id}', "KomposisiProduk@update")->name('komposisiUpdate');

    // Komposisi Pengemas Produk
    Route::get('/komposisi-pengemas-produk/{idProduk}', "PengemasProduk@indexKomposisi")->name('pengemasProdukPage');
    Route::post('/komposisi-pengemas-produk', "PengemasProduk@insert")->name('pengemasProdukInsert');
    Route::delete('/komposisi-pengemas-produk/{id}', "PengemasProduk@delete")->name('pengemasProdukDelete');
    Route::post('/komposisi-pengemas-produk/{id}', "PengemasProduk@update")->name('pengemasProdukUpdate');

    // Transfer Stok Bahan
    Route::get('/transfer-stok-bahan', "TransferStokBahan@index")->name('transferStokBahanPage');
    Route::get('/transfer-stok-bahan/{idBahan}', "TransferStokBahan@list")->name('transferStokBahanDaftarPage');
    Route::post('/transfer-stok-bahan', "TransferStokBahan@insert")->name('transferStokBahanInsert');

    // History Transfer Stok Bahan
    Route::get('/history-transfer-stok-bahan', 'TransferStokBahan@history')->name('historyTransferStokBahanPage');
    Route::get('/history-transfer-stok-bahan/pdf', 'TransferStokBahan@history_pdf')->name('historyTransferStokBahanPdf');
    Route::get('/history-transfer-stok-bahan/excel', 'TransferStokBahan@history_excel')->name('historyTransferStokBahanExcel');

    // Transfer Stok Produk
    Route::get('/transfer-stok-produk', "TransferStokProduk@index")->name('transferStokProdukPage');
    Route::get('/transfer-stok-produk/{idProduk}', "TransferStokProduk@list")->name('transferStokProdukDaftarPage');
    Route::post('/transfer-stok-produk', "TransferStokProduk@insert")->name('transferStokProdukInsert');

    // History Transfer Stok Produk
    Route::get('/history-transfer-stok-produk', 'TransferStokProduk@history')->name('historyTransferStokProdukPage');
    Route::get('/history-transfer-stok-produk/pdf', 'TransferStokProduk@history_pdf')->name('historyTransferStokProdukPdf');
    Route::get('/history-transfer-stok-produk/excel', 'TransferStokProduk@history_excel')->name('historyTransferStokProdukExcel');

    // Kartu Stok Bahan
    Route::get('/kartu-stok-bahan', 'KartuStokBahan@index')->name('kartuStokBahanPage');
    Route::post('/kartu-stok-bahan/update', 'KartuStokBahan@update')->name('kartuStokBahanPageUpdate');
    Route::get('/kartu-stok-bahan/excel', 'KartuStokBahan@excel')->name('kartuStokBahanExcel');
    Route::get('/kartu-stok-bahan/pdf', 'KartuStokBahan@pdf')->name('kartuStokBahanPdf');

    // Kartu Stok Produk
    Route::get('/kartu-stok-produk', 'KartuStokProduk@index')->name('kartuStokProdukPage');
    Route::get('/kartu-stok-produk/excel', 'KartuStokProduk@excel')->name('kartuStokProdukExcel');
    Route::get('/kartu-stok-produk/pdf', 'KartuStokProduk@pdf')->name('kartuStokProdukPdf');

    // Penyesuaian Stok Bahan
    Route::get('/penyesuaian-stok-bahan', 'PenyesuaianStokBahan@index')->name('penyesuaianStokBahanPage');
    Route::get('/penyesuaian-stok-bahan/{idBahan}', 'PenyesuaianStokBahan@stok')->name('stokPenyesuaianBahan');
    Route::post('/penyesuaian-stok-bahan/{id}', 'PenyesuaianStokBahan@update')->name('penyesuaianStokBahanUpdate');

    // History Penyesuaian stok Bahan
    Route::get('/history-penyesuaian-stok-bahan', 'PenyesuaianStokBahan@history')->name('historyPenyesuaianStokBahanPage');
    Route::get('/history-penyesuaian-stok-bahan/pdf', 'PenyesuaianStokBahan@history_pdf')->name('historyPenyesuaianStokBahanPdf');
    Route::get('/history-penyesuaian-stok-bahan/excel', 'PenyesuaianStokBahan@history_excel')->name('historyPenyesuaianStokBahanExcel');

    // Penyesuaian Stok Produk
    Route::get('/penyesuaian-stok-produk', 'PenyesuaianStokProduk@index')->name('penyesuaianStokProdukPage');
    Route::get('/penyesuaian-stok-produk/{idProduk}', 'PenyesuaianStokProduk@stok')->name('stokPenyesuaianProduk');
    Route::post('/penyesuaian-stok-produk/{idstok}', 'PenyesuaianStokProduk@update')->name('penyesuaianStokProdukUpdate');

    // History Penyesuaian stok Produk
    Route::get('/history-penyesuaian-stok-produk', 'PenyesuaianStokProduk@history')->name('historyPenyesuaianStokProdukPage');
    Route::get('/history-penyesuaian-stok-produk/pdf', 'PenyesuaianStokProduk@history_pdf')->name('historyPenyesuaianStokProdukPdf');
    Route::get('/history-penyesuaian-stok-produk/excel', 'PenyesuaianStokProduk@history_excel')->name('historyPenyesuaianStokProdukExcel');

    // Stok Alert Bahan
    Route::get('/stok-alert-bahan', 'StokAlert@bahan')->name('stokAlertBahan');

    // Stok Alert Produk
    Route::get('/stok-alert-produk', 'StokAlert@produk')->name('stokAlertProduk');

});


// ========= Produksi

Route::group(['namespace' => 'Produksi', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {
    // Produksi
    Route::get('/produksi/total', "Produksi@total")->name('produksiTotal');
    Route::get('/produksi', "Produksi@index")->name('produksiPage');
    Route::get('/produksi/history', "Produksi@history")->name('produksiHistoryPage');
    Route::get('/produksi/tambah', "Produksi@create")->name('produksiCreatePage');
    Route::post('/produksi/no-seri-produk', "Produksi@no_seri_produk")->name('produksiNoSeriProduk');
    Route::post('/produksi/bahan', "Produksi@bahan")->name('produksiBahan');
    Route::post('/produksi', "Produksi@insert")->name('produksiInsert');
    Route::get('/produksi/detail/{id}', "Produksi@detail")->name('produksiDetail');

    Route::get('/produksi/edit/{id}', "Produksi@edit")->name('produksiEdit');
    Route::post('/produksi/update/{id}', "Produksi@update")->name('produksiUpdate');
    Route::post('/produksi/detail/delete', "Produksi@deleteDetailProduksi")->name('detailProduksiDelete');
    Route::delete('/produksi/delete/{id}', "Produksi@deleteProduksi")->name('produksiDelete');
    Route::get('/produksi/publish/{id}', "Produksi@publish")->name('produksiPublish');

    // Produksi Hasil
    Route::get('/produksi/hasil/{id}', "ProduksiProgress@index")->name('produksiProgress');
    Route::post('/produksi/hasil/insert/{id}', "ProduksiProgress@insert")->name('produksiProgressInsert');
    Route::get('/produksi/hasil/history/{id}', "ProduksiProgress@history")->name('produksiProgressHistory');
    Route::delete('/produksi/progress/delete/{id}', "ProduksiProgress@delete")->name('produksiProgressDelete');
    Route::post('/produksi/progress/hpp', "ProduksiProgress@produk_hpp")->name('produksiProgressHpp');
    Route::post('/produksi/progress/qty-bahan-pengemas', "ProduksiProgress@bahan_pengemas")->name('produksiProgressQtyPengemas');
    Route::get('/produksi/progress/detail/{id}', "ProduksiProgress@detail")->name('produksiProgressDetail');

    // Target Produksi
    Route::get('/target-produksi', 'TargetProduksi@index')->name('targetProduksiPage');
    Route::post('/target-produksi', 'TargetProduksi@insert')->name('targetProduksiInsert');
    Route::post('/target-produksi/one', 'TargetProduksi@insertOne')->name('targetProduksiInsertOne');
    Route::post('/target-produksi/month', 'TargetProduksi@insertMonth')->name('targetProduksiInsertMonth');
    Route::get('/target-produksi/tahun/{tahun}', 'TargetProduksi@tahun')->name('targetProduksiTahun');
    Route::delete('/target-produksi/{id}', "TargetProduksi@delete")->name('targetProduksiDelete');
    Route::post('/target-produksi/update/{id}', "TargetProduksi@update")->name('targetProduksiUpdate');

    // Realisasi Target Produksi
    Route::get('/realisasi-target-produksi', "RealisasiTargetProduksi@index")->name('realisasiTargetProduksiPage');
    Route::get('/realisasi-target-produksi/{tahun}', "RealisasiTargetProduksi@tahun")->name('realisasiTargetProduksiTahun');

    // Analisa dan Saran Produksi
    Route::get('/analisa-saran-produksi/perkiraan-jumlah-produksi', 'AnalisaSaranProduksi@perkiraanJumlahProduksi')->name('perkiraanJumlahProduksi');
    Route::get('/analisa-saran-produksi/jumlah-bahan-terpakai', 'AnalisaSaranProduksi@jumlahBahanTerpakai')->name('jumlahBahanTerpakai');
    Route::get('/analisa-saran-produksi/keperluan-bahan-paket', 'AnalisaSaranProduksi@keperluanBahanTarget')->name('keperluanBahanTarget');
    Route::get('/analisa-saran-produksi/keperluan-bahan-paket/{tahun}', 'AnalisaSaranProduksi@keperluanBahanTahun')->name('keperluanBahanTahun');
});

// ========= Transaksi

Route::group(['namespace' => 'Transaksi', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Penjualan Produk

    Route::get('/penjualan/total', 'PenjualanProduk@total')->name('penjualanTotal');

    Route::get('/penjualan-produk', 'PenjualanProduk@index')->name('penjualanProdukPage');
    Route::get('/penjualan-produk/create', 'PenjualanProduk@create')->name('penjualanProdukCreate');
    Route::post('/penjualan-produk/no-faktur', 'PenjualanProduk@no_faktur')->name('penjualanProdukNoFaktur');

    Route::post('/penjualan-produk', 'PenjualanProduk@insert')->name('penjualanProdukInsert');
    Route::get('/penjualan-produk/{id}', 'PenjualanProduk@edit')->name('penjualanProdukEdit');
    Route::post('/penjualan-produk/{id}', 'PenjualanProduk@update')->name('penjualanProdukUpdate');
    Route::delete('/penjualan-produk/{id}', 'PenjualanProduk@delete')->name('penjualanProdukDelete');
    Route::get('/penjualan-produk/faktur/{id}', 'PenjualanProduk@faktur')->name('penjualanProdukFaktur');

    Route::post('/penjualan-produk-gudang', 'PenjualanProduk@produk_gudang')->name('penjualanProdukGudang');
    Route::post('/penjualan-produk-stok', 'PenjualanProduk@produk_stok')->name('penjualanProdukStok');
    Route::post('/penjualan-no-seri-produk', 'PenjualanProduk@no_seri_produk')->name('penjualanNoSeriProduk');
    Route::post('/penjualan-produk-harga', 'PenjualanProduk@produk_harga')->name('penjualanProdukHarga');

    /*    // Penjualan Produk Wholesale
        Route::get('/penjualan-produk-wholesale', 'PenjualanProdukWholesale@index')->name('penjualanProdukWholesalePage'); */


    // Pemesanan Produk Distributor
    /*Route::get('/pemesanan-produk-distributor', 'PemesananProdukDistributor@index')->name('pemesananProdukDistributorPage');*/

    // PO Bahan ke Supplier
    Route::get('/pre-order-bahan-ke-supplier', 'POBahanSupplier@index')->name('poBahanSupplierPage');
    Route::get('/pre-order-bahan-ke-supplier/create', 'POBahanSupplier@create')->name('poBahanSupplierCreate');
    Route::post('/pre-order-bahan-ke-supplier', 'POBahanSupplier@insert')->name('poBahanSupplierInsert');
    Route::get('/pre-order-bahan-ke-supplier/{id}', 'POBahanSupplier@edit')->name('poBahanSupplierEdit');
    Route::post('/pre-order-bahan-ke-supplier/{id}', 'POBahanSupplier@update')->name('poBahanSupplierUpdate');
    Route::post('/pre-order-bahan-ke-supplier-no-faktur', 'POBahanSupplier@no_faktur')->name('poBahanSupplierNoFaktur');

    Route::get('/pre-order-bahan-ke-supplier/pembelian/{id}', 'POBahanSupplier@pembelian')->name('poBahanSupplierPembelian');
    Route::post('/pre-order-bahan-ke-supplier/pembelian/{id}', 'POBahanSupplier@pembelian_insert')->name('poBahanSupplierPembelianInsert');

    Route::post('/pre-order-bahan-ke-supplier-stok-bahan', 'POBahanSupplier@bahan_stok')->name('poBahanSupplierBahanStok');

    Route::post('/po-bahan-supplier/user-check', "POBahanSupplier@user_check")->name('poBahanSupplierUserCheck');
    Route::get('/po-bahan-supplier/detail/{id}', "POBahanSupplier@detail")->name('poBahanSupplierDetail');
    Route::get('/po-bahan-supplier/cetak/{id}', "POBahanSupplier@cetak")->name('poBahanSupplierCetak');

    Route::post('/po-bahan-supplier/bahan-datang', "POBahanSupplier@bahan_datang")->name('poBahanSupplierBahanDatang');

    Route::delete('/po-bahan-ke-supplier/delete/{id}', "POBahanSupplier@delete")->name('poBahanSupplierDelete');
    Route::delete('/po-bahan-ke-supplier/delete/force/{id}', "POBahanSupplier@delete_force")->name('poBahanSupplierDeleteForce');


    // Distributor Order Online
    Route::get('/distributor-order-online', "DistributorOrderOnline@index")->name('distributorOrderOnlinePage');
    Route::post('/distributor-order-online/qty-konfirm', "DistributorOrderOnline@qty_konfirm")->name('distributorOrderOnlineQtyKonfirm');
    Route::get('/distributor-order-online/konfirmasi', 'DistributorOrderOnline@konfirmasi')->name('distributorOrderOnlineKonfirmasi');
    Route::get('/distributor-order-online/done', 'DistributorOrderOnline@done')->name('distributorOrderOnlineDone');
    Route::get('/distributor-order-online/create', 'DistributorOrderOnline@create')->name('distributorOrderOnlinereate');
    Route::post('/distributor-order-online-produk-stok', 'DistributorOrderOnline@produk_stok')->name('distributorOrderOnlineProdukStok');
    Route::post('/distributor-order-online', 'DistributorOrderOnline@insert')->name('distributorOrderOnlineInsert');
    Route::get('/distributor-order-online/detail/{id}', 'DistributorOrderOnline@detail')->name('distributorOrderOnlineDetail');
    Route::get('/distributor-order-online/batal/{id}', 'DistributorOrderOnline@batal')->name('distributorOrderOnlineBatal');
    Route::get('/distributor-order-online/penjualan/{id}', 'DistributorOrderOnline@penjualan')->name('distributorOrderOnlinePenjualan');


    // Pembelian Bahan ke Supplier
    Route::get('/pembelian-bahan-ke-supplier', 'PembelianBahanSupplier@index')->name('pembelianBahanSupplierPage');

    // Biaya Operasional Kantor
    Route::get('/biaya-operasional-kantor', 'BiayaOperasionalKantor@index')->name('biayaOperasionalKantorPage');

    // Biaya Operasional Produksi
    Route::get('/biaya-operasional-produksi', 'BiayaOperasionalProduksi@index')->name('biayaOperasionalProduksiPage');
});

Route::group(['namespace' => 'Distribusi', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    Route::get('/distribusi', "Distribusi@index")->name('distribusiPage');
    Route::get('/distribusi/{id}', "Distribusi@distribusi")->name('distribusi');
    Route::post('/distribusi', "Distribusi@distribusi_insert")->name('distribusiInsert');
    Route::get('/distribusi/history/{id}', "Distribusi@history")->name('distribusiHistory');
    Route::get('/distribusi/cetak/{id}', "Distribusi@pdf")->name('distribusiCetak');
    Route::delete('/distribusi/{id}', "Distribusi@delete")->name('distribusiDelete');


    //Route::get('/distribusi-test', "Distribusi@test")->name('distribusiPage');

});

// ========= Hutang Piutang

Route::group(['namespace' => 'HutangPiutang', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Hutang ke Supplier
    Route::get('/hutang-ke-supplier', "HutangSupplier@index")->name('hutangSupplierPage');
    Route::post('/hutang-ke-supplier/{id}', "HutangSupplier@update")->name('hutangSupplierUpdate');
    Route::post('/hutang-ke-supplier/pembayaran/{id}', "HutangSupplier@pembayaran_insert")->name('hutangSupplierPembayaran');
    Route::delete('/hutang-ke-supplier/{id}', "HutangSupplier@delete")->name('hutangSupplierDelete');

    // Hutang Lain - Lain
    Route::get('/hutang-lain-lain', "HutangLain@index")->name('hutangLainPage');
    Route::post('/hutang-lain-lain', "HutangLain@insert")->name('hutangLainInsert');
    Route::post('/hutang-lain-lain/{id}', "HutangLain@update")->name('hutangLainUpdate');
    Route::get('/hutang-lain-lain/detail/{id}', "HutangLain@detail")->name('hutangLainDetail');
    Route::post('/hutang-lain-lain/pembayaran/{id}', "HutangLain@pembayaran_insert")->name('hutangLainPembayaran');
    Route::delete('/hutang-lain-lain/{id}', "HutangLain@delete")->name('hutangLainDelete');

    // Piutang Pelanggan
    Route::get('/piutang-pelanggan', "PiutangPelanggan@index")->name('piutangPelangganPage');
    Route::post('/piutang-pelanggan/pembayaran/{id}', "PiutangPelanggan@pembayaran_insert")->name('piutangPelangganPembayaran');
    Route::get('/piutang-pelanggan/pdf/{id}', "PiutangPelanggan@pdf")->name('piutangPelangganPdf');
    Route::delete('/piutang-pelanggan/{id}', "PiutangPelanggan@delete")->name('piutangPelangganDelete');

    Route::get('/piutang-data-recycle', "PiutangPelanggan@recycle");


    // Piutang Lain - Lain
    Route::get('/piutang-lain-lain', "PiutangLain@index")->name('piutangLainPage');
    Route::post('/piutang-lain-lain', "PiutangLain@insert")->name('piutangLainInsert');
    Route::get('/piutang-lain-lain/edit-kredit', "PiutangLain@edit_kredit")->name('piutangLainEditKredit');
    Route::post('/piutang-lain-lain/{id}', "PiutangLain@update")->name('piutangLainUpdate');
    Route::post('/piutang-lain-lain/pembayaran/{id}', "PiutangLain@pembayaran_insert")->name('piutangLainPembayaran');
    Route::delete('/piutang-lain-lain/{id}', "PiutangLain@delete")->name('piutangLainDelete');

    Route::get('/piutang-lain-recycle', "PiutangLain@recycle");

});


// ========= Laporan Operasional

Route::group(['namespace' => 'LaporanOperasional', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Laporan Penjualan Global
    Route::get('/laporan-penjualan-global', "LaporanPenjualanGlobal@index")->name('laporanPenjualanGlobalPage');
    Route::get('/laporan-penjualan-global/pdf', "LaporanPenjualanGlobal@pdf")->name('laporanPenjualanGlobalPdf');
    Route::get('/laporan-penjualan-global/excel', "LaporanPenjualanGlobal@excel")->name('laporanPenjualanGlobalExcel');

    // Laporan Penjualan Detail
    Route::get('/laporan-penjualan-detail', "LaporanPenjualanDetail@index")->name('laporanPenjualanDetailPage');
    Route::get('/laporan-penjualan-detail/pdf', "LaporanPenjualanDetail@pdf")->name('laporanPenjualanDetailPdf');
    Route::get('/laporan-penjualan-detail/excel', "LaporanPenjualanDetail@excel")->name('laporanPenjualanDetailExcel');

    // Laporan Produksi
    Route::get('/laporan-produksi', "LaporanProduksi@index")->name('laporanProduksiPage');
    Route::get('/laporan-produksi/pdf', "LaporanProduksi@pdf")->name('laporanProduksiPdf');
    Route::get('/laporan-produksi/excel', "LaporanProduksi@excel")->name('laporanProduksiExcel');

    // Laporan Penggunaan Bahan
    Route::get('/laporan-penggunaan-bahan', "LaporanPenggunaanBahan@index")->name('laporanPenggunaanBahanPage');
    Route::get('/laporan-penggunaan-bahan/pdf', "LaporanPenggunaanBahan@pdf")->name('laporanPenggunaanBahanPdf');
    Route::get('/laporan-penggunaan-bahan/excel', "LaporanPenggunaanBahan@excel")->name('laporanPenggunaanBahanExcel');

    // Laporan Pembelian Bahan
    Route::get('/laporan-pembelian-bahan', "LaporanPembelianBahan@index")->name('laporanPembelianBahanPage');
    Route::get('/laporan-pembelian-bahan/pdf', "LaporanPembelianBahan@pdf")->name('laporanPembelianBahanPdf');
    Route::get('/laporan-pembelian-bahan/excel', "LaporanPembelianBahan@excel")->name('laporanPembelianBahanExcel');

});

// ========= Akunting

Route::group(['namespace' => 'Akunting', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Kode Perkiraan
    Route::get('/kode-perkiraan', "KodePerkiraan@index")->name('perkiraanPage');
    Route::post('/kode-perkiraan', 'KodePerkiraan@insert')->name('perkiraanInsert');
    Route::get('/kode-perkiraan/{kode}', 'KodePerkiraan@edit')->name('perkiraanEdit');
    Route::post('/kode-perkiraan/{kode}', 'KodePerkiraan@update')->name('perkiraanUpdate');
    Route::delete('/kode-perkiraan/{kode}', 'KodePerkiraan@delete')->name('perkiraanDelete');

    Route::get('/kode-perkiraan-pdf', "KodePerkiraan@pdf")->name('perkiraanPdf');
    Route::get('/kode-perkiraan-excel', "KodePerkiraan@excel")->name('perkiraanExcel');

    // Jurnal Umum
    Route::get('/jurnal-umum', "JurnalUmum@index")->name('jurnalUmumPage');
    Route::get('/jurnal-umum/create', "JurnalUmum@create")->name('jurnalUmumCreate');
    Route::post('/jurnal-umum', "JurnalUmum@insert")->name('jurnalUmumInsert');
    Route::get('/jurnal-umum/{id}', "JurnalUmum@edit")->name('jurnalUmumEdit');
    Route::post('/jurnal-umum/{id}', "JurnalUmum@update")->name('jurnalUmumUpdate');
    Route::delete('/jurnal-umum/{id}', "JurnalUmum@delete")->name('jurnalUmumDelete');

    Route::get('/jurnal-umum-pdf', "JurnalUmum@pdf")->name('jurnalUmumPdf');
    Route::get('/jurnal-umum-excel', "JurnalUmum@excel")->name('jurnalUmumExcel');

    // Arus Kas
    Route::get('/arus-kas', "ArusKas@index")->name('arusKasPage');
    Route::get('/arus-kas/pdf', "ArusKas@pdf")->name('arusKasPdf');
    Route::get('/arus-kas/excel', "ArusKas@excel")->name('arusKasExcel');

    // Laba Rugi
    Route::get('/laba-rugi', "LabaRugi@index")->name('labaRugiPage');
    Route::get('/laba-rugi/pdf', "LabaRugi@pdf")->name('labaRugiPdf');
    Route::get('/laba-rugi/excel', "LabaRugi@excel")->name('labaRugiExcel');

    // Neraca
    Route::get('/neraca', "Neraca@index")->name('neracaPage');
    Route::get('/neraca/pdf', "Neraca@pdf")->name('neracaPdf');
    Route::get('/neraca/excel', "Neraca@excel")->name('neracaExcel');

    // Asset
    Route::get('/asset', "Asset@index")->name('assetPage');
    Route::get('/asset/create', "Asset@create")->name('assetCreate');
    Route::post('/asset', "Asset@insert")->name('assetInsert');
    Route::get('/asset/penyusutan/{id}', "Asset@penyusutan_form")->name('assetPenyusutanModal');
    Route::post('/asset/penyusutan', "Asset@penyusutan_insert")->name('assetPenyusutan');

    Route::get('/asset/{id}', "Asset@edit")->name('assetEdit');
    Route::post('/asset/{id}', "Asset@update")->name('assetUpdate');
    Route::delete('/asset/{id}', "Asset@delete")->name('assetDelete');

    // Penyusutan Asset
    Route::get('/penyusutan-asset', "PenyusutanAsset@index")->name('penyusutanAssetPage');
    Route::get('/penyusutan-asset/pdf', "PenyusutanAsset@pdf")->name('penyusutanAssetPdf');
    Route::get('/penyusutan-asset/excel', "PenyusutanAsset@excel")->name('penyusutanAssetExcel');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

Route::get('#')->name('#');

//Inventory/Logistik
//Route::get('/bahan', "Bahan");
