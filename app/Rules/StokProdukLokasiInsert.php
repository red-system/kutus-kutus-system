<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;

use app\Models\mProduk;
use app\Models\mStokProduk;
use app\Models\mLokasi;

class StokProdukLokasiInsert implements Rule
{
    private $id_produk;
    private $id_lokasi;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_produk, $id_lokasi)
    {
        $this->id_produk = $id_produk;
        $this->id_lokasi = $id_lokasi;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id_lokasi = $value;
        $check = mStokProduk
            ::where([
                'id_lokasi'=>$id_lokasi,
                'id_produk'=>$this->id_produk
            ])
            ->count();

        if($check == 0) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $produk = mProduk::find($this->id_produk);
        $lokasi = mLokasi::find($this->id_lokasi);
        return 'Produk <strong>'.$produk->nama_produk.'</strong> sudah ada pada Gudang <strong>'.$lokasi->lokasi.'</strong>';
    }
}
