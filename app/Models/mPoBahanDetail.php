<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPoBahanDetail extends Model
{

    protected $table = 'tb_po_bahan_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_po_bahan',
        'id_bahan',
        'harga',
        'ppn_persen',
        'ppn_nominal',
        'harga_net',
        'qty',
        'sub_total'
    ];

    function po_bahan() {
        return $this->belongsTo(mPoBahan::class, 'id_po_bahan');
    }

    function bahan() {
        return $this->belongsTo(mBahan::class, 'id_bahan');
    }
}
