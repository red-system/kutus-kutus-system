<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>Piutang Pelanggan</h3></td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/>
            <br/>
            <h4>Detail Piutang</h4>
            <table width="100%">
                <tbody>
                <tr>
                    <td class="tableitem" width="20%"><p class="itemtext">No Piutang Pelanggan</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {{ $piutang_pelanggan->no_piutang_pelanggan }}</p></td>
                    <td class="tableitem" width="20%"><p class="itemtext">Jatuh Tempo</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {{ Main::format_date($piutang_pelanggan->pp_jatuh_tempo) }}</p></td>
                </tr>
                <tr>
                    <td class="tableitem" width="20%"><p class="itemtext">No Penjualan</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">: {{ $piutang_pelanggan->pp_no_faktur }}</p>
                    </td>
                    <td class="tableitem" width="20%"><p class="itemtext">Jumlah Piutang</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {{ Main::format_money($piutang_pelanggan->pp_sisa_amount) }}</p></td>
                </tr>
                <tr>
                    <td class="tableitem" width="20%"><p class="itemtext">Tanggal Piutang</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {{ Main::format_date($piutang_pelanggan->tanggal_piutang) }}</p></td>
                    <td class="tableitem" width="20%"><p class="itemtext">Keterangan</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">: {{ $piutang_pelanggan->keterangan }}</p>
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <h4>Detail Pembelian Produk</h4>
            <table width="100%">
                <tbody>
                <tr>
                    <td class="tableitem" width="20%"><p class="itemtext">No Penjualan</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">: {{ $penjualan['no_faktur'] }}</p></td>
                    <td class="tableitem" width="20%"><p class="itemtext">Distributor</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {{ "(".$penjualan['distributor']['kode_distributor'].") ".$penjualan['distributor']['nama_distributor'] }}</p>
                    </td>
                </tr>
                <tr>
                    <td class="tableitem" width="20%"><p class="itemtext">Tanggal Pembelian</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {{ Main::format_date($penjualan['tanggal']) }}</p></td>
                    <td class="tableitem" width="20%"><p class="itemtext">Pengiriman</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {!! Main::distributor_pengiriman($penjualan['pengiriman']) !!}</p></td>
                </tr>
                <tr>
                    <td class="tableitem" width="20%"><p class="itemtext">Total Pembelian Produk</p></td>
                    <td class="tableitem" width="30%"><p class="itemtext">
                            : {{ Main::format_money($penjualan['grand_total']) }}</p></td>
                </tr>
                </tbody>
            </table>
            <br/>
            <div id="table">

                <table>
                    <tr class="tabletitle">
                        <th class="item">No</th>
                        <th class="item">Nama Produk</th>
                        <th class="item">Qty</th>
                        <th class="item">Harga</th>
                        <th class="item">Potongan</th>
                        <th class="item">PPN</th>
                        <th class="item">Harga Net</th>
                        <th class="item">Sub Total</th>
                    </tr>
                    @php
                        $penjualan_produk = isset($penjualan['penjualan_produk']) ? $penjualan['penjualan_produk'] : [];
                    @endphp
                    @foreach($penjualan_produk as $r)
                        <tr class="service">
                            <td class="tableitem"><p class="itemtext">{{ $no++ }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->produk->nama_produk }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_number($r->qty) }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_money($r->harga)  }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_money($r->potongan) }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_money($r->ppn_nominal) }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_money($r->harga_net) }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_money($r->sub_total) }}</p></td>
                        </tr>
                    @endforeach
                </table>

                <br /><br />
            </div>

            <table width="100%">
                <tbody>
                <tr>
                    <td class="tableitem" width="50%" align="center"><p class="itemtext">Yang Menerima,</p></td>
                    <td class="tableitem" width="50%" align="center"><p class="itemtext">Yang Menyerahkan,</p></td>
                </tr>
                <tr>
                    <td class="tableitem" width="50%" align="center">
                        <br /><br /><br /><br />
                        <p class="itemtext">( ---------------------------- )</p>
                    </td>
                    <td class="tableitem" width="50%" align="center">
                        <br /><br /><br /><br />
                        <p class="itemtext">( ---------------------------- )</p>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>