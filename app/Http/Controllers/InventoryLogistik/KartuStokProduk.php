<?php

namespace app\Http\Controllers\InventoryLogistik;

use app\Models\mKategoriProduk;
use app\Models\mProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use app\Models\mArusStokProduk;
use app\Models\mLokasi;

use DB,
    PDF;

class KartuStokProduk extends Controller
{

    private $breadcrumb;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_7'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $dataMain = Main::data($this->breadcrumb);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_lokasi = $request->input('id_lokasi');
        $id_produk = $request->input('id_produk');
        $id_kategori_produk = $request->input('id_kategori_produk');

        $date_start = $date_start ? $date_start : mArusStokProduk::orderBy('tgl', 'ASC')->value('tgl');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mArusStokProduk::orderBy('tgl', 'DESC')->value('tgl');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $gudang = mLokasi::where('tipe', 'gudang')->orderBy('lokasi', 'ASC')->get();
        $kategori_produk = mKategoriProduk::orderBy('kategori_produk', 'ASC')->get();
        $produk = mProduk::orderBy('kode_produk', 'ASC');
        if ($id_kategori_produk != 'all') {
            $produk = $produk->where('id_kategori_produk', $id_kategori_produk);
        }
        $produk = $produk->get();



        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_produk = $id_produk ? $id_produk : 'all';
        $id_kategori_produk = $id_kategori_produk ? $id_kategori_produk : 'all';

        $params = [
            'id_lokasi' => $id_lokasi,
            'id_produk' => $id_produk,
            'id_kategori_produk' => $id_kategori_produk,
            'date_start' => $date_start_db,
            'date_end' => $date_end_db
        ];

        $list = mArusStokProduk
            ::select([
                'tb_arus_stok_produk.*',
                'tb_produk.kode_produk',
                'tb_produk.nama_produk',
                'tb_stok_produk.no_seri_produk',
                'tb_arus_stok_produk.keterangan AS arus_stok_produk_keterangan',
                'tb_lokasi.kode_lokasi',
                'tb_lokasi.lokasi',
            ])
            ->leftJoin('tb_produk', 'tb_arus_stok_produk.id_produk', '=', 'tb_produk.id')
            ->leftJoin('tb_stok_produk', 'tb_arus_stok_produk.id_stok_produk', '=', 'tb_stok_produk.id')
            ->leftJoin('tb_lokasi', 'tb_lokasi.id', '=', 'tb_stok_produk.id_lokasi')
            ->whereRaw('tgl >= ? AND tgl <= ?', [$date_start_db, $date_end_db]);

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_stok_produk.id_lokasi', $id_lokasi);
        }

        if ($id_produk != 'all') {
            $list = $list->where('tb_produk.id', $id_produk);
        }

        if ($id_kategori_produk != 'all') {
            $list = $list->where('tb_produk.id_kategori_produk', $id_kategori_produk);
        }

        $list = $list
            ->orderBy('tb_arus_stok_produk.id', 'ASC')
            ->get()
            ->groupBy('tgl');

        $data = [
            'tab' => 'transfer',
            'list' => $list,
            'gudang' => $gudang,
            'produk' => $produk,
            'kategori_produk' => $kategori_produk,
            'id_lokasi' => $id_lokasi,
            'id_produk' => $id_produk,
            'id_kategori_produk' => $id_kategori_produk,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
        ];

        $data = array_merge($dataMain, $data);

        return view('inventoryLogistik/kartuStokProduk/kartuStokProdukList', $data);
    }

    function pdf(Request $request)
    {

        ini_set("memory_limit", "-1");
        $id_produk = $request->input('id_produk');
        $id_lokasi = $request->input('id_lokasi');
        $id_kategori_produk = $request->input('id_kategori_produk');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $stok_masuk_col = 6;
        $stok_keluar_col = 7;

        $list = mArusStokProduk
            ::select([
                'tb_arus_stok_produk.*',
                'tb_produk.kode_produk',
                'tb_produk.nama_produk',
                'tb_stok_produk.no_seri_produk',
                'tb_arus_stok_produk.keterangan AS arus_stok_produk_keterangan',
                'tb_lokasi.kode_lokasi',
                'tb_lokasi.lokasi',
            ])
            ->leftJoin('tb_produk', 'tb_arus_stok_produk.id_produk', '=', 'tb_produk.id')
            ->leftJoin('tb_stok_produk', 'tb_arus_stok_produk.id_stok_produk', '=', 'tb_stok_produk.id')
            ->leftJoin('tb_lokasi', 'tb_lokasi.id', '=', 'tb_stok_produk.id_lokasi')
            ->whereRaw('tgl >= ? AND tgl <= ?', [$date_start, $date_end]);

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_stok_produk.id_lokasi', $id_lokasi);
            $lokasi = mLokasi::where('id', $id_lokasi)->first(['kode_lokasi', 'lokasi']);
            $nama_lokasi = '(' . $lokasi->kode_lokasi . ') ' . $lokasi->lokasi;
            $view_lokasi = 'solo';
            $stok_masuk_col -= 1;
            $stok_keluar_col -= 1;
        } else {
            $nama_lokasi = 'Semua Lokasi';
            $view_lokasi = 'all';
        }

        if ($id_produk != 'all') {
            $list = $list->where('tb_produk.id', $id_produk);
            $produk = mProduk::where('id', $id_produk)->first(['kode_produk', 'nama_produk']);
            $nama_produk = '(' . $produk->kode_produk . ') ' . $produk->nama_produk;
            $view_produk = 'solo';
            $stok_masuk_col -= 1;
            $stok_keluar_col -= 1;
        } else {
            $nama_produk = 'Semua Produk';
            $view_produk = 'all';
        }

        if ($id_kategori_produk != 'all') {
            $list = $list->where('tb_produk.id_kategori_produk', $id_kategori_produk);
            $nama_kategori_produk = mKategoriProduk::where('id', $id_kategori_produk)->value('kategori_produk');
            $view_kategori_produk = 'solo';
        } else {
            $nama_kategori_produk = 'Semua Kategori Produk';
            $view_kategori_produk = 'all';
        }

        $list = $list
            ->orderBy('tb_arus_stok_produk.id', 'ASC')
            ->get()
            ->groupBy('tgl');


        $data = [
            'list' => $list,
            'nama_produk' => $nama_produk,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'nama_lokasi' => $nama_lokasi,
            'nama_kategori_produk' => $nama_kategori_produk,
            'view_produk' => $view_produk,
            'view_kategori_produk' => $view_kategori_produk,
            'view_lokasi' => $view_lokasi,
            'stok_masuk_col' => $stok_masuk_col,
            'stok_keluar_col' => $stok_keluar_col
        ];

        $pdf = PDF::loadView('inventoryLogistik/kartuStokProduk/kartuStokProdukPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('Kartu Stok Produk ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");

        $id_produk = $request->input('id_produk');
        $id_lokasi = $request->input('id_lokasi');
        $id_kategori_produk = $request->input('id_kategori_produk');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $stok_masuk_col = 6;
        $stok_keluar_col = 7;

        $list = mArusStokProduk
            ::select([
                'tb_arus_stok_produk.*',
                'tb_produk.kode_produk',
                'tb_produk.nama_produk',
                'tb_stok_produk.no_seri_produk',
                'tb_arus_stok_produk.keterangan AS arus_stok_produk_keterangan',
                'tb_lokasi.kode_lokasi',
                'tb_lokasi.lokasi',
            ])
            ->leftJoin('tb_produk', 'tb_arus_stok_produk.id_produk', '=', 'tb_produk.id')
            ->leftJoin('tb_stok_produk', 'tb_arus_stok_produk.id_stok_produk', '=', 'tb_stok_produk.id')
            ->leftJoin('tb_lokasi', 'tb_lokasi.id', '=', 'tb_stok_produk.id_lokasi')
            ->whereRaw('tgl >= ? AND tgl <= ?', [$date_start, $date_end]);

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_stok_produk.id_lokasi', $id_lokasi);
            $lokasi = mLokasi::where('id', $id_lokasi)->first(['kode_lokasi', 'lokasi']);
            $nama_lokasi = '(' . $lokasi->kode_lokasi . ') ' . $lokasi->lokasi;
            $view_lokasi = 'solo';
            $stok_masuk_col -= 1;
            $stok_keluar_col -= 1;
        } else {
            $nama_lokasi = 'Semua Lokasi';
            $view_lokasi = 'all';
        }

        if ($id_produk != 'all') {
            $list = $list->where('tb_produk.id', $id_produk);
            $produk = mProduk::where('id', $id_produk)->first(['kode_produk', 'nama_produk']);
            $nama_produk = '(' . $produk->kode_produk . ') ' . $produk->nama_produk;
            $view_produk = 'solo';
            $stok_masuk_col -= 1;
            $stok_keluar_col -= 1;
        } else {
            $nama_produk = 'Semua Produk';
            $view_produk = 'all';
        }

        if ($id_kategori_produk != 'all') {
            $list = $list->where('tb_produk.id_kategori_produk', $id_kategori_produk);
            $nama_kategori_produk = mKategoriProduk::where('id', $id_kategori_produk)->value('kategori_produk');
            $view_kategori_produk = 'solo';
        } else {
            $nama_kategori_produk = 'Semua Kategori Produk';
            $view_kategori_produk = 'all';
        }

        $list = $list
            ->orderBy('tb_arus_stok_produk.id', 'ASC')
            ->get()
            ->groupBy('tgl');


        $data = [
            'list' => $list,
            'nama_produk' => $nama_produk,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'nama_lokasi' => $nama_lokasi,
            'nama_kategori_produk' => $nama_kategori_produk,
            'view_produk' => $view_produk,
            'view_kategori_produk' => $view_kategori_produk,
            'view_lokasi' => $view_lokasi,
            'stok_masuk_col' => $stok_masuk_col,
            'stok_keluar_col' => $stok_keluar_col
        ];

        return view('inventoryLogistik/kartuStokProduk/kartuStokProdukExcel', $data);
    }

}
