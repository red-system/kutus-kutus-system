<?php

namespace app\Http\Controllers\InventoryLogistik;

use app\Models\mBahan;
use app\Models\mKategoriBahan;
use app\Models\mLokasi;
use app\Models\mSatuan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use app\Models\mArusStokBahan;

use DB,
    PDF;

class KartuStokBahan extends Controller
{
    private $breadcrumb;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_6'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {

        $dataMain = Main::data($this->breadcrumb);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_lokasi = $request->input('id_lokasi');
        $id_bahan = $request->input('id_bahan');
        $id_kategori_bahan = $request->input('id_kategori_bahan');

        $date_start = $date_start ? $date_start : mArusStokBahan::orderBy('tgl', 'ASC')->value('tgl');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mArusStokBahan::orderBy('tgl', 'DESC')->value('tgl');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_bahan = $id_bahan ? $id_bahan : 'all';
        $id_kategori_bahan = $id_kategori_bahan ? $id_kategori_bahan : 'all';

        $gudang = mLokasi::where('tipe', 'gudang')->orderBy('lokasi', 'ASC')->get();
        $kategori_bahan = mKategoriBahan::orderby('kategori_bahan', 'ASC')->get();
        $bahan = mBahan::orderBy('kode_bahan', 'ASC');
        if ($id_kategori_bahan != 'all') {
            $bahan = $bahan->where('id_kategori_bahan', $id_kategori_bahan);
        }
        $bahan = $bahan->get();


        $params = [
            'id_lokasi' => $id_lokasi,
            'id_bahan' => $id_bahan,
            'id_kategori_bahan' => $id_kategori_bahan,
            'date_start' => $date_start_db,
            'date_end' => $date_end_db
        ];

        $list = mArusStokBahan
            ::select([
                'tb_arus_stok_bahan.id',
                'tgl',
                'tb_arus_stok_bahan.id_bahan',
                'tb_arus_stok_bahan.id_po_bahan',
                'tb_arus_stok_bahan.id_stok_bahan',
                'tb_arus_stok_bahan.stok_in',
                'tb_arus_stok_bahan.stok_out',
                'tb_arus_stok_bahan.last_stok',
                'tb_arus_stok_bahan.last_stok_total',
                'tb_arus_stok_bahan.keterangan',
                'tb_lokasi.kode_lokasi',
                'tb_lokasi.lokasi'
            ])
            ->with([
                'bahan:id,id_satuan,kode_bahan,nama_bahan',
                'bahan.satuan:id,satuan',
                'po_bahan:id,no_faktur'
            ])
            ->leftJoin('tb_stok_bahan', 'tb_arus_stok_bahan.id_stok_bahan', '=', 'tb_stok_bahan.id')
            ->leftJoin('tb_lokasi', 'tb_stok_bahan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_bahan', 'tb_stok_bahan.id_bahan', '=', 'tb_bahan.id')
            ->whereRaw('tgl >= ? AND tgl <= ?', [$date_start_db, $date_end_db]);

        if ($id_bahan != 'all') {
            $list = $list->where('tb_arus_stok_bahan.id_bahan', $id_bahan);
        }

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_stok_bahan.id_lokasi', $id_lokasi);
        }

        if ($id_kategori_bahan != 'all') {
            $list = $list->where('tb_bahan.id_kategori_bahan', $id_kategori_bahan);
        }

        $list = $list->orderBy('tgl', 'ASC')
            ->orderBy('tb_arus_stok_bahan.id', 'ASC')
            ->get()
            ->groupBy('tgl');


        $data = [
            'tab' => 'transfer',
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'gudang' => $gudang,
            'bahan' => $bahan,
            'kategori_bahan' => $kategori_bahan,
            'id_lokasi' => $id_lokasi,
            'id_bahan' => $id_bahan,
            'id_kategori_bahan' => $id_kategori_bahan,
            'params' => $params,
        ];

        $data = array_merge($dataMain, $data);

        return view('inventoryLogistik/kartuStokBahan/kartuStokBahanList', $data);
    }

    function update(Request $request)
    {
        $arus_stok_bahan_id = $request->input('arus_stok_bahan_id');
        $last_stok = $request->input('last_stok');

        //mArusStokBahan::where(['id' => $arus_stok_bahan_id])->update(['last_stok' => $last_stok]);

        \Illuminate\Support\Facades\DB::table('tb_arus_stok_bahan')->where('id', $arus_stok_bahan_id)->update(['last_stok'=>$last_stok]);
    }

    function pdf(Request $request)
    {

        ini_set("memory_limit", "-1");

        $stok_masuk_col = 8;
        $stok_keluar_col = 7;

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_lokasi = $request->input('id_lokasi');
        $id_bahan = $request->input('id_bahan');
        $id_kategori_bahan = $request->input('id_kategori_bahan');

        $date_start = $date_start ? $date_start : mArusStokBahan::orderBy('tgl', 'ASC')->value('tgl');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mArusStokBahan::orderBy('tgl', 'DESC')->value('tgl');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $list = mArusStokBahan
            ::select([
                'tb_arus_stok_bahan.id',
                'tgl',
                'tb_arus_stok_bahan.id_bahan',
                'tb_arus_stok_bahan.id_po_bahan',
                'tb_arus_stok_bahan.id_stok_bahan',
                'tb_arus_stok_bahan.stok_in',
                'tb_arus_stok_bahan.stok_out',
                'tb_arus_stok_bahan.last_stok',
                'tb_arus_stok_bahan.last_stok_total',
                'tb_arus_stok_bahan.keterangan',
                'tb_lokasi.kode_lokasi',
                'tb_lokasi.lokasi'
            ])
            ->with([
                'bahan:id,id_satuan,kode_bahan,nama_bahan',
                'bahan.satuan:id,satuan',
                'po_bahan:id,no_faktur'
            ])
            ->leftJoin('tb_stok_bahan', 'tb_arus_stok_bahan.id_stok_bahan', '=', 'tb_stok_bahan.id')
            ->leftJoin('tb_lokasi', 'tb_stok_bahan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_bahan', 'tb_stok_bahan.id_bahan', '=', 'tb_bahan.id')
            ->whereRaw('tgl >= ? AND tgl <= ?', [$date_start_db, $date_end_db]);


        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_bahan = $id_bahan ? $id_bahan : 'all';
        $id_kategori_bahan = $id_kategori_bahan ? $id_kategori_bahan : 'all';

        if ($id_bahan != 'all') {
            $list = $list->where('tb_arus_stok_bahan.id_bahan', $id_bahan);
            $bahan = mBahan::where('id', $id_bahan)->first(['id_satuan', 'kode_bahan', 'nama_bahan']);
            $nama_satuan = mSatuan::where('id', $bahan->id_satuan)->value('satuan');
            $nama_bahan = '(' . $bahan->kode_bahan . ') ' . $bahan->nama_bahan;
            $view_bahan = 'solo';
            $view_satuan = 'solo';
            $stok_masuk_col -= 2;
            $stok_keluar_col -= 2;
        } else {
            $nama_bahan = 'Semua Bahan';
            $nama_satuan = 'Semua Satuan';
            $view_bahan = 'all';
            $view_satuan = 'all';
        }

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_stok_bahan.id_lokasi', $id_lokasi);
            $lokasi = mLokasi::where('id', $id_lokasi)->first(['kode_lokasi', 'lokasi']);
            $nama_lokasi = '(' . $lokasi->kode_lokasi . ') ' . $lokasi->lokasi;
            $view_lokasi = 'solo';

            $stok_masuk_col -= 1;
            $stok_keluar_col -= 1;
        } else {
            $nama_lokasi = 'Semua Lokasi';
            $view_lokasi = 'all';
        }

        if ($id_kategori_bahan != 'all') {
            $list = $list->where('tb_bahan.id_kategori_bahan', $id_kategori_bahan);
            $kategori_bahan = mKategoriBahan::where('id', $id_kategori_bahan)->value('kategori_bahan');
            $nama_kategori_bahan = $kategori_bahan;
            $view_kategori_bahan = 'solo';
        } else {
            $nama_kategori_bahan = 'Semua Kategori Bahan';
            $view_kategori_bahan = 'all';
        }

        $list = $list->orderBy('tgl', 'ASC')
            ->orderBy('tb_arus_stok_bahan.id', 'ASC')
            ->get()
            ->groupBy('tgl');

        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'nama_lokasi' => $nama_lokasi,
            'nama_satuan' => $nama_satuan,
            'nama_bahan' => $nama_bahan,
            'nama_kategori_bahan' => $nama_kategori_bahan,
            'view_bahan' => $view_bahan,
            'view_lokasi' => $view_lokasi,
            'view_satuan' => $view_satuan,
            'view_kategori_baha' => $view_kategori_bahan,
            'stok_masuk_col' => $stok_masuk_col,
            'stok_keluar_col' => $stok_keluar_col
        ];

        //return view('inventoryLogistik/kartuStokBahan/kartuStokBahanPdf', $data);

        $pdf = PDF::loadView('inventoryLogistik/kartuStokBahan/kartuStokBahanPdf', $data);
        return $pdf
            ->setPaper('A3', 'landscape')
            ->download('Kartu Stok Bahan ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");

        $stok_masuk_col = 8;
        $stok_keluar_col = 7;

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_lokasi = $request->input('id_lokasi');
        $id_bahan = $request->input('id_bahan');
        $id_kategori_bahan = $request->input('id_kategori_bahan');

        $date_start = $date_start ? $date_start : mArusStokBahan::orderBy('tgl', 'ASC')->value('tgl');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mArusStokBahan::orderBy('tgl', 'DESC')->value('tgl');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $list = mArusStokBahan
            ::select([
                'tb_arus_stok_bahan.id',
                'tgl',
                'tb_arus_stok_bahan.id_bahan',
                'tb_arus_stok_bahan.id_po_bahan',
                'tb_arus_stok_bahan.id_stok_bahan',
                'tb_arus_stok_bahan.stok_in',
                'tb_arus_stok_bahan.stok_out',
                'tb_arus_stok_bahan.last_stok',
                'tb_arus_stok_bahan.last_stok_total',
                'tb_arus_stok_bahan.keterangan',
                'tb_lokasi.kode_lokasi',
                'tb_lokasi.lokasi'
            ])
            ->with([
                'bahan:id,id_satuan,kode_bahan,nama_bahan',
                'bahan.satuan:id,satuan',
                'po_bahan:id,no_faktur'
            ])
            ->leftJoin('tb_stok_bahan', 'tb_arus_stok_bahan.id_stok_bahan', '=', 'tb_stok_bahan.id')
            ->leftJoin('tb_lokasi', 'tb_stok_bahan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_bahan', 'tb_stok_bahan.id_bahan', '=', 'tb_bahan.id')
            ->whereRaw('tgl >= ? AND tgl <= ?', [$date_start_db, $date_end_db]);


        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_bahan = $id_bahan ? $id_bahan : 'all';
        $id_kategori_bahan = $id_kategori_bahan ? $id_kategori_bahan : 'all';

        if ($id_bahan != 'all') {
            $list = $list->where('tb_arus_stok_bahan.id_bahan', $id_bahan);
            $bahan = mBahan::where('id', $id_bahan)->first(['id_satuan', 'kode_bahan', 'nama_bahan']);
            $nama_satuan = mSatuan::where('id', $bahan->id_satuan)->value('satuan');
            $nama_bahan = '(' . $bahan->kode_bahan . ') ' . $bahan->nama_bahan;
            $view_bahan = 'solo';
            $view_satuan = 'solo';
            $stok_masuk_col -= 2;
            $stok_keluar_col -= 2;
        } else {
            $nama_bahan = 'Semua Bahan';
            $nama_satuan = 'Semua Satuan';
            $view_bahan = 'all';
            $view_satuan = 'all';
        }

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_stok_bahan.id_lokasi', $id_lokasi);
            $lokasi = mLokasi::where('id', $id_lokasi)->first(['kode_lokasi', 'lokasi']);
            $nama_lokasi = '(' . $lokasi->kode_lokasi . ') ' . $lokasi->lokasi;
            $view_lokasi = 'solo';

            $stok_masuk_col -= 1;
            $stok_keluar_col -= 1;
        } else {
            $nama_lokasi = 'Semua Lokasi';
            $view_lokasi = 'all';
        }

        if ($id_kategori_bahan != 'all') {
            $list = $list->where('tb_bahan.id_kategori_bahan', $id_kategori_bahan);
            $kategori_bahan = mKategoriBahan::where('id', $id_kategori_bahan)->value('kategori_bahan');
            $nama_kategori_bahan = $kategori_bahan;
            $view_kategori_bahan = 'solo';
        } else {
            $nama_kategori_bahan = 'Semua Kategori Bahan';
            $view_kategori_bahan = 'all';
        }

        $list = $list->orderBy('tgl', 'ASC')
            ->orderBy('tb_arus_stok_bahan.id', 'ASC')
            ->get()
            ->groupBy('tgl');

        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'nama_lokasi' => $nama_lokasi,
            'nama_satuan' => $nama_satuan,
            'nama_bahan' => $nama_bahan,
            'nama_kategori_bahan' => $nama_kategori_bahan,
            'view_bahan' => $view_bahan,
            'view_lokasi' => $view_lokasi,
            'view_satuan' => $view_satuan,
            'stok_masuk_col' => $stok_masuk_col,
            'stok_keluar_col' => $stok_keluar_col
        ];

        return view('inventoryLogistik/kartuStokBahan/kartuStokBahanExcel', $data);
    }


}
