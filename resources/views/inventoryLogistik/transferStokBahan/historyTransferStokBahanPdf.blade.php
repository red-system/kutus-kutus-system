<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>History Transfer Stok Bahan</h3></td>
                    </tr>
                    <tr>
                        <td>{{ $date_start.' s/d '.$date_end }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <br/>
            <div id="table">
                <table>
                    <thead>
                    <tr class="tabletitle">
                        <th class="item" width="10">No</th>
                        <th class="item">Tanggal</th>
                        <th class="item">Kode Bahan</th>
                        <th class="item">Nama Bahan</th>
                        <th class="item">Qty Transfer</th>
                        <th class="item">Dari Lokasi</th>
                        <th class="item">Lokasi Tujuan</th>
                        <th class="item">Last Stok</th>
                        <th class="item">Penerima</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $no = 1;
                        $total = 0;
                    @endphp
                    @foreach($list as $r)

                        @php
                            $total += $r->total_diperlukan;
                        @endphp

                        <tr class="service">
                            <td class="tableitem"><p class="itemtext">{{ $no++ }}.</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_date($r->tgl) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->kode_bahan }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->nama_bahan }}</p></td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->qty_transfer) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->dari }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->tujuan }}</p></td>
                            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->last_stok) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->keterangan }}</p></td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>