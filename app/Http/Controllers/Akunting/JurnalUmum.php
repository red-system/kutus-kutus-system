<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use app\Models\mPenjualanProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

use DB,
    PDF;

class JurnalUmum extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_2'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);
        $jurnal_umum = mAcJurnalUmum
            ::with('transaksi')
            ->whereBetween('jmu_tanggal', [$date_start, $date_end])
            ->orderBy('jurnal_umum_id', 'ASC')
            ->get();
        $total_debet = mAcJurnalUmum
            ::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
            ->whereDate('jmu_tanggal', [$date_start, $date_end])
            ->sum('trs_debet');
        $total_kredit = mAcJurnalUmum
            ::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
            ->whereDate('jmu_tanggal', [$date_start, $date_end])
            ->sum('trs_kredit');
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'jurnal_umum' => $jurnal_umum,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'jml_debet' => $total_debet,
            'jml_kredit' => $total_kredit
        ]);

        return view('akunting/jurnalUmum/jurnalUmumList', $data);
    }

    function create()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $kodeJurnalUmum = Config('constants.kodeJurnalUmum');
        $no_invoice = hAkunting::getNoTransaksiPayment($kodeJurnalUmum);
        $master = mAcMaster::orderBy('master_id', 'ASC')->get();
        $kode_perkiraan_option_list = hAkunting::kode_perkiraan_select_list();


        $data = array_merge($data, [
            'pageTitle' => 'Tambah Jurnal Umum',
            'no_invoice' => $no_invoice,
            'master' => $master,
            'kode_perkiraan_option_list' => $kode_perkiraan_option_list
        ]);

        return view('akunting/jurnalUmum/jurnalUmumCreate', $data);
    }

    function insert(Request $request)
    {

        $rules = [
            'jmu_keterangan' => 'required',
            'no_invoice' => 'required',
            'master_id' => 'required',
            'master_id.*' => 'required',
            'trs_debet' => 'required',
            'trs_debet.*' => 'required',
            'trs_kredit' => 'required',
            'trs_kredit.*' => 'required',
        ];

        $attributes = [
            'jmu_keterangan' => 'Keterangan',
            'no_invoice' => 'No Invoice',
            'master_id' => 'Kode Perkiraan',
            'trs_debet' => 'Nominal Debet',
            'trs_kredit' => 'Nominal Kredit',
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan';
            $attr['trs_debet.' . $i] = 'Nominal Debet';
            $attr['trs_kredit.' . $i] = 'Nominal Kredit';
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        $total_debet = array_sum($request->input('trs_debet'));
        $total_kredit = array_sum($request->input('trs_kredit'));

        if ($total_debet !== $total_kredit) {
            return response([
                'message' => 'Tidak Balance antara Total Debet dengan Total Kredit'
            ], 422);
        }


        DB::beginTransaction();
        try {

            $jmu_tanggal = $request->input('jmu_tanggal');
            $no_invoice = $request->input('no_invoice');
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $jmu_keterangan = $request->input('jmu_keterangan');

            $master_id_arr = $request->input('master_id');
            $trs_jenis_transaksi_arr = $request->input('trs_jenis_transaksi');
            $trs_debet_arr = $request->input('trs_debet');
            $trs_kredit_arr = $request->input('trs_kredit');
            $tipe_arus_kas_arr = $request->input('tipe_arus_kas');
            $trs_catatan_arr = $request->input('trs_catatan');

            $data_jurnal_umum = [
                'no_invoice' => $no_invoice,
                'jmu_tanggal' => Main::format_date_db($jmu_tanggal),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
            ];

            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first([
                        'mst_kode_rekening',
                        'mst_nama_rekening'
                    ]);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_jenis_transaksi = $trs_jenis_transaksi_arr[$index];
                $trs_debet = $trs_debet_arr[$index];
                $trs_kredit = $trs_kredit_arr[$index];
                $tipe_arus_kas = $tipe_arus_kas_arr[$index];
                $trs_catatan = $trs_catatan_arr[$index];

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                    'trs_setor' => 0,
                    'tgl_transaksi' => Main::format_date_db($jmu_tanggal)
                ];

                mAcTransaksi::create($data_transaksi);
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }


    }

    function edit($jurnal_umum_id)
    {
        $jurnal_umum_id = Main::decrypt($jurnal_umum_id);

        $data = Main::data($this->breadcrumb, $this->menuActive);
        $jurnal_umum = mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->first();
        $transaksi = mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->orderBy('transaksi_id', 'ASC')->get();
        $master = mAcMaster::orderBy('master_id', 'ASC')->get();
        $kode_perkiraan_option_list = hAkunting::kode_perkiraan_select_list();


        $data = array_merge($data, [
            'pageTitle' => 'Edit Jurnal Umum',
            'jurnal_umum' => $jurnal_umum,
            'transaksi' => $transaksi,
            'master' => $master,
            'kode_perkiraan_option_list' => $kode_perkiraan_option_list,
            'total_debet' => 0,
            'total_kredit' => 0
        ]);

        return view('akunting/jurnalUmum/jurnalUmumEdit', $data);

    }

    function update(Request $request, $jurnal_umum_id)
    {
        $rules = [
            'jmu_keterangan' => 'required',
            'no_invoice' => 'required',
            'master_id' => 'required',
            'master_id.*' => 'required',
            'trs_debet' => 'required',
            'trs_debet.*' => 'required',
            'trs_kredit' => 'required',
            'trs_kredit.*' => 'required',
        ];

        $attributes = [
            'jmu_keterangan' => 'Keterangan',
            'no_invoice' => 'No Invoice',
            'master_id' => 'Kode Perkiraan',
            'trs_debet' => 'Nominal Debet',
            'trs_kredit' => 'Nominal Kredit',
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan';
            $attr['trs_debet.' . $i] = 'Nominal Debet';
            $attr['trs_kredit.' . $i] = 'Nominal Kredit';
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        $total_debet = array_sum($request->input('trs_debet'));
        $total_kredit = array_sum($request->input('trs_kredit'));

        if ($total_debet !== $total_kredit) {
            return response([
                'message' => 'Tidak Balance antara Total Debet dengan Total Kredit'
            ], 422);
        }

        DB::beginTransaction();
        try {

            $jurnal_umum_id = Main::decrypt($jurnal_umum_id);
            $jmu_tanggal = $request->input('jmu_tanggal');
            $no_invoice = $request->input('no_invoice');
            $year = date('Y');
            $month = date('m');
            $jmu_keterangan = $request->input('jmu_keterangan');

            $master_id_arr = $request->input('master_id');
            $trs_jenis_transaksi_arr = $request->input('trs_jenis_transaksi');
            $trs_debet_arr = $request->input('trs_debet');
            $trs_kredit_arr = $request->input('trs_kredit');
            $tipe_arus_kas_arr = $request->input('tipe_arus_kas');
            $trs_catatan_arr = $request->input('trs_catatan');

            $data_jurnal_umum = [
                'no_invoice' => $no_invoice,
                'jmu_tanggal' => Main::format_date_db($jmu_tanggal),
                'jmu_keterangan' => $jmu_keterangan,
            ];

            mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->update($data_jurnal_umum);

            /**
             * Delete semua transaksi, lalu masukkan data transaksi kembali
             */

            mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();

            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first([
                        'mst_kode_rekening',
                        'mst_nama_rekening'
                    ]);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_jenis_transaksi = $trs_jenis_transaksi_arr[$index];
                $trs_debet = $trs_debet_arr[$index];
                $trs_kredit = $trs_kredit_arr[$index];
                $tipe_arus_kas = $tipe_arus_kas_arr[$index];
                $trs_catatan = $trs_catatan_arr[$index];

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                    'trs_setor' => 0,
                    'tgl_transaksi' => Main::format_date_db($jmu_tanggal)
                ];

                mAcTransaksi::create($data_transaksi);
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }

    function delete($jurnal_umum_id)
    {
        DB::beginTransaction();
        try {

            $jurnal_umum_id = Main::decrypt($jurnal_umum_id);
            $jurnal_umum = mAcJurnalUmum::find($jurnal_umum_id);

            $fields = [
                'id_history_penyesuaian_bahan',
                'id_history_penyesuaian_produk',
                'id_penjualan',
                'id_asset',
                'id_penyusutan_asset',
                'id_piutang_lain',
                'id_distribusi',
                'id_piutang_pelanggan',
                'id_hutang_supplier',
                'id_produksi',
                'id_progress_produksi',
                'id_po_bahan',
                'id_hutang_lain'
            ];

            $allow_dalete = TRUE;
            $message = '';
            $status = '';

            if ($jurnal_umum->id_history_penyesuaian_bahan) {
                $status = 'history_penyesuaian_bahan';
            } elseif ($jurnal_umum->id_history_penyesuaian_produk) {
                $status = 'history_penyesuaian_produk';
            } elseif ($jurnal_umum->id_penjualan) {
                $status = 'penjualan';
            } elseif ($jurnal_umum->id_asset) {
                $status = 'asset';
            } elseif ($jurnal_umum->id_penyusutan_asset) {
                $status = 'penyusutan_asset';
            } elseif ($jurnal_umum->id_piutang_lain) {
                $status = 'piutang_lain';
            } elseif ($jurnal_umum->id_distribusi) {
                $status = 'distribusi';
            } elseif ($jurnal_umum->id_piutang_pelanggan) {
                $status = 'piutang_pelanggan';
            } elseif ($jurnal_umum->id_hutang_supplier) {
                $status = 'hutang_supplier';
            } elseif ($jurnal_umum->id_produksi && $jurnal_umum->id_progress_produksi == '') {
                $status = 'produksi';
            } elseif ($jurnal_umum->id_produksi && $jurnal_umum->id_progress_produksi) {
                $status = 'progress_produksi';
            } elseif ($jurnal_umum->id_po_bahan) {
                $status = 'po_bahan';
            } elseif ($jurnal_umum->id_hutang_lain) {
                $status = 'hutang_lain';
            }

            switch ($status) {
                case "history_penyesuaian_bahan":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini dari Penyesuaian Stok Bahan, sehingga tidak bisa dihapus.
                                <br />
                                Karena penyesuaian bahan sudah dilakukan.
                                <br />
                                Jika dipaksa menghapus, akan berpengaruh ke Stok Produk dan Akunting.';
                    break;
                case "history_penyesuaian_produk":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini dari Penyesuaian Stok Produk, sehingga tidak bisa dihapus. 
                                <br />
                                Karena penyesuaian produk sudah dilakukan.
                                <br />
                                Jika dipaksa menghapus, akan berpengaruh ke Stok Produk dan Akunting.';
                    break;
                case "penjualan":
                    $check_penjualan_produk = mPenjualanProduk
                        ::where('id', $jurnal_umum->id_penjualan)
                        ->count();
                    if ($check_penjualan_produk > 0) {
                        $allow_dalete = FALSE;
                        $message = 'Jurnal umum ini berasal dari Penjualan Produk,
                                    <br />
                                    hapus terlebih dahulu penjualan produk yang terkait dengan Jurnal Umum ini, 
                                    <br />
                                    sehingga bisa menghapus Jurnal Umum ini.';
                    } else {
                        $allow_dalete = TRUE;
                    }
                    break;
                case "asset":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Asset
                                <br />
                                Hapus terlebih dahulu dari Menu Asset yang terkait dengan Jurnal Umum ini,
                                <br />
                                Sehingga Jurnal Umum ini otomatis juga akan terhapus.';
                    break;
                case "penyusutan_asset":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Penyusutan Asset bulanan (cron)
                                <br />
                                Hapus terlebih dahulu dari Menu Asset yang terkait dengan Jurnal Umum ini,
                                <br />
                                Sehingga Jurnal Umum ini otomatis juga akan terhapus.';
                    break;
                case "piutang_lain":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Piutang Lain
                                <br />
                                Hapus terlebih dahulu dari Menu Piutang Lain yang terkait dengan Jurnal Umum ini,
                                <br />
                                Sehingga Jurnal Umum ini otomatis juga akan terhapus.';
                    break;
                case "distribusi":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Distribusi Penjualan Produk.
                                <br />
                                Hapus terlebih dahulu distribusi terkait pada Menu Distribusi Penjualan Produk yang terkait dengan Jurnal Umum ini.
                                <br />
                                Sehingga Jurnal Umum ini otomatis juga akan terhapus.';
                    break;
                case "piutang_pelanggan":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Piutang Pelanggan atau Penjualan Produk
                                <br />
                                Hapus terlebih dahulu Piutang Pelanggan atau Penjualan Produk yang terkait dengan Jurnal Umum ini.
                                <br />
                                Sehingga Jurnal Umum ini otomatis juga akan terhapus.';
                    break;
                case "hutang_supplier":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Hutang Supplier dan PO Bahan ke Supplier.
                                <br />
                                Hapus terlebih dahulu Hutang Supplier atau PO Bahan ke Supplier yang terkait dengan Jurnal Umum ini.
                                <br />
                                Sehingga Jurnal Umum ini otomatis juga akan terhapus';
                    break;
                case "produksi":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Produksi yang sudah berjalan.
                                <br />
                                Sehingga Jurnal Umum ini tidak bisa dihapus.';
                    break;
                case "progress_produksi":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Progress Produksi yang sudah berjalan.
                                <br />
                                Sehingga Jurnal Umum ini tidak bisa dihapus.';
                    break;
                case "po_bahan":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari PO Bahan ke Supplier.
                                <br />
                                Hapus terlebih dahulu PO Bahan ke Supplier yang terkait dengan Jurnal Umum ini.
                                <br />
                                Jurnal Umum ini otomatis akan terhapus saat PO Bahan tersebut terhapus.';
                    break;
                case "hutang_lain":
                    $allow_dalete = FALSE;
                    $message = 'Jurnal umum ini berasal dari Hutang Lain - Lain.
                                <br />
                                Hapus terlebih dahulu Hutang Lain - Lain yang terkait dengan Jurnal Umum ini.
                                <br />
                                Jurnal Umum ini otomatis akan terhapus saat Hutang Lain - Lain tersebut terhapus';
                    break;
            }

            if ($allow_dalete) {
                mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->delete();
                mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();
            } else {
                return response([
                    'message' => $message
                ], 422);
            }


            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }


    function pdf(Request $request)
    {

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 30000000);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);
        $jurnal_umum = mAcJurnalUmum
            ::with('transaksi')
            ->whereBetween('jmu_tanggal', [$date_start, $date_end])
            ->orderBy('jurnal_umum_id', 'ASC')
            ->get();
        $total_debet = mAcJurnalUmum
            ::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
            ->whereDate('jmu_tanggal', [$date_start, $date_end])
            ->sum('trs_debet');
        $total_kredit = mAcJurnalUmum
            ::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
            ->whereDate('jmu_tanggal', [$date_start, $date_end])
            ->sum('trs_kredit');
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = [
            'no' => '1',
            'jurnal_umum' => $jurnal_umum,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'jml_debet' => $total_debet,
            'jml_kredit' => $total_kredit,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::loadView('akunting/jurnalUmum/jurnalUmumPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Jurnal Umum ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);
        $jurnal_umum = mAcJurnalUmum
            ::with('transaksi')
            ->whereBetween('jmu_tanggal', [$date_start, $date_end])
            ->orderBy('jurnal_umum_id', 'ASC')
            ->get();
        $total_debet = mAcJurnalUmum
            ::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
            ->whereDate('jmu_tanggal', [$date_start, $date_end])
            ->sum('trs_debet');
        $total_kredit = mAcJurnalUmum
            ::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
            ->whereDate('jmu_tanggal', [$date_start, $date_end])
            ->sum('trs_kredit');
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = [
            'no' => '1',
            'jurnal_umum' => $jurnal_umum,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'jml_debet' => $total_debet,
            'jml_kredit' => $total_kredit,
            'company' => Main::companyInfo()
        ];

        return view('akunting/jurnalUmum/jurnalUmumExcel', $data);
    }


}