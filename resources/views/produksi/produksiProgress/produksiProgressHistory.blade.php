@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/produksi.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('produksi/produksi/produksiDetailModal')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">
                {{--@include('produksi/produksiProgress/tabTitle')--}}
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-5 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="tgl_start"
                                           value="{{ $tgl_start }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="tgl_end" value="{{ $tgl_end }}">
                                </div>
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12">Gudang</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <select class="form-control m-select2" name="id_lokasi">
                                    <option value="all">Semua Gudang</option>
                                    @foreach($gudang as $r)
                                        <option value="{{ $r->id }}" {{ $id_lokasi == $r->id ? 'selected':'' }}>{{ $r->lokasi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-1">
                                <button type="submit" class="btn btn-success">Filter Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>


            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">

                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tanggal Progress</th>
                            {{--                            <th>Waktu Input</th>--}}
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Qty Selesai</th>
                            <th>Gudang</th>
                            <th>Nomer Seri Produk</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            @php( $total_progress += $r->qty_progress)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ Main::format_date($r->tgl_selesai) }}</td>
                                {{--<td>{{ date('H:i:s', strtotime($r->tgl_selesai)) }}</td>--}}
                                <td>{{ $r->produk->kode_produk }}</td>
                                <td>{{ $r->produk->nama_produk }}</td>
                                <td align="right">{{ Main::format_number($r->qty_progress) }}</td>
                                <td>{{ $r->lokasi['lokasi'] }}</td>
                                <td>{{ $r->stok_produk['no_seri_produk'] }}</td>
                                <td>
                                    <div class="btn-group m-btn-group m-btn-group--pill">
                                        <button type="button"
                                                class="btn-produksi-detail m-btn btn-sm btn btn-info"
                                                data-route='{{ route('produksiProgressDetail', ['id'=>$r->id]) }}'>
                                            <i class="fa fa-info"></i> Detail Produksi
                                        </button>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-sm m-btn--pill btn-hapus"
                                                data-route='{{ route('produksiProgressDelete', ['id'=>$r->id]) }}'
                                                data-message="{{ $message }}">
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4" align="center">
                                <strong>TOTAL</strong>
                            </td>
                            <td align="right" align="right">
                                <strong>{{ $total_progress }}</strong>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="produksi-buttons">
        <a href="{{ route("produksiHistoryPage") }}"
           class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
            <span>
                <i class="la la-angle-double-left"></i>
                <span>Kembali ke History Produksi</span>
            </span>
        </a>
    </div>
@endsection