@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/jurnal_umum.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('akunting.jurnalUmum.trTransaksi')

    <form method="post"
          action="{{ route('jurnalUmumInsert') }}"
          class="form-send"
          data-alert-show="true"
          data-alert-field-message="true"
          data-redirect="{{ route('jurnalUmumPage') }}"
          style="width: 100%">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            {{ csrf_field() }}

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12 required">Tanggal Transaksi</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <input type="text"
                                       class="form-control m-input m_datepicker"
                                       name="jmu_tanggal"
                                       autocomplete="off"
                                       value="{{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12 required">No Invoice</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <input type="text" name="no_invoice" class="form-control" value="{{ $no_invoice }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12 required">Keterangan</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <textarea class="form-control" name="jmu_keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-transaksi btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Data Transaksi
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table table-bordered table-striped table-hover table-transaksi">
                            <thead>
                                <tr>
                                    <th>Kode Perkiraan</th>
                                    <th>Jenis Transaksi</th>
                                    <th>Debet</th>
                                    <th>Kredit</th>
                                    <th>Tipe Arus Kas</th>
                                    <th>Catatan</th>
                                    <th>Menu</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" class="font-weight-bold text-center">
                                        STATUS
                                    </th>
                                    <th  colspan="2" class="text-center th-status-label">
                                        {!! $labelBalance !!}
                                    </th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="target-produksi-buttons">
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Simpan Jurnal Umum</span>
                </span>
            </button>
            <a href="{{ route("jurnalUmumPage") }}"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>
@endsection