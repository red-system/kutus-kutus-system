<form action="{{ route('targetProduksiInsertMonth') }}" method="post" class="m-form form-send m-form--label-align-right">
    {{ csrf_field() }}
    <input type="hidden" name="tahun_target" value="{{ $tahun }}">
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Produk </label>
                            <div class="col-lg-9">
                                <select class="m-select2" name="id_produk" autofocus style="width: 100%;">
                                    @foreach($produk as $r)
                                        <option value="{{ $r->id }}">{{ $r->kode_produk.' '.$r->nama_produk }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Tanggal Target </label>
                            <div class="col-lg-3">
                                <div class="input-group date">
                                    <input type="text"
                                           class="form-control m-input m_datepicker-month"
                                           name="bulan_target"
                                           readonly
                                           value="{{ date('m') }}"/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Target Produksi </label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control m-input touchspin-number" name="qty" value="1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>