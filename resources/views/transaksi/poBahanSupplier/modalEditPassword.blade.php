<form action="" method="post" class="m-form form-send">

    {{ csrf_field() }}

    <div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header btn-success">
                    <h5 class="modal-title m--font-light" id="exampleModalLabel">Login untuk Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-form">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">Username</label>
                        <div class="col-10 col-form-label">
                            <input type="text" name="username" class="form-control" autocomplete="new-usernamae">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">Password</label>
                        <div class="col-10">
                            <input type="password" name="password" class="form-control" autocomplete="new-password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"
                            class="btn btn-success">Edit PO Supplier
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>