<form action="{{ route('supplierInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Supplier </label>
                            <input type="text" class="form-control m-input" name="kode_supplier" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Supplier </label>
                            <input type="text" class="form-control m-input" name="nama_supplier">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Foto Supplier </label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-success btn-file">
                                        Browse Foto <input type="file" class="imgInp" name="foto_supplier" accept="image/*">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                            <img class='img-preview'/>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Telepon Supplier </label>
                            <input type="text" class="form-control m-input" name="telp_supplier">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Email Supplier </label>
                            <input type="text" class="form-control m-input" name="email_supplier">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Fax Supplier </label>
                            <input type="text" class="form-control m-input" name="fax_supplier">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat Supplier </label>
                            <textarea class="form-control m-input" name="alamat_supplier"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>