<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link akses-list_transfer_stok_bahan {{ $tab == 'transfer' ? 'active':'' }}" href="{{ route('transferStokBahanPage') }}">
                    <i class="la la-truck"></i> Transfer Stok Bahan
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link akses-history_transfer {{ $tab == 'history' ? 'active':'' }}" href="{{ route('historyTransferStokBahanPage') }}">
                    <i class="la la-history"></i> History Transfer
                </a>
            </li>
        </ul>
    </div>
</div>