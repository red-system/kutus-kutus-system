@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/produksi.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('produksi.produksi.produksiDetailModal')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('produksiCreatePage') }}"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Produksi</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tabs">
                @include('produksi.produksi.tabTitle')
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Produksi</th>
                            <th>Pabrik</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Status</th>
{{--                            <th>Progress </th>--}}
                            <th>Publish</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            @php
                                if($r->publish == 'yes') {
                                    $publish = '<i class="fa fa-check-circle m--font-success icon-status"></i>';
                                } else {
                                    $publish = '<i class="fa fa-minus-circle m--font-warning icon-status"></i>';
                                }

                                if($r->finish == 'yes') {
                                    $status = '<span class="m-badge m-badge--success m-badge--wide">Completed</span>';
                                } else {
                                    $status = '<span class="m-badge m-badge--warning m-badge--wide">On Progress</span>';
                                }
/*
                            $qty_produksi = \app\Models\mDetailProduksi::where('id_produksi', $r->id)->sum('qty');
                            $qty_progress = \app\Models\mProgressProduksi::where('id_produksi', $r->id)->sum('qty_progress');
                            $persentase = round(($qty_progress/$qty_produksi)*100, 3);*/

                            @endphp
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->kode_produksi }}</td>
                                <td>{{ $r->lokasi->lokasi }}</td>
                                <td>{{ $r->tgl_mulai_produksi }}</td>
                                <td>{{ $r->tgl_selesai_produksi }}</td>
                                <td>{!! $status !!}</td>
{{--                                <td>{{ $persentase }}%</td>--}}
                                <td align="center">{!! $publish !!}</td>
                                <td align="center">
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="dropdown">
                                        <button class="btn btn-accent btn-sm dropdown-toggle"
                                                type="button"
                                                id="dropdownMenuButton"
                                                data-toggle="dropdown"
                                                ria-haspopup="true"
                                                aria-expanded="false">
                                            <i class="fa fa-cog"></i> MENU
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"
                                             aria-labelledby="dropdownMenuButton"
                                             x-placement="bottom-start"
                                             style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);">
                                            @if($r->publish == 'no')
                                                <a class="btn-publish dropdown-item akses-publish"
                                                   href="#"
                                                   data-route="{{ route('produksiPublish', ['id'=>$r->id]) }}">
                                                    <i class="fa fa-check"></i> Publish
                                                </a>
                                            @endif
                                            <a class="btn-produksi-detail dropdown-item akses-detail"
                                               href="#"
                                               data-route='{{ route('produksiDetail', ['id'=>$r->id]) }}'>
                                                <i class="fa fa-info"></i> Detail
                                            </a>
                                            @if($r->publish == 'yes')
                                                <a class="dropdown-item akses-hasil_produksi"
                                                   href="{{ route('produksiProgress', ['id'=>Main::encrypt($r->id)]) }}">
                                                    <i class="fa fa-sync"></i> Hasil Produksi
                                                </a>
                                            @endif

                                            @if($r->publish == 'no')
                                                <a class="dropdown-item akses-edit"
                                                   href="{{ route('produksiEdit', ['id'=>Main::encrypt($r->id)]) }}">
                                                    <i class="fa fa-pencil-alt"></i> Edit
                                                </a>
                                                <div class="dropdown-divider"></div>
                                                <a class="btn-hapus dropdown-item m--font-danger akses-delete"
                                                   data-route="{{ route('produksiDelete', ['id'=>$r->id]) }}"
                                                   href="#">
                                                    <i class="la la-remove"></i> Hapus
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection