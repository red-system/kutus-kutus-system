<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mHistoryPenyesuaianBahan;
use app\Models\mHistoryPenyesuaianProduk;
use app\Models\mHistoryTransferBahan;
use app\Models\mHistoryTransferProduk;
use app\Models\mPenjualanProduk;
use app\Models\mBahan;
use app\Models\mPoBahanPembelianDetail;
use app\Models\mProduk;
use app\Models\mProduksi;
use app\Models\mProgressProduksi;
use app\Models\mStokBahan;
use app\Models\mStokProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\mLokasi;
use Illuminate\Support\Facades\Config;

class Lokasi extends Controller
{

    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_8'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mLokasi::orderBy('lokasi', 'ASC')->get();

        return view('masterData/lokasi/lokasiList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'kode_lokasi' => 'required',
            'lokasi' => 'required',
            'pic' => 'required',
            'alamat' => 'required',
            'telp' => 'required',
            'potongan' => 'required|numeric|min:0|max:100'
        ]);

        $data = $request->except('_token');
        $data['potongan'] = Main::convert_discount($data['potongan']);
        mLokasi::create($data);
    }

    function delete($id)
    {
        $id_lokasi = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $where = [
            'id_lokasi' => $id_lokasi
        ];
        $message = '';
        $list_table = [
            'penjualan_produk',
            'stok_produk',
            'produksi',
            'history_penyesuaian_produk',
            'history_transfer_bahan',
            'history_transfer_produk',
            'history_penyesuaian_bahan',
            'po_bahan_pembelian_detail',
            'stok_bahan',
            'progress_produksi',
            'produk',
            'bahan'
        ];

        $penjualan_produk = mPenjualanProduk::where($where)->count();
        $stok_produk = mStokProduk::where($where)->count();
        $produksi = mProduksi::where($where)->count();
        $history_penyesuaian_produk = mHistoryPenyesuaianProduk::where($where)->count();
        $history_transfer_bahan = mHistoryTransferBahan::where(['id_lokasi_dari'=>$id_lokasi, 'id_lokasi_tujuan'=>$id_lokasi])->count();
        $history_transfer_produk = mHistoryTransferProduk::where(['id_lokasi_dari'=>$id_lokasi, 'id_lokasi_tujuan'=>$id_lokasi])->count();
        $history_penyesuaian_bahan = mHistoryPenyesuaianBahan::where($where)->count();
        $po_bahan_pembelian_detail = mPoBahanPembelianDetail::where($where)->count();
        $stok_bahan = mStokBahan::where($where)->count();
        $progress_produksi = mProgressProduksi::where($where)->count();
        $produk = mProduk::where($where)->count();
        $bahan = mBahan::where($where)->count();

        foreach ($list_table as $table) {
            if ($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch ($check_table_problem) {
            case "penjualan_produk":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.transaksi_1')) . '</strong>';
                break;
            case "stok_produk":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu Stok Produk</strong>';
                break;
            case "produksi":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.produksi_1')) . '</strong>';
                break;
            case "history_penyesuaian_produk":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.inventory_9')) . '</strong>';
                break;
            case "history_transfer_bahan":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.inventory_4')) . '</strong>';
                break;
            case "history_transfer_produk":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.inventory_5')) . '</strong>';
                break;
            case "history_penyesuaian_bahan":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.inventory_8')) . '</strong>';
                break;
            case "po_bahan_pembelian_detail":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.transaksi_5')) . '</strong>';
                break;
            case "stok_bahan":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu Stok Bahan</strong>';
                break;
            case "progress_produksi":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.produksi_1')) . '</strong>';
                break;
            case "produk":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.inventory_2')) . '</strong>';
                break;
            case "bahan":
                $message = 'Tidak bisa dihapus, karena Lokasi ini digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.inventory_1')) . '</strong>';
                break;
        }

        if ($check_status) {
            mLokasi::where('id', $id)->delete();
        } else {
            $response = [
                'title' => 'Perhatian ...',
                'message' => $message
            ];

            return response($response, 422);
        }
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'kode_lokasi' => 'required',
            'lokasi' => 'required',
            'pic' => 'required',
            'alamat' => 'required',
            'telp' => 'required',
            'potongan' => 'required|numeric|min:0|max:100'
        ]);

        $data = $request->except("_token");
        $data['potongan'] = Main::convert_discount($data['potongan']);
        mLokasi::where(['id' => $id])->update($data);
    }
}
