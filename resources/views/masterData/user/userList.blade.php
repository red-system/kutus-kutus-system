@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('masterData/user/userCreate')
    @include('masterData/user/userEdit')


    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Username</th>
                            <th>Nama Karyawan</th>
                            <th>Foto</th>
                            <th>Posisi</th>
                            <th>User Role</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            @php
                                $r->username_edit = $r->username;
                            @endphp
                        <tr>
                            <td align="center">{{ $no++ }}.</td>
                            <td>{{ $r->username}}</td>
                            <td>{{ $r->karyawan->nama_karyawan }}</td>
                            <td><img src="{{ asset('upload/'.$r->karyawan->foto_karyawan) }}" width="{{ $imgWidth }}" class="img-thumbnail"></td>
                            <td>{{ $r->karyawan->posisi_karyawan }}</td>
                            <td>
                                <a href="{{ route('userRoleAkses', ['id'=> Main::encrypt($r->id_user_role)]) }}" target="_blank">
                                    {{ $r->user_role['role_name'] }}
                                </a>
                            </td>
                            <td align="center">
                                <textarea class="row-data hidden">@json($r)</textarea>
                                <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                    <button type="button"
                                            class="m-btn btn btn-success btn-edit akses-edit"
                                            data-route="{{ route('userUpdate', ['id'=>$r->id]) }}"
                                            data-redirect="?table_value={{ $no-1 }}.&table_index=0">
                                        <i class="la la-edit"></i> Edit
                                    </button>
                                    <button type="button"
                                            class="m-btn btn btn-danger btn-hapus akses-delete"
                                            data-route='{{ route('userDelete', ['id'=>$r->id]) }}'>
                                        <i class="la la-remove"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection