<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPiutangLain extends Model
{
    protected $table = 'tb_piutang_lain';
    protected $primaryKey = 'id';
    protected $fillable = [
        'table_id', //tb_distributor, tb_karyawan
        'table_name',
        'id_distributor',
        'id_karyawan',
        'pl_invoice',
        'pl_jatuh_tempo',
        'pl_tanggal',
        'pl_amount',
        'pl_keterangan',
        'pl_status',
        'pl_status2',
        'pl_sisa_amount',
        'master_id_kode_perkiraan',
        'kode_perkiraan',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name',
    ];

    function transaksi() {
        return $this->hasMany(mAcTransaksi::class, 'table_id');
    }

    function karyawan() {
        return $this->belongsTo(mKaryawan::class, 'id_karyawan');
    }

    function distributor() {
        return $this->belongsTo(mDistributor::class, 'id_distributor');
    }

    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }
}
