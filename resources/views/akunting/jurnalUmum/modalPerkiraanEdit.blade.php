<form action="" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Perkiraan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">

                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label required">Parent Data</label>
                                <div class="col-lg-9">
                                    <select name="mst_master_id"
                                            class="form-control selectpicker"
                                            required
                                            data-live-search="true">
                                        <option value="0">Parent Data</option>
                                        @foreach($perkiraan as $r)
                                            <option value="{{ $r['master_id'] }}">
                                                --- {{ $r['mst_kode_rekening'] . ' - ' . $r['mst_nama_rekening'] }}
                                            </option>
                                            @foreach($r['sub1'] as $r1)
                                                <option value="{{ $r1['master_id'] }}">
                                                    ------ {{ $r1['mst_kode_rekening'] . ' - ' . $r1['mst_nama_rekening'] }}
                                                </option>
                                                @foreach($r1['sub2'] as $r2)
                                                    <option value="{{ $r2['master_id'] }}">
                                                        --------- {{ $r2['mst_kode_rekening'] . ' - ' . $r2['mst_nama_rekening'] }}
                                                    </option>
                                                    @foreach($r2['sub3'] as $r3)
                                                        <option value="{{ $r3['master_id'] }}">
                                                            ------------ {{ $r3['mst_kode_rekening'] . ' - ' . $r3['mst_nama_rekening'] }}
                                                        </option>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Kode Rekening </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="mst_kode_rekening">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Nama Rekening </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="mst_nama_rekening">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Posisi </label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="mst_posisi">
                                        <option value="neraca">Neraca</option>
                                        <option value="laba rugi">Laba Rugi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Normal Balance </label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="mst_normal">
                                        <option value="debet">Debet</option>
                                        <option value="kredit">Kredit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Nominal (Rp.) </label>
                                <div class="col-lg-9">
                                    <input type="number" min="0" class="form-control" name="nominal" value="0">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Tipe Laporan </label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="mst_tipe_laporan">
                                        <option value="laba bersih">Laba Bersih</option>
                                        <option value="laba kotor">Laba Kotor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Tipe Nominal </label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="mst_tipe_nominal">
                                        <option value="pendapatan">Pendapatan</option>
                                        <option value="beban">Beban</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Tipe Neraca </label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="mst_neraca_tipe">
                                        <option value="asset">Asset</option>
                                        <option value="liabilitas">Liabilitas</option>
                                        <option value="ekuitas">Ekuitas</option>
                                        <option value="null">Kosongkan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Termasuk Arus Khas </label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="mst_kas_status">
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Termasuk Pembayaran </label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="mst_pembayaran">
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>