@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('inventoryLogistik/produk/produkCreate')
    @include('inventoryLogistik/produk/produkEdit')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Kategori Produk</th>
{{--                            <th>Lokasi</th>--}}
                            <th>Minimal Stok</th>
{{--                            <th>Harga Eceran</th>
                            <th>Harga Grosir</th>
                            <th>Harga Kosinyasi</th>--}}
                            <th>Diskon Persen</th>
                            <th>Total Stok</th>
{{--                            <th>Diskon Nominal</th>--}}
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)

                            @php
                                $total_produk += $r->total_stok;
                            @endphp
                        <tr>
                            <td align="center">{{ $no++ }}.</td>
                            <td>{{ $r->kode_produk }}</td>
                            <td>{{ $r->nama_produk }}</td>
                            <td>{{ $r->kategori_produk['kategori_produk'] }}</td>
{{--                            <td>{{ $r->lokasi->lokasi }}</td>--}}
                            <td>{{ Main::format_number($r->minimal_stok) }}</td>
{{--                            <td>{{ Main::format_money($r->harga_eceran) }}</td>
                            <td>{{ Main::format_money($r->harga_grosir) }}</td>
                            <td>{{ Main::format_money($r->harga_kosinyasi) }}</td>--}}
                            <td>{{ Main::format_discount($r->disc_persen) }}</td>
                            <td class="text-right">{{ Main::format_number($r->total_stok) }}</td>
{{--                            <td>{{ $r->disc_nominal }}</td>--}}
                            <td align="center">
                                <textarea class="row-data hidden">@json($r)</textarea>
                                <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                    <a href="{{ route('hargaProdukPage', ['idProduk'=>Main::encrypt($r->id)]) }}"
                                       class="m-btn btn btn-accent akses-harga">
                                        <i class="la la-usd"></i> Harga
                                    </a>
                                    <a href="{{ route('stokPage', ['idProduk'=>Main::encrypt($r->id)]) }}"
                                        class="m-btn btn btn-primary akses-stok">
                                        <i class="la la-dropbox"></i> Stok
                                    </a>
                                    <button type="button"
                                            class="m-btn btn btn-success btn-edit akses-edit"
                                            data-route="{{ route('produkUpdate', ['id'=>$r->id]) }}"
                                            data-redirect="?table_value={{ $no-1 }}.&table_index=0">
                                        <i class="la la-edit"></i> Edit
                                    </button>
                                    <button type="button"
                                            class="m-btn btn btn-danger btn-hapus akses-delete"
                                            data-route='{{ route('produkDelete', ['id'=>$r->id]) }}'>
                                        <i class="la la-remove"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="6" class="text-center">
                                    <strong>TOTAL</strong>
                                </th>
                                <th class="text-right">
                                    <strong>{{ Main::format_number($total_produk) }}</strong>
                                </th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection