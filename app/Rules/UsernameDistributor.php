<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;

use app\Models\mDistributor;

class UsernameDistributor implements Rule
{

    protected $username;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->username = $value;
        $check = mDistributor::where('username', $value)->count();
        if($check > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Username \"<strong>".$this->username."</strong>\" not available";
    }
}
