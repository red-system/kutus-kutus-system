<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;
use app\Models\mStokProduk;
use app\Models\mProduk;
use app\Models\mLokasi;

class StokProdukLokasiUpdate implements Rule
{
    private $id_stok_produk;
    private $id_lokasi;
    private $id_produk;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_stok_produk, $id_lokasi)
    {
        $this->id_stok_produk = $id_stok_produk;
        $this->id_lokasi = $id_lokasi;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id_lokasi_update = $value;
        $id_lokasi_awal = mStokProduk::select('id_lokasi')->where('id', $this->id_stok_produk)->first()->id_lokasi;
        $id_produk = mStokProduk::select('id_produk')->where('id', $this->id_stok_produk)->first()->id_produk;
        $this->id_produk = $id_produk;

        //echo $id_lokasi_awal.' = '.$id_lokasi_update.' = '.$this->id_stok_produk ;

        if($id_lokasi_awal == $id_lokasi_update) {
            return TRUE;
        } else {
            $check = mStokProduk
                ::where('id_lokasi', '!=', $id_lokasi_awal)
                ->where([
                    'id_lokasi'=>$id_lokasi_update,
                    'id_produk'=>$id_produk
                ])
                ->count();

            if($check == 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $produk = mProduk::find($this->id_produk);
        $lokasi = mLokasi::find($this->id_lokasi);
        return 'Produk <strong>'.$produk->nama_produk.'</strong> sudah ada pada Gudang <strong>'.$lokasi->lokasi.'</strong>';
    }
}
