<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>Laporan Penjualan Detail</h3></td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <table width="100%" class="table-header">
                <tbody>
                <tr>
                    <td>Tanggal Penjualan: {{ $date_start.' s/d '.$date_end }}</td>
                </tr>
                </tbody>
            </table>
            <br/>
            <div id="table">
                <table>
                    <thead>
                    <tr class="tabletitle">
                        <th class="item">No Faktur</th>
                        <th class="item">Tanggal</th>
                        <th class="item">Jenis Transaksi</th>
                        <th class="item">Distributor</th>
                        <th class="item">Nama Produk</th>
                        <th class="item">Gudang</th>
                        <th class="item">No Seri Produk</th>
                        <th class="item">Harga</th>
                        <th class="item">Potongan</th>
                        <th class="item">PPN({{ $ppnPersen }}%)</th>
                        <th class="item">Harga Net</th>
                        <th class="item">Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $r)
                        @php($total += $r->sub_total)
                        <tr class="service">
                            <td class="tableitem"><p class="itemtext">{{ $r->no_faktur }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_date($r->tanggal) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ ucwords($r->jenis_transaksi) }}</p></td>
                            <td class="tableitem"><p
                                        class="itemtext">{{ '('.$r->kode_distributor.') '.$r->nama_distributor }}</p>
                            </td>
                            <td class="tableitem"><p
                                        class="itemtext">{{ '('.$r->produk->kode_produk.') '.$r->produk->nama_produk }}</p>
                            </td>
                            <td class="tableitem"><p
                                        class="itemtext">{{ '('.$r->lokasi['kode_lokasi'].') '.$r->lokasi['lokasi'] }}</p>
                            </td>
                            <td class="tableitem"><p class="itemtext">{{ $r->stok_produk['no_seri_produk'] }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_number($r->harga) }}</p></td>
                            <td class="tableitem" align="right"><p<td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_number($r->potongan) }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_number($r->ppn_nominal) }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_number($r->harga_net) }}</p></td>
                            <td class="tableitem" align="right"><p
                                        class="itemtext">{{ Main::format_number($r->sub_total) }}</p></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tr>
                        <td colspan="11" align="center">
                            <strong>TOTAL</strong>
                        </td>
                        <td align="right">{{ Main::format_number($total) }}</td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>