<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mBahanProduksi extends Model
{

    protected $table = 'tb_bahan_produksi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produksi',
        'id_bahan',
        'id_satuan',
        'qty_diperlukan',
        'gudang_qty',
        'qty'
    ];

    function produksi() {
        return $this->belongsTo(mProduksi::class, 'id_produksi');
    }

    function bahan() {
        return $this->belongsTo(mBahan::class, 'id_bahan');
    }

    function satuan() {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    function bahan_produksi() {
        return $this->belongsTo(mBahanProduksi::class, 'id');
    }
}
