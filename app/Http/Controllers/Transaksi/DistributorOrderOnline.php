<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use app\Models\mLokasi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\mTargetProduksi;
use app\Models\mBahan;
use app\Models\mSupplier;

use app\Models\mDistributorOrder;
use app\Models\mDistributorOrderProduk;
use app\Models\mDistributor;
use app\Models\mAcMaster;
use app\Models\mPenjualan;
use app\Models\mPenjualanProduk;
use app\Models\mPenjualanPembayaran;
use app\Models\mProduk;
use app\Models\mHargaProduk;

class DistributorOrderOnline extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['transaksi_9'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_9'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mDistributorOrder
            ::withCount([
                'distributor_order_produk AS total_qty' => function ($query) {
                    $query->select(DB::raw("SUM(qty_order) AS qty_sum"));
                }
            ])
            ->wherein('status', ['order', 'process', 'reject', 'konfirm_admin'])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'list'
        ]);

        return view('transaksi/distributorOrderOnline/distributorOrderOnlineList', $data);
    }

    function detail($id)
    {
        $id_distributor_order = Main::decrypt($id);
        $distributor_order = mDistributorOrder
            ::with('distributor')
            ->where('id', $id_distributor_order)
            ->first();
        $distributor_order_produk = mDistributorOrderProduk
            ::with('produk')
            ->where('id_distributor_order', $id_distributor_order)
            ->get();

        $data = [
            'id_distributor_order' => $id_distributor_order,
            'distributor_order' => $distributor_order,
            'distributor_order_produk' => $distributor_order_produk
        ];

        return view('transaksi/distributorOrderOnline/modalBodyDetail', $data);
    }

    function qty_konfirm(Request $request)
    {
        $id_distributor_order = $request->input('id_distributor_order');
        $qty_konfirm_arr = $request->input('qty_konfirm');

        DB::beginTransaction();

        $response = mDistributorOrder
            ::where('id', $id_distributor_order)
            ->update([
                'status' => 'konfirm_admin',
                'konfirm_admin_at' => $this->datetime
            ]);
        !$response ? DB::rollBack() : NULL;

        foreach ($qty_konfirm_arr as $id_distributor_order_produk => $qty_konfirm) {
            $response = mDistributorOrderProduk::where('id', $id_distributor_order_produk)->update(['qty_konfirm' => $qty_konfirm]);
            !$response ? DB::rollBack() : NULL;
        }

        DB::commit();


    }

    function konfirmasi()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $list = mDistributorOrder
            ::whereIn('status', ['konfirm_distributor'])
            ->withCount([
                'distributor_order_produk AS total_qty_order' => function ($query) {
                    $query->select(DB::raw("SUM(qty_order) AS qty_sum"));
                }
            ])
            ->withCount([
                'distributor_order_produk AS total_qty_konfirm' => function ($query) {
                    $query->select(DB::raw("SUM(qty_konfirm) AS qty_sum"));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'konfirmasi'
        ]);

        return view('transaksi/distributorOrderOnline/distributorOrderOnlineKonfirm', $data);
    }

    function done()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $list = mDistributorOrder
            ::whereIn('status', ['done'])
            ->withCount([
                'distributor_order_produk AS total_qty_order' => function ($query) {
                    $query->select(DB::raw("SUM(qty_order) AS qty_sum"));
                }
            ])
            ->withCount([
                'distributor_order_produk AS total_qty_konfirm' => function ($query) {
                    $query->select(DB::raw("SUM(qty_konfirm) AS qty_sum"));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'done'
        ]);

        return view('transaksi/distributorOrderOnline/distributorOrderOnlineDone', $data);
    }

    function penjualan($id)
    {
        $id_distributor_order = Main::decrypt($id);
        $breadcrumb = [
            [
                'label' => 'Edit Penjualan Produk',
                'route' => ''
            ]
        ];
        $pageTitle = 'Distributor Order Online Penjualan';
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);

        $distributor_order = mDistributorOrder
            ::with('distributor')
            ->where('id', $id_distributor_order)
            ->first();
        $distributor_order_produk_process = [];
        $distributor_order_produk = mDistributorOrderProduk
            ::with('produk')
            ->where('id_distributor_order', $id_distributor_order)
            ->get();
        $ppn_persen = Config::get('constants.ppnPersen');
        $total = 0;
        $biaya_tambahan = 0;
        $potongan_akhir = 0;
        $terbayar = 0;
        $sisa_pembayaran = 0;
        $kembalian = 0;
        $urutan = $this->urutan_penjualan();
        $no_faktur = $this->no_faktur($distributor_order->id_distributor);

        foreach ($distributor_order_produk as $index => $r) {
            $harga = $this->harga_produk($r->id_produk, $r->qty_konfirm);
            $ppn_nominal = $harga * $ppn_persen;
            $harga_net = $harga + $ppn_nominal;
            $sub_total = $harga_net * $r->qty_konfirm;
            $total += $sub_total;

            $distributor_order_produk_process[$index] = $r;
            $distributor_order_produk_process[$index]['harga'] = $harga;
            $distributor_order_produk_process[$index]['ppn_nominal'] = $ppn_nominal;
            $distributor_order_produk_process[$index]['harga_net'] = $harga_net;
            $distributor_order_produk_process[$index]['sub_total'] = $sub_total;
            $distributor_order_produk_process[$index]['potongan'] = 0;
        }

        //return $distributor_order_produk_process;

        $grand_total = $total;
        $sisa_pembayaran = $total;

        $gudang = mLokasi::where(['tipe' => 'gudang'])->orderBy('lokasi', 'ASC')->get();
        $master = mAcMaster::whereIn('mst_kode_rekening', [1101, 1301, 1302, 1303, 1305, 519006])->get();
        $produk = mProduk
            ::withCount([
                'stok_produk AS stok_produk' => function ($query) {
                    //$query->where('qty', '<', 400);
                    $query->select(DB::raw("SUM(qty) AS qty_sum"));
                }
            ])
            ->orderBy('kode_produk', 'ASC')
            ->get();

        $data = array_merge($data, [
            'produk' => $produk,
            'gudang' => $gudang,
            'master' => $master,
            'urutan' => $urutan,

            'id_distributor_order' => $id_distributor_order,
            'pageTitle' => $pageTitle,
            'no_faktur' => $no_faktur,
            'distributor_order' => $distributor_order,
            'distributor_order_produk' => $distributor_order_produk_process,
            'total' => $total,
            'biaya_tambahan' => $biaya_tambahan,
            'potongan_akhir' => $potongan_akhir,
            'grand_total' => $grand_total,
            'terbayar' => $terbayar,
            'sisa_pembayaran' => $sisa_pembayaran,
            'kembalian' => $kembalian,
            'tab' => 'konfirmasi'
        ]);

        return view('transaksi/distributorOrderOnline/distributorOrderOnlinePenjualan', $data);

    }

    function harga_produk($id_produk, $qty)
    {
        $harga = 0;

        $hargaProduk = mHargaProduk
            ::where([
                ['id_produk', '=', $id_produk],
                //['rentang_awal', '>=', $qty],
                //['rentang_akhir', '<=', $qty]
            ])
            ->get();
        foreach ($hargaProduk as $r) {
            if ($qty >= $r->rentang_awal && $qty <= $r->rentang_akhir) {
                $harga = $r->harga;
                break;
            }
        }

        return $harga;
    }

    function urutan_penjualan()
    {
        $count = mPenjualan::count();
        if ($count == 0) {
            return 1;
        } else {
            $urutan = mPenjualan::orderBy('urutan', 'DESC')->first(['urutan']);
            return $urutan->urutan + 1;
        }
    }


    function no_faktur($id_distributor)
    {
        $kode_distributor = mDistributor::where('id', $id_distributor)->value('kode_distributor');
        $year = date('Y');
        $month = date('m');

        $urutan = mPenjualan
            ::where('id_distributor', $id_distributor)
            ->whereYear('tanggal', $year)
            ->whereMonth('tanggal', $month);
        $urutan_next = $urutan->count() > 0 ? $urutan->first(['urutan'])->urutan + 1 : 1;

        $no_faktur = $kode_distributor
            . '.' . str_pad($urutan_next, 2, "0", STR_PAD_LEFT)
            . '.' . $month
            . '.' . $year;

        return $no_faktur;

    }
}
