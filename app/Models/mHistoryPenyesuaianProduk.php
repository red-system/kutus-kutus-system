<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHistoryPenyesuaianProduk extends Model
{

    protected $table = 'tb_history_penyesuaian_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produk',
        'id_kategori_produk',
        'id_lokasi',
        'id_stok_produk',
        'tgl',
        'kode_produk',
        'nama_produk',
        'no_seri_produk',
        'kategori_produk',
        'lokasi',
        'qty_awal',
        'qty_akhir',
        'keterangan'
    ];

    function produk() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }

    function kategori_produk() {
        return $this->belongsTo(mKategoriProduk::class, 'id_kategori_produk');
    }

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }
}
