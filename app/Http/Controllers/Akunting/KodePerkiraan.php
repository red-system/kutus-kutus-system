<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mAcTransaksi;
use app\Models\mHutangLain;
use app\Models\mHutangSupplier;
use app\Models\mPenjualanPembayaran;
use app\Models\mPiutangLain;
use app\Models\mPiutangPelanggan;
use app\Models\mPoBahanPembelianPembayaran;
use app\Models\mAsset;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Models\mPerkiraan;
use app\Models\mDetailPerkiraan;
use app\Models\mAcMaster;

use DB,
    PDF;

class KodePerkiraan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);
        $perkiraan = [];

        $data1 = mPerkiraan::where('mst_master_id', 0)
            ->orderBy('mst_kode_rekening', 'ASC')->get();
        foreach ($data1 as $k => $r) {
            $r->detail;
            $sub1 = mPerkiraan::where('mst_master_id', $r->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
            $perkiraan[$k] = $r;
            $perkiraan[$k]['sub1'] = $sub1;
            foreach ($sub1 as $k1 => $r1) {
                $r1->detail;
                $sub2 = mPerkiraan::where('mst_master_id', $r1->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                $perkiraan[$k]['sub1'][$k1]['sub2'] = $sub2;
                foreach ($sub2 as $k2 => $r2) {
                    $r2->detail;
                    $sub3 = mPerkiraan::where('mst_master_id', $r2->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                    $perkiraan[$k]['sub1'][$k1]['sub2'][$k2]['sub3'] = $sub3;
                    foreach ($sub3 as $k3 => $r3) {
                        $r3->detail;
                    }
                }
            }
        }

        $data = array_merge($data, [
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
            'perkiraan' => $perkiraan
        ]);

        return view('akunting/kodePerkiraan/kodePerkiraanList', $data);
    }

    function rules($request)
    {
        $rules = [
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
        ];
        $customeMessage = [
            'required' => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request)
    {
        $request->validate([
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
        ]);

        $data_master = [
            'mst_master_id' => $request->input('mst_master_id'),
            'mst_kode_rekening' => $request->input('mst_kode_rekening'),
            'mst_nama_rekening' => $request->input('mst_nama_rekening'),
            'mst_posisi' => $request->input('mst_posisi'),
            'mst_normal' => $request->input('mst_normal'),
            'mst_tipe_laporan' => $request->input('mst_tipe_laporan'),
            'mst_tipe_nominal' => $request->input('mst_tipe_nominal'),
            'mst_neraca_tipe' => $request->input('mst_neraca_tipe'),
            'mst_kas_status' => $request->input('mst_kas_status'),
            'mst_pembayaran' => $request->input('mst_pembayaran'),
            'mst_tanggal_awal' => date('d/m/Y')
        ];

        DB::beginTransaction();

        /**
         * Transaction #1
         */
        $master = mPerkiraan::create($data_master);
        !$master ? DB::rollback() : NULL;

        $master_id = $master->master_id;
        if ($request->input('mst_normal') == 'debet') {
            $msd_awal_kredit = 0;
            $msd_awal_debet = $request->input('nominal');
        } else {
            $msd_awal_kredit = $request->input('nominal');
            $msd_awal_debet = 0;
        }

        $data_master_detail = [
            'master_id' => $master_id,
            'msd_year' => date('Y'),
            'msd_month' => date('j'),
            'msd_awal_kredit' => $msd_awal_kredit,
            'msd_awal_debet' => $msd_awal_debet
        ];

        /**
         * Transaction #2
         */
        $master_detail = mDetailPerkiraan::create($data_master_detail);
        !$master_detail ? DB::rollback() : NULL;
        DB::commit();

    }

    function edit($master_id = '')
    {
        $response = [
            'action' => route('perkiraanUpdate', ['kode' => $master_id]),
            'field' => mPerkiraan::find($master_id)
        ];
        return $response;
    }

    function update(Request $request, $master_id)
    {
        $request->validate([
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
        ]);

        $data_master = [
            'mst_master_id' => $request->input('mst_master_id'),
            'mst_kode_rekening' => $request->input('mst_kode_rekening'),
            'mst_nama_rekening' => $request->input('mst_nama_rekening'),
            'mst_posisi' => $request->input('mst_posisi'),
            'mst_normal' => $request->input('mst_normal'),
            'mst_tipe_laporan' => $request->input('mst_tipe_laporan'),
            'mst_tipe_nominal' => $request->input('mst_tipe_nominal'),
            'mst_neraca_tipe' => $request->input('mst_neraca_tipe'),
            'mst_kas_status' => $request->input('mst_kas_status'),
            'mst_pembayaran' => $request->input('mst_pembayaran'),
            'mst_tanggal_awal' => date('d/m/Y')
        ];

        DB::beginTransaction();

        $transaction = mPerkiraan::where('master_id', $master_id)->update($data_master);
        !$transaction ? DB::rollback() : NULL;

        $month = date('j');
        $year = date('Y');

        $detail = mDetailPerkiraan
            ::where([
                'master_id' => $master_id,
                'msd_year' => $year,
                'msd_month' => $month
            ])
            ->first();


        if ($request->input('mst_normal') == 'debet') {
            $msd_awal_kredit = 0;
            $msd_awal_debet = $request->nominal;
        } else {
            $msd_awal_kredit = $request->nominal;
            $msd_awal_debet = 0;
        }
        $data_detail = [
            'msd_year' => $year,
            'msd_month' => $month,
            'msd_awal_kredit' => $msd_awal_kredit,
            'msd_awal_debet' => $msd_awal_debet
        ];

        if ($detail == NULL) {
            $data_detail['master_id'] = $master_id;


        }


        $transaction = mDetailPerkiraan::create($data_detail);
        !$transaction ? DB::rollback() : NULL;

        DB::commit();
    }

    function delete($master_id)
    {
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $list_table = [
            'asset_asset',
            'asset_akumulasi',
            'asset_depresiasi',
            'asset_jenis_pembayaran',
            'hutang_lain_kredit',
            'hutang_lain_debet',
            'po_bahan_pembelian_pembayaran',
            'penjualan_pembayaran',
            'transaksi',
            'piutang_pelanggan',
            'piutang_lain',
            'hutang_supplier',
        ];

        $asset_asset = mAsset::where(['master_id_asset' => $master_id])->count();
        $asset_akumulasi = mAsset::where(['master_id_akumulasi' => $master_id])->count();
        $asset_depresiasi = mAsset::where(['master_id_depresiasi' => $master_id])->count();
        $asset_jenis_pembayaran = mAsset::where(['master_id_jenis_pembayaran' => $master_id])->count();
        $hutang_lain_kredit = mHutangLain::where(['master_id_kredit' => $master_id])->count();
        $hutang_lain_debet = mHutangLain::where(['master_id_debet' => $master_id])->count();
        $po_bahan_pembelian_pembayaran = mPoBahanPembelianPembayaran::where(['master_id' => $master_id])->count();
        $penjualan_pembayaran = mPenjualanPembayaran::where(['master_id' => $master_id])->count();
        $transaksi = mAcTransaksi::where(['master_id' => $master_id])->count();
        $piutang_pelanggan = mPiutangPelanggan::where(['master_id_kode_perkiraan' => $master_id])->count();
        $piutang_lain = mPiutangLain::where(['master_id_kode_perkiraan' => $master_id])->count();
        $hutang_supplier = mHutangSupplier::where(['master_id_kode_perkiraan' => $master_id])->count();

        foreach ($list_table as $table) {
            if ($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch ($check_table_problem) {
            case "asset_asset":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.laporanA_5')) . ' / Asset</strong>';
                break;
            case "asset_akumulasi":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.laporanA_5')) . ' / Akumulasi</strong>';
                break;
            case "asset_depresiasi":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.laporanA_5')) . ' / Depresiasi</strong>';
                break;
            case "asset_jenis_pembayaran":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.laporanA_5')) . ' / Jenis Pembayaran</strong>';
                break;
            case "hutang_lain_kredit":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.hutang_2')) . ' / Bagian Kredit</strong>';
                break;
            case "hutang_lain_debet":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.hutang_2')) . ' / Bagian Debet</strong>';
                break;
            case "po_bahan_pembelian_pembayaran":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.transaksi_5')) . ' / Bagian Pembelian</strong>';
                break;
            case "penjualan_pembayaran":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.transaksi_1')) . ' / Bagian Pembayaran</strong>';
                break;
            case "transaksi":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Seluruh Transaksi Debet dan Kredit</strong>';
                break;
            case "piutang_pelanggan":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.hutang_3')) . '</strong>';
                break;
            case "piutang_lain":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.hutang_4')) . '</strong>';
                break;
            case "hutang_supplier":
                $message = 'Tidak bisa dihapus, karena Kode Perkiraan digunakan di <strong>Menu ' . Main::menuAction(Config::get('constants.topMenu.hutang_1')) . '</strong>';
                break;
        }

        if ($check_status) {
            mAcMaster::where('master_id', $master_id)->delete();
        } else {
            $response = [
                'title' => 'Perhatian ...',
                'message' => $message
            ];
            return response($response, 422);
        }
        // mPerkiraan::where('mst_kode_rekening', $master_id)->delete();
        // mPerkiraanDetail::where('master_id', $master_id)->delete();
    }

    function pdf()
    {

        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);
        $perkiraan = [];

        $data1 = mPerkiraan::where('mst_master_id', 0)
            ->orderBy('mst_kode_rekening', 'ASC')->get();
        foreach ($data1 as $k => $r) {
            $r->detail;
            $sub1 = mPerkiraan::where('mst_master_id', $r->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
            $perkiraan[$k] = $r;
            $perkiraan[$k]['sub1'] = $sub1;
            foreach ($sub1 as $k1 => $r1) {
                $r1->detail;
                $sub2 = mPerkiraan::where('mst_master_id', $r1->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                $perkiraan[$k]['sub1'][$k1]['sub2'] = $sub2;
                foreach ($sub2 as $k2 => $r2) {
                    $r2->detail;
                    $sub3 = mPerkiraan::where('mst_master_id', $r2->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                    $perkiraan[$k]['sub1'][$k1]['sub2'][$k2]['sub3'] = $sub3;
                    foreach ($sub3 as $k3 => $r3) {
                        $r3->detail;
                    }
                }
            }
        }

        $data = [
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
            'perkiraan' => $perkiraan,
            'company'=>Main::companyInfo()
        ];
        $pdf = PDF::loadView('akunting/kodePerkiraan/kodePerkiraanPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Kode Perkiraan ' . date('d-m-Y') . '.pdf');
    }

    function excel()
    {
        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);
        $perkiraan = [];

        $data1 = mPerkiraan::where('mst_master_id', 0)
            ->orderBy('mst_kode_rekening', 'ASC')->get();
        foreach ($data1 as $k => $r) {
            $r->detail;
            $sub1 = mPerkiraan::where('mst_master_id', $r->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
            $perkiraan[$k] = $r;
            $perkiraan[$k]['sub1'] = $sub1;
            foreach ($sub1 as $k1 => $r1) {
                $r1->detail;
                $sub2 = mPerkiraan::where('mst_master_id', $r1->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                $perkiraan[$k]['sub1'][$k1]['sub2'] = $sub2;
                foreach ($sub2 as $k2 => $r2) {
                    $r2->detail;
                    $sub3 = mPerkiraan::where('mst_master_id', $r2->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                    $perkiraan[$k]['sub1'][$k1]['sub2'][$k2]['sub3'] = $sub3;
                    foreach ($sub3 as $k3 => $r3) {
                        $r3->detail;
                    }
                }
            }
        }

        $data = [
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
            'perkiraan' => $perkiraan,
            'company'=>Main::companyInfo()
        ];
        return view('akunting/kodePerkiraan/kodePerkiraanExcel', $data);
    }


}