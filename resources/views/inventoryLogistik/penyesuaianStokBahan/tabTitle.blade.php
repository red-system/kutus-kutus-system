<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item akses-penyesuaian_stok_bahan">
                <a class="nav-link m-tabs__link {{ $tab == 'penyesuaian' ? 'active':'' }}" href="{{ route('penyesuaianStokBahanPage') }}">
                    <i class="la la-refresh"></i> Penyesuaian Stok Bahan
                </a>
            </li>
            <li class="nav-item m-tabs__item akses-history">
                <a class="nav-link m-tabs__link {{ $tab == 'history' ? 'active':'' }}" href="{{ route('historyPenyesuaianStokBahanPage') }}">
                    <i class="la la-history"></i> History Penyesuaian Stok Bahan
                </a>
            </li>
        </ul>
    </div>
</div>