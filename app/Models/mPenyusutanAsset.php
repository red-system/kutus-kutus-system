<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPenyusutanAsset extends Model
{

    protected $table = 'tb_penyusutan_asset';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_asset',
        'penyusutan_perbulan',
        'bulan',
        'tahun',
    ];

    function asset() {
        return $this->belongsTo(mAsset::class, 'id_asset');
    }
}
