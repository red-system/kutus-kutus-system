<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;
use app\Models\mTargetProduksi;
use app\Models\mProduk;

class TargetProduksiInsert implements Rule
{

    private $bulan_target;
    private $tahun_target;
    private $id_produk;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($bulan_target, $tahun_target)
    {
        $this->bulan_target = $bulan_target;
        $this->tahun_target = $tahun_target;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->id_produk = $value;
        $where = [
            'id_produk'=>$value,
            'bulan_target'=>$this->bulan_target,
            'tahun_target'=>$this->tahun_target
        ];
        $check = mTargetProduksi::where($where)->count();
        if($check == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $bulan = $this->bulan_target;
        $tahun = $this->tahun_target;
        $produk = mProduk::find($this->id_produk);

        return 'Produk '.$produk->kode_produk.' '.$produk->nama_produk.' sudah ada di Bulan : '.$bulan.' Tahun : '.$tahun;
    }
}
