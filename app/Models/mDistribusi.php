<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mDistribusi extends Model
{
    protected $table = 'tb_distribusi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'tanggal_distribusi',
        'urutan',
        'no_distribusi',
        'catatan',
        'no_polisi',
        'alamat_lain'
    ];

    function penjualan() {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    function distribusi_detail() {
        return $this->hasMany(mDistribusiDetail::class, 'id_distribusi', 'id');
    }
}
