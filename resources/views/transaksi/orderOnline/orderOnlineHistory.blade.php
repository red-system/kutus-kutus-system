@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/distributor_order.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('transaksi.orderOnline.modalDetail')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tabs">
                @include('transaksi.orderOnline.tabTitle')
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>No Order</th>
                            <th>Tanggal</th>
                            <th>Total Qty Order</th>
                            <th>Total Qty Konfirm</th>
                            <th>Status</th>
                            <th width="150">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $r->no_order }}</td>
                                <td>{{ $r->tanggal }}</td>
                                <td>{{ Main::format_number($r->total_qty_order) }}</td>
                                <td>{{ Main::format_number($r->total_qty_konfirm) }}</td>
                                <td>{!! Main::distributor_order_status($r->status) !!}</td>
                                <td>
                                    <button type="button"
                                            class="btn-detail m-btn btn btn-primary m-btn--pill btn-sm"
                                            data-route="{{ route('orderOnlineDetailBaru', ['id'=>Main::encrypt($r->id)]) }}">
                                        <i class="la la-eye"></i> Detail
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection