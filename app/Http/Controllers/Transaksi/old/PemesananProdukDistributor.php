<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Models\mTargetProduksi;
use app\Models\mProduk;

class PemesananProdukDistributor extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_4'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);

        return view('transaksi/pemesananProdukDistributor/pemesananProdukDistributorList', $data);
    }

}