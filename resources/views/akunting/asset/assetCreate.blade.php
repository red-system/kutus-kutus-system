@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/asset.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <form method="post"
          action="{{ route('assetInsert') }}"
          class="form-send"
          data-alert-show="true"
          data-alert-field-message="true"
          data-redirect="{{ route('assetPage') }}"
          style="width: 100%">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            {{ csrf_field() }}

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Kode Asset</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="kode_asset" class="form-control"
                                               value="{{ $kode_asset }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Nama</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="nama" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Kategori Asset</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <select name="id_kategori_asset" class="form-control m-select2">
                                            <option value="">Pilih Kategori Asset</option>
                                            @foreach($kategori_asset as $r)
                                                <option value="{{ $r->id }}" data-penyusutan-status="{{ $r->penyusutan_status }}">{{ $r->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Tanggal Beli</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker tanggal_beli"
                                                   name="tanggal_beli" readonly="" value="">
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                        <input type="hidden" class="form-control m-input m_datepicker tanggal_beli"
                                               name="tanggal_beli_2" readonly="" value="">
                                        <input class="form-control date-picker" size="16" type="hidden"
                                               name="tanggal_sekarang" id="edit_tanggal_sekarang" value="{{ date('d-m-Y') }}"/>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Qty</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="qty" class="form-control touchspin-number" value="0">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Harga Beli</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="harga_beli" class="form-control touchspin-number" value="0">
                                        <input type="hidden" name="harga_beli_2" class="form-control" value="0">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Provisi</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="provisi" class="form-control touchspin-number" value="0">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Total Beli</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="total_beli" class="form-control touchspin-number" value="0" readonly>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Nilai Residu</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="nilai_residu" class="form-control touchspin-number" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Umur Ekonomis</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control touchspin-number" name="umur_ekonomis" value="0">
                                            <div class="input-group-append">
                                                <button class="btn btn-default" type="button">Tahun</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Lokasi</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="lokasi" class="form-control">
                                        <br/>
                                        <label class="m-checkbox m-checkbox--state-success">
                                            <input type="checkbox" name="tanggal_cek">
                                            <small>Tanggal Perolehan diatas tanggal 15 dibebankan bulan berikutnya.
                                            </small>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Metode</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="text" name="metode" class="form-control" value="Garis Lurus">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Tanggal Pensiun</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker"
                                                   name="tanggal_pensiun" readonly="" value="">
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Akumulasi Beban</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="number" name="akumulasi_beban" class="form-control" step="{{ $decimalStep }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Terhitung Tanggal</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker"
                                                   name="terhitung_tanggal" readonly="" value="">
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Nilai Buku</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="number" name="nilai_buku" class="form-control" step="{{ $decimalStep }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-4 col-sm-12 required">Beban Perbulan</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <input type="number" name="beban_perbulan" class="form-control" step="{{ $decimalStep }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <h4 class="m-portlet__head-text">
                                Kode Akun
                            </h4>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12 required">Asset Harta</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <select name="master_id_asset" class="form-control m-select2" data-p>
                                    <option value="">Pilih Asset Harta</option>
                                    {!! $kode_perliraan_option_list !!}
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Akumulasi Depresiasi</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <select name="master_id_akumulasi" class="form-control m-select2">
                                    <option value="">Pilih Akumulasi Depresiasi</option>
                                    {!! $kode_perliraan_option_list !!}
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12 required">Depresiasi</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <select name="master_id_depresiasi" class="form-control m-select2">
                                    <option value="">Pilih Depresiasi</option>
                                    {!! $kode_perliraan_option_list !!}
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12 required">Jenis Pembayaran</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <select name="master_id_jenis_pembayaran" class="form-control m-select2">
                                    <option value="">Pilih Jenis Pembayaran</option>
                                    {!! $kode_perliraan_option_list !!}
                                </select>
                            </div>
                        </div>
                        <br /><br />
                    </div>
                </div>
            </div>

        </div>

        <div class="target-produksi-buttons">
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Simpan Data Asset</span>
                </span>
            </button>
            <a href="{{ route("assetPage") }}"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>
@endsection