@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('inventoryLogistik/bahan/bahanCreate')
    @include('inventoryLogistik/bahan/bahanEdit')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Bahan</th>
                            <th>Nama Bahan</th>
                            {{--<th>Qty</th>--}}
{{--                            <th>Harga</th>--}}
                            <th>Kategori Bahan</th>
                            {{--<th>Lokasi</th>--}}
                            <th>Satuan</th>
                            <th>Supplier</th>
                            <th>Minimal Stok</th>
                            <th>Stok Sekarang</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)

                            @php
                                $total_bahan += $r->qty_total
                            @endphp
                        <tr>
                            <td align="center">{{ $no++ }}.</td>
                            <td>{{ $r->kode_bahan }}</td>
                            <td>{{ $r->nama_bahan }}</td>
                            {{--<td>{{ Main::format_number($r->qty) }}</td>--}}
{{--                            <td>{{ Main::format_money($r->harga) }}</td>--}}
                            <td>{{ $r->kategori_bahan->kategori_bahan }}</td>
{{--                            <td>{{ $r->lokasi->lokasi }}</td>--}}
                            <td>{{ $r->satuan->satuan }}</td>
                            <td>{{ $r->supplier->nama_supplier }}</td>
                            <td class="text-right">{{ Main::format_number($r->minimal_stok) }}</td>
                            <td class="text-right">{{ Main::format_number($r->qty_total)}}</td>
                            <td align="center">
                                <textarea class="row-data hidden">@json($r)</textarea>
                                <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm akses-stok">
                                    <a href="{{ route('stokBahanPage', ['idBahan'=>Main::encrypt($r->id)]) }}"
                                       class="m-btn btn btn-primary">
                                        <i class="la la-dropbox"></i> Stok
                                    </a>
                                    <button type="button"
                                            class="m-btn btn btn-success btn-edit akses-edit"
                                            data-route="{{ route('bahanUpdate', ['kode'=>$r->id]) }}"
                                            data-redirect="?table_value={{ $no-1 }}.&table_index=0">
                                        <i class="la la-edit"></i> Edit
                                    </button>
                                    <button type="button"
                                            class="m-btn btn btn-danger btn-hapus akses-delete"
                                            data-route='{{ route('bahanDelete', ['kode'=>$r->id]) }}'>
                                        <i class="la la-remove"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7" class="text-center">
                                    <strong>TOTAL</strong>
                                </td>
                                <td class="text-right">
                                    <strong>{{ Main::format_number($total_bahan) }}</strong>
                                </td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection