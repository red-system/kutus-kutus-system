<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mDistributor extends Model
{
    protected $table = 'tb_distributor';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_distributor',
        'nama_distributor',
        'foto_distributor',
        'alamat_distributor',
        'fax_distributor',
        'telp_distributor',
        'email_distributor',
        'username',
        'password'
    ];

    protected $hidden = [
        'password'
    ];
}
