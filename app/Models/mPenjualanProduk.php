<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPenjualanProduk extends Model
{

    protected $table = 'tb_penjualan_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'id_produk',
        'id_lokasi',
        'id_stok_produk',
        'qty',
        'harga',
        'hpp',
        'potongan',
        'ppn_persen',
        'ppn_nominal',
        'harga_net',
        'sub_total'
    ];

    function penjualan() {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    function produk() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function stok_produk() {
        return $this->belongsTo(mStokProduk::class, 'id_stok_produk');
    }
}
