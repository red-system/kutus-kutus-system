<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mAsset;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Helpers\Main;
use app\Models\mKategoriAsset;

class KategoriAsset extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label'=>$cons['masterData'],
                'route'=>''
            ],
            [
                'label'=>$cons['master_10'],
                'route'=>''
            ]
        ];
    }

    function index() {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mKategoriAsset::orderBy('id', 'DESC')->get();

        return view('masterData/kategoriAsset/kategoriAssetList', $data);
    }

    function insert(Request $request) {
        $request->validate([
            'nama'=>'required',
            'penyusutan_status' => 'required',
        ]);

        $data = $request->except('_token');
        mKategoriAsset::create($data);
    }

    function delete($id) {
        $id_kategori_asset = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $where = [
            'id_kategori_asset' => $id_kategori_asset
        ];
        $list_table = [
            'asset',
        ];

        $asset = mAsset::where($where)->count();

        foreach($list_table as $table) {
            if($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch($check_table_problem) {
            case "asset":
                $message = 'Tidak bisa dihapus, karena Kategori Asset digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.akunting_6')).'</strong>';
                break;
        }

        if($check_status) {
            mKategoriAsset::where('id', $id)->delete();
        } else {
            $response = [
                'title'=>'Perhatian ...',
                'message'=>$message
            ];
            return response($response, 422);
        }



    }

    function update(Request $request, $id) {
        $request->validate([
            'nama'=>'required',
            'penyusutan_status' => 'required',
        ]);

        $data = $request->except("_token");
        mKategoriAsset::where(['id'=>$id])->update($data);
    }
}
