<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use app\Models\mAcMaster;

use app\Models\mHutangSupplier;
use app\Models\mHutangLain;
use app\Models\mAcJurnalUmum;
use app\Models\mAcTransaksi;
use app\Models\mHutangCek;
use app\Models\mSupplier;
use app\Models\mHutangLainPembayaran;

use Illuminate\Support\Facades\Session;

class HutangLain extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['hutang_1'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['hutangPiutang'],
                'route' => ''
            ],
            [
                'label' => $cons['hutang_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mHutangLain
            ::with('supplier')
            ->where('hl_status', 'belum_bayar')
            ->orderBy('id', 'DESC')
            ->get();
        $kode = Config::get('constants.kodeHutangLain');
        $no_transaksi = hAkunting::getNoTransaksiPayment($kode);
        $supplier = mSupplier::orderBy('kode_supplier')->get();
        $master = mAcMaster::where('mst_pembayaran', 'yes')->get();
        $master_kredit = mAcMaster::where('mst_pembayaran', 'yes')->get();
        $master_debet = mAcMaster::where('master_id', 74)->orderBy('mst_kode_rekening')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'list',
            'no_transaksi' => $no_transaksi,
            'master_kredit' => $master_kredit,
            'supplier' => $supplier,
            'master_debet' => $master_debet,
            'master' => $master
        ]);

        return view('hutangPiutang/hutangLain/hutangLainList', $data);
    }

    function insert(Request $request)
    {

        $rules = [
            'hl_keterangan' => 'required'
        ];

        $attributes = [
            'hl_keterangan' => 'Judul Hutang Lain - Lain',
        ];

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $no_hutang_lain = $request->input('no_hutang_lain');
        $hl_tanggal = $request->input('hl_tanggal');
        $hl_tanggal = Main::format_date_db($hl_tanggal);
        $hl_jatuh_tempo = $request->input('hl_jatuh_tempo');
        $hl_jatuh_tempo = Main::format_date_db($hl_jatuh_tempo);
        $id_supplier = $request->input('id_supplier');
        $hl_amount = $request->input('hl_amount');
        $master_id_kredit = $request->input('master_id_kredit');
        $master_id_debet = $request->input('master_id_debet');
        $hl_keterangan = $request->input('hl_keterangan');
        $user = Session::get('user');

        $year = date('Y', strtotime($hl_tanggal));
        $month = date('m', strtotime($hl_tanggal));
        $day = date('d', strtotime($hl_tanggal));

        $jmu_no = hAkunting::jmu_no($year, $month, $day);

        DB::beginTransaction();
        try {

            $data_hutang_lain = [
                'id_supplier' => $id_supplier,
                'master_id_kredit' => $master_id_kredit,
                'master_id_debet' => $master_id_debet,
                'no_hutang_lain' => $no_hutang_lain,
                'hl_tanggal' => $hl_tanggal,
                'hl_jatuh_tempo' => $hl_jatuh_tempo,
                'hl_amount' => $hl_amount,
                'hl_sisa_amount' => $hl_amount,
                'hl_status' => 'belum_bayar',
                'hl_keterangan' => $hl_keterangan,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_by_name' => $user->karyawan->nama_karyawan,
                'updated_by_name' => $user->karyawan->nama_karyawan
            ];
            $response = mHutangLain::create($data_hutang_lain);
            $id_hutang_lain = $response->id;


            /**
             * Begin Akunting
             */

            $data_jurnal_umum = [
                'no_invoice' => $no_hutang_lain,
                'id_supplier' => $id_supplier,
                'id_hutang_lain' => $id_hutang_lain,
                'jmu_tanggal' => $hl_tanggal,
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => 'Pembuatan hutang lain-lain : ' . $hl_keterangan,
                'reference_number' => $no_hutang_lain,
            ];
            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;


            /**
             * insert data akun debet ke tb_ac_transaksi
             */

            $master_debet = mAcMaster
                ::where('master_id', $master_id_debet)
                ->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master_debet->mst_kode_rekening;
            $trs_nama_rekening = $master_debet->mst_nama_rekening;

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id_debet,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $hl_amount,
                'trs_kredit' => 0,
                'user_id' => $user->id,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $hl_keterangan,
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => date('Y-m-d'),
                'trs_setor' => 0,
                'tgl_transaksi' => $hl_tanggal
            ];
            mAcTransaksi::create($data_transaksi);

            /**
             * insert data akun kredit ke tb_ac_transaksi
             */

            $master_kredit = mAcMaster
                ::where('master_id', $master_id_kredit)
                ->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master_kredit->mst_kode_rekening;
            $trs_nama_rekening = $master_kredit->mst_nama_rekening;

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id_kredit,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet' => 0,
                'trs_kredit' => $hl_amount,
                'user_id' => $user->id,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $hl_keterangan,
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => date('Y-m-d'),
                'trs_setor' => 0,
                'tgl_transaksi' => $hl_tanggal
            ];
            mAcTransaksi::create($data_transaksi);

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function update(Request $request, $id)
    {

        $rules = [
            'hl_keterangan' => 'required'
        ];

        $attributes = [
            'hl_keterangan' => 'Judul Hutang Lain - Lain',
        ];

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $id_hutang_lain = $id;
        $no_hutang_lain = $request->input('no_hutang_lain');
        $hl_tanggal = $request->input('hl_tanggal');
        $hl_tanggal = Main::format_date_db($hl_tanggal);
        $hl_jatuh_tempo = $request->input('hl_jatuh_tempo');
        $hl_jatuh_tempo = Main::format_date_db($hl_jatuh_tempo);
        $id_supplier = $request->input('id_supplier');
        $hl_amount = $request->input('hl_amount');
        $master_id_kredit = $request->input('master_id_kredit');
        $master_id_debet = $request->input('master_id_debet');
        $hl_keterangan = $request->input('hl_keterangan');
        $user = Session::get('user');
        $updated_by = $user->id;
        $updated_by_name = $user->karyawan->nama_karyawan;
        $year = date('Y', strtotime($hl_tanggal));
        $month = date('m', strtotime($hl_tanggal));

        try {
            $data_update = [
                'hl_jatuh_tempo' => $hl_jatuh_tempo,
                'id_supplier' => $id_supplier,
                'hl_amount' => $hl_amount,
                'hl_sisa_amount' => $hl_amount,
                'hl_keterangan' => $hl_keterangan,
                'master_id_debet' => $master_id_debet,
                'master_id_kredit' => $master_id_kredit,
                'updated_by' => $updated_by,
                'updated_by_name' => $updated_by_name
            ];

            mHutangLain::where('id', $id_hutang_lain)->update($data_update);

            /**
             * Begin Akunting
             */

            $data_jurnal_umum = [
                'jmu_keterangan' => 'Pembaruan Hutang Lain-Lain: '.$hl_keterangan
            ];
            $jurnal_umum = mAcJurnalUmum
                ::where('id_hutang_lain', $id_hutang_lain)
                ->update($data_jurnal_umum);
            mAcTransaksi
                ::where('jurnal_umum_id', $jurnal_umum->jurnal_umum_id)
                ->delete();

            /**
             * insert data akun debet ke tb_ac_transaksi
             */

            $master_debet = mAcMaster
                ::where('master_id', $master_id_debet)
                ->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master_debet->mst_kode_rekening;
            $trs_nama_rekening = $master_debet->mst_nama_rekening;

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum->jurnal_umum_id,
                'master_id' => $master_id_debet,
                'table_id' => $id_hutang_lain,
                'table_name' => 'tb_hutang_lain',
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $hl_amount,
                'trs_kredit' => 0,
                'user_id' => $user->id,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $hl_keterangan,
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => $this->datetime,
                'trs_setor' => 0,
                'tgl_transaksi' => $hl_tanggal
            ];
            mAcTransaksi::create($data_transaksi);

            /**
             * insert data akun kredit ke tb_ac_transaksi
             */

            $master_kredit = mAcMaster
                ::where('master_id', $master_id_kredit)
                ->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master_kredit->mst_kode_rekening;
            $trs_nama_rekening = $master_kredit->mst_nama_rekening;

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum->jurnal_umum_id,
                'master_id' => $master_id_kredit,
                'table_id' => $id_hutang_lain,
                'table_name' => 'tb_hutang_lain',
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet' => 0,
                'trs_kredit' => $hl_amount,
                'user_id' => $user->id,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $hl_keterangan,
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => date('Y-m-d'),
                'trs_setor' => 0,
                'tgl_transaksi' => $hl_tanggal
            ];
            mAcTransaksi::create($data_transaksi);

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }


    function delete($id_hutang_lain)
    {
        DB::beginTransaction();
        try {

            $check = mHutangLainPembayaran::where('id_hutang_lain', $id_hutang_lain)->count();

            if ($check > 0) {
                return response([
                    'message' => 'Hutang ini <strong>sudah</strong> melakukan pembayaran sebelumnya, sehingga tidak bisa dihapus.'
                ], 422);
            } else {
                $jurnal_umum_id = mAcJurnalUmum
                    ::where('id_hutang_lain', $id_hutang_lain)
                    ->value('jurnal_umum_id');
                mHutangLain::where('id', $id_hutang_lain)->delete();
                mAcJurnalUmum
                    ::where('jurnal_umum_id', $jurnal_umum_id)
                    ->delete();
                mAcTransaksi
                    ::where('jurnal_umum_id', $jurnal_umum_id)
                    ->delete();
            }

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function pembayaran_insert(Request $request, $id)
    {
        $rules = [
            'master_id' => 'required',
            'master_id.*' => 'required'
        ];

        $attributes = [
            'master_id' => 'Kode Perkiraan',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        DB::beginTransaction();
        try {

            $id_hutang_lain = $id;
            $total = $request->input('total');
            $terbayar = $request->input('terbayar');
            $sisa_pembayaran = $request->input('sisa_pembayaran');
            $kembalian = $request->input('kembalian');
            $no_transaksi = $request->input('no_transaksi');
            $no_hutang_lain = $request->input('no_hutang_lain');
            $id_supplier = $request->input('id_supplier');
            $kode_supplier = $request->input('kode_supplier');
            $nama_supplier = $request->input('nama_supplier');
            $master_id_kredit = $request->input('master_id_kredit');
            $master_id_debet = $request->input('master_id_debet');
            /*            $kode_perkiraan = $request->input('kode_perkiraan');
                        $ps_no_faktur = $request->input('ps_no_faktur');*/
            /*            $hs_amount = $request->input('hs_amount');
                        $hs_sisa_amount = $request->input('hs_sisa_amount');*/
            $tanggal_transaksi = $request->input('tanggal_transaksi');
            $master_id_arr = $request->input('master_id');
            $jumlah_arr = $request->input('jumlah');
            $no_bg_arr = $request->input('no_bg');
            $tanggal_pencairan_arr = $request->input('tanggal_pencairan');
            $tipe_arus_kas = 'Operasi';
            $nama_bank_arr = $request->input('nama_bank');
            $keterangan_arr = $request->input('keterangan');
            $year = date('Y', strtotime($tanggal_transaksi));
            $month = date('m', strtotime($tanggal_transaksi));
            $day = date('d', strtotime($tanggal_transaksi));
            $jmu_no = $this->jmu_no($year, $month, $day);
            $jmu_keterangan = 'Pembayaran Hutang Lain-Lain : ' . $kode_supplier . '-' . $nama_supplier . ' No Faktur Hutang Lain: ' . $no_hutang_lain;
            $total_payment = 0;
            $user = Session::get('user');
            $user_id = $user->id;

            /**
             * Data Jurnal Umum
             */

            $data_jurnal = [
                'no_invoice' => $no_transaksi,
/*                'table_id' => $id_hutang_lain,
                'table_name' => 'tb_hutang_lain',*/
                'id_supplier' => $id_supplier,
                'id_hutang_lain' => $id_hutang_lain,
                'jmu_tanggal' => Main::format_date_db($tanggal_transaksi),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
                'reference_number' => $no_hutang_lain
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            foreach ($master_id_arr as $key => $master_id) {

                $jumlah = $jumlah_arr[$key];
                $keterangan = $keterangan_arr[$key];
                $no_bg = $no_bg_arr[$key];
                $tanggal_pencairan = $tanggal_pencairan_arr[$key];
                $nama_bank = $nama_bank_arr[$key];

                /**
                 * Insert Data Transaksi
                 */
                $master = mAcMaster::where('master_id', $master_id)->first();
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'table_id' => $id_hutang_lain,
                    'table_name' => 'tb_hutang_lain',
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet' => 0,
                    'trs_kredit' => $jumlah,
                    'user_id' => $user_id,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $keterangan,
                    'trs_no_check_bg' => $no_bg,
                    'trs_setor' => $jumlah,
                    'trs_tgl_pencairan' => Main::format_date_db($tanggal_pencairan),
                    'tgl_transaksi' => Main::format_date_db($tanggal_transaksi)
                ];

                mAcTransaksi::create($data_transaksi);

                /**
                 * Update Hutang Supplier sudah lunas atau belum
                 */

                $sisa_amount = mHutangLain::where('id', $id_hutang_lain)->value('hl_sisa_amount');
                $sisa_amount -= $jumlah;
                $total_payment += $jumlah;
                if ($sisa_amount == 0) {
                    mHutangLain
                        ::where('id', $id_hutang_lain)
                        ->update([
                            'hl_sisa_amount' => $sisa_amount,
                            'hl_status' => 'lunas'
                        ]);
                } else {
                    mHutangLain
                        ::where('id', $id_hutang_lain)
                        ->update([
                            'hl_sisa_amount' => $sisa_amount
                        ]);
                }

                if ($trs_kode_rekening == '2103') {
                    $data_hutang_cek = [
                        'no_cek_bg' => $no_bg,
                        'tgl_pencairan' => Main::format_date_db($tanggal_pencairan),
                        'tgl_cek' => $this->datetime,
                        'total_cek' => $jumlah,
                        'sisa' => $jumlah,
                        'nama_bank' => $nama_bank,
                        'cek_untuk' => $id_supplier,
                        'keterangan_cek' => $keterangan,
                    ];

                    mHutangCek::create($data_hutang_cek);
                }

                /**
                 * Memasukkan Hutang Lain Pembyaran
                 */

                $data_hutang_lain_pembayaran[] = [
                    'id_hutang_lain' => $id_hutang_lain,
                    'master_id' => $master_id,
                    'jumlah' => $jumlah,
                    'no_check_bg' => $no_bg,
                    'tanggal_pencairan' => Main::format_date_db($tanggal_pencairan),
                    'nama_bank' => $nama_bank,
                    'keterangan' => $keterangan,
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];

            }

            mHutangLainPembayaran::insert($data_hutang_lain_pembayaran);

            $master_id_hutang = $master_id_kredit;
            $master = mAcMaster::find($master_id_kredit);
            $trs_kode_rekening_hutang = $master->mst_kode_rekening;
            $trs_nama_rekening_hutang = $master->mst_nama_rekening;

            $data_transaksi_hutang = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id_hutang,
                'table_id' => $id_hutang_lain,
                'table_name' => 'tb_hutang_lain',
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $total_payment,
                'trs_kredit' => 0,
                'user_id' => $user_id,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening_hutang,
                'trs_nama_rekening' => $trs_nama_rekening_hutang,
                'trs_tipe_arus_kas' => $tipe_arus_kas,
                'trs_catatan' => "Pembayaran Hutang Lain-Lain",
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => Main::format_date_db($tanggal_pencairan),
                'trs_setor' => $total_payment,
                'tgl_transaksi' => Main::format_date_db($tanggal_transaksi)
            ];

            mAcTransaksi::create($data_transaksi_hutang);

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function jmu_no($year, $month, $day)
    {
        $where = [
            'jmu_year' => $year,
            'jmu_month' => $month,
            'jmu_day' => $day
        ];
        $count = mAcJurnalUmum::where($where)->count();
        if ($count == 0) {
            return 1;
        } else {
            $jmu_no = mAcJurnalUmum::where($where)->orderBy('jmu_no', 'DESC')->first(['jmu_no']);
            return $jmu_no->jmu_no + 1;
        }
    }
}
