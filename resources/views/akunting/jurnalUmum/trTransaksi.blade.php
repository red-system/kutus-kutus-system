<table class="row-transaksi m--hide">
    <tr>
        <td class="m--hide data">

        </td>
        <td>
            <select class="select2-transaksi" name="master_id[]"  style="width:100%;">
                <option value="">Pilih Kode Perkiraan</option>
                {!! $kode_perkiraan_option_list !!}
            </select>
        </td>
        <td>
            <select class="form-control jenis_transaksi" name="trs_jenis_transaksi[]">
                <option value="debet">Debet</option>
                <option value="kredit">Kredit</option>
            </select>
        </td>
        <td class="trs_debet">
            <input type="text" class="form-control touchspin-number-decimal-js trs-debet" name="trs_debet[]" value="0">
        </td>
        <td class="trs_kredit">
            <input type="text" class="form-control touchspin-number-decimal-js trs-kredit" name="trs_kredit[]" value="0" readonly>
        </td>
        <td>
            <select name="tipe_arus_kas[]" class="form-control">
                <option value="Operasi">Operasi</option>
                <option value="Pendanaan">Pendanaan</option>
                <option value="Investasi">Investasi</option>
            </select>
        </td>
        <td>
            <textarea class="form-control" name="trs_catatan[]"></textarea>
        </td>
        <td>
            <button type="button" class="btn-delete-row-transaksi btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i>
            </button>
        </td>
    </tr>
</table>