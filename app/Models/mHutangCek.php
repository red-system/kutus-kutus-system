<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHutangCek extends Model
{
    protected $table = 'tb_hutang_cek';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_cek_bg',
        'tgl_cek',
        'tgl_pencairan',
        'total_cek',
        'sisa',
        'nama_bank',
        'cek_untuk',
        'keterangan_cek',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name',
    ];

    function supplier() {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    function created_by() {
        return $this->belongsTo(mUser::class, 'created_by');
    }

    function updated_by() {
        return $this->belongsTo(mUser::class, 'updated_by');
    }


    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }
}
