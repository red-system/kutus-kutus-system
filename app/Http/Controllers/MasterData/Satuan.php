<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mKomposisiProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\mSatuan;
use Illuminate\Support\Facades\Config;
use app\Models\mBahan;

class Satuan extends Controller
{

    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label'=>$cons['masterData'],
                'route'=>''
            ],
            [
                'label'=>$cons['master_3'],
                'route'=>''
            ]
        ];
    }

    function index() {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mSatuan::orderBy('id', 'DESC')->get();

        return view('masterData/satuan/satuanList', $data);
    }

    function insert(Request $request) {
        $request->validate([
            'kode_satuan'=>'required',
            'satuan' => 'required'
        ]);

        $data = $request->except('_token');
        mSatuan::create($data);
    }

    function delete($id) {
        $id_satuan = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $where = [
            'id_satuan' => $id_satuan
        ];
        $list_table = [
            'bahan',
            'komposisi_produk',
        ];

        $komposisi_produk = mKomposisiProduk::where($where)->count();
        $bahan = mBahan::where($where)->count();

        foreach($list_table as $table) {
            if($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch($check_table_problem) {
            case "komposisi_produk":
                $message = 'Tidak bisa dihapus, karena Satuan digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.inventory_3')).'</strong>';
                break;
            case "bahan":
                $message = 'Tidak bisa dihapus, karena Satuan digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.inventory_1')).'</strong>';
                break;
        }

        if($check_status) {
            mSatuan::where('id', $id)->delete();
        } else {
            $response = [
                'title'=>'Perhatian ...',
                'message'=>$message
            ];

            return response($response, 422);
        }
    }

    function update(Request $request, $id) {
        $request->validate([
            'kode_satuan' => 'required',
            'satuan' => 'required'
        ]);

        $data = $request->except("_token");
        mSatuan::where(['id'=>$id])->update($data);
    }
}
