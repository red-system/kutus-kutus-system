<?php

namespace app\Http\Controllers\InventoryLogistik;

use app\Models\mArusStokProduk;
use app\Models\mStokBahan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use app\Models\mKategoriBahan;
use app\Models\mProduk;
use app\Models\mArusStokBahan;
use app\Models\mStokProduk;
use app\Models\mSatuan;
use app\Models\mLokasi;
use app\Models\mHistoryTransferProduk;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

use DB;
use PDF;

class TransferStokProduk extends Controller
{
    private $breadcrumb;
    private $cons;
    private $datetime;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->menuActive = $cons['inventory_5'];
        $this->datetime = date('Y-m-d H:i:s');


        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_5'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $data['tab'] = 'transfer';
        $data['list'] = mProduk
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_produk:id,kode_kategori_produk,kategori_produk'
            )
            ->withCount([
                'stok_produk AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        return view('inventoryLogistik/transferStokProduk/transferStokProdukList', $data);
    }

    function list($idProduk)
    {
        $idProduk = Main::decrypt($idProduk);
        $produk = mProduk::find($idProduk);
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_12'],
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $pageTitle = $produk->kode_produk . ' ' . $produk->nama_produk;
        $lokasi = mLokasi::where('tipe', 'gudang')->orderBy('lokasi', 'ASC')->get();
        $list = mStokProduk
            ::where('id_produk', $idProduk)
            ->orderBy('id', 'DESC')
            ->get();

        $data['list'] = $list;
        $data['pageTitle'] = $pageTitle;
        $data['idProduk'] = $idProduk;
        $data['lokasi'] = $lokasi;
        $data['tab'] = 'transfer';

        return view('inventoryLogistik/transferStokProduk/transferStokProdukDaftarList', $data);
    }

    function insert(Request $request)
    {
        $id = $request->input('id');
        $id_produk = $request->input('id_produk');
        $id_lokasi_dari = $request->input('id_lokasi_dari');
        $qty_dari = $request->input('qty_dari');
        $id_lokasi_tujuan = $request->input('id_lokasi_tujuan');
        $qty_transfer = $request->input('qty_transfer');
        $no_seri_produk = $request->input('no_seri_produk');
        $keterangan = $request->input('keterangan');

        $lokasi_dari = mLokasi::find($id_lokasi_dari);
        $lokasi_tujuan = mLokasi::find($id_lokasi_tujuan);
        $produk = mProduk::find($id_produk);

        $keterangan_transfer_out = 'Transfer produk keluar (' . $produk->kode_produk . ') ' . $produk->nama_bahan . ' dari (' . $lokasi_dari->kode_lokasi . ') ' . $lokasi_dari->lokasi . ' ke (' . $lokasi_tujuan->kode_lokasi . ') ' . $lokasi_tujuan->lokasi;
        $keterangan_transfer_in = 'Transfer produk masuk (' . $produk->kode_produk . ') ' . $produk->nama_bahan . ' dari (' . $lokasi_tujuan->kode_lokasi . ') ' . $lokasi_tujuan->lokasi . ' ke (' . $lokasi_dari->kode_lokasi . ') ' . $lokasi_dari->lokasi;

        /**
         * Last stok dari total id_produk yang ada,
         * dikarenakan id produk sama, dan di transfer saja, tidak ada pengurangan di qty total produk,
         * sehingga last stok antara IN dan OUT menjadi sama
         */
        $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
        $last_stok_total = mStokProduk::sum('qty');

        $request->validate([
            'qty_transfer' => 'required|numeric|min:1|max:' . $qty_dari,
        ]);

        DB::beginTransaction();
        try {

            $checkLokasiExist = mStokProduk
                ::where([
                    'id_produk' => $id_produk,
                    'id_lokasi' => $id_lokasi_tujuan,
                    'no_seri_produk' => $no_seri_produk
                ])
                ->count();
            /**
             * Check full transfer,
             * Jika semua qty di transfer, maka delete stok tersebut
             */
            if ($qty_dari == $qty_transfer) {
                mStokProduk::where('id', $id)->delete();
            }

            if ($checkLokasiExist == 0) {
                $data = [
                    'id_produk' => $id_produk,
                    'id_lokasi' => $id_lokasi_tujuan,
                    'no_seri_produk' => $no_seri_produk,
                    'qty' => $qty_transfer,
                    'last_stok' => $qty_transfer,
                    'keterangan' => $keterangan
                ];
                $response = mStokProduk::create($data);
                $id_stok_produk_in = $response->id;

                $qty = $qty_dari - $qty_transfer;
                $data_update = [
                    'qty' => $qty
                ];
                mStokProduk::where('id', $id)->update($data_update);


                /**
                 * Stok Out transfer stok bahan, dicatat ke arus stok bahan
                 */
                $data_arus_stok_produk_out = [
                    'tgl' => $this->datetime,
                    'table_id' => $id,
                    'table_name' => 'tb_stok_produk',
                    'id_stok_produk' => $id,
                    'id_produk' => $id_produk,
                    'stok_in' => 0,
                    'stok_out' => $qty_transfer,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_out,
                    'method' => 'insert'
                ];

                mArusStokProduk::create($data_arus_stok_produk_out);


                /**
                 * Stok In transfer stok bahan, dicatat ke arus stok bahan
                 */

                $data_arus_stok_produk_in = [
                    'tgl' => $this->datetime,
                    'table_id' => $id_stok_produk_in,
                    'table_name' => 'tb_stok_produk',
                    'id_stok_produk' => $id_stok_produk_in,
                    'id_produk' => $id_produk,
                    'stok_in' => $qty_transfer,
                    'stok_out' => 0,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_in,
                    'method' => 'insert'
                ];

                mArusStokProduk::create($data_arus_stok_produk_in);

            } else {

                /**
                 * Jika Qty dari kurang dari Qty transfer, maka stok dari dikurangi stok yang ditransfer
                 */
                if ($qty_transfer < $qty_dari) {
                    $qty = $qty_dari - $qty_transfer;
                    $data_update = [
                        'qty' => $qty
                    ];
                    mStokProduk::where('id', $id)->update($data_update);
                }

                $where = [
                    'id_produk' => $id_produk,
                    'id_lokasi' => $id_lokasi_tujuan,
                    'no_seri_produk' => $no_seri_produk
                ];
                $qty_tujuan = mStokProduk::where($where)->value('qty');
                $qty = $qty_tujuan + $qty_transfer;

                $dataUpdate = [
                    'qty' => $qty,
                    'last_stok' => $qty
                ];
                mStokProduk::where($where)->update($dataUpdate);
                $id_stok_produk_keluar = mStokProduk::where($where)->value('id');


                /**
                 * Stok Out transfer stok bahan, dicatat ke arus stok bahan
                 */
                $data_arus_stok_produk_out = [
                    'tgl' => $this->datetime,
                    'table_id' => $id,
                    'table_name' => 'tb_stok_produk',
                    'id_stok_produk' => $id,
                    'id_produk' => $id_produk,
                    'stok_in' => 0,
                    'stok_out' => $qty_transfer,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_out,
                    'method' => 'update'
                ];

                mArusStokProduk::create($data_arus_stok_produk_out);


                /**
                 * Stok In transfer stok bahan, dicatat ke arus stok bahan
                 */

                $data_arus_stok_produk_in = [
                    'tgl' => $this->datetime,
                    'table_id' => $id_stok_produk_keluar,
                    'table_name' => 'tb_stok_produk',
                    'id_stok_produk' => $id_stok_produk_keluar,
                    'id_produk' => $id_produk,
                    'stok_in' => $qty_transfer,
                    'stok_out' => 0,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_in,
                    'method' => 'insert'
                ];

                mArusStokProduk::create($data_arus_stok_produk_in);
            }

            /*        /**
                     * Insert to arus stok produk


                    $data_arus = [
                        'tgl'=>date('Y-m-d H:i:s'),
                        'id_stok_produk'=>$id,
                        'id_produk'=>$id_produk,
                        'stok_in'=>$qty_dari - $qty_transfer,
                        'stok_out'=>$qty_transfer,
                        'last_stok'=>$qty_dari,
                        'keterangan'=>$keterangan,
                        'method'=>'update'
                    ];

                    mArusStokProduk::create($data_arus);*/

            /**
             * Insert History Transfer Produk
             */

            $data_history = [
                'tgl' => date('Y-m-d H:i:s'),
                'id_produk' => $id_produk,
                'id_stok_produk' => $id,
                'kode_produk' => $produk->kode_produk,
                'nama_produk' => $produk->nama_produk,
                'no_seri_produk' => $no_seri_produk,
                'qty_transfer' => $qty_transfer,
                'id_lokasi_dari' => $id_lokasi_dari,
                'id_lokasi_tujuan' => $id_lokasi_tujuan,
                'dari' => $lokasi_dari->lokasi,
                'tujuan' => $lokasi_tujuan->lokasi,
                'last_stok' => $qty_dari - $qty_transfer
            ];
            mHistoryTransferProduk::create($data_history);

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

    }

    function history(Request $request)
    {
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_13'],
                'route' => URL::current()
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $tab = 'history';

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryTransferProduk
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryTransferProduk
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];


        $list = mHistoryTransferProduk
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'tab' => $tab,
            'list' => $list,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end
        ]);

        return view('inventoryLogistik/transferStokProduk/historyTransferStokProdukList', $data);
    }

    function history_pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $tab = 'history';

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryTransferProduk
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryTransferProduk
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];


        $list = mHistoryTransferProduk
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'tab' => $tab,
            'list' => $list,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' =>  Main::companyInfo(),
        ]);

        $pdf = PDF::loadView('inventoryLogistik/transferStokProduk/historyTransferStokProdukPdf', $data);
        //return $pdf->setPaper('A4', 'portrait')->stream();
        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('History Transfer Stok Produk ' . date('d-m-Y') . '.pdf');
    }

    function history_excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $tab = 'history';

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryTransferProduk
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryTransferProduk
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];


        $list = mHistoryTransferProduk
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'tab' => $tab,
            'list' => $list,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' =>  Main::companyInfo(),
        ]);

        return view('inventoryLogistik/transferStokProduk/historyTransferStokProdukExcel', $data);
    }
}
