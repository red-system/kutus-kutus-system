<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPoBahanPembelianDetail extends Model
{

    protected $table = 'tb_po_bahan_pembelian_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_po_bahan',
        'id_po_bahan_pembelian',
        'id_po_bahan_detail',
        'id_lokasi'
    ];

    function po_bahan() {
        return $this->belongsTo(mPoBahan::class, 'id_po_bahan');
    }

    function po_bahan_pembelian() {
        return $this->belongsTo(mPoBahanPembelian::class, 'id_po_bahan_pembelian');
    }

    function po_bahan_detail() {
        return $this->belongsTo(mPoBahanDetail::class, 'id_po_bahan_detail');
    }

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }
}
