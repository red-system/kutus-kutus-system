<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mDistributorOrder extends Model
{
    protected $table = 'tb_distributor_order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_distributor',
        'urutan',
        'no_order',
        'tanggal',
        'pengiriman',
        'status',
        'ordered_at',
        'konfirm_admin_at',
        'konfirm_distributor_at',
        'processed_at',
        'done_at',
        'rejected_at'
    ];

    function distributor() {
        return $this->belongsTo(mDistributor::class, 'id_distributor');
    }

    function distributor_order_produk() {
        return $this->hasMany(mDistributorOrderProduk::class, 'id_distributor_order');
    }
}
