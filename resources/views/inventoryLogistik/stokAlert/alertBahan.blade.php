@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/input-mask.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Bahan</th>
                                <th>Nama Bahan</th>
                                {{--<th>Qty</th>--}}
                                <th>Harga</th>
                                <th>Kategori Bahan</th>
{{--                                <th>Lokasi</th>--}}
                                <th>Minimal Stok</th>
                                <th>Total Stok Sekarang</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $r)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->kode_bahan }}</td>
                                <td>{{ $r->nama_bahan }}</td>
                                {{--<td>{{ Main::format_number($r->qty) }}</td>--}}
                                <td>{{ Main::format_money($r->harga) }}</td>
                                <td>{{ $r->kategori_bahan->kategori_bahan }}</td>
{{--                                <td>{{ $r->lokasi->lokasi }}</td>--}}
                                <td>{{ Main::format_number($r->minimal_stok) }}</td>
                                <td>{{ Main::format_number($r->total_stok) }}</td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
@endsection