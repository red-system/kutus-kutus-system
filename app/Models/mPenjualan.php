<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPenjualan extends Model
{

    protected $table = 'tb_penjualan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_distributor',
        'id_distributor_order',
        'id_ekspedisi',
        'urutan',
        'no_faktur',
        'status_distribusi',
        'tanggal',
        'jenis_transaksi',
        'pengiriman',
        'total',
        'biaya_tambahan',
        'potongan_akhir',
        'grand_total',
        'terbayar',
        'sisa_pembayaran',
        'kembalian',
        'status_distribusi_done_at'
    ];

    function distributor() {
        return $this->belongsTo(mDistributor::class, 'id_distributor');
    }

    function penjualan_produk() {
        return $this->hasMany(mPenjualanProduk::class, 'id_penjualan');
    }

    function ekspedisi() {
        return $this->belongsTo(mEkspedisi::class, 'id_ekspedisi');
    }
}
