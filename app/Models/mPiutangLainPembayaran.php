<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPiutangLainPembayaran extends Model
{
    protected $table = 'tb_piutang_lain_pembayaran';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_piutang_lain',
        'master_id',
        'jumlah',
        'no_check_bg',
        'tanggal_pencairan',
        'nama_bank',
        'keterangan',
    ];

    function piutang_lain()
    {
        return $this->belongsTo(mPiutangLain::class, 'id_piutang_lain');
    }

    function master()
    {
        return $this->belongsTo(mAcMaster::class, 'master_id');
    }
}
