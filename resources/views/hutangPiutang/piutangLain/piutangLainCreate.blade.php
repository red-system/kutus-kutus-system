<form action="{{ route('piutangLainInsert') }}"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}

    <input type="hidden" name="pl_invoice" value="{{ $no_transaksi }}">

    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header btn-success">
                    <h5 class="modal-title m--font-light" id="exampleModalLabel">Tambah Piutang Lain - Lain</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <h4>Data Piutang Lain - Lain</h4>
                        <hr />
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Judul Piutang Lain - Lain</label>
                            <div class="col-lg-8">
                                <textarea class="form-control m-input" name="pl_keterangan"></textarea>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Tanggal Piutang Lain - Lain</label>
                            <div class="col-lg-8">
                                <input type="text"
                                       class="form-control m-input m_datepicker"
                                       name="pl_tanggal"
                                       autocomplete="off"
                                       value="{{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No Piutang</label>
                            <div class="col-lg-8 col-form-label">
                                <span class="pl_invoice">{{ $no_transaksi }}</span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Tanggal Jatuh Tempo</label>
                            <div class="col-lg-8">
                                <input type="text"
                                       class="form-control m-input m_datepicker"
                                       name="pl_jatuh_tempo"
                                       autocomplete="off"
                                       value="{{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Tipe Penerima Piutang</label>
                            <div class="col-lg-8">
                                <div class="m-radio-inline">
                                    <label class="m-radio">
                                        <input type="radio" name="table_name" value="tb_karyawan" checked> Karyawan
                                        <span></span>
                                    </label>
                                    <label class="m-radio">
                                        <input type="radio" name="table_name" value="tb_distributor"> Distributor
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Nama Penerima Piutang</label>
                            <div class="col-lg-8 col-form-label table-id-karyawan">
                                <select class="form-control m-select2" name="table_id_karyawan" style="width: 100%">
                                    @foreach($karyawan as $r)
                                        <option value="{{ $r->id }}">{{ $r->kode_karyawan.' - '.$r->nama_karyawan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-8 col-form-label table-id-distributor">
                                <select class="form-control m-select2" name="table_id_distributor" style="width: 100%">
                                    @foreach($distributor as $r)
                                        <option value="{{ $r->id }}">{{ $r->kode_distributor.' - '.$r->nama_distributor }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Jumlah Piutang Lain - Lain</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control m-input touchspin-number-decimal" name="pl_amount" value="0">
                            </div>
                        </div>
                        <br />
                        <h4>Data Akunting</h4>
                        <hr />
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Akun Debet</label>
                            <div class="col-lg-8 col-form-label">
                                <select class="form-control m-select2" name="master_id_debet" style="width: 100%">
                                    @foreach($akun_debet as $r)
                                        <option value="{{ $r->master_id.'|'.$r->mst_kode_rekening }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="pull-left">
                            <strong>Status : </strong>
                            <span class="status-balance">
                                <span class="m-badge m-badge--success m-badge--wide">Balance</span>
                            </span>
                        </div>
                        <div class="pull-right">
                            <button type="button"
                                    class="btn-add-row-kredit m-btn btn btn-success m-btn--air m-btn--pill btn-sm pull-right">
                                <i class="la la-plus"></i> COA Kredit
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        <br />
                        <table class="table-kredit table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Kode Perkiraan Kredit</th>
                                    <th>Nominal Kredit</th>
                                    <th width="70">aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center">
                                        <strong>TOTAL</strong>
                                    </th>
                                    <th class="kredit-total">0</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
