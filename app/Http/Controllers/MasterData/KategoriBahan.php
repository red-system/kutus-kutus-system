<?php

namespace app\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Helpers\Main;
use app\Models\mKategoriBahan;
use app\Models\mHistoryPenyesuaianBahan;
use app\Models\mBahan;

class KategoriBahan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mKategoriBahan::orderBy('id', 'DESC')->get();

        return view('masterData/kategoriBahan/kategoriBahanList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'kode_kategori_bahan' => 'required',
            'kategori_bahan' => 'required'
        ]);

        $data = $request->except('_token');
        mKategoriBahan::create($data);
    }

    function delete($id)
    {
        $id_kategori_bahan = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $where = [
            'id_kategori_bahan' => $id_kategori_bahan
        ];
        $list_table = [
            'history_penyesuaian_bahan',
            'bahan',
        ];

        $history_penyesuaian_bahan = mHistoryPenyesuaianBahan::where($where)->count();
        $bahan = mBahan::where($where)->count();

        foreach($list_table as $table) {
            if($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch($check_table_problem) {
            case "history_penyesuaian_bahan":
                $message = 'Tidak bisa dihapus, karena Kategori Bahan digunakan di <strong>Menu History Penyesuaian Bahan</strong>';
                break;
            case "bahan":
                $message = 'Tidak bisa dihapus, karena Kategori Bahan digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.inventory_1')).'</strong>';
                break;
        }

        if($check_status) {
            mKategoriBahan::where('id', $id)->delete();
        } else {
            $response = [
                'title'=>'Perhatian ...',
                'message'=>$message
            ];

            return response($response, 422);
        }
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'kode_kategori_bahan' => 'required',
            'kategori_bahan' => 'required'
        ]);

        $data = $request->except("_token");
        mKategoriBahan::where(['id' => $id])->update($data);
    }
}
