<table class="row-produk m--hide">
    <tr>
        <td class="m--hide data">
            <input type="hidden" name="id_produk[]">
        </td>
        <td>
            <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                    type="button">
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td class="produk-nama">-</td>
        <td class="td-qty">
            <input type="text" name="qty_order[]" class="touchspin-penjualan" value="1" style="width: 70px">
        </td>
        <td>
            <button type="button" class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i>
            </button>
        </td>
    </tr>
</table>