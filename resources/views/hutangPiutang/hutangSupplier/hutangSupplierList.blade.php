@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/hutang_supplier.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('hutangPiutang.hutangSupplier.hutangSupplierEdit')
    @include('hutangPiutang.hutangSupplier.modalPembayaranCreate')
    @include('hutangPiutang.hutangSupplier.trPembayaran')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>No Faktu Hutang</th>
                            <th>Jatuh Tempo</th>
                            <th>No Faktur PO Bahan</th>
                            <th>Supplier</th>
                            <th>Total</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total = 0;
                        @endphp

                        @foreach($list as $r)
                            @php
                                $r['fullname_supplier'] = $r->supplier->kode_supplier.' - '.$r->supplier->nama_supplier;
                                $r['kode_supplier'] = $r->supplier->kode_supplier;
                                $r['nama_supplier'] = $r->supplier->nama_supplier;
                                $r['tanggal_hutang'] = Main::format_date($r->tanggal_hutang);
                                $r['js_jatuh_tempo'] = Main::format_date($r->js_jatuh_tempo);

                                $r['total'] = round($r->sisa_amount);
                                $r['sisa_pembayaran'] = round($r->sisa_amount);
                                $r['hs_amount'] = round($r->hs_amount);
                                $r['hs_sisa_amount'] = round($r->hs_sisa_amount);
                                $r['grand-total'] = Main::format_number($r->sisa_amount);
                                $r['sisa-pembayaran'] = Main::format_number($r->sisa_amount);

                                $total += $r->sisa_amount;
                            @endphp
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $r->no_faktur_hutang }}</td>
                                <td>{{ Main::format_date($r->tanggal_hutang) }}</td>
                                <td>{{ $r->ps_no_faktur }}</td>
                                <td>{{ $r->supplier->kode_supplier.'-'.$r->supplier->nama_supplier }}</td>
                                <td align="right">{{ Main::format_money($r->sisa_amount) }}</td>
                                <td>{{ $r->hs_keterangan }}</td>
                                <td>{!! Main::hutang_supplier_status($r->hs_status) !!}</td>
                                <td>
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="dropdown">
                                        <button class="btn btn-accent btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i> Menu
                                        </button>
                                        <div class="dropdown-menu"
                                             aria-labelledby="dropdownMenuButton"
                                             x-placement="bottom-start">
                                            @if($r->hs_amount == $r->sisa_amount)
                                                <a class="dropdown-item btn-edit akses-edit"
                                                   data-route="{{ route('hutangSupplierUpdate', ['id'=>$r->id]) }}"
                                                   href="#">
                                                    <i class="la la-edit"></i> Edit
                                                </a>
                                            @endif
                                            <a class="dropdown-item btn-pembayaran akses-pembayaran"
                                               data-route="{{ route('hutangSupplierPembayaran', ['id'=>$r->id]) }}"
                                               href="#">
                                                <i class="la la-money"></i> Pembayaran
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item btn-hapus akses-delete  m--font-danger"
                                               data-route='{{ route('hutangSupplierDelete', ['id'=>$r->id]) }}'
                                               data-message="Yakin hapus data ini ?<br />
                                                              Data ini berkaitan dengan data PO Bahan ke Supplier atau Transaksi Lainnya"
                                               href="#">
                                                <i class="la la-remove m--font-danger"></i> Hapus
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5" align="right">
                                <strong>Total</strong>
                            </td>
                            <td align="right">
                                <strong>{{ Main::format_money($total) }}</strong>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection