<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mKategoriAsset extends Model
{

    protected $table = 'tb_kategori_asset';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama',
        'penyusutan_status',
        'description',
    ];


    public function asset(){
        return $this->hasMany(mAsset::class,'id_kategori_asset');
    }
}
