<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mAcMasterDetail extends Model
{
    protected $table = 'tb_ac_master_detail';
    protected $primaryKey = 'master_detail_id';
    protected $fillable = [
        'master_detail_id',
        'master_id',
        'msd_year',
        'msd_month',
        'msd_awal_kredit',
        'msd_awal_debet'
    ];


    public function perkiraan()
    {
        return $this->belongsTo(mPerkiraan::class, 'master_id','master_id');
    }

    public function transaksi()
    {
        return $this->hasMany(mAcTransaksi::class,'master_id','master_id');
    }
}
