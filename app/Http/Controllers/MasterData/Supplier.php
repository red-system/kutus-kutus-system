<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mAcJurnalUmum;
use app\Models\mBahan;
use app\Models\mHutangCek;
use app\Models\mHutangLain;
use app\Models\mHutangSupplier;
use app\Models\mPoBahan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\mSupplier;
use Illuminate\Support\Facades\Config;

class Supplier extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label'=>$cons['masterData'],
                'route'=>''
            ],
            [
                'label'=>$cons['master_6'],
                'route'=>''
            ]
        ];
    }

    function index() {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mSupplier::orderBy('id', 'DESC')->get();

        return view('masterData/supplier/supplierList', $data);
    }

    function insert(Request $request) {
        $request->validate([
            'nama_supplier' => 'required',
            'alamat_supplier' => 'required',
            'telp_supplier' => 'required',
        ]);

        $data = $request->except(['_token', 'foto_supplier']);

        if($request->hasFile('foto_supplier')) {
            $file = $request->file('foto_supplier');
            $file->move('upload', $file->getClientOriginalName());

            $data['foto_supplier'] = $file->getClientOriginalName();
        } else {
            $data['foto_supplier'] = 'empty.png';
        }

        mSupplier::create($data);
    }

    function delete($id) {
        $id_supplier = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $where = [
            'id_supplier' => $id_supplier
        ];
        $list_table = [
            'po_bahan',
            'jurnal_umum',
            'hutang_lain',
            'bahan',
            'hutang_supplier'
        ];

        $po_bahan = mPoBahan::where($where)->count();
        $jurnal_umum = mAcJurnalUmum::where($where)->count();
        $hutang_lain = mHutangLain::where($where)->count();
        $bahan = mBahan::where($where)->count();
        $hutang_supplier = mHutangSupplier::where($where)->count();

        foreach($list_table as $table) {
            if($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch($check_table_problem) {
            case "po_bahan":
                $message = 'Tidak bisa dihapus, karena Supplier digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.transaksi_5')).'</strong>';
                break;
            case "jurnal_umum":
                $message = 'Tidak bisa dihapus, karena Supplier digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.akunting_2')).'</strong>';
                break;
            case "hutang_lain":
                $message = 'Tidak bisa dihapus, karena Supplier digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.hutang_2')).'</strong>';
                break;
            case "bahan":
                $message = 'Tidak bisa dihapus, karena Supplier digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.inventory_1')).'</strong>';
                break;
            case "hutang_supplier":
                $message = 'Tidak bisa dihapus, karena Supplier digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.hutang_1')).'</strong>';
                break;
        }

        if($check_status) {
            mSupplier::where('id', $id)->delete();
        } else {
            $response = [
                'title'=>'Perhatian ...',
                'message'=>$message
            ];

            return response($response, 422);
        }


    }

    function update(Request $request, $id) {
        $request->validate([
            'kode_supplier' => 'required',
            'nama_supplier' => 'required',
            'alamat_supplier' => 'required',
            'telp_supplier' => 'required',
        ]);

        $data = $request->except(["_token", "foto_supplier"]);

        if($request->hasFile('foto_supplier')) {
            $file = $request->file('foto_supplier');
            $file->move('upload', $file->getClientOriginalName());
            $data['foto_supplier'] = $file->getClientOriginalName();
        }

        mSupplier::where(['id'=>$id])->update($data);
    }
}
