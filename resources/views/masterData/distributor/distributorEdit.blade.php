<form action="" method="post" class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <h4><strong>Akun Login DIstributor</strong></h4>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 col-form-label required">Username</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="username">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Password </label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control m-input" name="password">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Ulangi Password </label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control m-input" name="ulangi_password">
                            </div>
                        </div>
                        <hr />

                        <h4><strong>Personal Data DIstributor</strong></h4>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 col-form-label required">Kode Distributor</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="kode_distributor">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Nama Distributor </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="nama_distributor">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label">Foto Distributor </label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-success btn-file">
                                            Browse Foto <input type="file" class="imgInp" name="foto_distributor"
                                                               accept="image/*" width="100">
                                        </span>
                                    </span>
                                    <input type="text" class="form-control" readonly>
                                </div>
                                <img class='img-preview'/>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Telepon Distributor </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="telp_distributor">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label">Email Distributor </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="email_distributor">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label">Fax Distributor </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="fax_distributor">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Alamat Distributor </label>
                            <div class="col-lg-9">
                                <textarea class="form-control m-input" name="alamat_distributor"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>