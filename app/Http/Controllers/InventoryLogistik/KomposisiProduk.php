<?php

namespace app\Http\Controllers\InventoryLogistik;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Helpers\Main;
use app\Models\mProduk;
use app\Models\mBahan;
use app\Models\mSatuan;
use app\Models\mKomposisiProduk;

class KomposisiProduk extends Controller
{

    private $breadcrumbProduk;
    private $breadcrumbKomposisi;
    private $cons;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->menuActive = $cons['inventory_3'];

        $this->breadcrumbKomposisi = [
            [
                'label' => $cons['inventory'],
                'route' => route('komposisiProdukPage')
            ],
            [
                'label' => $cons['inventory_3'],
                'route' => route('komposisiProdukPage')
            ]
        ];

        $this->breadcrumbProduk = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumbProduk);
        $data['list'] = mProduk
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_produk:id,kode_kategori_produk,kategori_produk'
            )
            ->orderBy('id', 'DESC')
            ->get();

        return view('inventoryLogistik/komposisiProduk/komposisiList', $data);
    }

    function indexKomposisi($idProduk)
    {
        $breadcrumbBahan = [
            [
                'label'=>$this->cons['inventory_15'],
                'route'=>''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumbKomposisi, $breadcrumbBahan);

        $idProduk = Main::decrypt($idProduk);
        $data = Main::data($breadcrumb, $this->menuActive);
        $produk = mProduk::where('id', $idProduk)->select(['kode_produk', 'nama_produk'])->first();
        $list = mKomposisiProduk
            ::with(
                'bahan.satuan'
            )
            ->where('id_produk', $idProduk)
            ->orderBy('id', 'DESC')
            ->get();

        $data['id_produk'] = $idProduk;
        $data['bahan'] = mBahan::orderBy('kode_bahan', 'ASC')->get();
        $data['satuan'] = mSatuan::orderBy('satuan', 'ASC')->get();
        $data['pageTitle'] = $produk->kode_produk . ' ' . $produk->nama_produk;
        $data['list'] = $list;
        $data['total'] = 0;

        return view('inventoryLogistik/komposisiProduk/komposisiProdukList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_bahan' => 'required',
            //'id_satuan' => 'required',
            'qty' => 'required',
        ]);

        $data = $request->except(['_token']);

        mKomposisiProduk::create($data);
    }

    function delete($id)
    {
        mKomposisiProduk::where('id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'id_bahan' => 'required',
            //'id_satuan' => 'required',
            'qty' => 'required',
        ]);

        $data = $request->except("_token");
        mKomposisiProduk::where(['id' => $id])->update($data);
    }
}
