@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">
                @include('produksi/analisaSaranProduksi/tabTitle')
                <div class="m-portlet__body akses-jumlah_bahan_terpakai">
                    <div class="tab-content">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Bahan</th>
                                <th>Nama Bahan</th>
                                <th>Total Bahan yang Digunakan</th>
                                <th>Total Bahan Sisa</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($no = 1)
                            @foreach($bahan_terpakai as $r)
                                <tr>
                                    <td>{{ $no++ }}.</td>
                                    <td>{{ $r['bahan']['kode_bahan'] }}</td>
                                    <td>{{ $r['bahan']['nama_bahan'] }}</td>
                                    <td align="right">{{ Main::format_number($r['totalBahanTerpakai']) }}</td>
                                    <td align="right">{{ Main::format_number($r['totalBahanSisa']) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection