@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('transaksi/distributorOrderOnline/modalProduk')
    @include('transaksi/distributorOrderOnline/modalProdukStok')

    @include('transaksi/distributorOrderOnline/trProduk')
    @include('transaksi/distributorOrderOnline/trPembayaran')

    <form action="{{ route('penjualanProdukInsert') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('distributorOrderOnlineKonfirmasi') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        @include('transaksi/distributorOrderOnline/modalPembayaran')

        {{ csrf_field() }}
        <input type="hidden" class="id_distributor_order" name="id_distributor_order" value="{{ $id_distributor_order }}">
        <input type="hidden" class="id_distributor" name="id_distributor" value="{{ $distributor_order->id_distributor }}">
        <input type="hidden" class="urutan" name="urutan" value="{{ $urutan }}">
        <input type="hidden" class="no_faktur" name="no_faktur" value="{{ $no_faktur }}">
        <input type="hidden" class="id_distributor" name="id_distributor" value="{{ $distributor_order->id_distributor }}">
        <input type="hidden" class="pengiriman" name="pengiriman" value="{{ $distributor_order->pengiriman }}">
        <input type="hidden" class="total" name="total" value="{{ $total }}">
        <input type="hidden" class="grand_total" name="grand_total" value="{{ $grand_total }}">
        <input type="hidden" class="terbayar" name="terbayar" value="{{ $terbayar }}">
        <input type="hidden" class="sisa_pembayaran" name="sisa_pembayaran" value="{{ $sisa_pembayaran }}">
        <input type="hidden" class="kembalian" name="kembalian" value="{{ $kembalian }}">
        <input type="hidden" class="jenis_transaksi" name="jenis_transaksi" value="distributor">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--tabs">
                    @include('transaksi.distributorOrderOnline.tabTitle')
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
                                <div class="col-9 col-form-label">{{ $no_faktur }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal" class="form-control m-input m_datepicker"
                                               readonly="" value="{{ date('d-m-Y', strtotime($distributor_order->tanggal)) }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jenis Transaksi</label>
                                <div class="col-4 col-form-label">Distributor</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pengiriman</label>
                                <div class="col-9 col-form-label">
                                    {{ ucwords($distributor_order->pengiriman) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Order</label>
                                <div class="col-9 col-form-label telepon-distributor">
                                    {{ $distributor_order->no_order }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Distributor</label>
                                <div class="col-9">
                                    <div class="input-group col-form-label">
                                        {{ $distributor_order->distributor->kode_distributor.' '.$distributor_order->distributor->nama_distributor }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <div class="col-9 col-form-label alamat-distributor">
                                    {{ $distributor_order->distributor->alamat_distributor }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Telepon</label>
                                <div class="col-9 col-form-label telepon-distributor">
                                    {{ $distributor_order->distributor->telp_distributor }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-produk btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Produk
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Gudang</th>
                                <th>No Seri Produk</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Potongan</th>
                                <th>PPN(%)</th>
                                <th>Harga Net</th>
                                <th>Sub Total</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($distributor_order_produk as $r)
                                <tr data-index="{{ $no++ }}">
                                    <td class="m--hide data">
                                        <input type="hidden" class="id_produk" name="id_produk[]" value="{{ $r->id_produk }}">
                                        <input type="hidden" class="harga" name="harga[]" value="{{ $r->harga }}">
{{--                                        <input type="hidden" class="ppn_nominal" name="ppn_nominal[]" value="{{ $r->ppn_nominal }}">--}}
                                        <input type="hidden" class="harga_net" name="harga_net[]" value="{{ $r->harga_net }}">
                                        <input type="hidden" class="sub_total" name="sub_total[]" value="{{ $r->sub_total }}">
                                        <input type="hidden" name="qty[]" class="touchspin-number qty" value="{{ $r->qty_konfirm }}">
                                    </td>
                                    <td>
                                        {{--<button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                                                type="button">
                                            <i class="la la-search"></i> Cari
                                        </button>--}}
                                        {{ $r->produk->kode_produk }}
                                    </td>
                                    <td class="produk-nama">
                                        {{ $r->produk->nama_produk }}
                                    </td>
                                    <td>
                                        <select class="form-control m-select2 id_lokasi" name="id_lokasi[]" style="width: 140px">
                                            <option value="">Pilih Gudang</option>
                                            @foreach($gudang as $r_gudang)
                                                <option value="{{ $r_gudang->id }}">{{ $r_gudang->kode_lokasi.' '.$r_gudang->lokasi }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control m-select2 id_stok_produk" name="id_stok_produk[]" style="width: 140px">
                                        </select>
                                    </td>
                                    <td class="td-qty">
                                        {{ Main::format_number($r->qty_konfirm) }}
                                        {{--<input type="text" name="qty[]" class="touchspin-number qty" value="{{ $r->qty_konfirm }}" style="width: 70px">--}}
                                    </td>
                                    <td class="td-harga">
                                        {{ Main::format_number($r->harga) }}
                                    </td>
                                    <td class="td-potongan">
                                        <input type="text" name="potongan[]" class="touchspin-number potongan" value="{{ $r->potongan }}" style="width: 60px">
                                    </td>
                                    <td class="td-ppn">
                                        {{--{{ Main::format_number($r->ppn_nominal) }}--}}
                                        <input type="text" name="ppn_nominal[]" class="touchspin-number ppn_nominal" value="{{ $r->ppn_nominal }}" style="width: 60px">
                                    </td>
                                    <td class="td-harga-net">
                                        {{ Main::format_number($r->harga_net) }}
                                    </td>
                                    <td class="td-sub-total">
                                        {{ Main::format_number($r->sub_total) }}
                                    </td>
                                    <td>
                                        <button type="button" class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                                                data-confirm="false">
                                            <i class="la la-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label">
                                            <strong><span class="total">{{ Main::format_number($total) }}</span></strong>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number biaya_tambahan" type="text"
                                                   name="biaya_tambahan" value="{{ $biaya_tambahan }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number potongan" type="text"
                                                   name="potongan_akhir" value="{{ $potongan_akhir }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label">
                                            <strong><span class="grand-total">{{ Main::format_number($grand_total) }}</span></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


        <div class="target-produksi-buttons">
            <button type="button"
                    class="btn-selanjutnya btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                    data-toggle="modal"
                    data-target="#modal-pembayaran"
                    data-backdrop="static"
                    data-keyboard="false">
                <span>
                    <i class="la la-arrow-right"></i>
                    <span>Proses Selanjutnya</span>
                </span>
            </button>
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Simpan Penjualan</span>
                </span>
            </button>
            <a href="{{ route("distributorOrderOnlineKonfirmasi") }}"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>


@endsection