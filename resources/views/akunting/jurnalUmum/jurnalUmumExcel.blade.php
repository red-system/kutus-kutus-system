<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Jurnal Umum " . date('d-m-Y') . ".xls");
?>

<h3 align="center">KODE PERKIRAAN</h3>
<td align="center">Tanggal : {{ $date_start.' s/d '.$date_end }}
    <br/>
    <table width="100%" border="1">
        <thead>
        <tr class="tabletitle">
            <th class="item" width="40">No</th>
            <th class="item">Tanggal</th>
            <th class="item">No Bukti</th>
            <th class="item">Keterangan</th>
            <th class="item">No Akun</th>
            <th class="item">Debet</th>
            <th class="item">Kredit</th>
            <th class="item">Catatan</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $total_debet = 0;
        $total_kredit = 0;
        ?>

        @foreach($jurnal_umum as $jmu)
            <tr style="background-color: #F4F5F8">
                <td class="tableitem" align="center"><p class="itemtext"> {{ $no++ }}</p></td>
                <td class="tableitem"><p class="itemtext">{{ Main::format_date($jmu->jmu_tanggal) }} </p>
                </td>
                <td class="tableitem"><p class="itemtext">{{ $jmu->no_invoice }}</p></td>
                <td class="tableitem" colspan="5"><p class="itemtext"> {{ $jmu->jmu_keterangan }}</p></td>
            </tr>
            <?php $this_ju_debet = 0; $this_ju_kredit = 0;?>
            @foreach($jmu->transaksi as $trs)
                <tr>
                    <td class="tableitem"><p class="itemtext"></p></td>
                    <td class="tableitem"><p class="itemtext"></p></td>
                    <td class="tableitem"><p class="itemtext"></p></td>
                    <td <?php if ($trs->trs_jenis_transaksi == 'kredit') echo 'align="right"';?> class="tableitem">
                        <p class="itemtext">{{ $trs->trs_nama_rekening }}</p></td>
                    <td <?php if ($trs->trs_jenis_transaksi == 'kredit') echo 'align="right"';?> class="tableitem">
                        <p class="itemtext">{{ $trs->trs_kode_rekening }}</p></td>
                    <td align="right" class="tableitem"><p
                                class="itemtext">{{ Main::format_number($trs->trs_debet) }}</p></td>
                    <td align="right" class="tableitem"><p
                                class="itemtext">{{ Main::format_number($trs->trs_kredit) }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $trs->trs_catatan }}</p></td>
                </tr>
                <?php
                $this_ju_debet += $trs->trs_debet;
                $this_ju_kredit += $trs->trs_kredit;
                $total_debet += $trs->trs_debet;
                $total_kredit += $trs->trs_kredit;
                ?>
            @endforeach
            <tr data-row-index="{{ $jmu->jurnal_umum_id }}">
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td align="right" class="tableitem"><p
                            class="itemtext">{{ Main::format_number($this_ju_debet) }}</p></td>
                <td align="right" class="tableitem"><p
                            class="itemtext">{{ Main::format_number($this_ju_kredit) }}</p></td>
                <td class="tableitem" align="center"><p class="itemtext">
                        @if(Main::format_number($this_ju_debet) == Main::format_number($this_ju_kredit))
                            <span class="m-badge m-badge--success m-badge--wide">
                                            Balance
                                        </span>
                    @else
                        <div class="pull-left">
                                            <span class="m-badge m-badge--danger m-badge--wide">
                                                Not Balance
                                            </span>
                        </div>
                        <div class="pull-right">
                            <h4> {{ Main::format_number($this_ju_debet - $this_ju_kredit) }}</h4>
                        </div>
                        <div class="clearfix"></div>
                        @endif
                        </p>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr class="">
            <th width="10" class="tableitem"><p class="itemtext"></p></th>
            <th colspan="4" class="tableitem">
                <p class="text-center itemtext">
                    <strong> TOTAL </strong>
                </p>
            </th>
            <th class="text-right tableitem">
                <p class="itemtext">
                    <strong>{{number_format($total_debet, 2)}} </strong>
                </p>
            </th>
            <th class="text-right tableitem">
                <p class="itemtext">
                    <strong>{{number_format($total_kredit, 2)}} </strong>
                </p>
            </th>
            @if($jml_debet==$jml_kredit || $total_debet == 0 && $total_kredit == 0)
                <th class="tableitem">
                    <p class="itemtext">
                                    <span class="m-badge m-badge--success m-badge--wide">
                                        Balance
                                    </span>
                    </p>
                </th>
            @endif
            @if($jml_debet != $jml_kredit)
                <th class="tableitem">
                    <p class="itemtext">
                    <div class="pull-left">
                                        <span class="m-badge m-badge--danger m-badge--wide">
                                            Not Balance
                                        </span>
                    </div>
                    <div class="pull-right">

                        <h4> {{ Main::format_number($total_debet - $total_kredit) }}</h4>
                    </div>
                    <div class="clearfix"></div>
                    </p>
                </th>
            @endif
        </tr>
        </tfoot>
    </table>