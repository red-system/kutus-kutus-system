WebFont.load({
    google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
    active: function () {
        sessionStorage.fonts = true;
    }
});

var decimalStep = $('#base-value').data('decimal-step');

var piutangLain_number = {
    buttondown_class: "btn btn-success",
    buttonup_class: "btn btn-warning",
    verticalbuttons: !0,
    verticalupclass: "la la-plus",
    verticaldownclass: "la la-minus",
    min: 0,
    max: 10000000000000000000000,
};

var touchspin_number = {
    buttondown_class: "btn btn-success",
    buttonup_class: "btn btn-warning",
    verticalbuttons: !0,
    verticalupclass: "la la-plus",
    verticaldownclass: "la la-minus",
    min: 0,
    max: 10000000000000000000000,
};

var touchspin_number_decimal = {
    buttondown_class: "btn btn-success",
    buttonup_class: "btn btn-warning",
    verticalbuttons: !0,
    verticalupclass: "la la-plus",
    verticaldownclass: "la la-minus",
    step: decimalStep,
    decimals: 2,
    min: 0,
    max: 10000000000000000000000,
};


$(document).ready(function () {

    var csrf_token = $('#base-value').data('csrf-token');
    var base_url = $('#base-value').data('base-url');

    user_role();
    user_role_menu_action();

    /**
     *  Begin General Operations
     */
    $('.form-send').submit(function (e) {
        e.preventDefault();

        var self = $(this);
        var confirm = $(this).data('confirm');


        if (confirm) {
            swal({
                title: "Perhatian ...",
                text: "Yakin Simpan data ini ?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    form_send(self);
                }
            });
        } else {
            form_send(self);
        }

        return false;
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-jurnal-umum').on('click', '.btn-hapus', function (e) {
        e.preventDefault();
        var self = $(this);
        var remove_by_index = $(this).data('remove-by-index');
        var remove_index = $(this).data('remove-index');
        var reload = $(this).data('reload');
        var message = $(this).data('message');

        if (typeof message === "undefined") {
            message = "Yakin hapus data ini ?";
        }

        swal({
            title: "Perhatian ...",
            html: message,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: self.data('route'),
                    type: "delete",
                    data: {
                        _token: csrf_token
                    },
                    success: function () {
                        if (remove_by_index) {
                            $('tr[data-row-index="' + remove_index + '"]').fadeOut().remove();
                        } else {
                            self.parents('tr').fadeOut().remove();
                        }

                        if (reload) {
                            window.location.reload();
                        }
                    },
                    error: function (request) {
                        var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                        swal({
                            title: title,
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });

        return false;
    });

    $('.datatable-general, .datatable-no-pagination').on('click', '.btn-edit', function (e) {
        e.preventDefault();
        var self = $(this);
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var json = JSON.parse(row);

        if (typeof route !== 'undefined') {
            $('#modal-edit').parents('form').attr('action', route);
        }

        if (typeof redirect !== 'undefined') {
            $('#modal-edit').parents('form').data('redirect', redirect);
        }

        $.each(json, function (key, val) {
            var type = $('[name="' + key + '"]').attr('type');
            if (type == 'file') {
                $('#modal-edit .img-preview').attr('src', base_url + '/upload/' + val);
            } else if (type == 'radio') {
                $('#modal-edit [name="' + key + '"][value=' + val + ']').attr('checked', 'checked');
            } else {
                $('#modal-edit [name="' + key + '"]').val(val);
                $('#modal-edit [name="' + key + '"]').trigger('change');
                $('#modal-edit .' + key).html(val);
            }
        });

        $('#modal-edit').modal('show');

        return false;
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-no-order').on('click', '.btn-detail', function (e) {
        e.preventDefault();

        var route = $(this).data('route');
        var btn_submit = $(this).data('btn-submit');

        if (btn_submit == false) {
            $('#modal-detail [type="submit"]').hide();
        } else {
            $('#modal-detail [type="submit"]').show();
        }

        $.ajax({
            beforeSend: function () {
                loading_start();
            },
            url: route,
            type: 'get',
            success: function (view) {
                $('#modal-detail .modal-body').html(view);
                $('#modal-detail').modal('show');

                if ($('#modal-detail .m_datepicker').length > 0) {
                    $('.m_datepicker').datepicker({
                        rtl: mUtil.isRTL(),
                        todayHighlight: !0,
                        orientation: "bottom left",
                        format: 'dd-mm-yyyy'
                    });
                }

                loading_finish();
            }
        });

        return false;
    });

    $('.modal').on('hidden.bs.modal', function () {
        $('.form-group').removeClass('has-danger');
        $('.form-control-feedback').remove();
        //$('.img-preview').attr('src','');
    });

    /*$('.modal').on('shown.bs.modal', function() {
        $('.modal input:first-child').focus();
    });*/

    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function (event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.img-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".imgInp").change(function () {
        readURL(this);
    });

    /**
     * End General Operations
     */

    /**
     * Begin Stok Produk
     */

    $('.stok-produk-create[name="id_lokasi"]').change(function () {
        var id_lokasi = $(this).val();

    });

    /**
     * End Stok Produk
     */


    /**
     * Begin Transfer Stok Produk
     */

    $('.datatable-general').on('click', '.btn-transfer-stok-produk', function () {
        var row = $(this).siblings('textarea').val();
        var json = JSON.parse(row);
        var id_lokasi = $(this).data('id-lokasi');
        $.each(json, function (key, value) {
            $('.' + key).html(value);
            $('[name="' + key + '"]').val(value);
        });
        $('[name="id_lokasi_tujuan"] option[value="' + id_lokasi + '"]').remove();
        $('#modal-transfer-stok-produk').modal('show');
    });

    /**
     * End Transfer Stok Produk
     */

    /**
     * Begin Transfer Stok Bahan
     */

    $('.datatable-general').on('click', '.btn-transfer-stok-bahan', function () {
        var row = $(this).siblings('textarea').val();
        var json = JSON.parse(row);
        var id_lokasi = $(this).data('id-lokasi');
        $.each(json, function (key, value) {
            $('.' + key).html(value);
            $('[name="' + key + '"]').val(value);
        });
        $('[name="id_lokasi_tujuan"] option[value="' + id_lokasi + '"]').remove();
        $('#modal-transfer-stok-bahan').modal('show');
    });

    /**
     * End Transfer Stok Bahan
     */

    /**
     * Begin Rentang Harga
     */

    rentang_hapus();

    $('.btn-rentang-tambah').click(function () {

        var row = $('#tr-rentang-tambah tbody').html();
        var check = $('.table-produk-harga tbody tr').length;

        if (check > 0) {
            $('.table-produk-harga tbody tr:last').after(row);
        } else {
            $('.table-produk-harga tbody').append(row);
        }


        rentang_hapus();
    });

    function rentang_hapus() {
        $('.btn-rentang-hapus').click(function () {
            $(this).parents('tr').remove();
        });
    }

    /**
     * End Rentang Harga
     */

    /**
     * Begin Dashboard
     */

    $('.applyBtn').click(function () {
        if ($('form').hasClass('form-dashboard-filter')) {
            $('.form-dashboard-filter').submit();
        }
    });

    /**
     * End Dashboard
     */

});

/**
 * Proses ini untuk meremove menu pada sidebar jika user role pada menu tersebut adalah FALSE
 * jika TRUE, maka tetap ditampilkan menunya
 */
function user_role() {
    var check_user_role = $('#user-role-value').length;

    if (check_user_role > 0) {

        var user_role = $('#user-role-value').val();
        var obj = JSON.parse(user_role);

        $.each(obj, function (key, val) {
            if (!val.akses_menu) {
                $('.akses-' + key).remove();
            }

            //console.log(val);

            $.each(val, function (key_2, val_2) {
                //console.log(key_2, val_2);
                if (!val_2.akses_menu) {
                    $('.akses-' + key_2).remove();
                    //console.log('delete');
                }

                //console.log(val_2);

                if (typeof val_2 === 'object') {
                    $.each(val_2, function (key_3, val_3) {
                        //console.log(key_3, val_3);
                    });
                }

                /**/

            });
        });
    }
}

/**
 * Proses ini untuk meremove menu action yang ada di halaman active, jika menu action tersebut adalah FALSE
 */
function user_role_menu_action() {
    var check_user_role_menu_action = $('#user-role-menu-action').length;

    if (check_user_role_menu_action > 0) {
        var user_role_menu_action = $('#user-role-menu-action').val();
        var obj = JSON.parse(user_role_menu_action);

        $.each(obj, function (key, val) {
            if (key === 'action') {
                $.each(val, function (key_2, val_2) {
                    //console.log(key_2, val_2);
                    if (!val_2) {
                        $('.akses-' + key_2).remove();
                    }
                });
            } else {
                if (!val) {
                    $('.akses-' + key).remove();
                }
            }
        });
    }
}

function form_send(self) {

    var action = self.attr('action');
    var method = self.attr('method');

    var redirect = self.data('redirect');
    var alert_show = self.data('alert-show');
    var alert_field_message = self.data('alert-field-message');
    var message = '';
    var message_field = '';

    var form = self;
    var formData = new FormData(form[0]);

    $.ajax({
        url: action,
        type: method,
        data: formData,
        async: false,
        beforeSend: function () {
            loading_start();
        },
        error: function (request, error) {
            loading_finish();
            $('.form-control-feedback').remove();
            $('.form-group').removeClass('has-danger');
            $.each(request.responseJSON.errors, function (key, val) {
                var type = $('[name="' + key + '"]').attr('type');
                $('[name="' + key + '"]').parents('.form-group').addClass('has-danger');
                message_field += val[0] + '<br />';
                /**
                 * check apakah tipe inputan merupakan file atau tidak
                 */
                if (type == 'file') {
                    $('[name="' + key + '"]').parents('.input-group').after('<div class="form-control-feedback">' + val[0] + '</div>');
                } else {
                    /**
                     * check apakah inputan merupakan piutangLain atau tidak
                     */
                    if ($('[name="' + key + '"]').parent('div').hasClass('bootstrap-touchspin')) {
                        $('[name="' + key + '"]').parent('div').after('<div class="form-control-feedback">' + val[0] + '</div>');
                    } else {
                        $('[name="' + key + '"]').after('<div class="form-control-feedback">' + val[0] + '</div>');
                    }
                }
            });

            if (alert_show == true) {
                if (alert_field_message == true) {
                    if (message_field) {
                        message = message_field;
                    } else {
                        message = request.responseJSON.message;
                    }
                } else {
                    message = request.responseJSON.message;
                }

                swal({
                    title: "Ada yang Salah",
                    html: message,
                    type: "warning"
                });
            }
        },
        success: function (data) {
            loading_finish();
            if (typeof redirect == 'undefined') {
                window.location.reload();
            } else {
                window.location.href = redirect;
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function loading_start() {
    $('.form-send [type="submit"]').prop('disabled', true);
    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
}

function loading_finish() {
    $('.form-send [type="submit"]').prop('disabled', false);
    $('.wrapper-loading').fadeOut().addClass('hidden');
}

function format_number(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}