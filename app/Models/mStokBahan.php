<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mStokBahan extends Model
{

    protected $table = 'tb_stok_bahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_bahan',
        'id_lokasi',
        'no_seri_bahan',
        'qty',
        'keterangan'
    ];

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function bahan() {
        return $this->belongsTo(mBahan::class, 'id_bahan');
    }
}
