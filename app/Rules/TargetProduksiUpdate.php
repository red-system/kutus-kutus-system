<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;
use app\Models\mTargetProduksi;
use app\Models\mProduk;

class targetProduksiUpdate implements Rule
{

    private $id_target_produksi;
    private $bulan_target;
    private $tahun_target;
    private $id_produk;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_target_produksi, $bulan_target, $tahun_target)
    {
        $this->id_target_produksi = $id_target_produksi;
        $this->bulan_target = $bulan_target;
        $this->tahun_target = $tahun_target;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $id_produk)
    {
        $this->id_produk = $id_produk;
        $target = mTargetProduksi::find($this->id_target_produksi);
        if (
            $target->bulan_target == $this->bulan_target
            && $target->tahun_target == $this->tahun_target
            && $target->id_produk == $id_produk) {
            return TRUE;
        } else {
            $check = mTargetProduksi::where([
                'id_produk' => $id_produk,
                'bulan_target' => $this->bulan_target,
                'tahun_target' => $this->tahun_target
            ])
                ->count();

            if ($check == 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $bulan = $this->bulan_target;
        $tahun = $this->tahun_target;
        $produk = mProduk::find($this->id_produk);

        return 'Produk ' . $produk->kode_produk . ' ' . $produk->nama_produk . ' sudah ada di Bulan : ' . $bulan . ' Tahun : ' . $tahun;
    }
}
