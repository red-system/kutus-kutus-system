<?php

namespace app\Http\Controllers\InventoryLogistik;

use app\Models\mAcJurnalUmum;
use app\Models\mArusStokProduk;
use app\Models\mBahanProduksi;
use app\Models\mDistributorOrderProduk;
use app\Models\mHistoryPenyesuaianProduk;
use app\Models\mHistoryTransferProduk;
use app\Models\mKomposisiProduk;
use app\Models\mPenjualanProduk;
use app\Models\mProduksi;
use app\Models\mProgressProduksi;
use app\Models\mStokProduk;
use app\Models\mTargetProduksi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use app\Models\mKategoriProduk;
use app\Models\mProduk;
use app\Models\mHargaProduk;
use app\Models\mLokasi;
use Illuminate\Support\Facades\Config;

use DB;

class Produk extends Controller
{

    private $breadcrumb;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['kategoriProduk'] = mKategoriProduk::orderBy('kategori_produk', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['list'] = mProduk
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_produk:id,kode_kategori_produk,kategori_produk'
            )
            ->withCount([
                'stok_produk AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();
        $data['total_produk'] = 0;

        return view('inventoryLogistik/produk/produkList', $data);
    }

    function harga($idProduk)
    {
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_16'],
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $idProduk = Main::decrypt($idProduk);
        $menuActive = $this->cons['inventory_2'];
        $data = Main::data($breadcrumb, $menuActive);
        $list = mHargaProduk
            ::where('id_produk', $idProduk)
            ->orderBy('rentang_awal', 'ASC')
            ->get();
        $produk = mProduk::select(['kode_produk', 'nama_produk'])->find($idProduk);
        $pageTitle = $produk->kode_produk . ' ' . $produk->nama_produk;

        $data['idProduk'] = $idProduk;
        $data['list'] = $list;
        $data['pageTitle'] = $pageTitle;
        $data['produk'] = $produk;

        return view('inventoryLogistik/produk/produkHargaList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'kode_produk' => 'required',
            'nama_produk' => 'required',
            'id_kategori_produk' => 'required',
            /*            'id_lokasi'=>'required',*/
            'minimal_stok' => 'required',
            /*            'harga_eceran'=>'required',
                        'harga_grosir'=>'required',
                        'harga_kosinyasi'=>'required',*/
            'disc_persen' => 'required|min:0|max:100'
        ]);

        $data_produk = $request->except(['_token']);
        //$data_produk['minimal_stok'] = Main::convert_number($data_produk['minimal_stok']);
        /*        $data_produk['harga_eceran'] = Main::convert_money($data_produk['harga_eceran']);
                $data_produk['harga_grosir'] = Main::convert_money($data_produk['harga_grosir']);
                $data_produk['harga_kosinyasi'] = Main::convert_money($data_produk['harga_kosinyasi']);*/
        //$data_produk['disc_persen'] = Main::convert_discount($data_produk['disc_persen']);

        mProduk::create($data_produk);
    }

    function delete($id)
    {
        $id_produk = $id;
        $where = [
            'id_produk' => $id_produk
        ];
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';


        $penjualan_produk = mPenjualanProduk::where($where)->count();
        $stok_produk = mStokProduk::where($where)->count();
        $history_penyesuaian_produk = mHistoryPenyesuaianProduk::where($where)->count();
        $arus_stok_produk = mArusStokProduk::where($where)->count();
        $history_transfer_produk = mHistoryTransferProduk::where($where)->count();
        $distributor_order_produk = mDistributorOrderProduk::where($where)->count();
        $target_produksi = mTargetProduksi::where($where)->count();
        $harga_produk = mHargaProduk::where($where)->count();
        $komposisi_produk = mKomposisiProduk::where($where)->count();
        $progress_produksi = mProgressProduksi::where($where)->count();

        $list_table = [
            'penjualan_produk',
            'stok_produk',
            'history_penyesuaian_produk',
            'arus_stok_produk',
            'history_transfer_produk',
            'distributor_order_produk',
            'target_produksi',
            'harga_produk',
            'komposisi_produk',
            'progress_produksi'
        ];

        foreach ($list_table as $table) {
            if ($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch ($check_table_problem) {
            case "penjualan_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>' . Main::menuAction(Config::get('constants.topMenu.transaksi_1')) . '</strong>';
                break;
            case "stok_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>Stok Produk</strong>';
                break;
            case "history_penyesuaian_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>History Penyesuaian Produk</strong>';
                break;
            case "arus_stok_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>Arus Stok Produk</strong>';
                break;
            case "history_transfer_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>History Transfer Produk</strong>';
                break;
            case "distributor_order_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>' . Main::menuAction(Config::get('constants.topMenu.transaksi_9')) . '</strong>';
                break;
            case "target_produksi":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>' . Main::menuAction(Config::get('constants.topMenu.produksi_2')) . '</strong>';
                break;
            case "harga_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>Harga Produk</strong>';
                break;
            case "komposisi_produk":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>' . Main::menuAction(Config::get('constants.topMenu.inventory_3')) . '</strong>';
                break;
            case "progress_produksi":
                $message = 'Tidak bisa dihapus, karena Produk sudah digunakan di <strong>Progress Produksi</strong>';
                break;
        }

        if ($check_status) {
            mProduk::where('id', $id)->delete();
        } else {
            $response = [
                "title" => 'Perhatian ...',
                "message" => $message
            ];

            return response($response, 422);
        }


    }

    function update(Request $request, $id)
    {
        $request->validate([
            'kode_produk' => 'required',
            'nama_produk' => 'required',
            'id_kategori_produk' => 'required',
            /*            'id_lokasi'=>'required',*/
            'minimal_stok' => 'required',
            /*            'harga_eceran'=>'required',
                        'harga_grosir'=>'required',
                        'harga_kosinyasi'=>'required',*/
            'disc_persen' => 'required|min:0|max:100'
        ]);

        $data_produk = $request->except("_token");
        //$data_produk['minimal_stok'] = Main::convert_number($data_produk['minimal_stok']);
        /*        $data_produk['harga_eceran'] = Main::convert_money($data_produk['harga_eceran']);
                $data_produk['harga_grosir'] = Main::convert_money($data_produk['harga_grosir']);
                $data_produk['harga_kosinyasi'] = Main::convert_money($data_produk['harga_kosinyasi']);*/
        $data_produk['disc_persen'] = Main::convert_discount($data_produk['disc_persen']);
        mProduk::where(['id' => $id])->update($data_produk);
    }

    function harga_insert(Request $request, $idProduk)
    {
        $rentang_awal = $request->input('rentang_awal');
        $rentang_akhir = $request->input('rentang_akhir');
        $harga = $request->input('harga');
        $status = TRUE;

        /**
         * Check rentang selanjutnya tidak lebih kecil dari rentang akhir
         */
        foreach ($harga as $key => $int) {
            if ($rentang_awal[$key] > $rentang_akhir[$key]) {
                $status = FALSE;
                break;
            } else {
                if (isset($rentang_awal[$key + 1])) {
                    if ($rentang_akhir[$key] >= $rentang_awal[$key + 1]) {
                        $status = FALSE;
                        break;
                    }
                }
            }
        }

        if (!$status) {
            $response = [
                'message' => "Rentang Jumlah Produk ada yang Salah. Silahkah diperbaiki",
                'errors' => []
            ];
            return response()->json($response, 422);
        } else {
            $this->harga_proses($idProduk, $rentang_awal, $rentang_akhir, $harga);
        }
    }

    /**
     * Untuk proses penyimpanan rentang harga
     *
     * @param $idProduk
     * @param $rentang_awal
     * @param $rentang_akhir
     * @param $harga
     */
    function harga_proses($idProduk, $rentang_awal, $rentang_akhir, $harga)
    {
        mHargaProduk::where('id_produk', $idProduk)->delete();
        foreach ($harga as $key => $rupiah) {
            $data = [
                'id_produk' => $idProduk,
                'rentang_awal' => $rentang_awal[$key],
                'rentang_akhir' => $rentang_akhir[$key],
                'harga' => $rupiah
            ];
            mHargaProduk::create($data);
        }

    }
}
