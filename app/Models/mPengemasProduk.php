<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPengemasProduk extends Model
{
    protected $table = 'tb_pengemas_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produk',
        'id_bahan',
        'id_satuan',
        'qty'
    ];

    function bahan() {
        return $this->belongsTo(mBahan::class, 'id_bahan');
    }

    function satuan() {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }
}
