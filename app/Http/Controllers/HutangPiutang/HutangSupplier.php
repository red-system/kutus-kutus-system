<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mHutangSupplierPembayaran;
use app\Models\mPoBahan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use app\Models\mAcMaster;

use app\Models\mHutangSupplier;
use app\Models\mAcJurnalUmum;
use app\Models\mAcTransaksi;
use app\Models\mHutangCek;

use Illuminate\Support\Facades\Session;

class HutangSupplier extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['hutang_1'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['hutangPiutang'],
                'route' => ''
            ],
            [
                'label' => $cons['hutang_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mHutangSupplier
            ::with('supplier')
            ->where('hs_status', 'belum_lunas')
            ->orderBy('id', 'DESC')
            ->get();
        $kode = Config::get('constants.kodeHutangSupplier');
        $no_transaksi = hAkunting::getNoTransaksiPayment($kode);
        $master = mAcMaster::where('mst_pembayaran', 'yes')->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'list',
            'no_transaksi' => $no_transaksi,
            'master' => $master
        ]);

        return view('hutangPiutang/hutangSupplier/hutangSupplierList', $data);
    }

    function update(Request $request, $id)
    {

        $request->validate([
            'tanggal_hutang' => 'required',
            'js_jatuh_tempo' => 'required',
            'hs_amount' => 'required'
        ]);

        $data = $request->except(['_token']);
        $data['tanggal_hutang'] = date('Y-m-d', strtotime($data['tanggal_hutang']));
        $data['js_jatuh_tempo'] = date('Y-m-d', strtotime($data['js_jatuh_tempo']));

        mHutangSupplier::where('id', $id)->update($data);
    }

    function pembayaran_insert(Request $request, $id)
    {
        $rules = [
            'master_id' => 'required',
            'master_id.*' => 'required'
        ];

        $attributes = [
            'master_id' => 'Kode Perkiraan',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        DB::beginTransaction();
        try {

            $id_hutang_supplier = $id;
            $total = $request->input('total');
            $terbayar = $request->input('terbayar');
            $sisa_pembayaran = $request->input('sisa_pembayaran');
            $kembalian = $request->input('kembalian');
            $no_transaksi = $request->input('no_transaksi');
            $no_faktur_hutang = $request->input('no_faktur_hutang');
            $id_supplier = $request->input('id_supplier');
            $kode_supplier = $request->input('kode_supplier');
            $nama_supplier = $request->input('nama_supplier');
            $master_id_kode_perkiraan = $request->input('master_id_kode_perkiraan');
            $master_id_debet_arr = [68];
            $kode_perkiraan = $request->input('kode_perkiraan');
            $ps_no_faktur = $request->input('ps_no_faktur');
            /*            $hs_amount = $request->input('hs_amount');
                        $hs_sisa_amount = $request->input('hs_sisa_amount');*/
            $tanggal_transaksi = $request->input('tanggal_transaksi');
            $master_id_arr = $request->input('master_id');
            $jumlah_arr = $request->input('jumlah');
            $no_bg_arr = $request->input('no_bg');
            $tanggal_pencairan_arr = $request->input('tanggal_pencairan');
            $tipe_arus_kas = 'Operasi';
            $nama_bank_arr = $request->input('nama_bank');
            $keterangan_arr = $request->input('keterangan');
            $year = date('Y', strtotime($tanggal_transaksi));
            $month = date('m', strtotime($tanggal_transaksi));
            $day = date('d', strtotime($tanggal_transaksi));
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $jmu_keterangan = 'Pembayaran Hutang Supplier : ' . $kode_supplier . '-' . $nama_supplier . ' No Faktur Hutang : ' . $no_faktur_hutang;
            $total_payment = 0;
            $user = Session::get('user');

            /**
             * Data Jurnal Umum
             */

            $data_jurnal = [
                'no_invoice' => $no_transaksi,
                'id_supplier' => $id_supplier,
                'id_hutang_supplier' => $id_hutang_supplier,
                /*                'pelaku_id' => $id_supplier,
                                'pelaku_table' => 'tb_supplier',*/
                'jmu_tanggal' => date('Y-m-d', strtotime($tanggal_transaksi)),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
                'reference_number' => $no_faktur_hutang
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            foreach ($master_id_arr as $key => $master_id) {

                $jumlah = $jumlah_arr[$key];
                $keterangan = $keterangan_arr[$key];
                $no_bg = $no_bg_arr[$key];
                $tanggal_pencairan = $tanggal_pencairan_arr[$key];
                $nama_bank = $nama_bank_arr[$key];

                /**
                 * Insert Data Transaksi
                 */
                $master = mAcMaster::where('master_id', $master_id)->first();
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    /*                    'table_id' => $jurnal_umum_id,
                                        'table_name' => 'tb_ac_jurnal_umum',*/
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet' => 0,
                    'trs_kredit' => $jumlah,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $keterangan,
                    'trs_no_check_bg' => $no_bg,
                    'trs_setor' => $jumlah,
                    'trs_tgl_pencairan' => Main::format_date_db($tanggal_pencairan),
                    'tgl_transaksi' => Main::format_date_db($tanggal_transaksi)
                ];

                mAcTransaksi::create($data_transaksi);

                /**
                 * Update Hutang Supplier sudah lunas atau belum
                 */

                $sisa_amount = mHutangSupplier::where('id', $id_hutang_supplier)->value('sisa_amount');
                $sisa_amount -= $jumlah_arr[$key];
                $total_payment += $jumlah_arr[$key];
                if ($sisa_amount == 0) {
                    mHutangSupplier
                        ::where('id', $id_hutang_supplier)
                        ->update([
                            'sisa_amount' => $sisa_amount,
                            'hs_status' => 'lunas'
                        ]);
                } else {
                    mHutangSupplier
                        ::where('id', $id_hutang_supplier)
                        ->update([
                            'sisa_amount' => $sisa_amount
                        ]);
                }


                $data_hutang_supplier_pembayaran = [
                    'id_hutang_supplier' => $id_hutang_supplier,
                    'master_id' => $master_id,
                    'jumlah' => $jumlah,
                    'no_check_bg' => $no_bg,
                    'tanggal_pencairan' => $tanggal_pencairan,
                    'nama_bank' => $nama_bank,
                    'keterangan' => $keterangan
                ];

                mHutangSupplierPembayaran::create($data_hutang_supplier_pembayaran);


                if ($trs_kode_rekening == '2103') {
                    $data_hutang_cek = [
                        'no_cek_bg' => $no_bg_arr[$key],
                        'tgl_pencairan' => date('Y-m-d', strtotime($tanggal_pencairan_arr[$key])),
                        'tgl_cek' => $this->datetime,
                        'total_cek' => $jumlah_arr[$key],
                        'sisa' => $jumlah_arr[$key],
                        'nama_bank' => $nama_bank_arr[$key],
                        'cek_untuk' => $id_supplier,
                        'keterangan_cek' => $keterangan_arr[$key],
                    ];

                    mHutangCek::create($data_hutang_cek);
                }

            }

            foreach ($master_id_debet_arr as $master_id) {
                $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening_hutang = $master->mst_kode_rekening;
                $trs_nama_rekening_hutang = $master->mst_nama_rekening;
                $trs_debet = 0;
                switch ($master_id) {
                    case 68: // Hutang Dagang
                        $trs_debet = $total_payment;
                        break;
                }

                $data_transaksi_hutang = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening_hutang,
                    'trs_nama_rekening' => $trs_nama_rekening_hutang,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => "Pembayaran Hutang",
                    'trs_charge' => 0,
                    'trs_no_check_bg' => 0,
                    'trs_tgl_pencairan' => date('Y-m-d'),
                    'trs_setor' => $trs_debet,
                    'tgl_transaksi' => date('Y-m-d', strtotime($tanggal_transaksi))
                ];

                mAcTransaksi::create($data_transaksi_hutang);
            }

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function delete($id_hutang_supplier)
    {
        DB::beginTransaction();
        try {

            $hutang_supplier = mHutangSupplier
                ::where('id', $id_hutang_supplier)
                ->first(['hs_amount', 'sisa_amount']);

            $hs_amount = $hutang_supplier->hs_amount;
            $sisa_amount = $hutang_supplier->sisa_amount;

            if ($hs_amount != $sisa_amount) {
                return response([
                    'message' => 'Hutang Supplier ini tidak bisa dihapus.
                                  <br />
                                  Karena sudah melakukan Pembayaran/Penerimaan sebelumnya.'
                ], 422);
            } else {
                $id_po_bahan = mHutangSupplier::where('id', $id_hutang_supplier)->value('id_po_bahan');
                $check_po_bahan = mPoBahan::where('id', $id_po_bahan)->count();
                if($check_po_bahan > 0) {
                    return response([
                        'message' => 'Hutang Supplier ini tidak bisa dihapus.
                                 <br />
                                 Hapus terlebih dahulu PO Bahan ke Supplier atau Transaksi lain yang berkaitan dengan Hutang Supplier ini.
                                 <br />
                                 <br />
                                 Sehingga, Hutang Supplier ini terhapus secara otomatis.'
                    ], 422);
                } else {
                    mHutangSupplier::where('id', $id_hutang_supplier)->delete();
                    mHutangSupplierPembayaran::where('id_hutang_supplier', $id_hutang_supplier)->delete();
                    $jurnal_umum_id = mAcJurnalUmum::where('id_hutang_supplier', $id_hutang_supplier)->value('jurnal_umum_id');
                    mAcJurnalUmum::where('id_hutang_supplier', $id_hutang_supplier)->delete();
                    mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();

                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }
}
