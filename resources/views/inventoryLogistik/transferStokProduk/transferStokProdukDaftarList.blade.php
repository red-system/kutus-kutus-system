@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('inventoryLogistik/transferStokProduk/transferProdukForm')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("transferStokProdukPage") }}"
                       class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Daftar Produk</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">

                @include('inventoryLogistik/transferStokProduk/tabTitle')
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Lokasi</th>
                            <th>No Seri Produk</th>
                            <th>Qty</th>
                            <th>Keterangan</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            @php(
                                $row = [
                                    'id'=>$r->id,
                                    'id_produk'=>$r->id_produk,
                                    'kode_produk'=>$r->produk->kode_produk,
                                    'nama_produk'=>$r->produk->nama_produk,
                                    'no_seri_produk'=>$r->no_seri_produk,
                                    'qty_dari'=>$r->qty,
                                    'qty_dari_format'=>Main::format_number($r->qty),
                                    'lokasi'=>$r->lokasi->lokasi,
                                    'id_lokasi_dari'=>$r->id_lokasi
                                ]
                            )
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->produk->kode_produk }}</td>
                                <td>{{ $r->produk->nama_produk }}</td>
                                <td>{{ $r->lokasi->lokasi }}</td>
                                <td>{{ $r->no_seri_produk }}</td>
                                <td>{{ Main::format_number($r->qty) }}</td>
                                <td>{{ $r->keterangan }}</td>
                                <td align="center">
                                    <textarea class="row-data hidden">@json($row)</textarea>
                                    <button type="button"
                                            class="m-btn btn btn-info m-btn--pill btn-sm btn-transfer-stok-produk"
                                            data-id-lokasi="{{ $r->id_lokasi }}">
                                        <i class="la la-truck"></i> Transfer Stok
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

@endsection