<?php

namespace app\Http\Controllers\InventoryLogistik;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

use DB;
use PDF;

use app\Helpers\Main;
use app\Models\mKategoriBahan;
use app\Models\mBahan;
use app\Models\mArusStokBahan;
use app\Models\mLokasi;
use app\Models\mStokBahan;
use app\Models\mSupplier;
use app\Models\mHistoryTransferBahan;

class TransferStokBahan extends Controller
{

    private $breadcrumb;
    private $cons;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->datetime = date('Y-m-d H:i:s');
        $this->menuActive = $cons['inventory_4'];

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_4'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['tab'] = 'transfer';
        $data['list'] = mBahan
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_bahan:id,kode_kategori_bahan,kategori_bahan',
                'satuan:id,kode_satuan,satuan',
                'supplier:id,kode_supplier,nama_supplier',
                'stok_bahan'
            )
            ->withCount([
                'stok_bahan as total_stok' => function ($query) {
                    return $query->select(DB::raw('SUM(qty) AS qty_total'));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        return view('inventoryLogistik/transferStokBahan/transferStokBahanList', $data);
    }

    function list($idBahan)
    {
        $idBahan = Main::decrypt($idBahan);
        $bahan = mBahan::find($idBahan);
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_12'],
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $pageTitle = $bahan->kode_bahan . ' ' . $bahan->nama_bahan;
        $lokasi = mLokasi::where('tipe', 'gudang')->orderBy('lokasi', 'ASC')->get();
        $list = mStokBahan
            ::where('id_bahan', $idBahan)
            ->orderBy('id', 'ASC')
            ->get();

        $data['list'] = $list;
        $data['pageTitle'] = $pageTitle;
        $data['idBahan'] = $idBahan;
        $data['lokasi'] = $lokasi;
        $data['tab'] = 'transfer';

        return view('inventoryLogistik/transferStokBahan/transferStokBahanDaftarList', $data);
    }

    function insert(Request $request)
    {
        $id = $request->input('id'); // adalah id_stok_bahan dari
        $id_bahan = $request->input('id_bahan');
        $id_lokasi_dari = $request->input('id_lokasi_dari');
        $qty_dari = $request->input('qty_dari');
        $id_lokasi_tujuan = $request->input('id_lokasi_tujuan');
        $qty_transfer = $request->input('qty_transfer');
        $keterangan = $request->input('keterangan');

        $lokasi_dari = mLokasi::find($id_lokasi_dari);
        $lokasi_tujuan = mLokasi::find($id_lokasi_tujuan);
        $bahan = mBahan::find($id_bahan);

        /**
         * Last stok dari total id_bahan yang ada,
         * dikarenakan id bahan sama, dan di transfer saja, tidak ada pengurangan di qty total bahan,
         * sehingga last stok antara IN dan OUT menjadi sama
         */
        $last_stok = mStokBahan::where('id_bahan', $id_bahan)->sum('qty');
        $last_stok_total = mStokBahan::sum('qty');

        $keterangan_transfer_out = 'Transfer bahan keluar (' . $bahan->kode_bahan . ') ' . $bahan->nama_bahan . ' dari (' . $lokasi_dari->kode_lokasi . ') ' . $lokasi_dari->lokasi . ' ke (' . $lokasi_tujuan->kode_lokasi . ') ' . $lokasi_tujuan->lokasi;
        $keterangan_transfer_in = 'Transfer bahan masuk (' . $bahan->kode_bahan . ') ' . $bahan->nama_bahan . ' dari (' . $lokasi_tujuan->kode_lokasi . ') ' . $lokasi_tujuan->lokasi . ' ke (' . $lokasi_dari->kode_lokasi . ') ' . $lokasi_dari->lokasi;

        $request->validate([
            'qty_transfer' => 'required|numeric|min:1|max:' . $qty_dari,
        ]);

        DB::beginTransaction();
        try {

            /**
             * Check full transfer,
             * Jika semua qty di transfer, maka delete stok tersebut
             */
            if ($qty_dari == $qty_transfer) {
                mStokBahan::where('id', $id)->delete();
            }

            $checkLokasiExist = mStokBahan
                ::where([
                    'id_bahan' => $id_bahan,
                    'id_lokasi' => $id_lokasi_tujuan,
                ])
                ->count();

            /**
             * Mengecheck, apakah sudah ada bahan di lokasi tujuan,
             * jika tidak ada, maka create baru dan update
             */
            if ($checkLokasiExist == 0) {
                $data = [
                    'id_bahan' => $id_bahan,
                    'id_lokasi' => $id_lokasi_tujuan,
                    'qty' => $qty_transfer,
                    'last_stok' => $qty_transfer,
                    'keterangan' => $keterangan
                ];
                $response = mStokBahan::create($data);
                $id_stok_bahan_in = $response->id; // id_stok_bahan tujuan

                $qty = $qty_dari - $qty_transfer;
                $data_update = [
                    'qty' => $qty
                ];
                mStokBahan::where('id', $id)->update($data_update);

                /**
                 * Stok Out transfer stok bahan, dicatat ke arus stok bahan
                 */

                $data_arus_stok_bahan_out = [
                    'tgl' => $this->datetime,
                    'table_id' => $id,
                    'table_name' => 'tb_stok_bahan',
                    'id_stok_bahan' => $id,
                    'id_bahan' => $id_bahan,
                    'stok_in' => 0,
                    'stok_out' => $qty_transfer,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_out,
                    'method' => 'insert'
                ];
                mArusStokBahan::create($data_arus_stok_bahan_out);

                /**
                 * Stok In transfer stok bahan, dicatat ke arus stok bahan
                 */
                $data_arus_stok_bahan_in = [
                    'tgl' => $this->datetime,
                    'table_id' => $id_stok_bahan_in,
                    'table_name' => 'tb_stok_bahan',
                    'id_stok_bahan' => $id_stok_bahan_in,
                    'id_bahan' => $id_bahan,
                    'stok_in' => $qty_transfer,
                    'stok_out' => 0,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_in,
                    'method' => 'update'
                ];
                mArusStokBahan::create($data_arus_stok_bahan_in);
            } else {

                /**
                 * Jika Qty dari kurang dari Qty transfer, maka stok dari dikurangi stok yang ditransfer
                 */
                if ($qty_transfer < $qty_dari) {
                    $qty = $qty_dari - $qty_transfer;
                    $data_update = [
                        'qty' => $qty
                    ];
                    mStokBahan::where('id', $id)->update($data_update);
                }

                $where = [
                    'id_bahan' => $id_bahan,
                    'id_lokasi' => $id_lokasi_tujuan
                ];
                $qty_tujuan = mStokBahan::where($where)->value('qty');
                $qty = $qty_tujuan + $qty_transfer;

                $dataUpdate = [
                    'qty' => $qty
                ];
                mStokBahan::where($where)->update($dataUpdate);
                $id_stok_bahan_keluar = mStokBahan::where($where)->value('id');


                /**
                 * Arus stok bahan keluar
                 */

                $data_arus_stok_bahan_out = [
                    'tgl' => $this->datetime,
                    'table_id' => $id,
                    'table_name' => 'tb_stok_bahan',
                    'id_stok_bahan' => $id,
                    'id_bahan' => $id_bahan,
                    'stok_in' => 0,
                    'stok_out' => $qty_transfer,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_out,
                    'method' => 'update'
                ];

                mArusStokBahan::create($data_arus_stok_bahan_out);

                /**
                 * Arus stok bahan masuk
                 */

                $data_arus_stok_bahan_in = [
                    'tgl' => $this->datetime,
                    'table_id' => $id_stok_bahan_keluar,
                    'table_name' => 'tb_stok_bahan',
                    'id_stok_bahan' => $id_stok_bahan_keluar,
                    'id_bahan' => $id_bahan,
                    'stok_in' => $qty_transfer,
                    'stok_out' => 0,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => $keterangan_transfer_in,
                    'method' => 'update'
                ];

                mArusStokBahan::create($data_arus_stok_bahan_in);

            }

            /*            /**
                         * Insert to arus stok Bahan


                        $data_arus = [
                            'tgl' => date('Y-m-d H:i:s'),
                            'id_stok_bahan' => $id,
                            'id_bahan' => $id_bahan,
                            //'stok_in'=>$qty_dari - $qty_transfer, // karena saat transfer tidak ada stok in, jadinya di disable
                            'stok_in' => 0,
                            'stok_out' => $qty_transfer,
                            'last_stok' => $qty_dari,
                            'keterangan' => $keterangan,
                            'method' => 'update'
                        ];

                        mArusStokBahan::create($data_arus);*/

            /**
             * Insert History Transfer Bahan
             */

            $data_history = [
                'tgl' => date('Y-m-d H:i:s'),
                'id_bahan' => $id_bahan,
                'id_stok_bahan' => $id,
                'kode_bahan' => $bahan->kode_bahan,
                'nama_bahan' => $bahan->nama_bahan,
                'qty_transfer' => $qty_transfer,
                'id_lokasi_dari' => $id_lokasi_dari,
                'id_lokasi_tujuan' => $id_lokasi_tujuan,
                'dari' => $lokasi_dari->lokasi,
                'tujuan' => $lokasi_tujuan->lokasi,
                'last_stok' => $qty_dari - $qty_transfer
            ];
            mHistoryTransferBahan::create($data_history);

            DB::commit();


        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

    }

    function history(Request $request)
    {
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_13'],
                'route' => URL::current()
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $tab = 'history';

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryTransferBahan
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryTransferBahan
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mHistoryTransferBahan
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'ASC')
            ->get();


        $data = array_merge($data, [
            'tab' => $tab,
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
        ]);

        return view('inventoryLogistik/transferStokBahan/historyTransferStokBahanList', $data);
    }

    function history_pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $tab = 'history';

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryTransferBahan
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryTransferBahan
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mHistoryTransferBahan
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'ASC')
            ->get();


        $data = array_merge($data, [
            'tab' => $tab,
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'company' =>  Main::companyInfo(),
        ]);

        $pdf = PDF::loadView('inventoryLogistik/transferstokBahan/historyTransferStokBahanPdf', $data);
        //return $pdf->setPaper('A4', 'portrait')->stream();


        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('History Transfer Stok Bahan ' . date('d-m-Y') . '.pdf');
    }

    function history_excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $tab = 'history';

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryTransferBahan
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryTransferBahan
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mHistoryTransferBahan
            ::whereBetween('tgl', $where_date)
            ->orderBy('id', 'ASC')
            ->get();


        $data = array_merge($data, [
            'tab' => $tab,
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'company' =>  Main::companyInfo(),
        ]);

        return view('inventoryLogistik/transferstokBahan/historyTransferStokBahanExcel', $data);
    }
}
