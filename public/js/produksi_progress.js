$(document).ready(function () {

    produksi_progress();
    change_qty_pengemas_bahan();

});

var route_produksi_progress_hpp = $('#base-value').data('route-produksi-progress-hpp');
var csrf_token = $('#base-value').data('csrf-token');

function produksi_progress() {

    $('.qty-progress').on('change keyup', function () {
        var self = $(this);
        var qty_progress = $(this).val();
        var id_produk = $(this).data('id-produk');
        var id_produksi = $(this).data('id-produksi');
        var id_detail_produksi = $(this).data('id-detail-produksi');

        $('.tr-bahan[data-id-detail-produksi="' + id_detail_produksi + '"]').each(function (index) {
            var qty_bahan_pengemas = $(this).data('qty-bahan-pengemas');
            var id_bahan = $(this).data('id-bahan');
            var qty_digunakan = parseInt(qty_bahan_pengemas) * parseInt(qty_progress);

            $('tr[data-id-detail-produksi="' + id_detail_produksi + '"][data-id-bahan="' + id_bahan + '"] .td-qty-bahan-digunakan').html(format_number(qty_digunakan));
            $('tr[data-id-detail-produksi="' + id_detail_produksi + '"][data-id-bahan="' + id_bahan + '"]').data('qty-bahan-digunakan', qty_digunakan);

            $('tr[data-id-detail-produksi="' + id_detail_produksi + '"][data-id-bahan="' + id_bahan + '"] .td-data .qty-bahan-digunakan').val(qty_digunakan);

            proses_jumlah_bahan_digunakan(id_detail_produksi, id_bahan, qty_digunakan);
            proses_status_bahan(id_detail_produksi, id_bahan);
        });

        if (qty_progress > 0) {
            proses_hpp(qty_progress, id_detail_produksi, id_produk, id_produksi);
        }
    });
}

function proses_jumlah_bahan_digunakan(id_detail_produksi, id_bahan, qty_digunakan) {
    var route_produksi_progress_qty_pengemas = $('#base-value').data('route-produksi-progress-qty-pengemas');
    var csrf_token = $('#base-value').data('csrf-token');
    var data_send = {
        id_detail_produksi: id_detail_produksi,
        id_bahan: id_bahan,
        qty_digunakan: qty_digunakan,
        _token: csrf_token
    };

    $.ajax({
        url: route_produksi_progress_qty_pengemas,
        type: 'post',
        data: data_send,
        success: function (data) {
            $('.tr-bahan[data-id-detail-produksi="' + id_detail_produksi + '"][data-id-bahan="' + id_bahan + '"] .td-pengemas-produk')
                .html(data);
            $('.touchspin-number').TouchSpin(touchspin_number);

            change_qty_pengemas_bahan();
        }
    });
}

function proses_status_bahan(id_detail_produksi, id_bahan) {
    var status_success = '<i class="fa fa-check-circle m--font-success icon-status"></i>';
    var status_error = '<span class="m-badge m-badge--danger m-badge--wide"><strong></strong></span>';
    var td_class = 'tr[data-id-detail-produksi="' + id_detail_produksi + '"][data-id-bahan="' + id_bahan + '"]';
    var total_stok = $(td_class).data('total-stok');
    var total_stok = parseInt(total_stok);
    var qty_bahan_digunakan = $(td_class).data('qty-bahan-digunakan');
    var qty_bahan_digunakan = parseInt(qty_bahan_digunakan);
    var status_implement = '';
    var qty_kurang = 0;
    //console.log(id_detail_produksi, id_bahan, total_stok, qty_bahan_digunakan);

    if (qty_bahan_digunakan > total_stok) {
        qty_kurang = format_number(qty_bahan_digunakan - total_stok);
        status_implement = '<span class="m-badge m-badge--danger m-badge--wide"><strong>-' + qty_kurang + '</strong></span>';
    } else {
        status_implement = status_success;
    }

    $(td_class + ' .td-status-bahan').html(status_implement);

}

function change_qty_pengemas_bahan() {
    $('.input-qty-bahan-pengemas').on('change keyup', function () {
        var qty_bahan_pengemas = $(this).val();
        var id_produksi = $(this).parents('tr').data('id-produksi');
        var id_produk = $(this).parents('tr').data('id-produk');
        var id_bahan = $(this).parents('tr').data('id-bahan');
        var id_detail_produksi = $(this).parents('tr').data('id-detail-produksi');
        var qty_progress = $('tr[data-id-detail-produksi="'+id_detail_produksi+'"] .qty-progress').val();

        var class_parent_bahan = '.tr-bahan[data-id-detail-produksi="' + id_detail_produksi + '"][data-id-bahan="' + id_bahan + '"]';

        var total_qty_bahan_pengemas = 0;
        $('.tr-stok-pengemas-bahan[data-id-bahan="' + id_bahan + '"] .td-qty-bahan-pengemas .input-qty-bahan-pengemas').each(function () {
            var qty = $(this).val();
            console.log(qty);
            total_qty_bahan_pengemas = parseFloat(total_qty_bahan_pengemas) + parseFloat(qty);
        });

        //console.log(total_qty_bahan_pengemas);

        $(class_parent_bahan).data('qty-bahan-digunakan', total_qty_bahan_pengemas);
        $(class_parent_bahan + ' .td-data .qty-bahan-digunakan').val(total_qty_bahan_pengemas);
        $(class_parent_bahan + ' .td-qty-bahan-digunakan').html(format_number(total_qty_bahan_pengemas));

        proses_status_bahan(id_detail_produksi, id_bahan);
        proses_hpp(qty_progress, id_detail_produksi, id_produk, id_produksi);
    });
}

function proses_hpp(qty_progress, id_detail_produksi, id_produk, id_produksi) {
    var bahan_pengemas = {};
    var no = 1;

    $('.qty-bahan-digunakan').each(function() {
        var id_bahan = $(this).data('id-bahan');
        var qty_pengemas = $(this).val();

        bahan_pengemas[no++] = {
            id_bahan: id_bahan,
            qty_pengemas: qty_pengemas
        };
    });

    var data_send = {
        _token: csrf_token,
        qty_progress: qty_progress,
        id_produk: id_produk,
        id_produksi: id_produksi,
        bahan_pengemas: bahan_pengemas
    };

    $.ajax({
        url: route_produksi_progress_hpp,
        type: 'post',
        data: data_send,
        beforeSend: loading_start(),
        success: function (hpp) {
            $('tr[data-id-detail-produksi="'+id_detail_produksi+'"] td.hpp input').val(hpp);

            loading_finish();
        }
    });
}