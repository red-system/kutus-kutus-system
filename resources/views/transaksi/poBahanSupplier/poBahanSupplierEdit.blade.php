@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/po_bahan_supplier.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('transaksi/poBahanSupplier/modalBahan')
    @include('transaksi/poBahanSupplier/modalBahanStok')
    @include('transaksi/poBahanSupplier/modalSupplier')
    @include('transaksi/poBahanSupplier/trBahan');

    <form action="{{ route('poBahanSupplierUpdate', ['id'=>$po_bahan->id]) }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('poBahanSupplierPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="urutan" value="{{ $po_bahan->urutan }}">
        <input type="hidden" name="no_faktur" value="{{ $po_bahan->no_faktur }}">
        <input type="hidden" name="id_supplier" value="{{ $po_bahan->id_supplier }}">
        <input type="hidden" name="kode_supplier" value="{{ $po_bahan->kode_supplier }}">
        <input type="hidden" name="total" value="{{ $po_bahan->total }}">
        <input type="hidden" name="grand_total" value="{{ $po_bahan->grand_total }}">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Dokumen</label>
                                <div class="col-6">
                                    <div class="input-group date">
                                        <input type="text"
                                               name="tanggal"
                                               class="form-control m-input m_datepicker"
                                               readonly=""
                                               value="{{ date('d-m-Y', strtotime($po_bahan->tanggal)) }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Supplier</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control nama_supplier"
                                               placeholder="Nama Supplier..."
                                               value="{{ '('.$po_bahan->supplier->kode_supplier.') '.$po_bahan->supplier->nama_supplier }}"
                                               disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-supplier">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nomer Telepon</label>
                                <div class="col-2">
                                    <div class="input-group col-form-label telp_supplier">{{ $po_bahan->supplier->telp_supplier }}</div>
                                </div>
                                <label for="example-text-input" class="col-2 col-form-label">Alamat</label>
                                <div class="col-5">
                                    <div class="input-group col-form-label alamat_supplier">{{ $po_bahan->supplier->alamat_supplier }}</div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">No Pre Order</label>
                                <div class="col-10 col-form-label no_faktur">
                                    {{ $po_bahan->no_faktur }}
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Pembayaran</label>
                                <div class="col-6">
                                    <select name="pembayaran" class="form-control m-select2">
                                        <option value="cash" {{ $po_bahan->pembayaran == 'cash' ? 'selected':'' }}>Cash</option>
                                        <option value="kredit" {{ $po_bahan->pembayaran == 'kredit' ? 'selected':'' }}>Kredit</option>
                                        <option value="transfer" {{ $po_bahan->pembayaran == 'transfer' ? 'selected':'' }}>Transfer</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Pengiriman</label>
                                <div class="col-6">
                                    <select name="pengiriman" class="form-control m-select2">
                                        <option value="darat" {{ $po_bahan->pengiriman == 'darat' ? 'selected':'' }}>Darat</option>
                                        <option value="laut" {{ $po_bahan->pengiriman == 'laut' ? 'selected':'' }}>Laut</option>
                                        <option value="udara" {{ $po_bahan->pembayaran == 'udara' ? 'selected':'' }}>Udara</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-bahan-row-add btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Bahan
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Bahan</th>
                                <th>Nama Bahan</th>
                                <th>Harga</th>
                                <th>PPN (%)</th>
                                <th>Harga Net</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                                <th width="100">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($po_bahan_detail as $r)
                                    <tr data-index="{{ $no++ }}">
                                        <td class="m--hide">
                                            <input type="hidden" name="id_bahan[]" value="{{ $r->id_bahan }}">
                                            <input type="hidden" name="harga_net[]" value="{{ $r->harga_net }}">
                                            <input type="hidden" name="sub_total[]" value="{{ $r->sub_total }}">
                                        </td>
                                        <td>
                                            <button class="btn-modal-bahan btn btn-accent btn-sm m-btn--pill"
                                                    type="button">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </td>
                                        <td class="nama-bahan">{{ $r->bahan->kode_bahan.' '.$r->bahan->nama_bahan }}</td>
                                        <td>
                                            <input type="text" name="harga[]" class="touchspin-number-decimal" value="{{ $r->harga }}">
                                        </td>
                                        <td class="ppn-nominal">
                                            <input type="text" name="ppn_nominal[]" class="touchspin-number-decimal" value="{{ $r->ppn_nominal }}">
                                        </td>
                                        <td class="harga-net">{{ Main::format_number($r->harga_net) }}</td>
                                        <td>
                                            <input type="text" name="qty[]" class="touchspin-number-decimal" value="{{ $r->qty }}">
                                        </td>
                                        <td class="sub-total">{{ Main::format_number($r->sub_total) }}</td>
                                        <td>
                                            <button type="button" class="btn-bahan-row-delete btn m-btn--pill btn-danger btn-sm"
                                                    data-confirm="false">
                                                <i class="la la-remove"></i> Hapus
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label total">{{ Main::format_number($po_bahan->total) }}</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number-decimal" type="text" name="biaya_tambahan" value="{{ $po_bahan->biaya_tambahan }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number-decimal" type="text" name="potongan" value="{{ $po_bahan->potongan }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label grand-total">{{ Main::format_number($po_bahan->grand_total) }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Perbarui PO Bahan</span>
                        </span>
            </button>

            <a href="{{ route("poBahanSupplierPage") }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar PO</span>
                        </span>
            </a>
        </div>

    </form>
@endsection