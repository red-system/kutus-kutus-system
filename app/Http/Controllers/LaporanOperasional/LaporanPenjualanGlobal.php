<?php

namespace app\Http\Controllers\LaporanOperasional;

use app\Helpers\Main;
use app\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use app\Models\mPenjualan;
use PDF;

class LaporanPenjualanGlobal extends Controller
{

    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['distribusi'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporanOperasional'],
                'route' => ''
            ],
            [
                'label' => $cons['laporanO_1'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $list = mPenjualan
            ::with('distributor')
            ->withCount([
                'penjualan_produk AS total_qty' => function ($query) {
                    $query->select(DB::raw('SUM(qty)'));
                },
                'penjualan_produk AS total_potongan' => function ($query) {
                    $query->select(DB::raw('SUM(potongan)'));
                },
                'penjualan_produk AS total_ppn_nominal' => function ($query) {
                    $query->select(DB::raw('SUM(ppn_nominal)'));
                },

            ])
            ->whereBetween('tanggal', $where_date)
            ->orderBy('id', 'DESC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'total_biaya_tambahan' => 0,
            'total_potongan' => 0,
            'total_ppn_nominal' => 0,
            'total_grand_total' => 0,
        ]);

        return view('laporanOperasional/laporanPenjualanGlobal/laporanPenjualanGlobalList', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $list = mPenjualan
            ::with('distributor')
            ->withCount([
                'penjualan_produk AS total_qty' => function ($query) {
                    $query->select(DB::raw('SUM(qty)'));
                },
                'penjualan_produk AS total_potongan' => function ($query) {
                    $query->select(DB::raw('SUM(potongan)'));
                },
                'penjualan_produk AS total_ppn_nominal' => function ($query) {
                    $query->select(DB::raw('SUM(ppn_nominal)'));
                },

            ])
            ->whereBetween('tanggal', $where_date)
            ->orderBy('id', 'DESC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'company' => Main::companyInfo(),
            'total_biaya_tambahan' => 0,
            'total_potongan' => 0,
            'total_ppn_nominal' => 0,
            'total_grand_total' => 0,
        ]);

        $pdf = PDF::loadView('laporanOperasional/laporanPenjualanGlobal/laporanPenjualanGlobalPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('Laporan Penjualan Global ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $list = mPenjualan
            ::with('distributor')
            ->withCount([
                'penjualan_produk AS total_qty' => function ($query) {
                    $query->select(DB::raw('SUM(qty)'));
                },
                'penjualan_produk AS total_potongan' => function ($query) {
                    $query->select(DB::raw('SUM(potongan)'));
                },
                'penjualan_produk AS total_ppn_nominal' => function ($query) {
                    $query->select(DB::raw('SUM(ppn_nominal)'));
                },

            ])
            ->whereBetween('tanggal', $where_date)
            ->orderBy('id', 'DESC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'total_biaya_tambahan' => 0,
            'total_potongan' => 0,
            'total_ppn_nominal' => 0,
            'total_grand_total' => 0,
        ]);

        return view('laporanOperasional/laporanPenjualanGlobal/laporanPenjualanGlobalExcel', $data);
    }

}
