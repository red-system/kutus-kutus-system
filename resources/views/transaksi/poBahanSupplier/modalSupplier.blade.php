<div class="modal" id="modal-supplier" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-success">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Supplier</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Supplier</th>
                        <th>Nama Supplier</th>
                        <th>Alamat Supplier</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php($no = 1)
                        @foreach($supplier as $r)
                            <tr data-id="{{ $r->id }}"
                                data-kode-supplier="{{ $r->kode_supplier }}"
                                data-nama-supplier="{{ $r->nama_supplier }}"
                                data-alamat-supplier="{{ $r->alamat_supplier }}"
                                data-telp-supplier="{{ $r->telp_supplier }}">
                                <td width="10">{{ $no++ }}.</td>
                                <td>{{ $r->kode_supplier }}</td>
                                <td>{{ $r->nama_supplier }}</td>
                                <td>{{ $r->alamat_supplier }}</td>
                                <td width="30">
                                    <button type="button" class="btn-supplier-select btn btn-success btn-sm m-btn--pill">
                                        <i class="la la-check"></i> Pilih
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>