<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mHistoryPenyesuaianProduk;
use app\Models\mProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;

use app\Models\mKategoriProduk;
use Illuminate\Support\Facades\Config;

class KategoriProduk extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mKategoriProduk::orderBy('id', 'DESC')->get();

        return view('masterData/kategoriProduk/kategoriProdukList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'kode_kategori_produk' => 'required',
            'kategori_produk' => 'required'
        ]);

        $data = $request->except('_token');
        mKategoriProduk::create($data);
    }

    function delete($id)
    {
        $id_kategori_produk = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $where = [
            'id_kategori_produk' => $id_kategori_produk
        ];
        $list_table = [
            'history_penyesuaian_produk',
            'produk'
        ];

        $history_penyesuaian_produk = mHistoryPenyesuaianProduk::where($where)->count();
        $produk = mProduk::where($where)->count();

        foreach($list_table as $table) {
            if($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch($check_table_problem) {
            case "history_penyesuaian_produk":
                $message = 'Tidak bisa dihapus, karena Kategori Produk digunakan di <strong>Menu History Penyesuaian Produk</strong>';
                break;
            case "produk":
                $message = 'Tidak bisa dihapus, karena Kategori Produk digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.inventory_2')).'</strong>';
                break;
        }

        if($check_status) {
            mKategoriProduk::where('id', $id)->delete();
        } else {
            $response = [
                'title'=>'Perhatian ...',
                'message'=>$message
            ];

            return response($response, 422);
        }
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'kode_kategori_produk' => 'required',
            'kategori_produk' => 'required'
        ]);

        $data = $request->except("_token");
        mKategoriProduk::where(['id' => $id])->update($data);
    }
}
