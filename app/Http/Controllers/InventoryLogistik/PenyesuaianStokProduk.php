<?php

namespace app\Http\Controllers\InventoryLogistik;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Helpers\Main;
use app\Models\mKategoriProduk;
use app\Models\mProduk;
use app\Models\mArusStokProduk;
use app\Models\mLokasi;
use app\Models\mStokProduk;
use app\Models\mHistoryPenyesuaianProduk;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use app\Helpers\hAkunting;

use DB;
use PDF;

class PenyesuaianStokProduk extends Controller
{

    private $breadcrumb;
    private $cons;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->menuActive = $cons['inventory_9'];
        $this->datetime = date('Y-m-d H:i:s');

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_9'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $data['kategoriProduk'] = mKategoriProduk::orderBy('kategori_produk', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['tab'] = 'penyesuaian';
        $data['list'] = mProduk
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_produk:id,kode_kategori_produk,kategori_produk'
            )
            ->withCount([
                'stok_produk AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->orderBy('nama_produk', 'ASC')
            ->get();

        return view('inventoryLogistik/penyesuaianStokProduk/penyesuaianStokProdukList', $data);
    }

    function stok($idProduk = '')
    {
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_14'],
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $idProduk = Main::decrypt($idProduk);
        $data = Main::data($breadcrumb, $this->menuActive);
        $produk = mProduk::with('kategori_produk')->find($idProduk);
        $pageTitle = $produk->kode_produk . ' - ' . $produk->nama_produk;
        $list = mStokProduk
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'produk:id,kode_produk,nama_produk'
            )
            ->where('id_produk', $idProduk)
            ->orderBy('id', 'ASC')
            ->get();


        $data['kategoriProduk'] = mKategoriProduk::orderBy('kategori_produk', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['pageTitle'] = $pageTitle;
        $data['list'] = $list;
        $data['produk'] = $produk;
        $data['tab'] = 'penyesuaian';


        return view('inventoryLogistik/penyesuaianStokProduk/stokPenyesuaianProduk', $data);
    }

    function history(Request $request)
    {
        $breadcrumb = [
            [
                'label' => $this->cons['inventory_13'],
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryPenyesuaianProduk
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryPenyesuaianProduk
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];


        $list = mHistoryPenyesuaianProduk
            ::orderBy('id', 'DESC')
            ->whereBetween('tgl', $where_date)
            ->get();

        $tab = 'history';

        $data = array_merge($data, [
            'list' => $list,
            'tab' => $tab,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end
        ]);

        return view('inventoryLogistik/penyesuaianStokProduk/historyPenyesuaianStokProdukList', $data);
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'qty' => 'required',
            'keterangan_penyesuaian' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $id_stok_produk = $id;
            $qty_awal = $request->input('qty_awal');
            $qty_stok_produk_before = $qty_awal;
            $qty = $request->input('qty');
            $qty_now = $qty;
            $id_produk = $request->input('id_produk');
            $id_kategori_produk = $request->input('id_kategori_produk');
            $id_lokasi = $request->input('id_lokasi');
            $tgl = $request->input('tgl');
            $kode_produk = $request->input('kode_produk');
            $nama_produk = $request->input('nama_produk');
            $no_seri_produk = $request->input('no_seri_produk');
            $kategori_produk = $request->input('kategori_produk');
            $lokasi = $request->input('lokasi');
            $keterangan_penyesuaian = $request->input('keterangan_penyesuaian');
            $tipe_arus_kas = 'Operasi';
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $kode_penyesuaian_stok_produk = Config::get('constants.kodePenyesuaianStokProduk');
            $hpp = mStokProduk::where('id', $id_stok_produk)->value('hpp');
            $total_hpp = abs($qty_stok_produk_before - $qty_now) * $hpp;

            if ($qty_now > $qty_stok_produk_before) {
                $stok_in = $qty_now - $qty_stok_produk_before;
                $stok_out = 0;
            } else {
                $stok_in = 0;
                $stok_out = $qty_stok_produk_before - $qty_now;
            }

            $data['qty'] = $qty;

            mStokProduk::where(['id' => $id_stok_produk])->update($data);
            $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
            $last_stok_total = mStokProduk::sum('qty');

            $data_penyesuaian = [
                'id_produk' => $id_produk,
                'id_kategori_produk' => $id_kategori_produk,
                'id_lokasi' => $id_lokasi,
                'id_stok_produk' => $id,
                'tgl' => $tgl,
                'kode_produk' => $kode_produk,
                'nama_produk' => $nama_produk,
                'no_seri_produk' => $no_seri_produk,
                'kategori_produk' => $kategori_produk,
                'lokasi' => $lokasi,
                'qty_awal' => $qty_awal,
                'qty_akhir' => $qty,
                'keterangan' => $keterangan_penyesuaian
            ];

            $response = mHistoryPenyesuaianProduk::create($data_penyesuaian);
            $id_history_penyesuaian_produk = $response->id;
            $no_invoice = $kode_penyesuaian_stok_produk . '-' . $id_history_penyesuaian_produk;

            $data_arus_stok_produk = [
                'tgl' => date('Y-m-d'),
                'table_id' => $id,
                'table_name' => 'tb_stok_produk',
                'id_stok_produk' => $id,
                'id_produk' => $id_produk,
                'id_history_penyesuaian_produk' => $id_history_penyesuaian_produk,
                'stok_in' => $stok_in,
                'stok_out' => $stok_out,
                'last_stok' => $last_stok,
                'last_stok_total' => $last_stok_total,
                'keterangan' => 'Penyesuaian Stok : ' . $keterangan_penyesuaian,
                'method' => 'update'
            ];

            mArusStokProduk::create($data_arus_stok_produk);

            /**
             * Begin Akunting
             */
            $data_jurnal_umum = [
                'no_invoice' => $no_invoice,
                'id_history_penyesuaian_produk' => $id_history_penyesuaian_produk,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_keterangan' => 'Penyesuaian Stok Produk : (' . $kode_produk . ') - ' . $nama_produk,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
            ];
            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            $master_id_kredit = [
                109, // Kerugian Penyesuaian Stok Barang Jadi
                52 // Persediaan Barang Jadi
            ];

            foreach ($master_id_kredit as $key => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                if ($qty_stok_produk_before > $qty_now) {
                    if ($master_id == 109) {
                        $debet_nom = $total_hpp;
                        $kredit_nom = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                    } elseif ($master_id == 52) {
                        $debet_nom = 0;
                        $kredit_nom = $total_hpp;
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                    }
                } else {
                    if ($master_id == 109) {
                        $debet_nom = 0;
                        $kredit_nom = $total_hpp;
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                    } elseif ($master_id == 52) {
                        $debet_nom = $total_hpp;
                        $kredit_nom = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                    }
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $debet_nom,
                    'trs_kredit' => $kredit_nom,
                    'user_id' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'tgl_transaksi' => $this->datetime,
                ];
                mAcTransaksi::create($data_transaksi);
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

    }

    function history_pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryPenyesuaianProduk
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryPenyesuaianProduk
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];


        $list = mHistoryPenyesuaianProduk
            ::orderBy('id', 'DESC')
            ->whereBetween('tgl', $where_date)
            ->get();

        $tab = 'history';

        $data = array_merge($data, [
            'list' => $list,
            'tab' => $tab,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ]);
        $pdf = PDF::loadView('inventoryLogistik/penyesuaianStokProduk/historyPenyesuaianStokProdukPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('History Penyesuaian Stok Produk ' . date('d-m-Y') . '.pdf');
    }

    function history_excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mHistoryPenyesuaianProduk
            ::select('tgl')
            ->orderBy('tgl', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_end_db = mHistoryPenyesuaianProduk
            ::select('tgl')
            ->orderBy('tgl', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl');
        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];


        $list = mHistoryPenyesuaianProduk
            ::orderBy('id', 'DESC')
            ->whereBetween('tgl', $where_date)
            ->get();

        $tab = 'history';

        $data = array_merge($data, [
            'list' => $list,
            'tab' => $tab,
            'params' => $params,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ]);
        return view('inventoryLogistik/penyesuaianStokProduk/historyPenyesuaianStokProdukExcel', $data);
    }
}
