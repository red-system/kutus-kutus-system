<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use app\Helpers\hAkunting;

use app\Models\mArusStokProduk;
use app\Models\mDistribusiDetail;
use app\Models\mDistributorOrder;
use app\Models\mHargaProduk;
use app\Models\mLokasi;
use app\Models\mPenjualanPembayaran;
use app\Models\mStokProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\DB;

use app\Models\mProduk;
use app\Models\mDistributor;
use app\Models\mPenjualan;
use app\Models\mPenjualanProduk;
use app\Models\mAcMaster;
use app\Models\mPiutangPelanggan;
use app\Models\mEkspedisi;
use app\Models\mAcJurnalUmum;
use app\Models\mAcTransaksi;
use app\Models\mCheques;
use View, PDF, Illuminate\Support\Facades\Validator;

class PenjualanProduk extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['transaksi_1'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_1'],
                'route' => ''
            ]
        ];
    }

    function total()
    {
        $data = [];
        $no = 0;
        $penjualan = mPenjualan
            ::with([
                'penjualan_produk'
            ])
            ->get();
        foreach ($penjualan as $r) {
            $penjualan_exist = mAcJurnalUmum::where('id_penjualan')->count();

            $data[$no]['id'] = $r->id;
            $data[$no]['no_faktur'] = $r->no_faktur;
            $data[$no]['tanggal'] = $r->tanggal;
            $data[$no]['grand_total'] = $r->grand_total;
            $data[$no]['jurnal_umum_exist'] = $penjualan_exist > 0 ? 'TRUE':'FALSE';
            $total_hpp = 0;
            foreach ($r->penjualan_produk as $r1) {
                $hpp = mStokProduk::where('id', $r1->id_stok_produk)->value('hpp');
                $total_hpp += $hpp * $r1->qty;
            }
            $data[$no]['total_hpp'] = $total_hpp;
            $no++;
        }

        return $data;
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mPenjualan
            ::with('distributor:id,kode_distributor,nama_distributor')
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list
        ]);

        return view('transaksi/penjualanProduk/penjualanProdukList', $data);
    }

    function create()
    {
        $breadcrumb = [
            [
                'label' => 'Membuat Penjualan Produk',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $pageTitle = 'Membuat Penjualan Produk';
        $distributor = mDistributor::orderBy('nama_distributor', ' ASC')->get();
//        $urutan = $this->urutan();
//        $no_faktur = $data['prefixNoPenjualan'] . $urutan;
        $master = mAcMaster
            ::where('mst_pembayaran', 'yes')
//            ->where('mst_nama_rekening', 'NOT LIKE', "%hutang%")
            ->get();
        $ekspedisi = mEkspedisi::orderBy('nama_ekspedisi')->get();
        $produk = mProduk
            ::withCount([
                'stok_produk AS total_stok_produk' => function ($query) {
                    //$query->where('qty', '<', 400);
                    $query->select(DB::raw("SUM(qty) AS qty_sum"));
                }
            ])
            ->orderBy('kode_produk', 'ASC')
            ->get();

        $data = array_merge($data, [
            'distributor' => $distributor,
            'produk' => $produk,
            'pageTitle' => $pageTitle,
//            'no_faktur' => $no_faktur,
            'master' => $master,
//            'urutan' => $urutan,
            'ekspedisi' => $ekspedisi
        ]);

        return view('transaksi/penjualanProduk/penjualanProdukCreate', $data);
    }

    function insert(Request $request)
    {

        $rules = [
            'id_distributor' => 'required',
            'id_lokasi' => 'required',
            'id_lokasi.*' => 'required',
            'master_id' => 'required',
            'master_id.*' => 'required',
            'id_produk' => 'required',
            'id_produk.*' => 'required',
            'id_stok_produk' => 'required',
            'id_stok_produk.*' => 'required'
        ];

        $attributes = [
            'id_distributor' => 'Distributor',
            'master_id' => 'Kode Perkiraan',
            'id_produk' => 'Produk',
            'id_lokasi' => 'Gudang',
            'id_stok_produk' => 'Nomer Seri Produk',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_produk.' . $i] = 'Produk ke-' . $next;
            $attr['id_lokasi.' . $i] = 'Gudang ke-' . $next;
            $attr['id_stok_produk.' . $i] = 'Nomer Seri Produk ke-' . $next;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        $data_penjualan_pembayaran = [];

        $urutan = $request->input('urutan');
        $id_distributor = $request->input('id_distributor');
        $distributor = mDistributor::find($id_distributor);
        $id_distributor_order = $request->input('id_distributor_order');
        $id_ekspedisi = $request->input('id_ekspedisi');
        $total = $request->input('total');
        $grand_total = $request->input('grand_total');
        $terbayar = $request->input('terbayar');
        $sisa_pembayaran = $request->input('sisa_pembayaran');
        $kembalian = $request->input('kembalian');
        $no_faktur = $request->input('no_faktur');
        $no_invoice = $no_faktur;
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $jenis_transaksi = $request->input('jenis_transaksi');
        $pengiriman = $request->input('pengiriman');
        $biaya_tambahan = $request->input('biaya_tambahan');
        $potongan_akhir = $request->input('potongan_akhir');
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $jmu_no = hAkunting::jmu_no($year, $month, $day);
        $id_piutang_pelanggan = '';
        $jurnal_umum_keterangan = $no_invoice == '-' ? 'Retail' : $no_invoice;
        $jurnal_umum_keterangan = 'Penjualan Produk ' . $jurnal_umum_keterangan;

        $id_produk_arr = $request->input('id_produk');
        $harga_arr = $request->input('harga');
        $ppn_nominal_arr = $request->input('ppn_nominal');
        $ppn_persen = Config::get('constants.ppnPersen');
        $harga_net_arr = $request->input('harga_net');
        $sub_total_arr = $request->input('sub_total');
        $id_lokasi_arr = $request->input('id_lokasi');
        $id_stok_produk_arr = $request->input('id_stok_produk');
        $qty_arr = $request->input('qty');
        $potongan_arr = $request->input('potongan');

        $master_id_arr = $request->input('master_id');
        $jumlah_arr = $request->input('jumlah');
        $jatuh_tempo_arr = $request->input('jatuh_tempo');
        $no_bg_arr = $request->input('no_bg');
        $keterangan_arr = $request->input('keterangan');

        DB::beginTransaction();
        try {

            /**
             * Check stok produk dengan yang dijual
             */
            $check_stok_produk = $this->check_stok_produk($id_stok_produk_arr, $qty_arr);
            if (!$check_stok_produk) {
                return response([
                    'message' => 'Stok Produk yang akan dijual <strong>Tidak Mencukupi</strong>'
                ], 422);
            }

            $data_penjualan = [
                'id_distributor' => $id_distributor,
                'id_distributor_order' => $id_distributor_order,
                'id_ekspedisi' => $id_ekspedisi,
                'urutan' => $urutan,
                'no_faktur' => $no_faktur,
                'tanggal' => $tanggal,
                'jenis_transaksi' => $jenis_transaksi,
                'pengiriman' => $pengiriman,
                'total' => $total,
                'biaya_tambahan' => $biaya_tambahan,
                'potongan_akhir' => $potongan_akhir,
                'grand_total' => $grand_total,
                'terbayar' => $terbayar,
                'sisa_pembayaran' => $sisa_pembayaran,
                'kembalian' => $kembalian,
            ];

            $response = mPenjualan::create($data_penjualan);
            $id_penjualan = $response->id;

            foreach ($id_produk_arr as $index => $id_produk) {

                $id_lokasi = $id_lokasi_arr[$index];
                $id_stok_produk = $id_stok_produk_arr[$index];
                $qty = $qty_arr[$index];
                $harga = $harga_arr[$index];
                $potongan = $potongan_arr[$index];
                $ppn_nominal = $ppn_nominal_arr[$index];
                $harga_net = $harga_net_arr[$index];
                $sub_total = $sub_total_arr[$index];
                $hpp_produk = mStokProduk::where('id', $id_stok_produk)->value('hpp');
                $hpp = $hpp_produk * $qty;

                $data_penjualan_produk = [
                    'id_penjualan' => $id_penjualan,
                    'id_produk' => $id_produk,
                    'id_lokasi' => $id_lokasi,
                    'id_stok_produk' => $id_stok_produk,
                    'qty' => $qty,
                    'harga' => $harga,
                    'hpp' => $hpp,
                    'potongan' => $potongan,
                    'ppn_persen' => $ppn_persen,
                    'ppn_nominal' => $ppn_nominal,
                    'harga_net' => $harga_net,
                    'sub_total' => $sub_total
                ];

                $response = mPenjualanProduk::create($data_penjualan_produk);
                $id_penjualan_produk = $response->id;

                /**
                 * Jika pengiriman = "no", maka masukkan kedalam arus stok produk sebagai out,
                 * dan kurangi stok produk
                 */
                if ($pengiriman == 'no') {

                    /**
                     * Untuk update stok produk, jika pengiriman adalah "No"
                     * kurangi stok produk, sesuai dengan qty produk yang dikirim
                     */
                    $stok_produk = mStokProduk::where('id', $id_stok_produk)->value('qty');
                    $qty_stok = $stok_produk - $qty;
                    $data_stok_produk = [
                        'qty' => $qty_stok
                    ];
                    mStokProduk::where('id', $id_stok_produk)->update($data_stok_produk);


                    $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
                    $last_stok_total = mStokProduk::sum('qty');
                    $data_arus_stok_produk = [
                        'tgl' => $this->datetime,
                        'table_id' => $id_penjualan_produk,
                        'table_name' => 'tb_penjualan_produk',
                        'id_penjualan' => $id_penjualan,
                        'id_penjualan_produk' => $id_penjualan_produk,
                        'id_stok_produk' => $id_stok_produk,
                        'id_produk' => $id_produk,
                        'stok_in' => 0,
                        'stok_out' => $qty,
                        'last_stok' => $last_stok,
                        'last_stok_total' => $last_stok_total,
                        'keterangan' => 'Penjualan Produk ke (' . $distributor->kode_distributor . ') ' . $distributor->nama_distributor,
                        'method' => 'insert'
                    ];

                    mArusStokProduk::create($data_arus_stok_produk);
                }
            }

            foreach ($master_id_arr as $index => $master_id) {
                $jatuh_tempo = Main::format_date_db($jatuh_tempo_arr[$index]);
                $jumlah = $jumlah_arr[$index];
                $no_bg = $no_bg_arr[$index];
                $keterangan = $keterangan_arr[$index];

                $data_penjualan_pembayaran[] = [
                    'id_penjualan' => $id_penjualan,
                    'master_id' => $master_id,
                    'jumlah' => $jumlah,
                    'jatuh_tempo' => $jatuh_tempo,
                    'no_check_bg' => $no_bg,
                    'keterangan' => $keterangan,
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];

                /**
                 * Insert jika piutang pelanggan, jika label perkiraan berisi kata piutang
                 */

                $nama_rekening = mAcMaster::where('master_id', $master_id)->value('mst_nama_rekening');
                $kode_rekening = mAcMaster::where('master_id', $master_id)->value('mst_kode_rekening');
                $nama_rekening = strtolower($nama_rekening);
                if (strpos($nama_rekening, 'piutang') !== FALSE) {

                    $no_piutang_pelanggan = $this->no_piutang_pelanggan();

                    $data_piutang_pelanggan = [
                        'id_distributor' => $id_distributor,
                        'id_penjualan' => $id_penjualan,
                        'table_id' => $id_penjualan,
                        'table_name' => 'tb_penjualan',
                        'no_piutang_pelanggan' => $no_piutang_pelanggan,
                        'pp_jatuh_tempo' => $jatuh_tempo,
                        'pp_no_faktur' => $no_faktur,
                        'pp_amount' => $jumlah,
                        'master_id_kode_perkiraan' => $master_id,
                        'kode_perkiraan' => $kode_rekening,
                        'pp_sisa_amount' => $jumlah,
                        'tanggal_piutang' => date('Y-m-d'),
                        'pp_keterangan' => $keterangan,
                        'pp_status' => 'belum_bayar',
                    ];

                    $response = mPiutangPelanggan::create($data_piutang_pelanggan);
                    $id_piutang_pelanggan = $response->id;
                }

            }

            mPenjualanPembayaran::insert($data_penjualan_pembayaran);


            if ($id_distributor_order) {
                $data_update = [
                    'status' => 'done',
                    'done_at' => $this->datetime
                ];
                mDistributorOrder::where('id', $id_distributor_order)->update($data_update);
            }


            /**
             * Begin Akunting
             */

            /**
             * Insert data to jurnal umum
             */
            $data_jurnal_umum = [
                'id_distributor' => $id_distributor,
                'no_invoice' => $no_invoice,
                'id_penjualan' => $id_penjualan,
                'id_piutang_pelanggan' => $id_piutang_pelanggan,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_keterangan' => $jurnal_umum_keterangan,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
            ];
            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            $this->akunting_transaksi(
                $jurnal_umum_id,
                $master_id_arr,
                $jatuh_tempo_arr,
                $jumlah_arr,
                $no_bg_arr,
                $keterangan_arr,
                $id_penjualan,
                $id_distributor,
                $no_invoice,
                $tanggal,
                $pengiriman,
                $qty_arr,
                $harga_arr,
                $potongan_arr,
                $ppn_nominal_arr,
                $biaya_tambahan,
                $potongan_akhir);


            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    /**
     *
     * Digunakan untuk menyimpan data transaksi untuk akunting, dibuatkan method baru karena, saat insert dan update, proses sama dilakukan
     *
     * @param $jurnal_umum_id
     * @param $master_id_arr
     * @param $jatuh_tempo_arr
     * @param $jumlah_arr
     * @param $no_bg_arr
     * @param $keterangan_arr
     * @param $id_penjualan
     * @param $id_distributor
     * @param $no_invoice
     * @param $tanggal
     * @param $pengiriman
     * @param $qty_arr
     * @param $harga_arr
     * @param $potongan_arr
     * @param $ppn_nominal_arr
     * @param $biaya_tambahan
     */
    function akunting_transaksi(
        $jurnal_umum_id,
        $master_id_arr,
        $jatuh_tempo_arr,
        $jumlah_arr,
        $no_bg_arr,
        $keterangan_arr,
        $id_penjualan,
        $id_distributor,
        $no_invoice,
        $tanggal,
        $pengiriman,
        $qty_arr,
        $harga_arr,
        $potongan_arr,
        $ppn_nominal_arr,
        $biaya_tambahan,
        $potongan_akhir = '')
    {

        $total_jumlah = 0;
        $year = date('Y');
        $month = date('m');
        $tipe_arus_kas = 'Operasi';


        /**
         * Memasukkan data debet ke transaksi
         */
        foreach ($master_id_arr as $index => $master_id) {
            $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master->mst_kode_rekening;
            $trs_nama_rekening = $master->mst_nama_rekening;
            $jatuh_tempo = Main::format_date_db($jatuh_tempo_arr[$index]);
            $jumlah = $jumlah_arr[$index];
            $no_bg = $no_bg_arr[$index];
            $keterangan = $keterangan_arr[$index];
            $total_jumlah += $jumlah;

            if ($trs_kode_rekening == '1303') {

                $data_cheques = [
                    'id_penjualan' => $id_penjualan,
                    'id_distributor' => $id_distributor,
                    'no_bg_check' => $no_bg,
                    'no_invoice' => $no_invoice,
                    'tgl_cek' => $tanggal,
                    'tanggal_pencairan' => $jatuh_tempo,
                    'cek_amount' => $jumlah,
                    'sisa' => $jumlah,
                    'cek_keterangan' => $keterangan
                ];
                mCheques::create($data_cheques);
            }

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $jumlah,
                'trs_kredit' => 0,
                'user_id' => 0,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => $tipe_arus_kas,
                'trs_catatan' => $keterangan,
                'trs_charge' => 0,
                'trs_no_check_bg' => $no_bg,
                'trs_tgl_pencairan' => $jatuh_tempo,
                'trs_setor' => 0,
                'tgl_transaksi' => $this->datetime,
            ];
            mAcTransaksi::create($data_transaksi);
        }


        if ($pengiriman == 'yes') {

            /**
             * Memasukkan data kredit ke transaksi
             */

            $master_id_arr = [
                69 // Hutang Penjualan Titipan
            ];
            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                $trs_kredit = 0;
                if ($master_id == 69) {
                    $trs_kredit = $total_jumlah;
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet' => 0,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => 'kredit',
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => '',
                    'trs_setor' => 0,
                    'tgl_transaksi' => $this->datetime
                ];

                mAcTransaksi::create($data_transaksi);

            }
        } else {
            /**
             * Memasukkan data Debet & Kredit ke Transaksi
             */
            $master_id_arr = [
                94, // Potongan Penjualan
                105, // Harga Pokok penjualan
                92, // Penjualan
                81, // PPN Keluaran
                52, // Persediaan
                141 // Biaya Oks Kirim Penjualan
            ];
            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                $trs_kredit = 0;
                $trs_debet = 0;
                $trs_jenis_transaksi = '';
                $trs_catatan = '';
                /**
                 * Untuk membedakan antara debet dan kredit dan nilai nominal yang masuk
                 */
                switch ($master_id) {
                    case 94: // Potongan Penjualan
                        $penjualan_produk = mPenjualanProduk::where('id_penjualan', $id_penjualan)->get(['potongan', 'qty']);
                        $potongan_total = 0;
                        foreach ($penjualan_produk as $penjualan) {
                            $qty = $penjualan->qty;
                            $potongan = $penjualan->potongan;
                            $potongan_total += $qty * $potongan;
                        }
                        $trs_debet = $potongan_total + $potongan_akhir;
                        $trs_kredit = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                        break;
                    case 105: // Harga Pokok Penjualan
                        $trs_debet = mPenjualanProduk
                            ::leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_penjualan_produk.id_penjualan')
                            ->where('tb_penjualan.id', $id_penjualan)
                            ->sum('tb_penjualan_produk.hpp');
                        $trs_kredit = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                        break;
                    case 92: // Penjualan
                        $trs_debet = 0;
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                        foreach ($qty_arr as $index => $qty) {
                            $harga = $harga_arr[$index];
                            $potongan = $potongan_arr[$index];
                            $trs_kredit += ($harga * $qty);
                        }
                        break;
                    case 81: // PPN Keluaran
                        $trs_debet = 0;
                        $trs_kredit = 0;

                        foreach($ppn_nominal_arr as $index => $ppn_nominal) {
                            $trs_kredit += $ppn_nominal *  $qty_arr[$index];
                        }

                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                        break;
                    case 52: // Persediaan
                        $trs_debet = 0;
                        $trs_kredit = mPenjualanProduk
                            ::leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_penjualan_produk.id_penjualan')
                            ->where('tb_penjualan.id', $id_penjualan)
                            ->sum('tb_penjualan_produk.hpp');
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                        break;
                    case 141: // Biaya Oks Kirim Penjualan
                        $trs_debet = 0;
                        $trs_kredit = $biaya_tambahan;
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                        break;
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => '',
                    'trs_setor' => 0,
                    'tgl_transaksi' => $this->datetime
                ];

                mAcTransaksi::create($data_transaksi);
            }
        }
    }

    function edit($id)
    {
        $id_penjualan = Main::decrypt($id);
        $breadcrumb = [
            [
                'label' => 'Edit Penjualan Produk',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $pageTitle = 'Edit Penjualan Produk';
        $distributor = mDistributor::orderBy('nama_distributor', ' ASC')->get();
        $master = mAcMaster
            ::where('mst_pembayaran', 'yes')
            //->where('mst_nama_rekening', 'NOT LIKE', "%hutang%")
            ->get();
        $penjualan = mPenjualan::where('id', $id_penjualan)->first();
        $penjualan_produk = mPenjualanProduk::with('produk')->where('id_penjualan', $id_penjualan)->orderBy('id', 'ASC')->get();
        $penjualan_pembayaran = mPenjualanPembayaran::where('id_penjualan', $id_penjualan)->orderBy('id', 'ASC')->get();
        $penjualan_distributor = mDistributor::find($penjualan->id_distributor);
        $ekspedisi = mEkspedisi::orderBy('nama_ekspedisi')->get();
        $produk = mProduk
            ::withCount([
                'stok_produk AS total_stok_produk' => function ($query) {
                    //$query->where('qty', '<', 400);
                    $query->select(DB::raw("SUM(qty) AS qty_sum"));
                }
            ])
            ->orderBy('kode_produk', 'ASC')
            ->get();

        $data = array_merge($data, [
            'distributor' => $distributor,
            'produk' => $produk,
            'pageTitle' => $pageTitle,
            'no_faktur' => $penjualan->no_faktur,
            'master' => $master,
            //'urutan' => $urutan,
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'penjualan_pembayaran' => $penjualan_pembayaran,
            'penjualan_distributor' => $penjualan_distributor,
            'ekspedisi' => $ekspedisi,
            'methodProses' => 'edit'
        ]);

        return view('transaksi/penjualanProduk/penjualanProdukEdit', $data);
    }

    /**
     *
     * Dalam proses update penjualan ini, banyak ada kondisi yang mempangruhi data arus stok produk, stok produk, penjualan,
     * penjualan produk dan penjualan pembayaran.
     *
     * Kondisi yang riskan disini, yang mempengaruhi data stok yaitu saat pengubahan status pengiriman dalam penjualan.
     * Daftar kondisi tersebut, seperti dibawah.
     *
     * 1. Status pengiriman dari "No" menjadi "Yes"
     *    proses dilakukan yaitu pengembalian stok produk dan pembaruan data arus stok produk ke qty semula sebelum
     *    dilakukan pengurangan stok.
     *
     * 2. Status pengiriman dari "Yes" menjadi "No"
     *    proses dilakukan yaitu mengurangi stok produk yang ada, sesuai dengan qty penjualan produk lalu seterusnya,
     *    dicatat pada arus stok produk
     *
     * 3. Status pengiriman "No" tetap menjadi "No", namun qty penjualan produk berubah atau qty tidak sama (bertambah atau dikurangi)
     *    proses dilakukan yaitu mengembalikan stok produk atau mengurangi stok produk sesuai dengan kondisi qty pembaruan dan sebelum pembaruan,
     *    Lalu update arus stok produk sesuai dengan qty dan last stok produk sekarang.
     *    Kenapa diupdate ? agar kartu stok produk tetap terurut.
     *
     * @param Request $request
     * @param $id
     */
    function update(Request $request, $id)
    {
        $rules = [
            'id_distributor' => 'required',
            'id_lokasi' => 'required',
            'id_lokas.*i' => 'required',
            'master_id' => 'required',
            'master_id.*' => 'required',
            'id_produk' => 'required',
            'id_produk.*' => 'required',
            'id_stok_produk' => 'required',
            'id_stok_produk.*' => 'required'
        ];

        $attributes = [
            'id_distributor' => 'Distributor',
            'master_id' => 'Pembayaran',
            'id_produk' => 'Produk',
            'id_lokasi' => 'Gudang',
            'id_stok_produk' => 'Nomer Seri Produk',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_lokasi.' . $i] = 'Gudang ke-' . $next;
            $attr['id_produk.' . $i] = 'Produk ke-' . $next;
            $attr['id_stok_produk.' . $i] = 'Nomer Seri Produk ke-' . $next;
            $attr['master_id.' . $i] = 'Pembayaran ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $id_penjualan = Main::decrypt($id);

        $data_penjualan_pembayaran = [];

        $urutan = $request->input('urutan');
        $id_distributor = $request->input('id_distributor');
        $distributor = mDistributor::find($id_distributor);
        $id_ekspedisi = $request->input('id_ekspedisi');
        $total = $request->input('total');
        $grand_total = $request->input('grand_total');
        $terbayar = $request->input('terbayar');
        $sisa_pembayaran = $request->input('sisa_pembayaran');
        $kembalian = $request->input('kembalian');
        $no_faktur = $request->input('no_faktur');
        $no_invoice = $no_faktur;
        $id_penjualan_produk_delete = $request->input('id_penjualan_produk_delete');
        $id_penjualan_produk_delete_arr = explode(',', $id_penjualan_produk_delete);
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $jenis_transaksi = $request->input('jenis_transaksi');
        $pengiriman = $request->input('pengiriman');
        $biaya_tambahan = $request->input('biaya_tambahan');
        $potongan_akhir = $request->input('potongan_akhir');
        $id_piutang_pelanggan = '';
        $jurnal_umum_keterangan = $no_invoice == '-' ? 'Retail' : $no_invoice;
        $jurnal_umum_keterangan = 'Penjualan Produk ' . $jurnal_umum_keterangan;

        $id_produk_arr = $request->input('id_produk');
        $harga_arr = $request->input('harga');
        $ppn_nominal_arr = $request->input('ppn_nominal');
        $ppn_persen = Config::get('constants.ppnPersen');
        $harga_net_arr = $request->input('harga_net');
        $sub_total_arr = $request->input('sub_total');
        $id_lokasi_arr = $request->input('id_lokasi');
        $id_stok_produk_arr = $request->input('id_stok_produk');
        $qty_arr = $request->input('qty');
        $potongan_arr = $request->input('potongan');

        $master_id_arr = $request->input('master_id');
        $jumlah_arr = $request->input('jumlah');
        $jatuh_tempo_arr = $request->input('jatuh_tempo');
        $no_bg_arr = $request->input('no_bg');
        $keterangan_arr = $request->input('keterangan');

        DB::beginTransaction();
        try {


            /**
             * Check stok produk dengan yang dijual
             */
/*            $check_stok_produk = $this->check_stok_produk_update($id_stok_produk_arr, $qty_arr);
            if (!$check_stok_produk) {
                return response([
                    'message' => 'Stok Produk yang akan dijual <strong>Tidak Mencukupi</strong>'
                ], 422);
            }*/

            $data_penjualan = [
                'id_distributor' => $id_distributor,
                'id_ekspedisi' => $id_ekspedisi,
                'urutan' => $urutan,
                'no_faktur' => $no_faktur,
                'tanggal' => $tanggal,
                'jenis_transaksi' => $jenis_transaksi,
                'pengiriman' => $pengiriman,
                'total' => $total,
                'biaya_tambahan' => $biaya_tambahan,
                'potongan_akhir' => $potongan_akhir,
                'grand_total' => $grand_total,
                'terbayar' => $terbayar,
                'sisa_pembayaran' => $sisa_pembayaran,
                'kembalian' => $kembalian
            ];


            //mPenjualanProduk::where('id_penjualan', $id_penjualan)->delete();

            mPenjualanPembayaran::where('id_penjualan', $id_penjualan)->delete();
            mPiutangPelanggan::where('id_penjualan', $id_penjualan)->delete();

            $penjualan_before = mPenjualan::where('id', $id_penjualan)->first();
            mPenjualan::where('id', $id_penjualan)->update($data_penjualan);


            /**
             * Jika ada penjualan produk item yang sebelumnya sudah di save dan dilakukan penghapusan,
             * stok produk akan dikembalikan
             */
            if ($id_penjualan_produk_delete !== '') {
                foreach ($id_penjualan_produk_delete_arr as $id_penjualan_produk) {
                    $check_exist = mPenjualanProduk::where('id', $id_penjualan_produk)->count();

                    /**
                     * Check, apakah id_penjualan_produk yang akan dihapus, benar-benar ada di database &&
                     * penjualan sebelum di update, value pengiriman = "No"
                     */
                    if ($check_exist > 0 && $penjualan_before->pengiriman == 'no') {
                        $penjualan_produk = mPenjualanProduk::find($id_penjualan_produk);
                        $stok_produk_terkini = mStokProduk::where('id', $penjualan_produk->id_stok_produk)->value('qty');
                        $qty_tambah = $stok_produk_terkini + $penjualan_produk->qty;

                        /**
                         * Proses pengembalian stok produk, karena penjualan di delete
                         */
                        $data_update = [
                            'qty' => $qty_tambah
                        ];

                        mStokProduk
                            ::where('id', $penjualan_produk->id_stok_produk)
                            ->update($data_update);

                        /**
                         * Mencatat arus stok produk, karena delete penjualan produk
                         */

                        $last_stok = mStokProduk::where('id_produk', $penjualan_produk->id_produk)->sum('qty');
                        $last_stok_total = mStokProduk::sum('qty');

                        $data_arus_stok_produk = [
                            'tgl' => $this->datetime,
                            'table_id' => $id_penjualan_produk,
                            'table_name' => 'tb_penjualan_produk',
                            'id_penjualan' => $id_penjualan,
                            'id_penjualan_produk' => $id_penjualan_produk,
                            'id_stok_produk' => $penjualan_produk->id_stok_produk,
                            'id_produk' => $penjualan_produk->id_produk,
                            'stok_in' => $penjualan_produk->qty,
                            'stok_out' => 0,
                            'last_stok' => $last_stok,
                            'last_stok_total' => $last_stok_total,
                            'keterangan' => 'Penghapusan penjualan produk ke (' . $distributor->kode_distributor . ') ' . $distributor->nama_distributor . ' yang sebelumnya dibuat dengan tidak ada pengiriman penjualan',
                            'method' => 'delete'
                        ];
                        mArusStokProduk::create($data_arus_stok_produk);
                    }

                    mPenjualanProduk::where('id', $id_penjualan_produk)->delete();
                }
            }


            foreach ($id_produk_arr as $id_penjualan_produk => $id_produk) {

                $id_lokasi = $id_lokasi_arr[$id_penjualan_produk];
                $id_stok_produk = $id_stok_produk_arr[$id_penjualan_produk];
                $qty = $qty_arr[$id_penjualan_produk];
                $harga = $harga_arr[$id_penjualan_produk];
                $potongan = $potongan_arr[$id_penjualan_produk];
                $ppn_nominal = $ppn_nominal_arr[$id_penjualan_produk];
                $harga_net = $harga_net_arr[$id_penjualan_produk];
                $sub_total = $sub_total_arr[$id_penjualan_produk];
                $hpp_produk = mStokProduk::where('id', $id_stok_produk)->value('hpp');
                $hpp = $hpp_produk * $qty;


                /**
                 * Check apakah produk yang dikirimkan ada yang baru atau sama saja
                 */
                $check = mPenjualanProduk
                    ::where('id', $id_penjualan_produk)
                    ->count();

                /**
                 * Jika produk / id_penjualan_produk(dari index array) tidak ada, berarti produk tersebut baru ditambahkan
                 * ke dalam list item penjualan produk
                 */
                if ($check == 0) {

                    $data_penjualan_produk = [
                        'id_penjualan' => $id_penjualan,
                        'id_produk' => $id_produk,
                        'id_lokasi' => $id_lokasi,
                        'id_stok_produk' => $id_stok_produk,
                        'qty' => $qty,
                        'harga' => $harga,
                        'hpp' => $hpp,
                        'potongan' => $potongan,
                        'ppn_persen' => $ppn_persen,
                        'ppn_nominal' => $ppn_nominal,
                        'harga_net' => $harga_net,
                        'sub_total' => $sub_total
                    ];

                    $response = mPenjualanProduk::create($data_penjualan_produk);
                    $id_penjualan_produk_new = $response->id;

                    if ($pengiriman == 'no') {
                        $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
                        $last_stok_total = mStokProduk::sum('qty');
                        $data_arus_stok_produk = [
                            'tgl' => $this->datetime,
                            'table_id' => $id_penjualan_produk_new,
                            'table_name' => 'tb_penjualan_produk',
                            'id_penjualan_produk' => $id_penjualan_produk_new,
                            'id_stok_produk' => $id_stok_produk,
                            'id_produk' => $id_produk,
                            'stok_in' => 0,
                            'stok_out' => $qty,
                            'last_stok' => $last_stok,
                            'last_stok_total' => $last_stok_total,
                            'keterangan' => 'Penjualan Produk ke (' . $distributor->kode_distributor . ') ' . $distributor->nama_distributor,
                            'method' => 'insert'
                        ];

                        mArusStokProduk::create($data_arus_stok_produk);

                        /**
                         * Untuk update stok produk, jika pengiriman adalah "No"
                         * kurangi stok produk, sesuai dengan qty produk yang dikirim
                         */
                        $data_stok_produk = [
                            'qty' => $last_stok
                        ];
                        mStokProduk::where('id', $id_stok_produk)->update($data_stok_produk);
                    }


                    /**
                     * Jika tidak ada produk tambahan, berarti lakukan proses seperti biasa
                     */
                } else {
                    /**
                     * Update penjualan produk
                     */

                    $penjualan_produk_qty_before = mPenjualanProduk::where('id', $id_penjualan_produk)->value('qty');

                    $data_penjualan_produk = [
                        'id_penjualan' => $id_penjualan,
                        'id_produk' => $id_produk,
                        'id_lokasi' => $id_lokasi,
                        'id_stok_produk' => $id_stok_produk,
                        'qty' => $qty,
                        'harga' => $harga,
                        'hpp' => $hpp,
                        'potongan' => $potongan,
                        'ppn_persen' => $ppn_persen,
                        'ppn_nominal' => $ppn_nominal,
                        'harga_net' => $harga_net,
                        'sub_total' => $sub_total
                    ];

                    mPenjualanProduk::where('id', $id_penjualan_produk)->update($data_penjualan_produk);

                    /**
                     * Jika pengiriman sebelum diperbarui dan yang diperbarui tetap "No", berarti update data stok produk dan
                     * arus stok produk
                     */
                    if ($penjualan_before->pengiriman == 'no' && $pengiriman == 'no') {

                        $qty_update = $qty;
                        $qty_stok_produk_now = mStokProduk::where('id', $id_stok_produk)->value('qty');
                        if ($qty_update > $penjualan_produk_qty_before) {
                            $stok_in = 0;
                            $stok_out = $qty_update - $penjualan_produk_qty_before;
                            $qty_stok_produk = $qty_stok_produk_now - $stok_out;
                        } else {
                            $stok_in = $penjualan_produk_qty_before - $qty_update;
                            $stok_out = 0;
                            $qty_stok_produk = $qty_stok_produk_now + $stok_in;
                        }

                        mStokProduk::where('id', $id_stok_produk)->update(['qty' => $qty_stok_produk]);


                        $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
                        $last_stok_total = mStokProduk::sum('qty');

                        $data_arus_stok_produk = [
                            'tgl' => $this->datetime,
                            'table_id' => $id_penjualan_produk,
                            'table_name' => 'tb_penjualan_produk',
                            'id_penjualan' => $id_penjualan,
                            'id_penjualan_produk' => $id_penjualan_produk,
                            'id_stok_produk' => $id_stok_produk,
                            'id_produk' => $id_produk,
                            'stok_in' => $stok_in,
                            'stok_out' => $stok_out,
                            'last_stok' => $last_stok,
                            'last_stok_total' => $last_stok_total,
                            'method' => 'update',
                            'keterangan' => 'Penjualan Produk Update'
                        ];

                        mArusStokProduk::create($data_arus_stok_produk);

                    }

                    /**
                     * Jika sebelumnya penjualan melakukan pengiriman dan sekarang di perbarui tidak melakukan pengiriman
                     * maka masukkan ke arus stok produk, dan melakukan pengurangan stok produk
                     */
                    if ($penjualan_before->pengiriman == 'yes' && $pengiriman == 'no') {
                        $check_distribusi = mDistribusiDetail
                            ::where([
                                'id_penjualan' => $id_penjualan
                            ])
                            ->where('qty_dikirim', '>', 0)
                            ->count();

                        if ($check_distribusi > 0) {
                            return response([
                                'message' => 'Penjualan produk melakukan distribusi pengiriman dan distribusi produk <strong>sudah berjalan</strong>',
                                'errors' => []
                            ], 422);
                        } else {

                            /**
                             * Untuk update stok produk, jika pengiriman adalah "No"
                             * kurangi stok produk, sesuai dengan qty produk yang dikirim
                             */


                            $stok_sekarang = mStokProduk::where('id', $id_stok_produk)->value('qty');
                            $qty_stok = $stok_sekarang - $qty;

                            $data_stok_produk = [
                                'qty' => $qty_stok
                            ];
                            mStokProduk::where('id', $id_stok_produk)->update($data_stok_produk);

                            $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
                            $last_stok_total = mStokProduk::sum('qty');
                            $data_arus_stok_produk = [
                                'tgl' => $this->datetime,
                                'table_id' => $id_penjualan_produk,
                                'table_name' => 'tb_penjualan_produk',
                                'id_penjualan' => $id_penjualan,
                                'id_penjualan_produk' => $id_penjualan_produk,
                                'id_stok_produk' => $id_stok_produk,
                                'id_produk' => $id_produk,
                                'stok_in' => 0,
                                'stok_out' => $qty,
                                'last_stok' => $last_stok,
                                'last_stok_total' => $last_stok_total,
                                'keterangan' => 'Penjualan Produk ke (' . $distributor->kode_distributor . ') ' . $distributor->nama_distributor,
                                'method' => 'update'
                            ];

                            mArusStokProduk::create($data_arus_stok_produk);
                        }

                    }

                }

            }

            foreach ($master_id_arr as $id_penjualan_pembayaran => $master_id) {
                $jatuh_tempo = date('Y-m-d', strtotime($jatuh_tempo_arr[$id_penjualan_pembayaran]));
                $jumlah = $jumlah_arr[$id_penjualan_pembayaran];
                $no_bg = $no_bg_arr[$id_penjualan_pembayaran];
                $keterangan = $keterangan_arr[$id_penjualan_pembayaran];

                $data_penjualan_pembayaran[] = [
                    'id_penjualan' => $id_penjualan,
                    'master_id' => $master_id,
                    'jumlah' => $jumlah,
                    'jatuh_tempo' => $jatuh_tempo,
                    'no_check_bg' => $no_bg,
                    'keterangan' => $keterangan,
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];

                /**
                 * Insert jika piutang pelanggan, jika label perkiraan berisi kata piutang
                 */

                $nama_rekening = mAcMaster::where('master_id', $master_id)->value('mst_nama_rekening');
                $kode_rekening = mAcMaster::where('master_id', $master_id)->value('mst_kode_rekening');
                $nama_rekening = strtolower($nama_rekening);
                if (strpos($nama_rekening, 'piutang') !== FALSE) {

                    $no_piutang_pelanggan = $this->no_piutang_pelanggan();

                    $data_piutang_pelanggan = [
                        'id_distributor' => $id_distributor,
                        'id_penjualan' => $id_penjualan,
                        'table_id' => $id_penjualan,
                        'table_name' => 'tb_penjualan',
                        'no_piutang_pelanggan' => $no_piutang_pelanggan,
                        'pp_jatuh_tempo' => $jatuh_tempo,
                        'pp_no_faktur' => $no_faktur,
                        'pp_amount' => $jumlah,
                        'master_id_kode_perkiraan' => $master_id,
                        'kode_perkiraan' => $kode_rekening,
                        'pp_sisa_amount' => $jumlah,
                        'tanggal_piutang' => date('Y-m-d'),
                        'pp_keterangan' => $keterangan,
                        'pp_status' => 'belum_bayar',
                    ];

                    $response = mPiutangPelanggan::create($data_piutang_pelanggan);
                    $id_piutang_pelanggan = $response->id;
                }
            }


            /**
             * Proses pengembalian stok produk dan pencatatan arus stok produk,
             * jika penjualan tidak melakukan pengiriman dan penjualan berubah penjadi pengiriman barang
             */
            if ($penjualan_before->pengiriman == 'no' && $pengiriman == 'yes') {
                $penjualan_produk = mPenjualanProduk::where('id_penjualan', $id_penjualan)->get();
                foreach ($penjualan_produk as $r) {
                    $stok_produk_terkini = mStokProduk::where('id', $r->id_stok_produk)->value('qty');
                    $qty_tambah = $stok_produk_terkini + $r->qty;
                    /**
                     * Proses pengembalian stok produk, karena penjualan di delete
                     */
                    $data_update = [
                        'qty' => $qty_tambah
                    ];

                    mStokProduk
                        ::where('id', $r->id_stok_produk)
                        ->update($data_update);

                    $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
                    $last_stok_total = mStokProduk::sum('qty');
                    /**
                     * Mencatat arus stok produk, karena delete penjualan produk
                     */

                    $data_arus_stok_produk = [
                        'tgl' => $this->datetime,
                        'table_id' => $r->id,
                        'table_name' => 'tb_penjualan_produk',
                        'id_penjualan' => $id_penjualan,
                        'id_penjualan_produk' => $r->id,
                        'id_stok_produk' => $r->id_stok_produk,
                        'id_produk' => $r->id_produk,
                        'stok_in' => $r->qty,
                        'stok_out' => 0,
                        'last_stok' => $last_stok,
                        'last_stok_total' => $last_stok_total,
                        'keterangan' => 'Penjualan dari tidak pengiriman ke melakukan pengiriman',
                        'method' => 'delete'
                    ];
                    mArusStokProduk::create($data_arus_stok_produk);
                }
            }

            mPenjualanPembayaran::insert($data_penjualan_pembayaran);

            /**
             * Begin Akunting
             */
            $jurnal_umum_id = mAcJurnalUmum::where('id_penjualan', $id_penjualan)->value('jurnal_umum_id');

            /**
             * Untuk update, jika kode perkiraan diganti ke piutang pelanggan
             */
            if ($id_piutang_pelanggan != '') {
                $data_update = [
                    'id_piutang_pelanggan' => $id_piutang_pelanggan,
                    'jmu_keterangan' => $jurnal_umum_keterangan,
                    'no_invoice' => $no_invoice
                ];
                $data_where = [
                    'jurnal_umum_id' => $jurnal_umum_id
                ];
                mAcJurnalUmum::where($data_where)->update($data_update);
            }

            mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();
            $this->akunting_transaksi(
                $jurnal_umum_id,
                $master_id_arr,
                $jatuh_tempo_arr,
                $jumlah_arr,
                $no_bg_arr,
                $keterangan_arr,
                $id_penjualan,
                $id_distributor,
                $no_faktur,
                $tanggal,
                $pengiriman,
                $qty_arr,
                $harga_arr,
                $potongan_arr,
                $ppn_nominal_arr,
                $biaya_tambahan,
                $potongan_akhir
            );

            DB::commit();


        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function delete($id)
    {

        DB::beginTransaction();
        try {

            $id_penjualan = Main::decrypt($id);
            $penjualan = mPenjualan::find($id_penjualan);
            $jurnal_umum_id = mAcJurnalUmum::where('id_penjualan', $id_penjualan)->value('jurnal_umum_id');
            $check_piutang_pelanggan = mPiutangPelanggan::where('id_penjualan', $id_penjualan)->count();


            /**
             * Check apakah penerimaan penjualan produk terdapat di piutang pelanggan
             * Jika ada, check kembali, apakah hutang pelanggan sudah melakukan pembayaran atau belum dengan mengecheck
             * nominal piutang pelanggan dengan sisa nominal piutang pelanggan
             *           Jika ada, maka tidak bisa melakukan delete piutang pelanggan
             *           Jika tidak ada, maka bisa melakukan delete piutan pelanggan
             * Jika tidak ada, maka melakukan proses selanjutnya
             */
            if ($check_piutang_pelanggan > 0) {
                $allow_delete_piutang_pelanggan = TRUE;
                $piutang_pelanggan = mPiutangPelanggan
                    ::where('id_penjualan', $id_penjualan)
                    ->get(['pp_amount', 'pp_sisa_amount']);
                foreach ($piutang_pelanggan as $r1_piutang_pelanggan) {
                    if ($r1_piutang_pelanggan->pp_amount != $r1_piutang_pelanggan->pp_sisa_amount) {
                        $allow_delete_piutang_pelanggan = FALSE;
                        break;
                    }
                }

                if ($allow_delete_piutang_pelanggan) {
                    mPiutangPelanggan::where('id_penjualan', $id_penjualan)->delete();
                } else {
                    return response([
                        'message' => 'Penjualan Produk tidak bisa dihapus, karena sudah melakukan Penerimaan Piutang Pelanggan.'
                    ], 422);
                }

            }

            /**
             * jika penjualan yang dilakukan tidak melakukan pengiriman, qty penjualan akan dikembalikan ke stok produk
             * dan dicatat di arus stok produk
             */
            if ($penjualan->pengiriman == 'no') {

                $penjualan_produk = mPenjualanProduk::where('id_penjualan', $id_penjualan)->get();
                foreach ($penjualan_produk as $r) {
                    $stok_produk_terkini = mStokProduk::where('id', $r->id_stok_produk)->value('qty');
                    $qty_tambah = $stok_produk_terkini + $r->qty;

                    /**
                     * Proses pengembalian stok produk, karena penjualan di delete
                     */
                    $data_update = [
                        'qty' => $qty_tambah
                    ];

                    mStokProduk
                        ::where('id', $r->id_stok_produk)
                        ->update($data_update);

                    /**
                     * Mencatat arus stok produk, karena delete penjualan produk
                     */
                    $last_stok = mStokProduk::where('id_produk', $r->id_produk)->sum('qty');
                    $last_stok_total = mStokProduk::sum('qty');
                    $data_arus_stok_produk = [
                        'tgl' => $this->datetime,
                        'table_id' => $r->id,
                        'table_name' => 'tb_penjualan_produk',
                        'id_penjualan' => $id_penjualan,
                        'id_penjualan_produk' => $r->id,
                        'id_stok_produk' => $r->id_stok_produk,
                        'id_produk' => $r->id_produk,
                        'stok_in' => $r->qty,
                        'stok_out' => 0,
                        'last_stok' => $last_stok,
                        'last_stok_total' => $last_stok_total,
                        'keterangan' => 'Pengembalian produk, karena penjualan dihapus dan pengiriman = "No"',
                        'method' => 'delete'
                    ];
                    mArusStokProduk::create($data_arus_stok_produk);
                }

            } else {

                /**
                 * Check apakah penjualan sudah ada distribusi atau belum, jika sudah, tidak bisa melakukan
                 * penghapusan data penjualan
                 */

                $check_distribusi = mDistribusiDetail
                    ::where([
                        'id_penjualan' => $id_penjualan
                    ])
                    ->where('qty_dikirim', '>', 0)
                    ->count();

                if ($check_distribusi > 0) {
                    return response([
                        'message' => 'Tidak bisa dihapus, Karena penjualan produk melakukan distribusi pengiriman dan distribusi produk <strong>sudah berjalan</strong>',
                        'errors' => []
                    ], 422);
                }
            }


            /**
             * Melakukan penghapusan data penjualan, penjualan produk, penjualan pembayaran, jurnal umum dan transaksi
             * karena data stok tidak ada pengurangan, dan belum melakukan distribusi produk
             */

            mPenjualan::where('id', $id_penjualan)->delete();
            mPenjualanProduk::where('id_penjualan', $id_penjualan)->delete();
            mPenjualanPembayaran::where('id_penjualan', $id_penjualan)->delete();
            mAcJurnalUmum::where('id_penjualan', $id_penjualan)->delete();
            mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }

    function faktur($id)
    {
        $id_penjualan = Main::decrypt($id);
        $penjualan = mPenjualan
            ::with('distributor:id,nama_distributor,alamat_distributor')
            ->where('id', $id_penjualan)
            ->first();
        $penjualan_produk = mPenjualanProduk::where('id_penjualan', $id_penjualan)->get();
        //$penjualan_pembayaran = mPenjualanPembayaran::where('id_penjualan', $id_penjualan)->get();
        $company = Main::companyInfo();

        $data = [
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'no' => 1,
            'company' => $company
        ];

        //return view('transaksi/penjualanProduk/penjualanProdukInvoice', $data);

        $pdf = PDF::loadView('transaksi/penjualanProduk/penjualanProdukInvoice', $data);
        //return $pdf->stream();
        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('Penjualan Produk ' . $penjualan->no_faktur . '.pdf');

    }


    function no_faktur(Request $request)
    {
        $id_distributor = $request->input('id_distributor');
        $kode_distributor = $request->input('kode_distributor');
        $tanggal_transaksi = $request->input('tanggal_transaksi');
        $year = date('Y');
        $month = date('m');

        $urutan = mPenjualan
            ::where('id_distributor', $id_distributor)
            ->whereYear('tanggal', $year)
            ->whereMonth('tanggal', $month);
        $urutan_next = $urutan->count() > 0 ? $urutan->orderBy('id','DESC')->value('urutan') + 1 : 1;

        $no_faktur = $kode_distributor
            . '.' . str_pad($urutan_next, 2, "0", STR_PAD_LEFT)
            . '.' . $month
            . '.' . $year;

        $data_response = [
            'no_faktur' => $no_faktur,
            'urutan'=>$urutan_next,
        ];

        return $data_response;

    }

    function urutan()
    {
        $count = mPenjualan::count();
        if ($count == 0) {
            return 1;
        } else {
            $urutan = mPenjualan::orderBy('urutan', 'DESC')->first(['urutan']);
            return $urutan->urutan + 1;
        }
    }

    function urutan_piutang_pelanggan()
    {
        $count = mHutangSupplier::count();
        if ($count == 0) {
            return 1;
        } else {
            $urutan = mHutangSupplier::orderBy('urutan', 'DESC')->first(['urutan']);
            return $urutan->urutan + 1;
        }
    }

    function no_piutang_pelanggan()
    {
        $tahun = date('Y');
        $thn = substr($tahun, 2, 2);
        $bulan = date('m');
        $tgl = date('d');
        $kode_label = Config::get('constants.kodePiutangPelanggan');
        $no_po_last = mPiutangPelanggan
            ::where('no_piutang_pelanggan', 'like', $thn . $bulan . $tgl . '-' . $kode_label . '%')
            ->max('no_piutang_pelanggan');
        //mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        $lastNoUrut = substr($no_po_last, 11, 4);
        if ($lastNoUrut == 0) {
            $nextNoUrut = 0 + 1;
        } else {
            $nextNoUrut = $lastNoUrut + 1;
        }
        return $thn . $bulan . $tgl . '-' . $kode_label . '-' . sprintf('%04s', $nextNoUrut);
    }

    function produk_stok(Request $request)
    {
        $id_produk = $request->input('id_produk');
        $produkStok = mStokProduk
            ::with('lokasi')
            ->where([
                'id_produk' => $id_produk,
            ])
            ->where('qty', '>', 0)
            ->get();

        $data = [
            'produkStok' => $produkStok,
            'no' => 1
        ];

        return view('transaksi/penjualanProduk/tbodyProdukStok', $data);
    }

    function no_seri_produk(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_produk = $request->input('id_produk');

        $stok_produk = mStokProduk
            ::where(['id_lokasi' => $id_lokasi, 'id_produk' => $id_produk])
            ->where('qty', '>', 0)
            ->orderBy('no_seri_produk')
            ->get(['id', 'no_seri_produk', 'qty']);

        return $stok_produk;
    }

    function produk_harga(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_produk = $request->input('id_produk');
        $qty = $request->input('qty');
        $harga = 0;

        $hargaProduk = mHargaProduk
            ::where([
                ['id_produk', '=', $id_produk],
                //['rentang_awal', '>=', $qty],
                //['rentang_akhir', '<=', $qty]
            ])
            ->get();
        foreach ($hargaProduk as $r) {
            if ($qty >= $r->rentang_awal && $qty <= $r->rentang_akhir) {
                $harga = $r->harga;
                break;
            }
        }


        $data = [
            'number_raw' => $harga,
            'number_format' => Main::format_number($harga)
        ];

        return $data;
    }

    function produk_gudang(Request $request)
    {
        $id_produk = $request->input('id_produk');

        $gudang = mStokProduk
            ::with('lokasi')
            ->distinct()
            ->where([
                'id_produk' => $id_produk,
            ])
            ->get(['id_lokasi']);
        return $gudang;
    }

    function check_stok_produk($id_stok_produk_arr, $qty_arr)
    {
        $valid = TRUE;
        foreach ($id_stok_produk_arr as $index => $id_stok_produk) {
            $stok_produk_qty = mStokProduk::where('id', $id_stok_produk)->value('qty');
            $jual_qty = $qty_arr[$index];

            if ($stok_produk_qty < $jual_qty) {
                $valid = FALSE;
                break;
            }
        }

        return $valid;
    }

    function check_stok_produk_update($id_stok_produk_arr, $qty_arr)
    {
        $valid = TRUE;
        foreach ($id_stok_produk_arr as $index => $id_stok_produk) {
            $stok_produk_qty = mStokProduk::where('id', $id_stok_produk)->value('qty');
            $jual_qty = $qty_arr[$index];

            if ($stok_produk_qty < $jual_qty) {
                $valid = FALSE;
                break;
            }
        }

        return $valid;
    }
}
