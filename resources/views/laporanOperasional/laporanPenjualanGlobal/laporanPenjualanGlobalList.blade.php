@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">


            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ $date_start }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end" value="{{ $date_end }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('laporanPenjualanGlobalPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('laporanPenjualanGlobalExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th>No Faktur</th>
                            <th>Tanggal</th>
                            <th>Jenis Transaksi</th>
                            <th>Distributor</th>
                            <th>Total</th>
                            <th>Biaya Tambahan</th>
                            <th>Potongan</th>
                            <th>PPN</th>
                            <th>Grand Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            @php
                                $total_grand_total += $r->grand_total;
                                $total_biaya_tambahan += $r->biaya_tambahan;
                                $total_potongan += $r->total_potongan;
                                $total_ppn_nominal += $r->total_ppn_nominal;
                            @endphp
                            <tr>
                                <td>{{ $r->no_faktur }}</td>
                                <td>{{ Main::format_date($r->tanggal) }}</td>
                                <td>{{ ucwords($r->jenis_transaksi) }}</td>
                                <td>{{ '('.$r->distributor->kode_distributor.') '.$r->distributor->nama_distributor }}</td>
                                <td align="right">{{ Main::format_number($r->total) }}</td>
                                <td align="right">{{ Main::format_number($r->biaya_tambahan) }}</td>
                                <td align="right">{{ Main::format_number($r->total_potongan) }}</td>
                                <td align="right">{{ Main::format_number($r->total_ppn_nominal) }}</td>
                                <td align="right">{{ Main::format_number($r->grand_total) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5" align="center" class="font-weight-bold">
                                Total
                            </td>
                            <td align="right" class="font-weight-bold">{{ Main::format_number($total_biaya_tambahan) }}</td>
                            <td align="right" class="font-weight-bold">{{ Main::format_number($total_potongan) }}</td>
                            <td align="right" class="font-weight-bold">{{ Main::format_number($total_ppn_nominal) }}</td>
                            <td align="right" class="font-weight-bold">{{ Main::format_number($total_grand_total) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection