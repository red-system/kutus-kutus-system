<?php

namespace app\Http\Controllers\Distribusi;

use app\Helpers\hAkunting;
use app\Helpers\Main;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use app\Models\mArusStokProduk;
use app\Models\mDistributor;
use app\Models\mLokasi;
use app\Models\mPenjualanProduk;
use app\Models\mStokProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\mTargetProduksi;
use app\Models\mBahan;
use app\Models\mSupplier;

use app\Models\mDistributorOrder;
use app\Models\mDistributorOrderProduk;
use app\Models\mPenjualan;
use app\Models\mDistribusi;
use app\Models\mDistribusiDetail;

use Illuminate\Support\Facades\Session;
use PDF;

class Distribusi extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['distribusi'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['distribusi'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mPenjualan
            ::with('distributor')
            ->withCount([
                'penjualan_produk AS total_qty' => function ($query) {
                    $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->where('pengiriman', 'yes')
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list
        ]);

        return view('distribusi/distribusi/distribusiList', $data);
    }

    function distribusi($id)
    {
        $id_penjualan = Main::decrypt($id);
        $dataMain = Main::data($this->breadcrumb, $this->menuActive);
        $penjualan = mPenjualan::with('distributor')->where('id', $id_penjualan)->first();
        $urutan = $this->urutan($penjualan->id_distributor);
        $no_distribusi = $this->no_distribusi($penjualan->id_distributor, $urutan);
        $penjualan_produk = mPenjualanProduk
            ::with(['produk', 'stok_produk', 'lokasi'])
            ->where('id_penjualan', $id_penjualan)
            ->get();

        $data = array_merge($dataMain, [
            'tab' => 'distribusi',
            'id_penjualan' => $id_penjualan,
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'urutan' => $urutan,
            'no_distribusi' => $no_distribusi
        ]);

        return view('distribusi/distribusi/distribusiDo', $data);
    }

    function distribusi_insert(Request $request)
    {
        $valid_sisa_kirim = TRUE;
        $valid_qty_produk_tercover = TRUE;
        $nama_produk_warning_stok = '';
        $sisa_stok_produk = 0;

        $id_penjualan = $request->input('id_penjualan');
        $penjualan = mPenjualan::find($id_penjualan);
        $distributor = mDistributor::find($penjualan->id_distributor);
        $urutan = $request->input('urutan');
        $no_distribusi = $request->input('no_distribusi');
        $catatan = $request->input('catatan');
        $alamat_lain = $request->input('alamat_lain');
        $no_polisi = $request->input('no_polisi');
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $jmu_no = hAkunting::jmu_no($year, $month, $day);
        $jmu_keterangan = '';
        $list_nama_produk = '';

        $id_penjualan_produk_arr = $request->input('id_penjualan_produk');
        $id_stok_produk_arr = $request->input('id_stok_produk');
        $id_produk_arr = $request->input('id_produk');
        $kode_produk_arr = $request->input('kode_produk');
        $nama_produk_arr = $request->input('nama_produk');
        $no_seri_produk_arr = $request->input('no_seri_produk');
        $kode_lokasi_arr = $request->input('kode_lokasi');
        $lokasi_arr = $request->input('lokasi');
        $qty_minta_arr = $request->input('qty_minta');
        $qty_terkirim_arr = $request->input('qty_terkirim');
        $qty_dikirim_arr = $request->input('qty_dikirim');
        $remark_arr = $request->input('remark');
        $id_distribusi_detail_arr = [];

        foreach ($id_penjualan_produk_arr as $index => $id_penjualan_produk) {
            $qty_kirim = $qty_dikirim_arr[$index] + $qty_terkirim_arr[$index];
            if ($qty_kirim > $qty_minta_arr[$index]) {
                $valid_sisa_kirim = FALSE;
                break;
            }
        }

        if ($valid_sisa_kirim) {

            DB::beginTransaction();

            try {

                $data_distribusi = [
                    'id_penjualan' => $id_penjualan,
                    'tanggal_distribusi' => $this->datetime,
                    'urutan' => $urutan,
                    'no_distribusi' => $no_distribusi,
                    'no_polisi' => $no_polisi,
                    'catatan' => $catatan,
                    'alamat_lain' => $alamat_lain
                ];

                $response = mDistribusi::create($data_distribusi);
                $id_distribusi = $response->id;

                Session::put(['id_distribusi' => $id_distribusi]);

                foreach ($id_penjualan_produk_arr as $index => $id_penjualan_produk) {

                    $id_stok_produk = $id_stok_produk_arr[$index];
                    $id_produk = $id_produk_arr[$index];
                    $qty_dikirim = $qty_dikirim_arr[$index];
                    $kode_produk = $kode_produk_arr[$index];
                    $nama_produk = $nama_produk_arr[$index];
                    $no_seri_produk = $no_seri_produk_arr[$index];
                    $kode_lokasi = $kode_lokasi_arr[$index];
                    $lokasi = $lokasi_arr[$index];
                    $remark = $remark_arr[$index];
                    $list_nama_produk .= $nama_produk . ', ';

                    $data_distribusi_detail = [
                        'id_penjualan' => $id_penjualan,
                        'id_distribusi' => $id_distribusi,
                        'id_penjualan_produk' => $id_penjualan_produk,
                        'id_stok_produk' => $id_stok_produk,
                        'qty_dikirim' => $qty_dikirim,
                        'remark' => $remark,
                        'created_at' => $this->datetime,
                        'updated_at' => $this->datetime
                    ];

                    $response = mDistribusiDetail::create($data_distribusi_detail);
                    $id_distribusi_detail = $response->id;
                    $id_distribusi_detail_arr[] = $id_distribusi_detail;

                    /**
                     * Proses pengurangan stok produk
                     */

                    $qty_stok_produk = mStokProduk::where('id', $id_stok_produk)->value('qty');

/*                    if($id_stok_produk == '292') {
                        $stok_produk = mStokProduk::where('id', $id_stok_produk)->first();
                        return response([
                            'message'=>$id_stok_produk.' '.json_encode($stok_produk)
                        ], 422);
                    }*/


                    /**
                     * check stok produk yang ada tercover untuk melakukan distribusi atau tidak
                     */

                    echo $qty_stok_produk.' - '.$qty_dikirim.'<br />';

                    if ($qty_stok_produk < $qty_dikirim) {
                        $valid_qty_produk_tercover = FALSE;
                        $nama_produk_warning_stok = $kode_produk . ' ' . $nama_produk;
                        $sisa_stok_produk = $qty_stok_produk;
                        break;
                    }

                    $qty_now = $qty_stok_produk - $qty_dikirim;

                    mStokProduk
                        ::where('id', $id_stok_produk)
                        ->update(['qty' => $qty_now]);

                    $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
                    $last_stok_total = mStokProduk::sum('qty');

                    $data_arus_stok_produk = [
                        'tgl' => $this->datetime,
                        /*                        'table_id' => $id_penjualan_produk,
                                                'table_name' => 'tb_penjualan_produk',*/
                        'id_penjualan' => $id_penjualan,
                        'id_penjualan_produk' => $id_penjualan_produk,
                        'id_distribusi' => $id_distribusi,
                        'id_distribusi_detail' => $id_distribusi_detail,
                        'id_stok_produk' => $id_stok_produk,
                        'id_produk' => $id_produk,
                        'stok_in' => 0,
                        'stok_out' => $qty_dikirim,
                        'last_stok' => $last_stok,
                        'last_stok_total' => $last_stok_total,
                        'method' => 'insert',
                        'keterangan' => 'Distribusi ke (' . $distributor->kode_distributor . ') ' . $distributor->nama_distributor
                    ];

                    mArusStokProduk::create($data_arus_stok_produk);

                }

                if (!$valid_qty_produk_tercover) {
                    $message = 'Stok Produk : ' . $nama_produk_warning_stok . '
                            <br />
                            No Batch : ' . $no_seri_produk . '
                            <br />
                            Lokasi Gudang : ' . $kode_lokasi . ' ' . $lokasi . '
                            <br />
                            Hanya tinggal <strong>' . $qty_stok_produk . ' - ' . $qty_dikirim . ' - '.$id_stok_produk.'</strong>.
                            <br />
                            <strong>Sehingga tidak bisa melakukan Distribusi.</strong>';
                    return response([
                        'message' => $message
                    ], 422);
                }


                /**
                 * Proses apakah qty sudah terkirim semua atau belum,
                 * jika sudah, maka status_distribusi penjualan menjadi done
                 */
                $check_total_terkirim = $this->check_distribusi_terkirim_semua(
                    $id_penjualan_produk_arr,
                    $qty_dikirim_arr,
                    $qty_terkirim_arr,
                    $qty_minta_arr
                );

                if ($check_total_terkirim) {
                    $data_update = [
                        'status_distribusi' => 'done',
                        'status_distribusi_done_at' => $this->datetime
                    ];
                    mPenjualan::where('id', $id_penjualan)->update($data_update);
                }

                /**
                 * Begin Akunting
                 */
                $jmu_keterangan = ' Distribusi produk: ' . $list_nama_produk . ' ke distributor : ' . $distributor->nama_distributor;
                $data_jurnal_umum = [
                    'no_invoice' => $no_distribusi,
                    'id_penjualan' => $id_penjualan,
                    'id_distributor' => $penjualan->id_distributor,
                    'id_distribusi' => $id_distribusi,
                    'jmu_tanggal' => $this->datetime,
                    'jmu_no' => $jmu_no,
                    'jmu_keterangan' => $jmu_keterangan,
                    'jmu_year' => $year,
                    'jmu_month' => $month,
                    'jmu_day' => $day
                ];

                $response = mAcJurnalUmum::create($data_jurnal_umum);
                $jurnal_umum_id = $response->jurnal_umum_id;

                $this->akunting_transaksi(
                    $jurnal_umum_id,
                    $id_penjualan,
                    $id_distribusi,
                    $catatan,
                    $id_distribusi_detail_arr
                );

                DB::commit();

            } catch (\Exception $exception) {
                throw new $exception;

                DB::rollBack();
            }

        } else {
            return response([
                'message' => 'Qty Kirim <strong>Melebihi</strong> Sisa Qty Pengiriman',
                'errors' => [
                    ''
                ]
            ], 422);
        }

    }

    function history($id_penjualan)
    {

        $id_distribusi = Session::get('id_distribusi');
        $download_surat_jalan = '';
        if ($id_distribusi) {
            $url = route('distribusiCetak', ['id' => Main::encrypt($id_distribusi)]);
            $download_surat_jalan = "<script>window.open('" . $url . "', '_blank')</script>";
            Session::forget('id_distribusi');
        }

        $id_penjualan = Main::decrypt($id_penjualan);
        $dataMain = Main::data($this->breadcrumb, $this->menuActive);

        $penjualan = mPenjualan
            ::with('distributor')
            ->where('id', $id_penjualan)
            ->first();

        $distribusi = mDistribusi
            ::with([
                'penjualan'
            ])
            ->withCount([
                'distribusi_detail AS total_dikirim' => function ($query) {
                    return $query->select(DB::raw('SUM(qty_dikirim) AS qty_sum'));
                }
            ])
            ->where('id_penjualan', $id_penjualan)
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($dataMain, [
            'tab' => 'history',
            'penjualan' => $penjualan,
            'distribusi' => $distribusi,
            'id_penjualan' => $id_penjualan,
            'download_surat_jalan' => $download_surat_jalan
        ]);

        return view('distribusi/distribusi/distribusiHistory', $data);
    }

    function delete($id_distribusi)
    {
        $distribusi = mDistribusi::find($id_distribusi);
        $id_penjualan = $distribusi->id_penjualan;
        $jurnal_umum_id = mAcJurnalUmum::where('id_distribusi', $id_distribusi)->value('jurnal_umum_id');
        $id_distribusi_detail_arr = [];
        $keterangan = 'Delete dari distribusi : pengembalian stok saat delete distribusi';

        DB::beginTransaction();

        try {

            mPenjualan
                ::where('id', $id_penjualan)
                ->update(['status_distribusi' => 'not_yet']);

            $distribusi_detail = mDistribusiDetail
                ::with('stok_produk:id,id_produk')
                ->where('id_distribusi', $id_distribusi)
                ->get();
            foreach ($distribusi_detail as $r) {
                $id_distribusi_detail_arr[] = $r->id;
                $qty = mStokProduk::where('id', $r->id_stok_produk)->value('qty');
                $qty_now = $qty + $r->qty_dikirim;
                mStokProduk::where('id', $r->id_stok_produk)->update(['qty' => $qty_now]);

                $last_stok = mStokProduk::where('id_produk', $r->stok_produk->id_produk)->sum('qty');
                $last_stok_total = mStokProduk::sum('qty');

                $data_arus_stok_produk = [
                    'tgl' => $this->datetime,
                    'table_id' => $r->id,
                    'table_name' => 'tb_distribusi_detail',
                    'id_penjualan' => $id_penjualan,
                    'id_distribusi' => $id_distribusi,
                    'id_distribusi_detail' => $r->id,
                    'id_stok_produk' => $r->id_stok_produk,
                    'id_produk' => $r->stok_produk->id_produk,
                    'stok_in' => $r->qty_dikirim,
                    'stok_out' => 0,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'method' => 'delete',
                    'keterangan' => $keterangan
                ];
                mArusStokProduk::create($data_arus_stok_produk);

            }

            mDistribusi::where('id', $id_distribusi)->delete();
            mDistribusiDetail::where('id_distribusi', $id_distribusi)->delete();
            mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->delete();
            mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();


            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }

    function pdf($id)
    {
        $id_distribusi = Main::decrypt($id);
        $distribusi = mDistribusi
            ::with([
                'penjualan.distributor:id,kode_distributor,nama_distributor,alamat_distributor'
            ])
            ->where('id', $id_distribusi)
            ->first();
        $distribusi_detail = mDistribusiDetail
            ::with([
                'penjualan_produk.produk.kategori_produk'
            ])
            ->where('id_distribusi', $id_distribusi)
            ->get();
        $company = Main::companyInfo();

        $data = [
            'distribusi' => $distribusi,
            'distribusi_detail' => $distribusi_detail,
            'company' => $company,
            'no' => 1
        ];

        $pdf = PDF::loadView('distribusi/distribusi/distribusiPdf', $data);
        //return $pdf->stream();
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Surat Jalan - ' . $distribusi->no_distribusi . '.pdf');
    }

    function urutan($id_distributor)
    {
        $check = mDistribusi
            ::select('tb_distribusi.urutan')
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_distribusi.id_penjualan')
            ->whereYear('tb_distribusi.tanggal_distribusi', date('Y'))
            ->whereMonth('tb_distribusi.tanggal_distribusi', date('m'))
            ->where('tb_penjualan.id_distributor', $id_distributor);

        //echo json_encode($check->count());
        //echo json_encode($check->orderby('tb_distribusi.urutan', 'DESC')->first()->urutan);

        if ($check->count() == 0) {
            return 1;
        } else {
            $urutan = $check
                ->orderBy('tb_distribusi.urutan', 'DESC')
                ->first()->urutan;
            return $urutan + 1;
        }
    }

    /**
     *
     * Berguna untuk mengecheck apakah qty yang akan dikirimkan sudah pas dengan sisa qty pengiriman atau melebihi
     *
     * @param $id_penjualan_produk_arr
     * @param $qty_dikirim_arr
     * @param $qty_terkirim_arr
     * @param $qty_minta_arr
     */
    function check_distribusi_terkirim_semua($id_penjualan_produk_arr, $qty_dikirim_arr, $qty_terkirim_arr, $qty_minta_arr)
    {
        $valid = TRUE;
        foreach ($id_penjualan_produk_arr as $index => $id_penjualan_produk) {
            $qty_kirim = $qty_dikirim_arr[$index] + $qty_terkirim_arr[$index];
            if ($qty_kirim != $qty_minta_arr[$index]) {
                $valid = FALSE;
                break;
            }
        }

        return $valid;
    }

    function no_distribusi($id_distributor, $urutan)
    {
        $kode_distributor = mDistributor::where('id', $id_distributor)->value('kode_distributor');
        $urutan = str_pad($urutan, 2, '0', STR_PAD_LEFT);
        $no_distribusi = $kode_distributor . '.' . $urutan . '.' . date('m') . '.' . date('Y');

        return $no_distribusi;
    }

    function akunting_transaksi(
        $jurnal_umum_id,
        $id_penjualan,
        $id_distribusi,
        $catatan,
        $id_distribusi_detail_arr
    )
    {

        $master_id_arr = [69, 94, 105, 92, 81, 141, 52];
        $year = date('Y');
        $month = date('m');
        foreach ($master_id_arr as $index => $master_id) {
            $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master->mst_kode_rekening;
            $trs_nama_rekening = $master->mst_nama_rekening;
            $trs_jenis_transaksi = '';
            $trs_debet = '';
            $trs_kredit = '';

            switch ($master_id) {
                case 69: // Hitung Penjualan Titipan
                    $trs_jenis_transaksi = 'debet';
                    $trs_debet
                        = (
                            $this->kalkulasi_penjualan($id_distribusi_detail_arr)
                            + $this->kalkulasi_biaya_tambahan($id_penjualan)
                            + $this->kalkulasi_ppn_keluaran($id_distribusi_detail_arr)
                        )
                        - $this->kalkulasi_potongan_penjualan($id_distribusi_detail_arr);
                    $trs_kredit = 0;
                    break;
                case 94: // Potongan penjualan
                    $trs_jenis_transaksi = 'debet';
                    $trs_debet = $this->kalkulasi_potongan_penjualan($id_distribusi_detail_arr);
                    $trs_kredit = 0;
                    break;
                case 105: // Harga Pokok Penjualan
                    $trs_jenis_transaksi = 'debet';
                    $trs_debet = $this->kalkulas_hpp($id_distribusi_detail_arr);
                    $trs_kredit = 0;
                    break;
                case 92: // Penjualan
                    $trs_jenis_transaksi = 'kredit';
                    $trs_debet = 0;
                    $trs_kredit = $this->kalkulasi_penjualan($id_distribusi_detail_arr);
                    break;
                case 81: // PPN Keluaran
                    $trs_jenis_transaksi = 'kredit';
                    $trs_debet = 0;
                    $trs_kredit = $this->kalkulasi_ppn_keluaran($id_distribusi_detail_arr);
                    break;
                case 141: // Biaya Oks Kirim Penjualan
                    $trs_jenis_transaksi = 'kredit';
                    $trs_debet = 0;
                    $trs_kredit = $this->kalkulasi_biaya_oks_kirim_penjualan($id_penjualan);
                    break;
                case 52: // Persediaan
                    $trs_jenis_transaksi = 'kredit';
                    $trs_debet = 0;
                    $trs_kredit = $this->kalkulasi_persedian($id_distribusi_detail_arr);
                    break;
            }

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id,
                'id_distribusi' => $id_distribusi,
                'trs_jenis_transaksi' => $trs_jenis_transaksi,
                'tgl_transaksi' => $this->datetime,
                'trs_debet' => $trs_debet,
                'trs_kredit' => $trs_kredit,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $catatan,
                'trs_tgl_pencairan' => $this->datetime,
            ];

            mAcTransaksi::create($data_transaksi);
        }
    }

    function test()
    {
        return $this->kalkulasi_ppn_keluaran([90, 87]);
    }

    function kalkulasi_penjualan($id_distribusi_detail_arr)
    {
        $penjualan = 0;
        $distribusi_detail = mDistribusiDetail
            ::select(['id', 'id_penjualan_produk', 'qty_dikirim'])
            ->with('penjualan_produk:id,harga,potongan')
            ->whereIn('id', $id_distribusi_detail_arr)
            ->get();
        foreach ($distribusi_detail as $r) {
            $harga = $r->penjualan_produk->harga;
            $qty_dikirim = $r->qty_dikirim;
            $potongan = $r->penjualan_produk->potongan;
            $penjualan += ($harga * $qty_dikirim) - ($potongan * $qty_dikirim);
        }

        return $penjualan;
    }

    function kalkulasi_biaya_oks_kirim_penjualan($id_penjualan)
    {
        $biaya_tambahan = mPenjualan::where('id', $id_penjualan)->value('biaya_tambahan');
        $qty_total = mPenjualanProduk::where('id_penjualan', $id_penjualan)->sum('qty');
        $qty_dikirim_total = mDistribusiDetail::where('id_penjualan', $id_penjualan)->sum('qty_dikirim');
        if ($biaya_tambahan == 0) {
            $biaya_tambahan = 0;
        } else {
            $biaya_tambahan = ($biaya_tambahan / $qty_total) * $qty_dikirim_total;
        }

        return $biaya_tambahan;
    }

    function kalkulasi_biaya_tambahan($id_penjualan)
    {
        $biaya_tambahan = mPenjualan::where('id', $id_penjualan)->value('biaya_tambahan');

        return $biaya_tambahan;
    }

    function kalkulasi_potongan_penjualan($id_distribusi_detail_arr)
    {
        $potongan_penjualan = 0;
        $distribusi_detail = mDistribusiDetail
            ::select(['id', 'id_penjualan_produk', 'qty_dikirim'])
            ->with('penjualan_produk:id,potongan')
            ->whereIn('id', $id_distribusi_detail_arr)
            ->get();
        foreach ($distribusi_detail as $r) {
            $qty_dikirim = $r->qty_dikirim;
            $potongan = $r->penjualan_produk->potongan;
            $potongan_penjualan += $potongan * $qty_dikirim;
        }

        return $potongan_penjualan;
    }

    function kalkulasi_ppn_keluaran($id_distribusi_detail_arr)
    {
        $ppn_keluaran = 0;
        $distribusi_detail = mDistribusiDetail
            ::select(['id', 'id_penjualan_produk', 'qty_dikirim'])
            ->with('penjualan_produk:id,ppn_nominal')
            ->whereIn('id', $id_distribusi_detail_arr)
            ->get();
        foreach ($distribusi_detail as $r) {
            $qty_dikirim = $r->qty_dikirim;
            $ppn_nominal = $r->penjualan_produk->ppn_nominal;
            $ppn_keluaran += $ppn_nominal * $qty_dikirim;
        }

        return $ppn_keluaran;
    }

    function kalkulasi_persedian($id_distribusi_detail_arr)
    {
        $persediaan = 0;
        $distribusi_detail = mDistribusiDetail
            ::select(['id', 'id_stok_produk', 'qty_dikirim'])
            ->with('stok_produk:id,hpp')
            ->whereIn('id', $id_distribusi_detail_arr)
            ->get();
        foreach ($distribusi_detail as $r) {
            $qty_dikirim = $r->qty_dikirim;
            $hpp = $r->stok_produk->hpp;
            $persediaan += $hpp * $qty_dikirim;
        }

        return $persediaan;
    }

    function kalkulas_hpp($id_distribusi_detail_arr)
    {
        $hpp_total = 0;
        $distribusi_detail = mDistribusiDetail
            ::select(['id', 'id_stok_produk', 'qty_dikirim'])
            ->with('stok_produk:id,hpp')
            ->whereIn('id', $id_distribusi_detail_arr)
            ->get();
        foreach ($distribusi_detail as $r) {
            $qty_dikirim = $r->qty_dikirim;
            $hpp = $r->stok_produk->hpp;
            $hpp_total += $hpp * $qty_dikirim;
        }

        return $hpp_total;
    }

}
