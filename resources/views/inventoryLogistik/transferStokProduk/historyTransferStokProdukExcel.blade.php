<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=History Transfer Stok Produk " . date('d-m-Y') . ".xls");
?>

<h3 align="center">History Transfer Stok Bahan</h3>
Tanggal History: {{ $date_start }} s/d {{ $date_end }}
<br />
<table width="100%" border="1">
    <thead>
    <tr class="tabletitle">
        <th width="20">No</th>
        <th>Tanggal</th>
        <th>Kode Produk</th>
        <th>Nama Produk</th>
        <th>No Seri Produk</th>
        <th>Qty Transfer</th>
        <th>Dari Lokasi</th>
        <th>Lokasi Tujuan</th>
        <th>Last Stok</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
        $total = 0;
    @endphp
    @foreach($list as $r)

        @php
            $total += $r->total_diperlukan;
        @endphp

        <tr class="service">
            <td>{{ $no++ }}</td>
            <td>{{ $r->tgl }}</td>
            <td>{{ $r->kode_produk }}</td>
            <td>{{ $r->nama_produk }}</td>
            <td>{{ $r->no_seri_produk }}</td>
            <td>{{ Main::format_number($r->qty_transfer) }}</td>
            <td>{{ $r->dari }}</td>
            <td>{{ $r->tujuan }}</td>
            <td>{{ Main::format_number($r->last_stok) }}</td>

        </tr>
    @endforeach
    </tbody>
</table>