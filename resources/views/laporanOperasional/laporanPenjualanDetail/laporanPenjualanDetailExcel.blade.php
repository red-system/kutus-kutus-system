<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Penjualan Detail " . date('d-m-Y') . ".xls");
?>

<h3 align="center">LAPORAN PENJUALAN DETAIL</h3>
<table width="100%">
    <tbody>
    <tr>
        <td align="center">Tanggal Penjualan: {{ $date_start }} s/d {{ $date_end }}</td>
    </tr>
    </tbody>
</table>
<br />
<table width="100%" border="1">
    <thead>
    <tr>
        <th>No Faktur</th>
        <th>Tanggal</th>
        <th>Jenis Transaksi</th>
        <th>Distributor</th>
        <th>Nama Produk</th>
        <th>Gudang</th>
        <th>No Seri Produk</th>
        <th>Harga</th>
        <th>Potongan</th>
        <th>PPN({{ $ppnPersen }}%)</th>
        <th>Harga Net</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        @php($total += $r->sub_total)
        <tr>
            <td>{{ $r->no_faktur }}</td>
            <td>{{ Main::format_date($r->tanggal) }}</td>
            <td>{{ ucwords($r->jenis_transaksi) }}</td>
            <td>{{ '('.$r->kode_distributor.') '.$r->nama_distributor }}</td>
            <td>{{ '('.$r->produk->kode_produk.') '.$r->produk->nama_produk }}</td>
            <td>{{ '('.$r->lokasi['kode_lokasi'].') '.$r->lokasi['lokasi'] }}</td>
            <td>{{ $r->stok_produk['no_seri_produk'] }}</td>
            <td align="right">{{ Main::format_number($r->harga) }}</td>
            <td align="right">{{ Main::format_number($r->potongan) }}</td>
            <td align="right">{{ Main::format_number($r->ppn_nominal) }}</td>
            <td align="right">{{ Main::format_number($r->harga_net) }}</td>
            <td align="right">{{ Main::format_number($r->sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="11" align="center">
            <strong>Total</strong>
        </td>
        <td align="right">{{ Main::format_number($total) }}</td>
    </tr>
    </tfoot>
</table>