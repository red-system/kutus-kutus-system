<form action="" method="post" class="m-form form-send m-form--label-align-right">
    {{ csrf_field() }}

    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">

        <input type="hidden" name="id_bahan">
        <input type="hidden" name="id_kategori_bahan">
        <input type="hidden" name="id_lokasi">
        <input type="hidden" name="kode_bahan">
        <input type="hidden" name="nama_bahan">
        <input type="hidden" name="kategori_bahan">
        <input type="hidden" name="lokasi">
        <input type="hidden" name="qty_awal">


        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Penyesuaian Stok Bahan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 form-control-label">Kode Bahan </label>
                                <div class="col-lg-8 ">
                                    <span class="kode_bahan"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 form-control-label">Nama Bahan </label>
                                <div class="col-lg-8 ">
                                    <span class="nama_bahan"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 form-control-label">Kategori Bahan </label>
                                <div class="col-lg-8 ">
                                    <span class="kategori_bahan"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 form-control-label">Lokasi </label>
                                <div class="col-lg-8 ">
                                    <span class="lokasi"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 form-control-label">Keterangan </label>
                                <div class="col-lg-8 ">
                                    <span class="keterangan"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 form-control-label required">Qty </label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control m-input touchspin-number" name="qty">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 form-control-label required">Keterangan Penyesuaian </label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" name="keterangan_penyesuaian"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Sesuaikan Bahan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>