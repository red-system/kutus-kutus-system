<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mArusStokProduk extends Model
{

    protected $table = 'tb_arus_stok_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tgl',
        'table_id',
        'table_name',
        'id_penjualan',
        'id_penjualan_produk',
        'id_distribusi',
        'id_distribusi_detail',
        'id_produksi',
        'id_progress_produksi',
        'id_stok_produk',
        'id_produk',
        'id_history_penyesuaian_produk',
        'stok_in',
        'stok_out',
        'last_stok',
        'last_stok_total',
        'keterangan',
        'method'
    ];

    function produk() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }

    function stok_produk() {
        return $this->belongsTo(mStokProduk::class, 'id_stok_produk');
    }
}
