<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>Laba Rugi</h3></td>
                    </tr>
                    <tr>
                        <td>{{ $date_start.' s/d '.$date_end }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <br/>
            <div id="table">

                <table>
                    <thead>
                    <tr class="service">
                        <th width="10%" class="tableitem">
                            <p class="itemtext">
                                Kode Rekening
                            </p>
                        </th>
                        <th width="60%" class="tableitem">
                            <p class="itemtext">
                                Nama Rekening
                            </p>
                        </th>
                        <th width="20%" class="text-right tableitem">
                            <p class="itemtext">
                                Nominal
                            </p>
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @php($total_pendapatan = 0)
                    @foreach($kode_pendapatan as $pendapatan)
                        @php($total_pendapatan = $total_pendapatan + $biaya[$pendapatan->mst_kode_rekening])
                        <tr class="tableitem">
                            <td class="font-weight-bold itemtext">
                                <p class="itemtext">
                                    {{ $pendapatan->mst_kode_rekening }}
                                </p>
                            </td>
                            <td class="font-weight-bold itemtext">
                                <p class="itemtext">
                                    {{ $pendapatan->mst_nama_rekening }}
                                </p>
                            </td>
                            <td class="itemtext"
                                align="right">
                                <p class="itemtext">
                                    {{ Main::format_number($biaya[$pendapatan->mst_kode_rekening])}}
                                </p>
                            </td>
                        </tr>
                        @foreach($pendapatan->childs as $pendapatanUsaha)
                            <?php
                            // $total_pendapatan=$total_pendapatan+$biaya[$pendapatanUsaha->mst_kode_rekening];
                            ?>
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $pendapatanUsaha->mst_kode_rekening }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {!! $space1.$pendapatanUsaha->mst_nama_rekening !!}
                                    </p>
                                </td>
                                <td class="tableitem"
                                    align="right">
                                    <p class="itemtext">
                                        {{ Main::format_number($biaya[$pendapatanUsaha->mst_kode_rekening]) }}
                                    </p>
                                </td>
                            </tr>
                            @foreach($pendapatanUsaha->childs as $penjualan)
                                <?php
                                // $total_pendapatan=$total_pendapatan+$biaya[$penjualan->mst_kode_rekening];
                                ?>
                                <tr class="tableitem">
                                    <td class="tableitem">
                                        <p class="itemtext">
                                            {{ $penjualan->mst_kode_rekening }}
                                        </p>
                                    </td>
                                    <td class="tableitem">
                                        <p class="itemtext">
                                            {!! $space2.$penjualan->mst_nama_rekening !!}
                                        </p>
                                    </td>
                                    <td class="tableitem"
                                        align="right">
                                        <p class="itemtext">
                                            {{ Main::format_number($biaya[$penjualan->mst_kode_rekening]) }}
                                        </p>
                                    </td>
                                </tr>
                                @foreach($penjualan->childs as $penjualanChilds)
                                    <?php
                                    // $total_pendapatan=$total_pendapatan+$biaya[$penjualanChilds->mst_kode_rekening];
                                    ?>
                                    <tr class="tableitem">
                                        <td class="tableitem">
                                            <p class="itemtext">
                                                {{ $penjualanChilds->mst_kode_rekening }}
                                            </p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">
                                                {!! $space3.$penjualanChilds->mst_nama_rekening  !!}
                                            </p>
                                        </td>
                                        <td class="tableitem"
                                            align="right">
                                            <p class="itemtext">
                                                {{ Main::format_number($biaya[$penjualanChilds->mst_kode_rekening]) }}
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                    @endforeach
                    <tr class="bg-info m--font-light tableitem">
                        <td colspan="2" class="text-center font-weight-bold tableitem">
                            <p class="itemtext">
                                <h5>TOTAL</h5>
                            </p>
                        </td>
                        <td align="right" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                <h5>
                                    {{ Main::format_number($total_pendapatan) }}
                                </h5>
                            </p>
                        </td>
                    </tr>
                    @php($total_hpp = 0)
                    @foreach($kode_hpp as $hpp)
                        @php($total_hpp = $total_hpp + $biaya[$hpp->mst_kode_rekening])
                        <tr class=" tableitem">
                            <td class="tableitem">
                                <p class="itemtext">
                                    {{ $hpp->mst_kode_rekening }}
                                </p>
                            </td>
                            <td class="tableitem">
                                <p class="itemtext">
                                    {!! $hpp->mst_nama_rekening !!}
                                </p>
                            </td>
                            <td align="right"
                                class="tableitem">
                                <p class="itemtext">
                                    {{ Main::format_number($biaya[$hpp->mst_kode_rekening]) }}
                                </p>
                            </td>
                        </tr>
                        @foreach($hpp->childs as $hppChild)
                            <tr class="tableitem">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{$hppChild->mst_kode_rekening}}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {!! $space1.$hppChild->mst_nama_rekening !!}
                                    </p>
                                </td>
                                <td align="right"
                                    class="tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($biaya[$hppChild->mst_kode_rekening]) }}
                                    </p>
                                </td>
                            </tr>
                            @foreach($hppChild->childs as $hppChild2)
                                <tr class="tableitem">
                                    <td class="tableitem">
                                        <p class="itemtext">
                                            {{ $hppChild2->mst_kode_rekening }}
                                        </p>
                                    </td>
                                    <td class="tableitem">
                                        <p class="itemtext">
                                            {!! $space2.$hppChild2->mst_nama_rekening !!}
                                        </p>
                                    </td>
                                    <td align="right"
                                        class="tableitem">
                                        <p class="itemtext">
                                            {{ Main::format_number($biaya[$hppChild2->mst_kode_rekening]) }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endforeach
                    <tr class="tableitem">
                        <td align="center"
                            class="tableitem">
                            <p class="itemtext">
                                <h5> TOTAL </h5>
                            </p>
                        </td>
                        <td align="right"
                            class="tableitem">
                            <p class="itemtext">
                                <h5>{{ Main::format_number($total_hpp) }} </h5>
                            </p>
                        </td>
                    </tr>
                    <tr class="tableitem">
                        <td colspan="2" align="center" class="tableitem">
                            <p class="itemtext">
                                <h4>LABA (RUGI) KOTOR</h4>
                            </p>
                        </td>
                        <td align="right"
                            class="tableitem">
                            <p class="itemtext">
                                <h4>
                                    {{ Main::format_number($total_pendapatan-$total_hpp) }}
                                </h4>
                            </p>
                        </td>
                    </tr>
                    @php($total_biaya = 0)
                    @foreach($kode_biaya as $biaya2)
                        @php($total_biaya = $total_biaya + $biaya[$biaya2->mst_kode_rekening])
                        <tr class="tableitem">
                            <td class="tableitem">
                                <p class="itemtext">
                                    {{ $biaya2->mst_kode_rekening }}
                                </p>
                            </td>
                            <td class="tableitem">
                                <p class="itemtext">
                                    {!! $biaya2->mst_nama_rekening !!}
                                </p>
                            </td>
                            <td align="right"
                                class="tableitem">
                                <p class="itemtext">
                                    {{ Main::format_number($biaya[$biaya2->mst_kode_rekening]) }}
                                </p>
                            </td>
                        </tr>
                        @foreach($biaya2->childs as $biayaChild)
                            <?php
                            // $total_biaya=$total_biaya+$biaya[$biayaChild->mst_kode_rekening];
                            ?>
                            <tr class="tableitem">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $biayaChild->mst_kode_rekening }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {!! $space2.$biayaChild->mst_nama_rekening !!}
                                    </p>
                                </td>
                                <td align="right"
                                    class="tableitem">
                                    <p class="itemtext">
                                        <strong>
                                            {{ Main::format_number($biaya[$biayaChild->mst_kode_rekening]) }}
                                        </strong>
                                    </p>
                                </td>
                            </tr>
                            @foreach($biayaChild->childs as $biayaChild2)
                                <?php
                                // $total_biaya=$total_biaya+$biaya[$biayaChild2->mst_kode_rekening];
                                ?>
                                <tr class="tableitem">
                                    <td class="tableitem">
                                        <p class="itemtext">
                                            {{ $biayaChild2->mst_kode_rekening }}
                                        </p>
                                    </td>
                                    <td class="tableitem">
                                        <p class="itemtext">
                                            {!! $space3.$biayaChild2->mst_nama_rekening !!}
                                        </p>
                                    </td>
                                    <td align="right"
                                        class="tableitem">
                                        <p class="itemtext">
                                            {{ Main::format_number($biaya[$biayaChild2->mst_kode_rekening]) }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endforeach
                    <tr class="tableitem">
                        <td colspan="2" align="center" class="tableitem">
                            <p class="itemtext">
                                <h4>
                                    TOTAL
                                </h4>
                            </p>
                        </td>
                        <td align="right" class="tableitem">
                            <p class="itemtext">
                                <h4>
                                    {{ Main::format_number($total_biaya) }}
                                </h4>
                            </p>
                        </td>
                    </tr>
                    @php($total_pendapatan_diluar_usaha = 0)
                    @foreach($kode_pendapatan_diluar_usaha as $pendapatan_diluar_usaha)
                        @php(
                        $total_pendapatan_diluar_usaha = $total_pendapatan_diluar_usaha + $biaya[$pendapatan_diluar_usaha->mst_kode_rekening]
                        )
                        <tr class="tableitem">
                            <td class="tableitem">
                                <p class="itemtext">
                                    {{ $pendapatan_diluar_usaha->mst_kode_rekening }}
                                </p>
                            </td>
                            <td class="tableitem">
                                <p class="itemtext">
                                    {!! $pendapatan_diluar_usaha->mst_nama_rekening !!}
                                </p>
                            </td>
                            <td align="right"
                                class="tableitem">
                                <p class="itemtext">
                                    {{ Main::format_number($biaya[$pendapatan_diluar_usaha->mst_kode_rekening]) }}
                                </p>
                            </td>
                        </tr>
                        @foreach($pendapatan_diluar_usaha->childs as $pendapatan_diluar_usahaChild)
                            <tr class="tableitem">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $pendapatan_diluar_usahaChild->mst_kode_rekening }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {!! $space1.$pendapatan_diluar_usahaChild->mst_nama_rekening !!}
                                    </p>
                                </td>
                                <td align="right"
                                    class="tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($biaya[$pendapatan_diluar_usahaChild->mst_kode_rekening]) }}
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr class="tableitem">
                        <td colspan="2" align="center" class="tableitem">
                            <p class="itemtext">
                                <h5> TOTAL </h5>
                            </p>
                        </td>
                        <td align="right" class="tableitem">
                            <p class="itemtext">
                                <h5>
                                    {{ Main::format_number($total_pendapatan_diluar_usaha) }}
                                </h5>
                            </p>
                        </td>
                    </tr>
                    <tr class="tableitem">
                        <td colspan="2" align="center" class="itemtext">
                            <p class="itemtext">
                                <h4>LABA (RUGI) BERSIH </h4>
                            </p>
                        </td>
                        <td align="right" class="itemtext">
                            <p class="itemtext">
                                <h4>
                                    {{ Main::format_number($total_pendapatan-$total_hpp-$total_biaya+$total_pendapatan_diluar_usaha) }} </strong>
                                </h4>
                            </p>
                        </td>
                    </tr>
                    </tbody>

                </table>


            </div>

        </div>
    </div>
</div>