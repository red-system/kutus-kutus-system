@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">
                @include('produksi/analisaSaranProduksi/tabTitle')
                <div class="m-portlet__body akses-perkiraan_jumlah_produksi">
                    <div class="tab-content">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Perkiraan Produksi Produk</th>
                                <th>Jumlah Bahan Diperlukan</th>
                                <th>Jumlah Bahan Tersedia</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($perkiraan_produksi as $r)
                                <tr>
                                    <td>{{ $no++ }}.</td>
                                    <td>{{ $r->kode_produk }}</td>
                                    <td>{{ $r->nama_produk }}</td>
                                    <td align="right">{{ Main::format_number($r->kira_produksi) }}</td>
                                    <td align="right">{{ Main::format_number($r->bahan_perlu) }}</td>
                                    <td align="right">{{ Main::format_number($r->bahan_tersedia) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection