<?php

namespace app\Http\Controllers\Produksi;

use app\Helpers\hAkunting;
use app\Models\mArusStokBahan;
use app\Models\mStokBahan;
use Illuminate\View\View;

use DB,
    Illuminate\Support\Facades\Validator;

use app\Helpers\Main;
use app\Helpers\hProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\mDistributor;
use Illuminate\Support\Facades\Config;

use app\Models\mProduksi;
use app\Models\mLokasi;
use app\Models\mProduk;
use app\Models\mKomposisiProduk;
use app\Models\mBahan;
use app\Models\mDetailProduksi;
use app\Models\mBahanProduksi;
use app\Models\mProgressProduksi;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;

class Produksi extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['produksi_1'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['produksi'],
                'route' => ''
            ],
            [
                'label' => $cons['produksi_1'],
                'route' => ''
            ]
        ];

        error_reporting(0);
    }

    function total() {
        $data = [];
        $produksi = mProduksi
            ::with(['bahan_produksi', 'bahan_pengemas'])
            ->get();
        $no = 0;

        foreach($produksi as $r1_produksi) {
            $produksi_exist = mAcJurnalUmum::where('id_produksi', $r1_produksi->id)->count();

            $data[$no]['id'] = $r1_produksi->id;
            $data[$no]['tanggal'] = $r1_produksi->publish_date;
            $data[$no]['kode_produksi'] = $r1_produksi->kode_produksi;
            $data[$no]['jurnal_umum_exist'] = $produksi_exist > 0 ? 'TRUE':'FALSE';
            $total_bahan_produksi = 0;
            $total_bahan_pengemas = 0;
            foreach($r1_produksi->bahan_produksi as $r2_bahan_produksi) {
                $hpp_bahan = mBahan::where('id', $r2_bahan_produksi->id_bahan)->value('harga');
                $total_bahan_produksi += $hpp_bahan * $r2_bahan_produksi->qty_diperlukan;
            }

            foreach($r1_produksi->bahan_pengemas as $r2_bahan_pengemas) {
                $hpp_bahan = mBahan::where('id', $r2_bahan_pengemas->id_bahan)->value('harga');
                $total_bahan_pengemas += $hpp_bahan * $r2_bahan_pengemas->qty;
            }

            $data[$no]['total_bahan_produksi'] = $total_bahan_produksi;
            $data[$no]['total_bahan_pengemas'] = $total_bahan_pengemas;

            $no++;
        }

        return $data;
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mProduksi
            ::with('lokasi')
            ->where([
                'status' => 'progress',
            ])
            ->orderBy('id', 'DESC')
            ->get();
        $data['tab'] = 'produksi';
        $data['pageMethod'] = 'list';

        return view('produksi/produksi/produksiList', $data);
    }

    function history()
    {
        $breadcrumb = [
            [
                'label' => 'History',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $dataMain = Main::data($breadcrumb, $this->menuActive);
        $total_produksi = 0;

        $tgl_mulai_produksi = mProduksi::orderBy('tgl_mulai_produksi', 'ASC')->value('tgl_mulai_produksi');
        $tgl_mulai_produksi = date('d-m-Y', strtotime($tgl_mulai_produksi));
        $tgl_mulai_produksi = isset($_GET['tgl_mulai_produksi']) ? $_GET['tgl_mulai_produksi'] : $tgl_mulai_produksi;

        $tgl_selesai_produksi = mProduksi::orderBy('tgl_selesai_produksi', 'DESC')->value('tgl_selesai_produksi');
        $tgl_selesai_produksi = date('d-m-Y', strtotime($tgl_selesai_produksi));
        $tgl_selesai_produksi = isset($_GET['tgl_selesai_produksi']) ? $_GET['tgl_selesai_produksi'] : $tgl_selesai_produksi;

        $tgl_mulai = date('Y-m-d', strtotime($tgl_mulai_produksi));
        $tgl_selesai = date('Y-m-d', strtotime($tgl_selesai_produksi));

        $list = mProduksi
            ::with(['lokasi'])
            ->withCount([
                'progress_produksi AS total_produksi' => function ($query) {
                    $query->select(DB::raw('SUM(qty_progress)'));
                }
            ])
            ->where([
                'status' => 'finish'
            ])
            ->whereBetween('tgl_mulai_produksi', [$tgl_mulai, $tgl_selesai])
            ->whereBetween('tgl_selesai_produksi', [$tgl_mulai, $tgl_selesai])
            ->orderBy('id', 'DESC')
            ->get();

        $data = [
            'list' => $list,
            'tab' => 'history',
            'tgl_mulai_produksi' => $tgl_mulai_produksi,
            'tgl_selesai_produksi' => $tgl_selesai_produksi,
            'total_produksi' => $total_produksi
        ];

        $data = array_merge($dataMain, $data);

        return view('produksi/produksi/produksiHistory', $data);
    }

    function detail($id_produksi)
    {
        $produksi = mProduksi::with('lokasi')->where('id', $id_produksi)->first();
        $detail_produksi = mDetailProduksi::with('produk')->where('id_produksi', $id_produksi)->orderBy('id', 'ASC')->get();
        $bahan_produksi = mBahanProduksi::with('bahan')->where('id_produksi', $id_produksi)->OrderBy('id', 'ASC')->get();

        $data = [
            'produksi' => $produksi,
            'detail_produksi' => $detail_produksi,
            'bahan_produksi' => $bahan_produksi
        ];

        return view('produksi/produksi/produksiDetailTable', $data);
    }

    function create()
    {
        $breadcrumb = [
            [
                'label' => 'Tambah',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $dataMain = Main::data($breadcrumb, $this->menuActive);
        $lokasi = mLokasi::where('tipe', 'pabrik')->orderBy('lokasi', 'ASC')->get();
        $produk = mProduk::select(['id', 'kode_produk', 'nama_produk'])->orderBy('kode_produk', 'ASC')->get();
        $urutan_produksi = $this->urutan_produksi();

        $data = [
            'lokasi' => $lokasi,
            'produk' => $produk,
            'urutan_produksi' => $urutan_produksi,
            'pageMethod' => 'create'
        ];

        $data = array_merge($dataMain, $data);

        return view('produksi/produksi/produksiCreate', $data);
    }

    function edit($id = '')
    {
        $breadcrumb = [
            [
                'label' => 'Edit',
                'route' => ''
            ]
        ];
        $id_produksi = Main::decrypt($id);
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $dataMain = Main::data($breadcrumb, $this->menuActive);
        $lokasi = mLokasi::where('tipe', 'pabrik')->orderBy('lokasi', 'ASC')->get();
        $produk = mProduk::select(['id', 'kode_produk', 'nama_produk'])->orderBy('kode_produk', 'ASC')->get();
        $produksi = mProduksi::find($id_produksi);
        $detail_produksi = mDetailProduksi::with('produk')->where('id_produksi', $id_produksi)->get();
        $bahan_produksi = mBahanProduksi::with('bahan')->where('id_produksi', $id_produksi)->get();

        $data = [
            'lokasi' => $lokasi,
            'produk' => $produk,
            'produksi' => $produksi,
            'detail_produksi' => $detail_produksi,
            'bahan_produksi' => $bahan_produksi,
            'pageMethod' => 'edit'
        ];

        $data = array_merge($dataMain, $data);

        return view('produksi/produksi/produksiEdit', $data);
    }

    function urutan_produksi()
    {
        $last_urutan = mProduksi::orderBy('urutan', 'DESC')->value('urutan');
        $next_urutan = $last_urutan + 1;

        return $next_urutan;
    }

    function insert(Request $request)
    {
        $rules = [
            'kode_produksi' => 'required',
            'tgl_mulai_produksi' => 'required',
            'tgl_selesai_produksi' => 'required',
            'id_lokasi' => 'required',
            'id_produk' => 'required',
            'id_produk.*' => 'required',
            'id_bahan' => 'required',
            'id_bahan.*' => 'required',
        ];

        $attributes = [
            'kode_produksi' => 'Kode Produksi',
            'tgl_mulai_produksi' => 'Tanggal Mulai Produksi',
            'tgl_selesai_produksi' => 'Tanggal Selesai Produksi',
            'id_lokasi' => 'Pabrik',
            'id_produk' => 'Produk',
            'id_bahan' => 'Bahan'
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_produk.' . $i] = 'Produk ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $qty_diperlukan = $request->input('qty_diperlukan');
        $qty_digunakan = $request->input('qty_digunakan');
        $kode_produksi = $request->input('kode_produksi');
        $id_lokasi = $request->input('id_lokasi');
        $tgl_mulai_produksi = $request->input('tgl_mulai_produksi');
        $tgl_selesai_produksi = $request->input('tgl_selesai_produksi');
        $catatan = $request->input('catatan');

        $id_produk_arr = $request->input('id_produk');
        $keterangan_arr = $request->input('keterangan');
        $qty_arr = $request->input('qty');

        $id_bahan_arr = $request->input('id_bahan');
        $id_satuan_arr = $request->input('id_satuan');
        $qty_diperlukan_arr = $request->input('qty_diperlukan');
        $qty_digunakan_arr = $request->input('qty_digunakan');


        $valid = $this->bahanProduksiValidation($qty_diperlukan, $qty_digunakan);
        if (!$valid) {
            $response = [
                'message' => '<strong>Penggunaan Jumlah Bahan </strong> untuk produksi tidak memenuhi <strong>Jumlah Bahan Digunakan</strong>'
            ];
            return response($response, 400)->header('Content-Type', 'application/json');
        }

        DB::beginTransaction();
        try {

            /**
             * Untuk simpan data produksi
             */
            $month = date('m');
            $year = date('y');
            $urutan = $this->urutan_produksi();
            $data_produksi = [
                'kode_produksi' => $kode_produksi,
                'id_lokasi' => $id_lokasi,
                'tgl_mulai_produksi' => date('Y-m-d', strtotime($tgl_mulai_produksi)) . ' ' . date('H:i:s'),
                'tgl_selesai_produksi' => date('Y-m-d', strtotime($tgl_selesai_produksi)) . ' ' . date('H:i:s'),
                'urutan' => $urutan,
                'catatan' => $catatan,
                'status' => 'progress',
                'publish' => 'no',
                'finish' => 'no'
            ];
            $response = mProduksi::create($data_produksi);
            $id_produksi = $response->id;

            /**
             * Untuk simpan data detail produksi, seperti produk yang ingin di produksi
             */
            foreach ($id_produk_arr as $index => $row) {

                $id_produk = $id_produk_arr[$index];
                $keterangan = $keterangan_arr[$index];
                $qty = $qty_arr[$index];

                $data_detail_produksi = [
                    'id_produksi' => $id_produksi,
                    'id_produk' => $id_produk,
                    'keterangan' => $keterangan,
                    'qty' => $qty,
                    'month' => $month,
                    'year' => $year,
                    'qty_progress' => 0,
                ];
                mDetailProduksi::create($data_detail_produksi);
            }

            /**
             * Untuk menyimpan bahan yang digunakan untuk produksi dan arus bahan keluar
             */
            if (count($id_bahan_arr) > 0) {
                foreach ($id_bahan_arr as $index => $id_bahan) {

                    $id_satuan = $id_satuan_arr[$index];
                    $qty_diperlukan = $qty_diperlukan_arr[$index];
                    $qty_digunakan = $qty_digunakan_arr[$index];

                    $data_bahan_produksi = [
                        'id_produksi' => $id_produksi,
                        'id_bahan' => $id_bahan,
                        'id_satuan' => $id_satuan,
                        'qty_diperlukan' => $qty_diperlukan,
                        'gudang_qty' => json_encode($qty_digunakan)
                    ];
                    mBahanProduksi::create($data_bahan_produksi);

                }
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

    }

    /**
     * Untuk mengubah status publish produksi menjadi yes dan mengurangi stok bahan sesuai dengan qty bahan yang dipakai saat produksi
     *
     * @param $id_produksi
     */
    function publish($id_produksi)
    {

        DB::beginTransaction();

        try {

            $valid = TRUE;
            $produksi = mProduksi::find($id_produksi);
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $kode_produksi = $produksi->kode_produksi;
            $total_nominal_hpp_bahan = hProduk::total_nominal_hpp_bahan($id_produksi);

            $stokBahan = mBahanProduksi::select(['gudang_qty', 'id_bahan'])->where('id_produksi', $id_produksi)->get();
            foreach ($stokBahan as $r) {
                $gudang_qty = json_decode($r->gudang_qty, TRUE);
                foreach ($gudang_qty as $id_stok_bahan => $qty_digunakan) {
                    $qtyBefore = mStokBahan::select('qty')->where('id', $id_stok_bahan)->first()->qty;
                    if ($qty_digunakan > $qtyBefore) {
                        $valid = FALSE;
                        break;
                    }
                }
            }

            /**
             * Check apakah bahan yang digunakan saat  publish sudah mencukup bahan yang tersedia saat ini
             */
            if (!$valid) {
                $message = $valid ? '' : '<strong class="m--font-danger">Stok Kurang</strong> karena bahan sudah digunakan di <strong>Produksi Lain</strong>';
                return response([
                    'message' => $message
                ], 422);
            }

            /**
             * Check apakah bahan yang digunakan saat publish sudah mencukupi bahan yang tersedia saat ini
             */
            if ($valid) {
                $data_update = [
                    'publish'=>'yes',
                    'publish_date'=>$this->datetime
                ];
                mProduksi::where('id', $id_produksi)->update($data_update);
                $detail_produksi = mDetailProduksi
                    ::with('produk:id,kode_produk,nama_produk')
                    ->where('id_produksi', $id_produksi)
                    ->get();
                $produk_list = '';
                foreach ($detail_produksi as $r) {
                    $produk_list .= '(' . $r->produk->kode_produk . ') ' . $r->produk->nama_produk . ', ';
                }
                foreach ($stokBahan as $r) {
                    $gudang_qty = json_decode($r->gudang_qty, TRUE);
                    foreach ($gudang_qty as $id_stok_bahan => $qty_digunakan) {

                        /**
                         * Jika qty yang digunakan tersebut, lebih dari 0
                         */
                        if ($qty_digunakan > 0) {
                            /**
                             * Untuk update stok bahan, yang sudah digunakan untuk produksi
                             */
                            $qtyBefore = mStokBahan::where('id', $id_stok_bahan)->value('qty');
                            $qtyNow = $qtyBefore - $qty_digunakan;
                            mStokBahan::where('id', $id_stok_bahan)->update(['qty' => $qtyNow]);

                            /**
                             * Untuk memasukkan arus stok bahan keluar
                             */
                            //$last_stok = $qty;
                            $last_stok = mStokBahan::where('id_bahan', $r->id_bahan)->sum('qty');
                            $last_stok_total = mStokBahan::sum('qty');
                            $stok_out = $qty_digunakan;
                            $data_arus_bahan = [
                                'tgl' => date('Y-m-d H:i:s'),
                                'table_id' => $id_produksi,
                                'table_name' => 'tb_produksi',
                                'id_stok_bahan' => $id_stok_bahan,
                                'id_bahan' => $r->id_bahan,
                                'stok_in' => 0,
                                'stok_out' => $stok_out,
                                'last_stok' => $last_stok,
                                'last_stok_total' => $last_stok_total,
                                'keterangan' => 'Digunakan untuk produksi produk ' . $produk_list,
                                'method' => 'insert'
                            ];

                            mArusStokBahan::create($data_arus_bahan);
                        }
                    }
                }
            }


            $master_id_debet = [
                194 // Produk dalam proses,
            ];
            $master_id_kredit = [
                191 // Persediaan Bahan Baku
            ];

            /**
             * Begin Akunting
             */
            $data_jurnal = [
                'id_produksi' => $id_produksi,
                'no_invoice' => $kode_produksi,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_keterangan' => 'Publish Produksi ' . $kode_produksi,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            /**
             * Transaksi Debet
             */
            foreach ($master_id_debet as $index => $master_id) {
                $master = mAcMaster::find($master_id);
                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'debet',
                    'tgl_transaksi' => $this->datetime,
                    'trs_debet' => $total_nominal_hpp_bahan,
                    'trs_kredit' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $master->mst_kode_rekening,
                    'trs_nama_rekening' => $master->mst_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => '',
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                ];

                mAcTransaksi::create($data_transaksi);
            }

            /**
             * Transaksi Kredit
             */
            foreach ($master_id_kredit as $index => $master_id) {
                $master = mAcMaster::find($master_id);
                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'kredit',
                    'tgl_transaksi' => $this->datetime,
                    'trs_debet' => 0,
                    'trs_kredit' => $total_nominal_hpp_bahan,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $master->mst_kode_rekening,
                    'trs_nama_rekening' => $master->mst_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => '',
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                ];

                mAcTransaksi::create($data_transaksi);
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

        /**
         * Validasi & Proses pengurangan stok saat bahan produksi dipilih dan setelah produksi di publish
         */


    }

    function update(Request $request, $id_produksi)
    {
        $rules = [
            'kode_produksi' => 'required',
            'tgl_mulai_produksi' => 'required',
            'tgl_selesai_produksi' => 'required',
            'id_lokasi' => 'required',
            'id_produk' => 'required',
            'id_produk.*' => 'required',
            'id_bahan' => 'required',
            'id_bahan.*' => 'required',
        ];

        $attributes = [
            'kode_produksi' => 'Kode Produksi',
            'tgl_mulai_produksi' => 'Tanggal Mulai Produksi',
            'tgl_selesai_produksi' => 'Tanggal Selesai Produksi',
            'id_lokasi' => 'Pabrik',
            'id_produk' => 'Produk',
            'id_bahan' => 'Bahan'
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_produk.' . $i] = 'Produk ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $valid = $this->bahanProduksiValidation($request->qty_diperlukan, $request->qty_digunakan);
        if (!$valid) {
            $response = [
                'message' => '<strong>Penggunaan Jumlah Bahan </strong> untuk produksi tidak memenuhi <strong>Jumlah Bahan Digunakan</strong>'
            ];
            return response($response, 400)->header('Content-Type', 'application/json');
        }


        DB::beginTransaction();

        try {

            $month = date('m');
            $year = date('y');

            /**
             * Update data produksi
             */
            $data_produksi = [
                'kode_produksi' => $request->kode_produksi,
                'tgl_mulai_produksi' => date('Y-m-d', strtotime($request->tgl_mulai_produksi)) . ' ' . date('H:i:s'),
                'tgl_selesai_produksi' => date('Y-m-d', strtotime($request->tgl_selesai_produksi)) . ' ' . date('H:i:s'),
                'catatan' => $request->catatan,
                'status' => 'progress'
            ];
            mProduksi::where('id', $id_produksi)->update($data_produksi);

            /**
             * Update data Detail Produksi
             */
            foreach ($request->id_produk as $id_detail_produksi => $row) {
                $check = mDetailProduksi::where(['id' => $id_detail_produksi])->count();
                if ($check == 0) {
                    $data_detail_produksi = [
                        'id_produksi' => $id_produksi,
                        'id_produk' => $request->id_produk[$id_detail_produksi],
                        'month' => $month,
                        'year' => $year,
                        'qty' => $request->qty[$id_detail_produksi],
                        'keterangan' => $request->keterangan[$id_detail_produksi]
                    ];
                    mDetailProduksi::create($data_detail_produksi);
                } else {
                    $data_detail_produksi = [
                        'id_produk' => $request->id_produk[$id_detail_produksi],
                        'qty' => $request->qty[$id_detail_produksi],
                        'keterangan' => $request->keterangan[$id_detail_produksi]
                    ];

                    mDetailProduksi::where('id', $id_detail_produksi)->update($data_detail_produksi);
                }
            }

            /**
             * Delete lalu Insert data Bahan Produksi
             */
            mBahanProduksi::where('id_produksi', $id_produksi)->delete();
            if (!empty($request->id_bahan)) {
                foreach ($request->id_bahan as $index => $row) {
                    $data_bahan_produksi = [
                        'id_produksi' => $id_produksi,
                        'id_bahan' => $request->id_bahan[$index],
                        'id_satuan' => $request->id_satuan[$index],
                        'qty_diperlukan' => $request->qty_diperlukan[$index],
                        'gudang_qty' => json_encode($request->qty_digunakan[$index])
                    ];
                    mBahanProduksi::create($data_bahan_produksi);
                }
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

    }

    /**
     *
     * untuk memvalidasi, apakah total bahan yang digunakan pada produksi produk, mencukupi stok bahan yang tersedia
     *
     * @param $qty_diperlukan_list
     * @param $qty_digunakan_list
     * @return bool
     */
    function bahanProduksiValidation($qty_diperlukan_list, $qty_digunakan_list)
    {
        $valid = TRUE;
        if (!empty($qty_digunakan_list)) {
            foreach ($qty_diperlukan_list as $index => $qty_diperlukan) {
                //echo $qty_diperlukan . ', ' . json_encode($qty_digunakan_list[$index]);
                if (isset($qty_digunakan_list[$index])) {
                    $sum_qty_digunakan = 0;
                    foreach ($qty_digunakan_list[$index] as $id_lokasi => $qty) {
                        $sum_qty_digunakan += $qty;
                    }

                    if ($qty_diperlukan > $sum_qty_digunakan) {
                        $valid = FALSE;
                        break;
                    }
                }
            }
        }

        return $valid;
    }

    function deleteDetailProduksi(Request $request)
    {

        DB::beginTransaction();

        try {

            $id_detail_produksi = $request->id_detail_produksi;
            mDetailProduksi::where('id', $id_detail_produksi)->delete();

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }

    function deleteProduksi($id_produksi)
    {
        DB::beginTransaction();

        try {
            mProduksi::where('id', $id_produksi)->delete();
            mDetailProduksi::where('id_produksi', $id_produksi)->delete();
            mBahanProduksi::where('id_produksi', $id_produksi)->delete();

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }

    function no_seri_produk(Request $request)
    {
        $request->validate([
            'id_produk' => 'required',
            'id_lokasi' => 'required'
        ]);

        $month = date('m');
        $year = date('y');
        $id_produk = $request->id_produk;
        $id_lokasi = $request->id_lokasi;

        $no_seri_produk = hProduk::no_seri_produk($month, $year, $id_produk, $id_lokasi);

        return [
            'no_seri_produk' => $no_seri_produk
        ];
    }

    /**
     * untuk mengkalkulasi komposisi bahan yang diperlukan dalam produksi beberapa produk
     * dan me-return row table
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    function bahan(Request $request)
    {
        $decimalStep = Config::get('constants.decimalStep');
        $produk_bahan_list = $request->input('produk_bahan_list');
        if ($produk_bahan_list == '') {
            $produk_bahan_list = array();
        }

        $bahan_qty = [];
        $data = [];
        $id_bahan = [];

        /**
         * - untuk mendapatkan bahan berdasarkan produk yang dipilih
         * - untuk mengkalikan qty produk yg diproduksi dengan qty bahan yang dipakai pada produksi produk tersebut
         */
        foreach ($produk_bahan_list as $key => $row) {
            $komposisi = mKomposisiProduk::with('bahan', 'satuan')->where('id_produk', $row['id_produk'])->get();
            foreach ($komposisi as $row_komposisi) {
                if ($row['id_produk'] == $row_komposisi['id_produk']) {
                    $qty = $row_komposisi['qty'] * $row['qty'];
                } else {
                    $qty = $row_komposisi['qty'];
                }

                $id_bahan_now = isset($row_komposisi['id_bahan']) ? $row_komposisi['id_bahan'] : 0;
                $id_satuan_now = isset($row_komposisi['id_satuan']) ? $row_komposisi['id_satuan'] : 0;
                $satuan_now = isset($row_komposisi['satuan']['satuan']) ? $row_komposisi['satuan']['satuan'] : 0;
                $nama_bahan_now = isset($row_komposisi['bahan']['nama_bahan']) ? $row_komposisi['bahan']['nama_bahan'] : 0;

                $bahan_qty[] = [
                    'id_produk' => $row['id_produk'],
                    'id_bahan' => $id_bahan_now,
                    'id_satuan' => $id_satuan_now,
                    'satuan' => $satuan_now,
                    'bahan' => $nama_bahan_now,
                    'qty' => $qty
                ];
            }
        }

        /**
         * untuk mendapatkan id_bahan uniq & list id_bahan beserta qty nya
         */
        foreach ($bahan_qty as $key => $row) {
            $data[][$row['id_bahan']] = $row['qty'];
            if (!in_array($row['id_bahan'], $id_bahan)) {
                $id_bahan[] = $row['id_bahan'];
            }
        }

        /**
         * untuk mentotalkan qty yang ada di list id_bahan lain
         */
        $data_2 = [];
        foreach ($data as $key => $row) {
            foreach ($id_bahan as $id_bahan_id) {
                if (isset($row[$id_bahan_id])) {
                    if (isset($data_2[$id_bahan_id]['qty'])) {
                        $data_2[$id_bahan_id]['qty'] = $data_2[$id_bahan_id]['qty'] + $row[$id_bahan_id];
                    } else {
                        $data_2[$id_bahan_id]['qty'] = $row[$id_bahan_id];
                    }
                }
            }
        }

        /**
         * untuk mendapatkan data bahan sesuai id yang di dapat pada proses sebelumnya
         */
        $data_return = [];
        foreach ($data_2 as $id_bahan => $row) {
            $bahan = mBahan
                ::with(
                //'lokasi',
                    'satuan'
                )
                ->where('id', $id_bahan)
                ->first();
            $qty_tersedia = mStokBahan::where('id_bahan', $id_bahan)->sum('qty');
            $gudang_bahan = mStokBahan::with('lokasi')->where('id_bahan', $id_bahan)->get();

            $data_return[] = [
                'id_bahan' => $id_bahan,
                'id_satuan' => $bahan['id_satuan'],
                'kode_bahan' => $bahan['kode_bahan'],
                'nama_bahan' => $bahan['nama_bahan'],
                'qty_diperlukan' => $row['qty'],
                'qty_tersedia' => $qty_tersedia,
                //'gudang' => $bahan->lokasi->lokasi,
                'gudang_bahan' => $gudang_bahan,
                'satuan' => $bahan['satuan']['satuan']
            ];
        }

        //$data_return = $this->bahan_proses($data_return);

        return view('produksi/produksi/produksiBahanList', ['data' => $data_return, 'decimalStep' => $decimalStep]);
    }

    function bahan_proses($data_return)
    {
        $data = [];
        foreach ($data_return as $key => $row) {
            $data[$key] = $row;

            $qty_pengurangan_gudang = $row['qty_diperlukan'];
            $qty_kurang = '<i class="fa fa-check-circle m--font-success"></i>';
            if ($row['qty_diperlukan'] > $row['qty_tersedia']) {
                $qty_kurang = $row['qty_tersedia'] - $row['qty_diperlukan'];
                $qty_kurang = Main::format_number($qty_kurang);
            }

            $data_gudang = [];
            foreach ($row['gudang_bahan'] as $key_gudang => $row_gudang_bahan) {
                $qty_bahan = 0;
                if ($qty_pengurangan_gudang != 0) {
                    if ($qty_pengurangan_gudang > $row_gudang_bahan['qty']) {
                        $qty_bahan = $row_gudang_bahan['qty'];
                        $qty_pengurangan_gudang -= $row_gudang_bahan['qty'];
                    } else {
                        $qty_bahan = $row_gudang_bahan['qty'] - $qty_pengurangan_gudang;
                        $qty_pengurangan_gudang = 0;
                    }
                }

                $data_gudang[$key] = [
                    'lokasi' => $row_gudang_bahan->lokasi->lokasi,
                    'qty' => $row_gudang_bahan['qty'],
                    'qty_bahan' => $qty_bahan,
                    'id_lokasi' => $row_gudang_bahan->id
                ];
            }

            $data[$key]['gudang_bahan'] = $data_gudang;

            $data[$key] = array_merge($data[$key], [
                'qty_pengurangan_gudang' => $qty_pengurangan_gudang,
                'qty_kurang' => $qty_kurang
            ]);
        }

        return $data;
    }
}
