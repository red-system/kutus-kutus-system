<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Models\mPerkiraan;
use app\Models\mDetailPerkiraan;

use DB,
    PDF;

class Neraca extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_2'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_5'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);


        $data['total_asset'] = 0;
        $data['total_liabilitas'] = 0;
        $data['total_ekuitas'] = 0;
        $data['data_asset'] = mAcMaster::where('mst_neraca_tipe', 'asset')->where('mst_master_id', 0)->get();
        $data['data_ekuitas'] = mAcMaster::where('mst_neraca_tipe', 'ekuitas')->where('mst_master_id', 0)->get();
        $data['data_liabilitas'] = mAcMaster::where('mst_neraca_tipe', 'liabilitas')->get();
        $data['detail_perkiraan'] = mAcMaster::all();

        /**
         * Data Asset
         */
        foreach ($data['data_asset'] as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    if ($asset_childs_2->mst_normal == 'kredit') {
                        $query = mAcJurnalUmum
                            ::select(['trs_kredit', 'trs_debet'])
                            ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                            ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                            ->where('tb_ac_transaksi.master_id', $asset_childs_2->master_id);
                        $kredit = $query->sum('trs_kredit');
                        $debet = $query->sum('trs_debet');
                        $asset = $kredit - $debet;
                        $data['asset'][$asset_childs_2->master_id] = $asset;
                        $data['total_asset'] = $data['total_asset'] - $asset;
                        $total_asset_childs = $total_asset_childs - $asset;

                    }
                    if ($asset_childs_2->mst_normal == 'debet') {
                        $query = mAcJurnalUmum
                            ::select(['trs_kredit', 'trs_debet'])
                            ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                            ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                            ->where('tb_ac_transaksi.master_id', $asset_childs_2->master_id);
                        $kredit = $query->sum('trs_kredit');
                        $debet = $query->sum('trs_debet');
                        $asset = $debet - $kredit;
                        $data['asset'][$asset_childs_2->master_id] = $asset;
                        $data['total_asset'] = $data['total_asset'] + $asset;
                        $total_asset_childs = $total_asset_childs + $asset;
                    }

                }

                if ($asset_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $asset_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $asset = $kredit - $debet;
                    $data['asset'][$asset_childs->master_id] = $asset - $total_asset_childs;
                    $data['total_asset'] = $data['total_asset'] - $asset;
                    $total_detail_asset = $total_detail_asset - $data['asset'][$asset_childs->master_id];
                }
                if ($asset_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $asset_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $asset = $debet - $kredit;
                    $data['asset'][$asset_childs->master_id] = $asset + $total_asset_childs;
                    $data['total_asset'] = $data['total_asset'] + $asset;
                    $total_detail_asset = $total_detail_asset + $data['asset'][$asset_childs->master_id];
                }
            }
            if ($detail_asset->mst_normal == 'kredit') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $detail_asset->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $asset = $kredit - $debet;
                $data['asset'][$detail_asset->master_id] = $asset - $total_detail_asset;
                $data['total_asset'] = $data['total_asset'] - $asset;
            }
            if ($detail_asset->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $detail_asset->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $asset = $debet - $kredit;
                $data['asset'][$detail_asset->master_id] = $asset + $total_detail_asset;
                $data['total_asset'] = $data['total_asset'] + $asset;
            }
        }

        /**
         * Data Ekuitas
         */
        foreach ($data['data_ekuitas'] as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                if ($ekuitas_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $ekuitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $ekuitas = $kredit - $debet;
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] + $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas + $ekuitas;
                }
                if ($ekuitas_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $ekuitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $ekuitas = $debet - $kredit;
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] - $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas - $ekuitas;
                }
                if ($ekuitas_childs->mst_kode_rekening == '2403') {
                    $ekuitas = $this->count_rugi_laba($date_end);
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] + $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas + $ekuitas;
                }
            }

            if ($data_ekuitas->mst_normal == 'kredit') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_ekuitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $ekuitas = $kredit - $debet;
                $data['ekuitas'][$data_ekuitas->master_id] = $ekuitas + $total_data_ekuitas;
            }
            if ($data_ekuitas->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_ekuitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $ekuitas = $debet - $kredit;
                $data['ekuitas'][$data_ekuitas->master_id] = $ekuitas - $total_data_ekuitas;
            }

        }

        /**
         * Data Liabilitas
         */

        //return $data['data_liabilitas'];
        foreach ($data['data_liabilitas'] as $data_liabilitas) {
            $total_data_liabilitas = 0;

            foreach ($data_liabilitas->childs as $liabilitas_childs) {

                if ($liabilitas_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $liabilitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $liabilitas = $kredit - $debet;
                    $data['liabilitas'][$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas = $total_data_liabilitas + $liabilitas;
                    } else {
                        $total_data_liabilitas = $total_data_liabilitas - $liabilitas;
                    }
                }

                if ($liabilitas_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $liabilitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $liabilitas = $debet - $kredit;
                    $data['liabilitas'][$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas = $total_data_liabilitas - $liabilitas;
                    } else {
                        $total_data_liabilitas = $total_data_liabilitas + $liabilitas;
                    }
                }

            }

            if ($data_liabilitas->mst_normal == 'kredit') {

                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_liabilitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $liabilitas = $kredit - $debet;
                $data['liabilitas'][$data_liabilitas->master_id] = $liabilitas + $total_data_liabilitas;
            }

            if ($data_liabilitas->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_liabilitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $liabilitas = $debet - $kredit;
                $data['liabilitas'][$data_liabilitas->master_id] = $liabilitas - $total_data_liabilitas;
            }


            $data['total_liabilitas'] += $total_data_liabilitas;
        }


        $data['hitung'] = $data['total_liabilitas'] + $data['total_ekuitas'];
        if (number_format($data['hitung'], 2) == number_format($data['total_asset'], 2)) {
            $data['status'] = 'BALANCE';
            $data['selisih'] = 0;
        } else {
            $data['status'] = 'NOT BALANCE';
            $data['selisih'] = $data['total_asset'] - $data['hitung'];
        }
        // $data['status']         = 0;
        $data['space1'] = hAkunting::perkiraan_space(1);
        $data['space2'] = hAkunting::perkiraan_space(2);
        $data['space3'] = hAkunting::perkiraan_space(3);
        $data['space4'] = hAkunting::perkiraan_space(4);

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'total_assets_last' => 0,
        ]);

        return view('akunting/neraca/neracaList', $data);
    }


    function count_rugi_laba($tanggal)
    {
        $date_end = $tanggal;
        $laba_rugi = 0;
        //transaksi pendapatan
        $data['kode_pendapatan'] = mAcMaster::where('mst_posisi', 'laba rugi')->get();
        foreach ($data['kode_pendapatan'] as $perkiraan) {
            if ($perkiraan->mst_normal == 'kredit') {
                $kredit = $perkiraan->transaksi->where('tgl_transaksi', '<=', $date_end)->sum('trs_kredit');
                $debet = $perkiraan->transaksi->where('tgl_transaksi', '<=', $date_end)->sum('trs_debet');
                $data['biaya'][$perkiraan->mst_kode_rekening] = $kredit - $debet;
                $laba_rugi = $laba_rugi + $data['biaya'][$perkiraan->mst_kode_rekening];

            }
            if ($perkiraan->mst_normal == 'debet') {
                $kredit = $perkiraan->transaksi->where('tgl_transaksi', '<=', $date_end)->sum('trs_kredit');
                $debet = $perkiraan->transaksi->where('tgl_transaksi', '<=', $date_end)->sum('trs_debet');

                $data['biaya'][$perkiraan->mst_kode_rekening] = $debet - $kredit;
                $laba_rugi = $laba_rugi - $data['biaya'][$perkiraan->mst_kode_rekening];
            }
        }
        return $laba_rugi;
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);


        $data['total_asset'] = 0;
        $data['total_liabilitas'] = 0;
        $data['total_ekuitas'] = 0;
        $data['data_asset'] = mAcMaster::where('mst_neraca_tipe', 'asset')->where('mst_master_id', 0)->get();
        $data['data_ekuitas'] = mAcMaster::where('mst_neraca_tipe', 'ekuitas')->where('mst_master_id', 0)->get();
        $data['data_liabilitas'] = mAcMaster::where('mst_neraca_tipe', 'liabilitas')->get();
        $data['detail_perkiraan'] = mAcMaster::all();

        /**
         * Data Asset
         */
        foreach ($data['data_asset'] as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    if ($asset_childs_2->mst_normal == 'kredit') {
                        $query = mAcJurnalUmum
                            ::select(['trs_kredit', 'trs_debet'])
                            ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                            ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                            ->where('tb_ac_transaksi.master_id', $asset_childs_2->master_id);
                        $kredit = $query->sum('trs_kredit');
                        $debet = $query->sum('trs_debet');
                        $asset = $kredit - $debet;
                        $data['asset'][$asset_childs_2->master_id] = $asset;
                        $data['total_asset'] = $data['total_asset'] - $asset;
                        $total_asset_childs = $total_asset_childs - $asset;

                    }
                    if ($asset_childs_2->mst_normal == 'debet') {
                        $query = mAcJurnalUmum
                            ::select(['trs_kredit', 'trs_debet'])
                            ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                            ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                            ->where('tb_ac_transaksi.master_id', $asset_childs_2->master_id);
                        $kredit = $query->sum('trs_kredit');
                        $debet = $query->sum('trs_debet');
                        $asset = $debet - $kredit;
                        $data['asset'][$asset_childs_2->master_id] = $asset;
                        $data['total_asset'] = $data['total_asset'] + $asset;
                        $total_asset_childs = $total_asset_childs + $asset;
                    }

                }

                if ($asset_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $asset_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $asset = $kredit - $debet;
                    $data['asset'][$asset_childs->master_id] = $asset - $total_asset_childs;
                    $data['total_asset'] = $data['total_asset'] - $asset;
                    $total_detail_asset = $total_detail_asset - $data['asset'][$asset_childs->master_id];
                }
                if ($asset_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $asset_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $asset = $debet - $kredit;
                    $data['asset'][$asset_childs->master_id] = $asset + $total_asset_childs;
                    $data['total_asset'] = $data['total_asset'] + $asset;
                    $total_detail_asset = $total_detail_asset + $data['asset'][$asset_childs->master_id];
                }
            }
            if ($detail_asset->mst_normal == 'kredit') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $detail_asset->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $asset = $kredit - $debet;
                $data['asset'][$detail_asset->master_id] = $asset - $total_detail_asset;
                $data['total_asset'] = $data['total_asset'] - $asset;
            }
            if ($detail_asset->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $detail_asset->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $asset = $debet - $kredit;
                $data['asset'][$detail_asset->master_id] = $asset + $total_detail_asset;
                $data['total_asset'] = $data['total_asset'] + $asset;
            }
        }

        /**
         * Data Ekuitas
         */
        foreach ($data['data_ekuitas'] as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                if ($ekuitas_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $ekuitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $ekuitas = $kredit - $debet;
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] + $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas + $ekuitas;
                }
                if ($ekuitas_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $ekuitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $ekuitas = $debet - $kredit;
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] - $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas - $ekuitas;
                }
                if ($ekuitas_childs->mst_kode_rekening == '2403') {
                    $ekuitas = $this->count_rugi_laba($date_end);
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] + $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas + $ekuitas;
                }
            }

            if ($data_ekuitas->mst_normal == 'kredit') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_ekuitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $ekuitas = $kredit - $debet;
                $data['ekuitas'][$data_ekuitas->master_id] = $ekuitas + $total_data_ekuitas;
            }
            if ($data_ekuitas->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_ekuitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $ekuitas = $debet - $kredit;
                $data['ekuitas'][$data_ekuitas->master_id] = $ekuitas - $total_data_ekuitas;
            }

        }

        /**
         * Data Liabilitas
         */

        //return $data['data_liabilitas'];
        foreach ($data['data_liabilitas'] as $data_liabilitas) {
            $total_data_liabilitas = 0;

            foreach ($data_liabilitas->childs as $liabilitas_childs) {

                if ($liabilitas_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $liabilitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $liabilitas = $kredit - $debet;
                    $data['liabilitas'][$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas = $total_data_liabilitas + $liabilitas;
                    } else {
                        $total_data_liabilitas = $total_data_liabilitas - $liabilitas;
                    }
                }

                if ($liabilitas_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $liabilitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $liabilitas = $debet - $kredit;
                    $data['liabilitas'][$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas = $total_data_liabilitas - $liabilitas;
                    } else {
                        $total_data_liabilitas = $total_data_liabilitas + $liabilitas;
                    }
                }

            }

            if ($data_liabilitas->mst_normal == 'kredit') {

                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_liabilitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $liabilitas = $kredit - $debet;
                $data['liabilitas'][$data_liabilitas->master_id] = $liabilitas + $total_data_liabilitas;
            }

            if ($data_liabilitas->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_liabilitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $liabilitas = $debet - $kredit;
                $data['liabilitas'][$data_liabilitas->master_id] = $liabilitas - $total_data_liabilitas;
            }


            $data['total_liabilitas'] += $total_data_liabilitas;
        }


        $data['hitung'] = $data['total_liabilitas'] + $data['total_ekuitas'];
        if (number_format($data['hitung'], 2) == number_format($data['total_asset'], 2)) {
            $data['status'] = 'BALANCE';
            $data['selisih'] = 0;
        } else {
            $data['status'] = 'NOT BALANCE';
            $data['selisih'] = $data['total_asset'] - $data['hitung'];
        }
        // $data['status']         = 0;
        $data['space1'] = hAkunting::perkiraan_space(1);
        $data['space2'] = hAkunting::perkiraan_space(2);
        $data['space3'] = hAkunting::perkiraan_space(3);
        $data['space4'] = hAkunting::perkiraan_space(4);

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'total_assets_last' => 0,
            'company' => Main::companyInfo()
        ]);

        $pdf = PDF::loadView('akunting/neraca/neracaPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Neraca ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);


        $data['total_asset'] = 0;
        $data['total_liabilitas'] = 0;
        $data['total_ekuitas'] = 0;
        $data['data_asset'] = mAcMaster::where('mst_neraca_tipe', 'asset')->where('mst_master_id', 0)->get();
        $data['data_ekuitas'] = mAcMaster::where('mst_neraca_tipe', 'ekuitas')->where('mst_master_id', 0)->get();
        $data['data_liabilitas'] = mAcMaster::where('mst_neraca_tipe', 'liabilitas')->get();
        $data['detail_perkiraan'] = mAcMaster::all();

        /**
         * Data Asset
         */
        foreach ($data['data_asset'] as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    if ($asset_childs_2->mst_normal == 'kredit') {
                        $query = mAcJurnalUmum
                            ::select(['trs_kredit', 'trs_debet'])
                            ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                            ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                            ->where('tb_ac_transaksi.master_id', $asset_childs_2->master_id);
                        $kredit = $query->sum('trs_kredit');
                        $debet = $query->sum('trs_debet');
                        $asset = $kredit - $debet;
                        $data['asset'][$asset_childs_2->master_id] = $asset;
                        $data['total_asset'] = $data['total_asset'] - $asset;
                        $total_asset_childs = $total_asset_childs - $asset;

                    }
                    if ($asset_childs_2->mst_normal == 'debet') {
                        $query = mAcJurnalUmum
                            ::select(['trs_kredit', 'trs_debet'])
                            ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                            ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                            ->where('tb_ac_transaksi.master_id', $asset_childs_2->master_id);
                        $kredit = $query->sum('trs_kredit');
                        $debet = $query->sum('trs_debet');
                        $asset = $debet - $kredit;
                        $data['asset'][$asset_childs_2->master_id] = $asset;
                        $data['total_asset'] = $data['total_asset'] + $asset;
                        $total_asset_childs = $total_asset_childs + $asset;
                    }

                }

                if ($asset_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $asset_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $asset = $kredit - $debet;
                    $data['asset'][$asset_childs->master_id] = $asset - $total_asset_childs;
                    $data['total_asset'] = $data['total_asset'] - $asset;
                    $total_detail_asset = $total_detail_asset - $data['asset'][$asset_childs->master_id];
                }
                if ($asset_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $asset_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $asset = $debet - $kredit;
                    $data['asset'][$asset_childs->master_id] = $asset + $total_asset_childs;
                    $data['total_asset'] = $data['total_asset'] + $asset;
                    $total_detail_asset = $total_detail_asset + $data['asset'][$asset_childs->master_id];
                }
            }
            if ($detail_asset->mst_normal == 'kredit') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $detail_asset->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $asset = $kredit - $debet;
                $data['asset'][$detail_asset->master_id] = $asset - $total_detail_asset;
                $data['total_asset'] = $data['total_asset'] - $asset;
            }
            if ($detail_asset->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $detail_asset->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $asset = $debet - $kredit;
                $data['asset'][$detail_asset->master_id] = $asset + $total_detail_asset;
                $data['total_asset'] = $data['total_asset'] + $asset;
            }
        }

        /**
         * Data Ekuitas
         */
        foreach ($data['data_ekuitas'] as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                if ($ekuitas_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $ekuitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $ekuitas = $kredit - $debet;
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] + $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas + $ekuitas;
                }
                if ($ekuitas_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $ekuitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $ekuitas = $debet - $kredit;
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] - $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas - $ekuitas;
                }
                if ($ekuitas_childs->mst_kode_rekening == '2403') {
                    $ekuitas = $this->count_rugi_laba($date_end);
                    $data['ekuitas'][$ekuitas_childs->master_id] = $ekuitas;
                    $data['total_ekuitas'] = $data['total_ekuitas'] + $ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas + $ekuitas;
                }
            }

            if ($data_ekuitas->mst_normal == 'kredit') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_ekuitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $ekuitas = $kredit - $debet;
                $data['ekuitas'][$data_ekuitas->master_id] = $ekuitas + $total_data_ekuitas;
            }
            if ($data_ekuitas->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_ekuitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $ekuitas = $debet - $kredit;
                $data['ekuitas'][$data_ekuitas->master_id] = $ekuitas - $total_data_ekuitas;
            }

        }

        /**
         * Data Liabilitas
         */

        //return $data['data_liabilitas'];
        foreach ($data['data_liabilitas'] as $data_liabilitas) {
            $total_data_liabilitas = 0;

            foreach ($data_liabilitas->childs as $liabilitas_childs) {

                if ($liabilitas_childs->mst_normal == 'kredit') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $liabilitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $liabilitas = $kredit - $debet;
                    $data['liabilitas'][$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas = $total_data_liabilitas + $liabilitas;
                    } else {
                        $total_data_liabilitas = $total_data_liabilitas - $liabilitas;
                    }
                }

                if ($liabilitas_childs->mst_normal == 'debet') {
                    $query = mAcJurnalUmum
                        ::select(['trs_kredit', 'trs_debet'])
                        ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                        ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                        ->where('tb_ac_transaksi.master_id', $liabilitas_childs->master_id);
                    $kredit = $query->sum('trs_kredit');
                    $debet = $query->sum('trs_debet');
                    $liabilitas = $debet - $kredit;
                    $data['liabilitas'][$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas = $total_data_liabilitas - $liabilitas;
                    } else {
                        $total_data_liabilitas = $total_data_liabilitas + $liabilitas;
                    }
                }

            }

            if ($data_liabilitas->mst_normal == 'kredit') {

                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_liabilitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $liabilitas = $kredit - $debet;
                $data['liabilitas'][$data_liabilitas->master_id] = $liabilitas + $total_data_liabilitas;
            }

            if ($data_liabilitas->mst_normal == 'debet') {
                $query = mAcJurnalUmum
                    ::select(['trs_kredit', 'trs_debet'])
                    ->leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id', '=', 'tb_ac_jurnal_umum.jurnal_umum_id')
                    ->where('tb_ac_jurnal_umum.jmu_tanggal', '<=', $date_end)
                    ->where('tb_ac_transaksi.master_id', $data_liabilitas->master_id);
                $kredit = $query->sum('trs_kredit');
                $debet = $query->sum('trs_debet');
                $liabilitas = $debet - $kredit;
                $data['liabilitas'][$data_liabilitas->master_id] = $liabilitas - $total_data_liabilitas;
            }


            $data['total_liabilitas'] += $total_data_liabilitas;
        }


        $data['hitung'] = $data['total_liabilitas'] + $data['total_ekuitas'];
        if (number_format($data['hitung'], 2) == number_format($data['total_asset'], 2)) {
            $data['status'] = 'BALANCE';
            $data['selisih'] = 0;
        } else {
            $data['status'] = 'NOT BALANCE';
            $data['selisih'] = $data['total_asset'] - $data['hitung'];
        }
        // $data['status']         = 0;
        $data['space1'] = hAkunting::perkiraan_space(1);
        $data['space2'] = hAkunting::perkiraan_space(2);
        $data['space3'] = hAkunting::perkiraan_space(3);
        $data['space4'] = hAkunting::perkiraan_space(4);

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'total_assets_last' => 0,
            'company' => Main::companyInfo()
        ]);

        return view('akunting/neraca/neracaExcel', $data);
    }


}