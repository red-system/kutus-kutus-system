<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\hAkunting;
use app\Helpers\hProduk;
use app\Helpers\Main;
use app\Models\mAcJurnalUmum;
use app\Models\mAcTransaksi;
use app\Models\mArusStokBahan;
use app\Models\mHutangSupplierPembayaran;
use app\Models\mLokasi;
use app\Models\mStokBahan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\mBahan;
use app\Models\mSupplier;
use app\Models\mPoBahan;
use app\Models\mPoBahanDetail;
use app\Models\mPoBahanPembelian;
use app\Models\mPoBahanPembelianDetail;
use app\Models\mPoBahanPembelianPembayaran;
use app\Models\mAcMaster;
use app\Models\mHutangSupplier;

use app\Rules\UsernameCheckerLoginNow;
use Illuminate\Support\Facades\Session;

use PDF;
use Illuminate\Support\Facades\Validator;

class POBahanSupplier extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $maxPoCountHpp = 3;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['transaksi_5'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_5'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mPoBahan::with('supplier')->orderBy('id', 'DESC')->get();
        $data = array_merge($data, [
            'list' => $list,
            'sum_grand_total' => 0
        ]);

        return view('transaksi/poBahanSupplier/poBahanSupplierList', $data);
    }

    function create()
    {
        $breadcrumb = [
            [
                'label' => 'Buat PO',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $supplier = mSupplier::orderBy('nama_supplier', 'ASC')->get();
        $urutan = $this->urutan();
        $no_faktur = $data['prefixNoPoBahan'] . $urutan;
        $bahan = mBahan
            ::withCount([
                'stok_bahan AS stok_bahan' => function ($query) {
                    $query->select(DB::raw('SUM(qty) as stok_bahan'));
                }
            ])
            ->orderBy('kode_bahan', 'ASC')->get();

        $data = array_merge($data, [
            'supplier' => $supplier,
            'bahan' => $bahan,
            'urutan' => $urutan,
            'no_faktur' => $no_faktur
        ]);

        return view('transaksi/poBahanSupplier/poBahanSupplierCreate', $data);
    }

    function insert(Request $request)
    {

        $rules = [
            'id_supplier' => 'required',
            'id_bahan' => 'required',
            'id_bahan.*' => 'required',
        ];

        $attributes = [
            'id_supplier' => 'Supplier',
            'id_bahan' => 'Bahan',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_bahan.' . $i] = 'Bahan ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $data_po_bahan_detail = [];
        $id_supplier = $request->input('id_supplier');
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $urutan = $request->input('urutan');
        $no_faktur = $request->input('no_faktur');
        $status_pembelian = 'not_yet';
        $pembayaran = $request->input('pembayaran');
        $pengiriman = $request->input('pengiriman');

        $id_bahan_arr = $request->input('id_bahan');
        $harga_net_arr = $request->input('harga_net');
        $sub_total_arr = $request->input('sub_total');
        $ppn_nominal_arr = $request->input('ppn_nominal');
        $ppn_persen = Config::get('constants.ppnPersen');
        $harga_arr = $request->input('harga');
        $qty_arr = $request->input('qty');

        $total = $request->input('total');
        $biaya_tambahan = $request->input('biaya_tambahan');
        $potongan = $request->input('potongan');
        $grand_total = $request->input('grand_total');

        DB::beginTransaction();
        try {

            $data_po_bahan = [
                'id_supplier' => $id_supplier,
                'urutan' => $urutan,
                'no_faktur' => $no_faktur,
                'tanggal' => $tanggal,
                'pembayaran' => $pembayaran,
                'pengiriman' => $pengiriman,
                'total' => $total,
                'biaya_tambahan' => $biaya_tambahan,
                'potongan' => $potongan,
                'grand_total' => $grand_total,
                'status_pembelian' => $status_pembelian
            ];

            $response = mPoBahan::create($data_po_bahan);
            $id_po_bahan = $response->id;

            foreach ($id_bahan_arr as $index => $id_bahan) {
                $data_po_bahan_detail[] = [
                    'id_po_bahan' => $id_po_bahan,
                    'id_bahan' => $id_bahan,
                    'harga' => $harga_arr[$index],
                    'ppn_persen' => $ppn_persen,
                    'ppn_nominal' => $ppn_nominal_arr[$index],
                    'harga_net' => $harga_net_arr[$index],
                    'qty' => $qty_arr[$index],
                    'sub_total' => $sub_total_arr[$index],
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];
            }

            mPoBahanDetail::insert($data_po_bahan_detail);

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function edit($id_po_bahan)
    {
        $id_po_bahan = Main::decrypt($id_po_bahan);
        $breadcrumb = [
            [
                'label' => 'Edit PO',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $po_bahan = mPoBahan::with('supplier')->find($id_po_bahan);
        $po_bahan_detail = mPoBahanDetail::with('bahan')->where('id_po_bahan', $id_po_bahan)->get();
        $supplier = mSupplier::orderBy('nama_supplier', 'ASC')->get();
        $bahan = mBahan
            ::withCount([
                'stok_bahan AS stok_bahan' => function ($query) {
                    $query->select(DB::raw('SUM(qty) as stok_bahan'));
                }
            ])
            ->orderBy('kode_bahan', 'ASC')->get();

        $data = array_merge($data, [
            'supplier' => $supplier,
            'bahan' => $bahan,
            'po_bahan' => $po_bahan,
            'po_bahan_detail' => $po_bahan_detail
        ]);

        return view('transaksi/poBahanSupplier/poBahanSupplierEdit', $data);
    }

    function update(Request $request, $id_po_bahan)
    {

        $rules = [
            'id_supplier' => 'required',
            'id_bahan' => 'required',
            'id_bahan.*' => 'required',
        ];

        $attributes = [
            'id_supplier' => 'Supplier',
            'id_bahan' => 'Bahan',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_bahan.' . $i] = 'Bahan ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $data_po_bahan_detail = [];
        $id_supplier = $request->input('id_supplier');
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $urutan = $request->input('urutan');
        $no_faktur = $request->input('no_faktur');
        $pembayaran = $request->input('pembayaran');
        $pengiriman = $request->input('pengiriman');

        $id_bahan_arr = $request->input('id_bahan');
        $harga_net_arr = $request->input('harga_net');
        $sub_total_arr = $request->input('sub_total');
        $ppn_nominal_arr = $request->input('ppn_nominal');
        $ppn_persen = Config::get('constants.ppnPersen');
        $harga_arr = $request->input('harga');
        $qty_arr = $request->input('qty');

        $total = $request->input('total');
        $biaya_tambahan = $request->input('biaya_tambahan');
        $potongan = $request->input('potongan');
        $grand_total = $request->input('grand_total');

        DB::beginTransaction();
        try {

            $data_po_bahan = [
                'id_supplier' => $id_supplier,
                'urutan' => $urutan,
                'no_faktur' => $no_faktur,
                'tanggal' => $tanggal,
                'total' => $total,
                'biaya_tambahan' => $biaya_tambahan,
                'potongan' => $potongan,
                'grand_total' => $grand_total,
                'pembayaran' => $pembayaran,
                'pengiriman' => $pengiriman
            ];

            mPoBahan::where('id', $id_po_bahan)->update($data_po_bahan);
            mPoBahanDetail::where('id_po_bahan', $id_po_bahan)->delete();

            foreach ($id_bahan_arr as $index => $id_bahan) {
                $data_po_bahan_detail[] = [
                    'id_po_bahan' => $id_po_bahan,
                    'id_bahan' => $id_bahan,
                    'harga' => $harga_arr[$index],
                    'ppn_persen' => $ppn_persen,
                    'ppn_nominal' => $ppn_nominal_arr[$index],
                    'harga_net' => $harga_net_arr[$index],
                    'qty' => $qty_arr[$index],
                    'sub_total' => $sub_total_arr[$index],
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];
            }

            mPoBahanDetail::insert($data_po_bahan_detail);

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function detail($id_po_penjualan)
    {
        $po_bahan = mPoBahan
            ::with('supplier')
            ->where('id', $id_po_penjualan)
            ->first();
        $po_bahan_detail = mPoBahanDetail
            ::with('bahan')
            ->where('id_po_bahan', $id_po_penjualan)
            ->get();

        $data = [
            'no' => 1,
            'po_bahan' => $po_bahan,
            'po_bahan_detail' => $po_bahan_detail
        ];

        return view('transaksi/poBahanSupplier/modalBodyPoBahanDetail', $data);
    }

    function pembelian($id)
    {
        $id_po_bahan = Main::decrypt($id);
        $breadcrumb = [
            [
                'label' => 'Pembelian Bahan ke Supplier',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $urutan = $this->urutan_pembelian();
        $no_pembelian = $data['prefixNoPoBahanPembelian'] . $urutan;
        $gudang = mLokasi::where('tipe', 'gudang')->orderBy('lokasi', 'ASC')->get();
        $master = mAcMaster
            ::where('mst_pembayaran', 'yes')
            ->where('mst_nama_rekening', 'NOT LIKE', "%piutang%")
            ->get();
        $pageTitle = 'Pembelian Bahan ke Supplier';
        $po_bahan = mPoBahan
            ::with('supplier')
            ->where('id', $id_po_bahan)
            ->first();
        $po_bahan_detail = mPoBahanDetail
            ::with('bahan')
            ->where('id_po_bahan', $id_po_bahan)
            ->orderBy('id', 'ASC')
            ->get();

        $data = array_merge($data, [
            'id_po_bahan' => $id_po_bahan,
            'po_bahan' => $po_bahan,
            'po_bahan_detail' => $po_bahan_detail,
            'gudang' => $gudang,
            'urutan' => $urutan,
            'pageTitle' => $pageTitle,
            'no_pembelian' => $no_pembelian,
            'master' => $master
        ]);

        return view('transaksi/poBahanSupplier/poBahanSupplierPembelian', $data);
    }

    function pembelian_insert(Request $request, $id)
    {

        $rules = [
            'master_id' => 'required',
            'master_id.*' => 'required',
            'id_lokasi' => 'required',
            'id_lokasi.*' => 'required'
        ];

        $attributes = [
            'master_id' => 'Kode Perkiraan',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
            $attr['id_lokasi.' . $i] = 'Gudang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $data_po_bahan_pembelian_detail = [];
        $id_po_bahan = Main::decrypt($id);
        $po_bahan = mPoBahan::find($id_po_bahan);
        $supplier = mSupplier::find($po_bahan->id_supplier);
        $urutan = $request->input('urutan');
        $no_pembelian = $request->input('no_pembelian');
        $tanggal = date('Y-m-d', strtotime($request->input('tanggal')));
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $jmu_no = hAkunting::jmu_no($year, $month, $day);

        $id_po_bahan_detail_arr = $request->input('id_po_bahan_detail');
        $id_bahan_arr = $request->input('id_bahan');
        $id_lokasi_arr = $request->input('id_lokasi');
        $qty_arr = $request->input('qty');

        $master_id_arr = $request->input('master_id');
        $jumlah_arr = $request->input('jumlah');
        $jatuh_tempo_arr = $request->input('jatuh_tempo');
        $no_bg_arr = $request->input('no_bg');
        $keterangan_arr = $request->input('keterangan');

        DB::beginTransaction();
        try {

            /**
             * Check apakah pembelian bahan sudah sesuai dengan grand total po bahan,
             * agar tidak ada sisa pembayaran, jika ada sisa pembayaran, akan menambah fitur, untuk membayar lanjutan
             */
            $jumlah_total = array_sum($jumlah_arr);
            if ($jumlah_total != $po_bahan->grand_total) {
                return response([
                    'message' => 'Total pembayaran tidak sama dengan grand total'
                ], 422);
            }


            $data_po_bahan = [
                'status_pembelian' => 'done'
            ];
            mPoBahan::where('id', $id_po_bahan)->update($data_po_bahan);

            /**
             * Memproses PO Bahan
             */
            hProduk::hpp_bahan();

            /**
             * Proses Pembelian Bahan
             */
            $data_po_bahan_pembelian = [
                'id_po_bahan' => $id_po_bahan,
                'urutan' => $urutan,
                'no_pembelian' => $no_pembelian,
                'tanggal' => $tanggal
            ];

            $response = mPoBahanPembelian::create($data_po_bahan_pembelian);
            $id_po_bahan_pembelian = $response->id;

            foreach ($id_po_bahan_detail_arr as $index => $id_po_bahan_detail) {

                // insert po bahan pembelian detail
                $data_po_bahan_pembelian_detail[] = [
                    'id_po_bahan' => $id_po_bahan,
                    'id_po_bahan_pembelian' => $id_po_bahan_pembelian,
                    'id_po_bahan_detail' => $id_po_bahan_detail,
                    'id_lokasi' => $id_lokasi_arr[$index],
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];
            }

            mPoBahanPembelianDetail::insert($data_po_bahan_pembelian_detail);

            foreach ($master_id_arr as $index => $master_id) {
                $jatuh_tempo = date('Y-m-d', strtotime($jatuh_tempo_arr[$index]));

                /**
                 * Insert hutang supplier jika label perkiraan adalah hutang
                 */

                $nama_rekening = mAcMaster::where('master_id', $master_id)->value('mst_nama_rekening');
                $nama_rekening = strtolower($nama_rekening);
                if (strpos($nama_rekening, 'hutang') !== FALSE) {
                    $urutan = $this->urutan_hutang_supplier();
                    $id_faktur_hutang = $id_po_bahan;
                    $tipe_faktur_hutang = 'tb_po_bahan';
                    $no_faktur_hutang = $this->no_faktur_hutang();
                    $tanggal_hutang = date('Y-m-d');
                    $js_jatuh_tempo = $jatuh_tempo;
                    $ps_no_faktur = $po_bahan->no_faktur;
                    $id_supplier = $po_bahan->id_supplier;
                    $hs_amount = $jumlah_arr[$index];
                    $sisa_amount = $jumlah_arr[$index];
                    $hs_keterangan = $keterangan_arr[$index];
                    $hs_status = 'belum_lunas';
                    $master_id_kode_perkiraan = $master_id;
                    $kode_perkiraan = mAcMaster
                        ::where('master_id', $master_id)
                        ->value('mst_kode_rekening');
                    $user = Session::get('user');
                    $created_by = $user->id;
                    $updated_by = $user->id;
                    $created_by_name = $user->karyawan->nama_karyawan;
                    $updated_by_name = $user->karyawan->nama_karyawan;

                    //$kode_perkiraan =


                    $data_hutang_supplier = [
                        'urutan' => $urutan,
                        'id_po_bahan' => $id_po_bahan,
                        'id_faktur_hutang' => $id_faktur_hutang,
                        'tipe_faktur_hutang' => $tipe_faktur_hutang,
                        'no_faktur_hutang' => $no_faktur_hutang,
                        'tanggal_hutang' => $tanggal_hutang,
                        'js_jatuh_tempo' => $js_jatuh_tempo,
                        'ps_no_faktur' => $ps_no_faktur,
                        'id_supplier' => $id_supplier,
                        'hs_amount' => $hs_amount,
                        'sisa_amount' => $sisa_amount,
                        'hs_keterangan' => $hs_keterangan,
                        'hs_status' => $hs_status,
                        'master_id_kode_perkiraan' => $master_id_kode_perkiraan,
                        'kode_perkiraan' => $kode_perkiraan,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_by_name' => $created_by_name,
                        'updated_by_name' => $updated_by_name
                    ];

                    mHutangSupplier::create($data_hutang_supplier);
                }

                $data_pembelian_pembayaran[] = [
                    'id_po_bahan' => $id_po_bahan,
                    'id_po_bahan_pembelian' => $id_po_bahan_pembelian,
                    'master_id' => $master_id,
                    'jumlah' => $jumlah_arr[$index],
                    'jatuh_tempo' => $jatuh_tempo,
                    'no_check_bg' => $no_bg_arr[$index],
                    'keterangan' => $keterangan_arr[$index],
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];
            }

            mPoBahanPembelianPembayaran::insert($data_pembelian_pembayaran);

            /**
             * Begin Akunting
             */
            $data_jurnal = [
                'id_po_bahan' => $id_po_bahan,
                'no_invoice' => $po_bahan->no_faktur,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_keterangan' => 'PO Bahan Supplier ' . $po_bahan->no_faktur . ' - Pembelian',
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;
            $master_id_debet = [
                39, // Biaya/Pembelian Dibayar Dimuka,
                55, // PPN Masukan Atas Pembelian
            ];

            /**
             * Transaksi Kredit
             */
            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster::find($master_id);
                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'kredit',
                    'tgl_transaksi' => $this->datetime,
                    'trs_debet' => 0,
                    'trs_kredit' => $jumlah_arr[$index],
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $master->mst_kode_rekening,
                    'trs_nama_rekening' => $master->mst_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => $keterangan_arr[$index],
                    'trs_no_check_bg' => $no_bg_arr[$index],
                    'trs_tgl_pencairan' => $this->datetime,
                ];

                mAcTransaksi::create($data_transaksi);
            }

            /**
             * Transaksi Debet
             */
            foreach ($master_id_debet as $index => $master_id) {
                $master = mAcMaster::find($master_id);
                $debet = 0;
                $kredit = 0;

                switch ($master_id) {
//                    case 191: // Persediaan Barang Jadi
//                        $debet = $this->debet_persediaan($id_po_bahan_detail_arr);
//                        break;
                    case 55: // PPN Masukan Atas Pembelian
                        $debet = $this->debet_ppn_masukan_atas_pembelian($id_po_bahan_detail_arr);
                        break;
                    case 39: // Biaya/Pembelian Dibayar Dimuka
                        $debet = $this->debet_biaya_pembelian_dibayar_dimuka($id_po_bahan_detail_arr);
                        break;
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'debet',
                    'tgl_transaksi' => $this->datetime,
                    'trs_debet' => $debet,
                    'trs_kredit' => $kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $master->mst_kode_rekening,
                    'trs_nama_rekening' => $master->mst_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => '',
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                ];

                mAcTransaksi::create($data_transaksi);
            }


            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function bahan_datang(Request $request)
    {
        $id_po_bahan = $request->input('id_po_bahan');
        $po_bahan = mPoBahan::with('supplier')->where('id', $id_po_bahan)->first();
        $grand_total = $po_bahan->grand_total;
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $jmu_no = hAkunting::jmu_no($year, $month, $day);
        $po_bahan_pembelian_detail = mPoBahanPembelianDetail
            ::with('po_bahan_detail')
            ->where('id_po_bahan', $id_po_bahan)
            ->get();

        DB::beginTransaction();
        try {

            /**
             * Update PO Bahan menjadi Bahan Sudah Datang
             */

            $data_update = [
                'status_bahan_datang' => 'arrive',
                'tanggal_bahan_datang' => $this->datetime
            ];
            mPoBahan
                ::where('id', $id_po_bahan)
                ->update($data_update);

            /**
             * Pindah stok bahan ke gudang
             */

            foreach ($po_bahan_pembelian_detail as $r) {
                $id_bahan = $r->po_bahan_detail->id_bahan;
                $id_lokasi = $r->id_lokasi;
                $qty = $r->po_bahan_detail->qty;
                $id_po_bahan_detail = $r->id_po_bahan_detail;

                $qty_bahan = mStokBahan
                    ::select(['qty', 'id'])
                    ->where([
                        'id_bahan' => $id_bahan,
                        'id_lokasi' => $id_lokasi
                    ]);

                /**
                 * Mengecheck, apakah bahan dan lokasi sudah ada di stok bahan atau tidak,
                 * jika tidak ada, maka akan create data stok bahan,
                 * jika ada, maka akan update data stok bahan
                 * lalu terakhir, create data arus stok bahan
                 */
                if ($qty_bahan->count() == 0) {

                    $data_stok_bahan = [
                        'id_bahan' => $id_bahan,
                        'id_lokasi' => $id_lokasi,
                        //'no_seri_bahan' =>
                        'qty' => $qty,
                        'keterangan' => 'PO Bahan Supplier'
                    ];

                    /**
                     * Mengambil last stok arus kartu stok terakhir, lalu menambahkannya dengan stok masuk sekarang
                     */
                    $response = mStokBahan::create($data_stok_bahan);
                    $last_stok_kartu = mArusStokBahan::where('id_bahan', $id_bahan)->where('id_bahan', $id_bahan)->orderBy('id', 'DESC')->limit(1)->value('last_stok');
                    $last_stok = $last_stok_kartu + $qty;
                    $last_stok_total = mStokBahan::sum('qty');

                    /**
                     * Mencatat PO Bahan Supplier di arus stok bahan
                     */
                    $id_stok_bahan = $response->id;
                    $data_arus_stok_bahan = [
                        'tgl' => $this->datetime,
                        'table_id' => $id_po_bahan_detail,
                        'table_name' => 'tb_po_bahan_detail',
                        'id_stok_bahan' => $id_stok_bahan,
                        'id_po_bahan_detail' => $id_po_bahan_detail,
                        'id_po_bahan' => $id_po_bahan,
                        'id_bahan' => $id_bahan,
                        'stok_in' => $qty,
                        'stok_out' => 0,
                        'last_stok' => $last_stok,
                        'last_stok_total' => $last_stok_total,
                        'keterangan' => 'PO Bahan Supplier (' . $po_bahan->supplier->kode_supplier . ') ' . $po_bahan->supplier->nama_supplier . ' - Bahan Datang',
                        'method' => 'insert'
                    ];

                    mArusStokBahan::create($data_arus_stok_bahan);

                } else {
                    $id_stok_bahan = $qty_bahan->first()->id;
                    $qty_bahan = $qty_bahan->first()->qty;
                    $qty_bahan_now = $qty_bahan + $qty;

                    mStokBahan
                        ::where([
                            'id_bahan' => $id_bahan,
                            'id_lokasi' => $id_lokasi
                        ])
                        ->update(['qty' => $qty_bahan_now]);


                    /**
                     * Mengambil last stok arus kartu stok terakhir, lalu menambahkannya dengan stok masuk sekarang
                     */
                    $last_stok_kartu = mArusStokBahan::where('id_bahan', $id_bahan)->where('id_bahan', $id_bahan)->orderBy('id', 'DESC')->limit(1)->value('last_stok');
                    $last_stok = $last_stok_kartu + $qty;
                    $last_stok_total = mStokBahan::sum('qty');

                    /**
                     * Mencatat PO Bahan Supplier di arus stok bahan
                     */
                    $data_arus_stok_bahan = [
                        'tgl' => $this->datetime,
                        'table_id' => $id_po_bahan_detail,
                        'table_name' => 'tb_po_bahan_detail',
                        'id_stok_bahan' => $id_stok_bahan,
                        'id_po_bahan_detail' => $id_po_bahan_detail,
                        'id_po_bahan' => $id_po_bahan,
                        'id_bahan' => $id_bahan,
                        'stok_in' => $qty,
                        'stok_out' => 0,
                        'last_stok' => $last_stok,
                        'last_stok_total' => $last_stok_total,
                        'keterangan' => 'PO Bahan Supplier (' . $po_bahan->supplier->kode_supplier . ') ' . $po_bahan->supplier->nama_supplier . ' - Bahan Datang',
                        'method' => 'update'
                    ];

                    mArusStokBahan::create($data_arus_stok_bahan);
                }
            }

            $master_id_debet = [
                191 // Persediaan bahan baku
            ];
            $master_id_kredit = [
                39 // Biaya/Pembelian Dibayar Dimuka
            ];

            /**
             * Begin Akunting
             */
            $data_jurnal = [
                'id_po_bahan' => $id_po_bahan,
                'no_invoice' => $po_bahan->no_faktur,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_keterangan' => 'PO Bahan Supplier ' . $po_bahan->no_faktur . ' - Bahan Datang',
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            /**
             * Transaksi Debet
             */
            foreach ($master_id_debet as $index => $master_id) {
                $master = mAcMaster::find($master_id);
                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'debet',
                    'tgl_transaksi' => $this->datetime,
                    'trs_debet' => $grand_total,
                    'trs_kredit' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $master->mst_kode_rekening,
                    'trs_nama_rekening' => $master->mst_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => '',
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                ];

                mAcTransaksi::create($data_transaksi);
            }

            /**
             * Transaksi Kredit
             */
            foreach ($master_id_kredit as $index => $master_id) {
                $master = mAcMaster::find($master_id);
                $debet = 0;
                $kredit = 0;

                switch ($master_id) {
                    case 39: // Biaya/Pembelian Dibayar Dimuka
                        $kredit = $grand_total;
                        break;
                }

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'kredit',
                    'tgl_transaksi' => $this->datetime,
                    'trs_debet' => $debet,
                    'trs_kredit' => $kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $master->mst_kode_rekening,
                    'trs_nama_rekening' => $master->mst_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => '',
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                ];

                mAcTransaksi::create($data_transaksi);
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

    }

    function debet_persediaan($id_po_bahan_detail_arr)
    {
        $po_bahan_detail = mPoBahanDetail::whereIn('id', $id_po_bahan_detail_arr)->get(['harga', 'qty']);
        $debet_persediaan = 0;
        foreach ($po_bahan_detail as $r) {
            $debet_persediaan += ($r->harga * $r->qty);
        }

        return $debet_persediaan;
    }

    function debet_ppn_masukan_atas_pembelian($id_po_bahan_detail_arr)
    {
        $ppn_masukan = 0;
        $po_bahan_detail = mPoBahanDetail::whereIn('id', $id_po_bahan_detail_arr)->get(['ppn_nominal', 'qty']);
        foreach ($po_bahan_detail as $r) {
            $ppn_masukan += ($r->ppn_nominal * $r->qty);
        }
        return $ppn_masukan;
    }

    function debet_biaya_pembelian_dibayar_dimuka($id_po_bahan_detail_arr)
    {
        $biaya_dibayar_dimuka = 0;
        $po_bahan_detail = mPoBahanDetail::whereIn('id', $id_po_bahan_detail_arr)->get(['harga', 'qty']);
        foreach ($po_bahan_detail as $r) {
            $biaya_dibayar_dimuka += ($r->harga * $r->qty);
        }
        return $biaya_dibayar_dimuka;
    }

    function cetak($id)
    {
        $id_po_bahan = Main::decrypt($id);
        $po_bahan = mPoBahan
            ::with('supplier')
            ->where('id', $id_po_bahan)
            ->first();
        $po_bahan_detail = mPoBahanDetail
            ::with('bahan.satuan:id,satuan')
            ->where('id_po_bahan', $id_po_bahan)
            ->orderBy('id', 'ASC')
            ->get();
        $company = Main::companyInfo();
        $data = [
            'po_bahan' => $po_bahan,
            'po_bahan_detail' => $po_bahan_detail,
            'company' => $company,
            'no' => 1
        ];

        //return view('transaksi/poBahanSupplier/poBahanSupplierPdf', $data);

        $pdf = PDF::loadView('transaksi/poBahanSupplier/poBahanSupplierPdf', $data);
        //return $pdf->stream();
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('PO Bahan ' . $po_bahan->no_faktur . '.pdf');

    }


    function delete($id_po_bahan)
    {
        DB::beginTransaction();
        try {

            $po_bahan = mPoBahan::find($id_po_bahan);
            $check_pembelian = mPoBahanPembelianPembayaran
                ::where('id_po_bahan', $id_po_bahan)
                ->count();
            $status = '';
            $message = '';
            $allow_delete = TRUE;

            if ($po_bahan->status_bahan_datang == 'arrive') {
                $status = 'bahan_datang';
                $allow_delete = FALSE;
            }

            if ($check_pembelian > 0) {
                $status = 'pembayaran';
                $allow_delete = FALSE;
            }

            if ($allow_delete) {

                mPoBahan::where('id', $id_po_bahan)->delete();
                mPoBahanDetail::where('id_po_bahan', $id_po_bahan)->delete();

            } else {

                switch ($status) {
                    case "bahan_datang":
                        $message = 'Bahan sudah datang dan menambah stok bahan.
                                    <br />
                                    Sehingga tidak bisa menghapus PO Bahan ini.';
                        break;
                    case "pembayaran":
                        $message = 'Pembayaran sudah dilakukan dan Lunas.
                                    <br />
                                    Sehingga tidak bisa menghapus PO Bahan ini';
                        break;
                }

                return response([
                    'message' => $message
                ], 422);

            }


            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }

    function delete_force($id_po_bahan)
    {
        DB::beginTransaction();
        try {

            $po_bahan_detail = mPoBahanDetail::where('id_po_bahan', $id_po_bahan)->get(['id', 'id_bahan', 'qty']);
            $valid_delete = TRUE;

            /**
             * Check, apakah stok bahan, tersedia untuk dikurangi atau karena sudah digunakan untuk
             * produksi produk
             */
            foreach ($po_bahan_detail as $r) {
                $qty = $r->qty;
                $id_bahan = $r->id_bahan;
                $stok_bahan_now = mStokBahan::where('id_bahan', $id_bahan)->sum('qty');

                if ($qty > $stok_bahan_now) {
                    $valid_delete = FALSE;
                    break;
                }
            }

            if (!$valid_delete) {
                return response([
                    'message' => 'Stok yang telah dibeli, sudah berkurang atau sudah digunakan untuk produksi'
                ], 422);
            }


            /**
             * Mencatat PO Bahan Supplier di arus stok bahan
             */


            foreach ($po_bahan_detail as $r) {
                $id_po_bahan_detail = $r->id;

                $last_stok_kartu = mArusStokBahan::where('id_bahan', $id_bahan)->where('id_bahan', $id_bahan)->orderBy('id', 'DESC')->limit(1)->value('last_stok');
                $last_stok = $last_stok_kartu - $qty;
                $last_stok_total = mStokBahan::sum('qty');

                $data_arus_stok_bahan = [
                    'tgl' => $this->datetime,
//                    'id_stok_bahan' => $id_stok_bahan,
                    'id_po_bahan_detail' => $id_po_bahan_detail,
                    'id_po_bahan' => $id_po_bahan,
                    'id_bahan' => $id_bahan,
                    'stok_in' => 0,
                    'stok_out' => $qty,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => 'PO Bahan Supplier - Hapus Paksa',
                    'method' => 'delete'
                ];

                mArusStokBahan::create($data_arus_stok_bahan);
            }


            $this->restok_bahan($id_po_bahan);
            mPoBahan::where('id', $id_po_bahan)->delete();
            mPoBahanDetail::where('id_po_bahan', $id_po_bahan)->delete();
            mPoBahanPembelian::where('id_po_bahan', $id_po_bahan)->delete();
            mPoBahanPembelianDetail::where('id_po_bahan', $id_po_bahan)->delete();
            mPoBahanPembelianPembayaran::where('id_po_bahan', $id_po_bahan)->delete();

            $id_hutang_supplier = mHutangSupplier::where('id_po_bahan', $id_po_bahan)->value('id');
            $jurnal_umum_id_data = mAcJurnalUmum::where('id_po_bahan', $id_po_bahan)->get(['jurnal_umum_id']);

            mHutangSupplier::where('id', $id_hutang_supplier)->delete();
            mHutangSupplierPembayaran::where('id_hutang_supplier', $id_hutang_supplier)->delete();

            foreach ($jurnal_umum_id_data as $r) {
                $jurnal_umum_id = $r->jurnal_umum_id;
                mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->delete();
                mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();
            }


            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    /**
     *
     * Proses untuk mengurangi stok bahan yang sudah terlanjut terisi, saat PO Bahan ke Supplier dihapus
     *
     * @param $id_po_bahan
     */
    function restok_bahan($id_po_bahan)
    {
        $po_bahan_detail = mPoBahanDetail::where('id_po_bahan', $id_po_bahan)->get(['id_bahan', 'qty']);
        foreach ($po_bahan_detail as $r) {
            $qty_po_bahan = $r->qty;
            $id_bahan = $r->id_bahan;
            $jumlah_stok_bahan = mStokBahan::where('id_bahan', $id_bahan)->count();

            /**
             * Jika stok bahan berdasarkan item bahan, berjumlah 1
             */
            if ($jumlah_stok_bahan == 1) {
                $qty_stok_bahan_now = mStokBahan::where('id_bahan', $id_bahan)->first('qty');
                $qty_stok_bahan_update = $qty_stok_bahan_now - $qty_po_bahan;

                $data_update = [
                    'qty' => $qty_stok_bahan_update
                ];
                mStokBahan::where('id_bahan', $id_bahan)->update($data_update);

                /**
                 * Jika stok bahan berdasarkan item bahan, berjumlah lebih dari 1
                 */
            } else {
                $stok_bahan_multiple = mStokBahan::where('id_bahan', $id_bahan)->get(['id', 'qty']);

                /**
                 * Proses pengurangan stok bahan berdasarkan item bahan
                 */
                foreach ($stok_bahan_multiple as $r_stok_bahan_multiple) {
                    $qty_stok_bahan = $r_stok_bahan_multiple->qty;
                    $id_stok_bahan = $r_stok_bahan_multiple->id;

                    if ($qty_stok_bahan < $qty_po_bahan) {
                        $qty_po_bahan = $qty_po_bahan - $qty_stok_bahan;
                        $stok_bahan_update = 0;
                    } else {
                        $stok_bahan_update = $qty_stok_bahan - $qty_po_bahan;
                    }

                    $data_update = [
                        'qty' => $stok_bahan_update
                    ];

                    mStokBahan::where('id', $id_stok_bahan)->update($data_update);
                }
            }
        }
    }

    function no_faktur(Request $request)
    {
        $kode_pre_order = Config::get('constants.kodePreOrder');
        $id_supplier = $request->input('id_supplier');
        $kode_supplier = $request->input('kode_supplier');

        $year = date('Y');
        $month = date('m');

        $urutan = mPoBahan
            ::where('id_supplier', $id_supplier)
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month);
        $urutan_next = $urutan->count() > 0 ? $urutan->orderBy('id', 'DESC')->first(['urutan'])->urutan + 1 : 1;

        $no_faktur = str_pad($urutan_next, 2, "0", STR_PAD_LEFT)
            . '/' . $kode_pre_order
            . '/' . $kode_supplier
            . '/' . $month
            . '/' . $year;

        return $no_faktur;
    }

    function user_check(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => ['required', new UsernameCheckerLoginNow($request->username)]
        ]);
    }

    function no_faktur_hutang()
    {
        $tahun = date('Y');
        $thn = substr($tahun, 2, 2);
        $bulan = date('m');
        $tgl = date('d');
        $kode_label = Config::get('constants.kodeHutangSupplier');
        $no_po_last = mHutangSupplier
            ::where('no_faktur_hutang', 'like', $thn . $bulan . $tgl . '-' . $kode_label . '%')
            ->max('no_faktur_hutang');
        //mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        $lastNoUrut = substr($no_po_last, 11, 4);
        if ($lastNoUrut == 0) {
            $nextNoUrut = 0 + 1;
        } else {
            $nextNoUrut = $lastNoUrut + 1;
        }
        return $thn . $bulan . $tgl . '-' . $kode_label . '-' . sprintf('%04s', $nextNoUrut);
    }

    function urutan()
    {
        $count = mPoBahan::count();
        if ($count == 0) {
            return 1;
        } else {
            $urutan = mPoBahan::orderBy('urutan', 'DESC')->first(['urutan']);
            return $urutan->urutan + 1;
        }
    }

    function urutan_pembelian()
    {
        $count = mPoBahanPembelian::count();
        if ($count == 0) {
            return 1;
        } else {
            $count_2 = mPoBahanPembelian::whereDate('created_at', '=', date('Y-m-d'))->count();
            if ($count_2 == 0) {
                return 1;
            } else {
                $urutan = mPoBahanPembelian
                    ::whereDate('created_at', '=', date('Y-m-d'))
                    ->orderBy('urutan', 'DESC')
                    ->first(['urutan']);
                return $urutan->urutan + 1;
            }
        }
    }

    function urutan_hutang_supplier()
    {
        $count = mHutangSupplier::count();
        if ($count == 0) {
            return 1;
        } else {
            $urutan = mHutangSupplier::orderBy('urutan', 'DESC')->first(['urutan']);
            return $urutan->urutan + 1;
        }
    }

    function bahan_stok(Request $request)
    {
        $id_bahan = $request->input('id_bahan');
        $bahanStok = mStokBahan
            ::with(['bahan', 'lokasi'])
            ->where('id_bahan', $id_bahan)
            ->where('qty', '>', 0)
            ->get();

        $data = [
            'bahanStok' => $bahanStok,
            'no' => 1
        ];

        return view('transaksi/poBahanSupplier/tbodyBahanStok', $data);
    }

}
