<table class="table table-bordered m-table m-table--border-success">
    <thead>
    <tr>
        <th width="100">Lokasi</th>
        <th>Stok Bahan</th>
        <th>No Seri Bahan</th>
        <th>Jumlah Bahan</th>
    </tr>
    </thead>
    <tbody>

    @php
        $qty_gudang = $qty_digunakan;
    @endphp


    @foreach($stok_bahan as $r_stok_bahan)


        @php
            $sisa = 0;
            $uniqid = uniqid();
            if ($qty_digunakan > $r_stok_bahan->qty) {
                $qty_digunakan_temp = $qty_digunakan;
                $qty_digunakan = $r_stok_bahan->qty;
                $sisa = $qty_digunakan_temp - $r_stok_bahan->qty;
            }
        @endphp
        <tr class="tr-stok-pengemas-bahan"
            data-id-produksi="{{ $id_produksi }}"
            data-id-produk="{{ $id_produk }}"
            data-id-bahan="{{ $r_stok_bahan->id_bahan }}"
            data-id-detail-produksi="{{ $id_detail_produksi }}">
            <td>{{ $r_stok_bahan->lokasi->lokasi }}</td>
            <td align="right">{{ Main::format_number($r_stok_bahan->qty) }}</td>
            <td>{{ $r_stok_bahan->no_seri_bahan }}</td>
            <td class="td-qty-bahan-pengemas">
                <input type="text"
                       class="input-qty-bahan-pengemas form-control touchspin-number"
                       name="qty_bahan_pengemas[{{ $uniqid }}][qty]"
                       value="{{ $qty_digunakan }}">
                <input type="hidden"
                       name="qty_bahan_pengemas[{{ $uniqid }}][id_stok_bahan]"
                       value="{{ $r_stok_bahan->id }}">
                <input type="hidden"
                       name="qty_bahan_pengemas[{{ $uniqid }}][id_detail_produksi]"
                       value="{{ $id_detail_produksi }}">
            </td>
        </tr>

        @php

            if ($qty_digunakan > $r_stok_bahan->qty) {
                $qty_digunakan = $sisa;
            } else {
                $qty_digunakan = $sisa;
            }


        @endphp


    @endforeach
    </tbody>
</table>