<table class="bahan-row m--hide">
    <tr>
        <td class="m--hide">
            <input type="hidden" name="id_bahan[]">
            <input type="hidden" name="harga_net[]" value="0">
            <input type="hidden" name="sub_total[]" value="0">
        </td>
        <td>
            <button class="btn-modal-bahan btn btn-accent btn-sm m-btn--pill"
                    type="button">
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td class="nama-bahan"></td>
        <td>
            <input type="text" name="harga[]" class="touchspin-number-decimal-js" value="1">
        </td>
        <td class="ppn-nominal">
            <input type="text" name="ppn_nominal[]" class="touchspin-number-decimal-js" value="0">
        </td>
        <td class="harga-net">0</td>
        <td>
            <input type="text" name="qty[]" class="touchspin-number-decimal-js" value="1">
        </td>
        <td class="sub-total">0</td>
        <td>
            <button type="button" class="btn-bahan-row-delete btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
</table>