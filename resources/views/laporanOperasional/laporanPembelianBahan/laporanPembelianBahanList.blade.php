@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ $date_start }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end" value="{{ $date_end }}">
                                </div>
                            </div>


                            <label class="col-form-label col-lg-1 col-sm-12">Bahan</label>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <select class="m-select2" name="id_bahan">
                                    <option value="all">Semua Bahan</option>
                                    @foreach($bahan as $r)
                                        <option value="{{ $r->id }}" {{ $r->id == $id_bahan ? 'selected': '' }}>{{ '('.$r->kode_bahan.') '.$r->nama_bahan }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-list">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('laporanPembelianBahanPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('laporanPembelianBahanExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            @foreach($po_bahan as $r_po_bahan)

                @php
                    $id_bahan_arr = [];
                    foreach($r_po_bahan->po_bahan_detail as $r_po_bahan_detail) {
                        $id_bahan_arr[] = $r_po_bahan_detail->id_bahan;
                    }

                    if($id_bahan == 'all') {
                        $bahan_exist = TRUE;
                    } else {
                        $bahan_exist = FALSE;
                        $bahan_exist = in_array($id_bahan, $id_bahan_arr) ? TRUE: FALSE;
                    }

                @endphp

                @if($bahan_exist)

                <div class="m-portlet m-portlet--responsive-mobile akses-list">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    {{ $no_card++ }}.
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label">No Pembelian</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ $r_po_bahan->no_faktur }}
                                        </div>
                                        <label class="col-lg-3 col-form-label">Tanggal</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ $r_po_bahan->tanggal }}
                                        </div>
                                        <label class="col-lg-3 col-form-label">Pembayaran</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ ucwords($r_po_bahan->pembayaran) }}
                                        </div>
                                        <label class="col-lg-3 col-form-label">Pengiriman</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ ucwords($r_po_bahan->pengiriman) }}
                                        </div>
                                        <label class="col-lg-3 col-form-label">Supplier</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ '('.$r_po_bahan->supplier->kode_supplier.') '.$r_po_bahan->supplier->nama_supplier }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label">Total</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ Main::format_money($r_po_bahan->total) }}
                                        </div>
                                        <label class="col-lg-3 col-form-label">Biaya Tambahan</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ Main::format_money($r_po_bahan->biaya_tambahan) }}
                                        </div>
                                        <label class="col-lg-3 col-form-label">Potongan</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ Main::format_money($r_po_bahan->potongan) }}
                                        </div>
                                        <label class="col-lg-3 col-form-label">Grand Total</label>
                                        <div class="col-lg-9 col-form-label">
                                            : {{ Main::format_money($r_po_bahan->grand_total) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Bahan</th>
                                <th>Nama Bahan</th>
                                <th>Harga</th>
                                <th>PPN</th>
                                <th>Harga Net</th>
                                <th>Qty</th>
                                <th>Sub Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $no = 1;
                                $harga = 0;
                                $ppn_nominal = 0;
                                $harga_net = 0;
                                $qty = 0;
                                $sub_total = 0;
                            @endphp
                            @foreach($r_po_bahan->po_bahan_detail as $r_po_bahan_detail)
                                @php
                                    $harga += $r_po_bahan_detail->harga;
                                    $ppn_nominal += $r_po_bahan_detail->ppn_nominal;
                                    $harga_net += $r_po_bahan_detail->harga_net;
                                    $qty += $r_po_bahan_detail->qty;
                                    $sub_total += $r_po_bahan_detail->sub_total;
                                @endphp
                                <tr>
                                    <td>{{ $no++ }}.</td>
                                    <td>{{ $r_po_bahan_detail['bahan']['kode_bahan'] }}</td>
                                    <td>{{ $r_po_bahan_detail['bahan']['nama_bahan'] }}</td>
                                    <td align="right">{{ Main::format_money($r_po_bahan_detail->harga) }}</td>
                                    <td align="right">{{ Main::format_money($r_po_bahan_detail->ppn_nominal) }}</td>
                                    <td align="right">{{ Main::format_money($r_po_bahan_detail->harga_net) }}</td>
                                    <td align="right">{{ Main::format_number($r_po_bahan_detail->qty) }}</td>
                                    <td align="right">{{ Main::format_money($r_po_bahan_detail->sub_total) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3" align="right">
                                    <strong>Total</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_money($harga) }}</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_money($ppn_nominal) }}</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_money($harga_net) }}</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_number($qty) }}</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_money($sub_total) }}</strong>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection