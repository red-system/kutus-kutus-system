<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mSupplier extends Model
{
    protected $table = 'tb_supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_supplier',
        'nama_supplier',
        'foto_supplier',
        'alamat_supplier',
        'fax_supplier',
        'telp_supplier',
        'email_supplier'
    ];
}
