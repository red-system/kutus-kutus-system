@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/piutang_lain.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('hutangPiutang.piutangLain.piutangLainCreate')
    @include('hutangPiutang.piutangLain.piutangLainEdit')
    @include('hutangPiutang.piutangLain.modalPembayaranCreate')
    @include('hutangPiutang.piutangLain.trPembayaran')
    @include('hutangPiutang.piutangLain.trKredit')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Piutang Lain - Lain</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Invoice</th>
                            <th>Jatuh Tempo</th>
                            <th>Nama</th>
                            <th>Total</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>


                        @php
                            $total = 0;
                        @endphp

                        @foreach($list as $r)
                            @php
                                $nama = '';
                                if($r->id_karyawan) {
                                    $nama = $r->karyawan->kode_karyawan.' - '.$r->karyawan->nama_karyawan;
                                } else {
                                    $nama = $r->distributor->kode_distributor.' - '.$r->distributor->nama_distributor;
                                }

                                $r['fullname'] = $nama;
                                $r['pl_tanggal'] = Main::format_date($r->pl_tanggal);
                                $r['pl_jatuh_tempo'] = Main::format_date($r->pl_jatuh_tempo);
                                $r['table_id_karyawan'] = $r->table_id;
                                $r['table_id_distributor'] = $r->table_id;

                                $r['total'] = $r->pl_sisa_amount;
                                $r['sisa_pembayaran'] = round($r->pl_sisa_amount);
                                $r['pl_amount'] = $r->pl_amount;
                                $r['pl_sisa_amount'] = $r->pl_sisa_amount;
                                $r['grand-total'] = Main::format_number($r->pl_sisa_amount);
                                $r['sisa-pembayaran'] = Main::format_number($r->pl_sisa_amount);

                                $total += $r->pl_sisa_amount;

                            @endphp
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $r->pl_invoice }}</td>
                                <td>{{ Main::format_date($r->pl_jatuh_tempo) }}</td>
                                <td>{{ $nama }}</td>
                                <td align="right">{{ Main::format_money($r->pl_sisa_amount) }}</td>
                                <td>{{ $r->pl_keterangan }}</td>
                                <td>{!! Main::hutang_lain_status($r->pl_status) !!}</td>
                                <td>
                                    <textarea class="row-data hidden">@json($r)</textarea>

                                    <div class="dropdown">
                                        <button class="btn btn-accent btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i> Menu
                                        </button>
                                        <div class="dropdown-menu"
                                             aria-labelledby="dropdownMenuButton"
                                             x-placement="bottom-start">
                                            @if($r->pl_amount == $r->pl_sisa_amount)
                                                <a class="dropdown-item btn-edit akses-edit"
                                                   data-route="{{ route('piutangLainUpdate', ['id'=>$r->id]) }}"
                                                   href="#">
                                                    <i class="la la-edit"></i> Edit
                                                </a>
                                            @endif
                                            <a class="dropdown-item btn-pembayaran akses-pembayaran"
                                               data-route="{{ route('piutangLainPembayaran', ['id'=>$r->id]) }}"
                                               href="#">
                                                <i class="la la-money"></i> Pembayaran
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item btn-hapus akses-delete  m--font-danger"
                                               data-route='{{ route('piutangLainDelete', ['id'=>$r->id]) }}'
                                               href="#">
                                                <i class="la la-remove m--font-danger"></i> Hapus
                                            </a>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4" align="right">
                                <strong>Total</strong>
                            </td>
                            <td align="right">
                                <strong>{{ Main::format_money($total) }}</strong>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection