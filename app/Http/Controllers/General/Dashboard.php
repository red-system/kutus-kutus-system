<?php

namespace app\Http\Controllers\General;

use app\Models\mDistributorOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;

use app\Models\mPenjualan;
use app\Models\mPenjualanProduk;
use app\Models\mAcTransaksi;
use app\Models\mProduk;
use app\Models\mBahan;
use app\Models\mPiutangLain;
use app\Models\mPiutangPelanggan;

use DB;
use Illuminate\Support\Facades\Session;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
        $level = Session::get('level');
        if ($level == 'distributor') {
            $data = $this->data_dashboard_distributor($request);
            return view('dashboard/dashboard_distributor', $data);
        } elseif ($level == 'administrator') {
            $data = $this->data_dashboard_admin($request);
            return view('dashboard/dashboard_admin', $data);
        }

    }

    function data_dashboard_admin($request)
    {
        $date_range_get = $request->input('date_range');
        $date_range = explode(' - ', $date_range_get);

        $date_start = $date_range_get ? $date_range[0] : date('01-m-Y');
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $date_range_get ? $date_range[1] : date('d-m-Y');
        $date_end_db = Main::format_date_db($date_end);
        $where_date = [$date_start_db, $date_end_db];
        $date_year_now = date('Y', strtotime($date_end_db));
        $data_penjualan = [];

        $data = Main::data($this->breadcrumb);
        $total_hpp = mPenjualanProduk
            ::leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_penjualan_produk.id_penjualan')
            ->whereBetween('tb_penjualan.tanggal', $where_date)
            ->sum('tb_penjualan_produk.hpp');
        $total_biaya = mAcTransaksi
            ::whereBetween('tgl_transaksi', $where_date)
            ->sum('trs_kredit');

        $total_nominal_transaksi = mPenjualan::whereBetween('tanggal', $where_date)->sum('grand_total');
        $gross_profit = $total_nominal_transaksi - $total_hpp;
        $net_profit = $total_nominal_transaksi - $total_hpp - $total_biaya;
        $total_data_transaksi = mPenjualan::whereBetween('tanggal', $where_date)->count();
        $produk = mProduk
            ::withCount([
                'detail_produksi AS total_penjualan' => function ($query) use ($where_date) {
                    $query
                        ->select(DB::raw('SUM(tb_detail_produksi.qty)'))
                        ->leftJoin('tb_produksi', 'tb_produksi.id', '=', 'tb_detail_produksi.id_produksi')
                        ->whereBetween('tb_produksi.tgl_mulai_produksi', $where_date);
                },
                'penjualan_produk AS total_produksi' => function ($query) use ($where_date) {
                    $query
                        ->select(DB::raw('SUM(tb_penjualan_produk.qty)'))
                        ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_penjualan_produk.id_penjualan')
                        ->whereBetween('tb_penjualan.tanggal', $where_date);
                }
            ])
            ->orderBy('nama_produk', 'ASC')
            ->get();
        $bahan = mBahan
            ::with('satuan:id,satuan')
            ->withCount([
                'bahan_produksi AS total_bahan' => function ($query) use ($where_date) {
                    $query
                        ->select(DB::raw('SUM(qty_diperlukan)'))
                        ->leftJoin('tb_produksi', 'tb_produksi.id', '=', 'tb_bahan_produksi.id_produksi')
                        ->whereBetween('tb_produksi.tgl_mulai_produksi', $where_date);
                }
            ])
            ->get();

        foreach ($this->bulan as $index => $month) {
            $total_penjualan = 0;
            $penjualan = mPenjualan
                ::select('tb_penjualan_produk.qty')
                ->leftJoin('tb_penjualan_produk', 'tb_penjualan_produk.id_penjualan', '=', 'tb_penjualan.id')
                ->whereMonth('tb_penjualan.tanggal', $index)
                ->whereYear('tb_penjualan.tanggal', $date_year_now)
                ->get();
            foreach ($penjualan as $r) {
                $total_penjualan += $r->qty;
            }


            $data_penjualan[] = [
                'month' => $month,
                'total_penjualan' => $total_penjualan
            ];
        }

        $data = array_merge($data, array(
            'total_nominal_transaksi' => Main::format_number($total_nominal_transaksi),
            'gross_profit' => Main::format_number($gross_profit),
            'net_profit' => Main::format_number($net_profit),
            'total_data_transaksi' => Main::format_number($total_data_transaksi),
            'produk' => $produk,
            'bahan' => $bahan,
            'data_penjualan' => $data_penjualan,

            'penjualan_produk_count' => 0,
            'detail_produksi_count' => 0,
            'total_penggunaan_bahan' => 0,
            'date_start' => $date_start,
            'date_end' => $date_end
        ));

        return $data;
    }

    function data_dashboard_distributor()
    {

        $data = Main::data($this->breadcrumb);
        $id_distributor = Session::get('user.id');

        $list_piutang_pelanggan = mPiutangPelanggan
            ::where([
                'id_distributor' => $id_distributor,
                'pp_status' => 'belum_bayar'
            ])
            ->orderBy('id', 'ASC')
            ->get();
        $list_piutang_lain = mPiutangLain
            ::where([
                'id_distributor' => $id_distributor,
                'pl_status' => 'belum_bayar'
            ])
            ->orderBy('id_distributor', $id_distributor)
            ->get();
        $list_penjualan = mPenjualan
            ::where([
                'id_distributor' => $id_distributor
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, array(
            'list_piutang_pelanggan' => $list_piutang_pelanggan,
            'list_piutang_lain' => $list_piutang_lain,
            'list_penjualan' => $list_penjualan,

            'no_piutang' => 1,
            'no_history' => 1,
            'piutang_total' => 0,
            'history_total' => 0,
        ));

        return $data;
    }

    function detail_piutang_pelanggan($id)
    {
        $id_piutang_pelanggan = Main::decrypt($id);
        $piutang_pelanggan = mPiutangPelanggan::find($id_piutang_pelanggan);
        $penjualan = mPenjualan
            ::where([
                'id' => $piutang_pelanggan->id_penjualan
            ])
            ->first();
        $penjualan_produk = mPenjualanProduk
            ::with([
                'produk',
                'lokasi',
                'stok_produk'
            ])
            ->where([
                'id_penjualan' => $piutang_pelanggan->id_penjualan
            ])
            ->get();

        $data = [
            'piutang_pelanggan' => $piutang_pelanggan,
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'no' => 1
        ];

        return view('dashboard/piutangPelangganDetail', $data);
    }

    function detail_piutang_lain($id)
    {
        $id_piutang_lain = Main::decrypt($id);
        $piutang_lain = mPiutangLain::find($id_piutang_lain);

        $data = [
            'piutang_lain' => $piutang_lain,
            //'piutang_lain' => $piutang_lain
        ];

        return view('dashboard/piutangLainDetail', $data);
    }

    function detail_history_belanja($id)
    {
        $id_penjualan = Main::decrypt($id);
        $penjualan = mPenjualan
            ::where([
                'id' => $id_penjualan
            ])
            ->first();
        $penjualan_produk = mPenjualanProduk
            ::with([
                'produk',
                'lokasi',
                'stok_produk'
            ])
            ->where([
                'id_penjualan' => $id_penjualan
            ])
            ->get();

        $data = [
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'no' => 1
        ];

        return view('dashboard/historyBelanjaDetail', $data);
    }

    function testUntukSekarang() {
        $test = "sudah adakah yang ada disini dengan yang ada disana";
    }
}
