@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>No Faktur Penjualan</th>
                            <th>Tanggal Faktur</th>
                            <th>Jenis Transaksi</th>
                            <th>Pelanggan</th>
                            <th>Total Kirim</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $r->no_faktur }}</td>
                                <td>{{ Main::format_date($r->tanggal) }}</td>
                                <td>{{ ucwords($r->jenis_transaksi) }}</td>
                                <td>{{ $r->distributor->kode_distributor.'-'.$r->distributor->nama_distributor }}</td>
                                <td>{{ Main::format_number($r->total_qty) }}</td>
                                <td>
                                    @if($r->status_distribusi == 'done')
                                        <a href="{{ route('distribusiHistory', ['id'=>Main::encrypt($r->id)]) }}"
                                           class="m-btn btn btn-accent btn-sm m-btn--pill m-btn--air akses-history_distribusi">
                                            <i class="la la-refresh"></i> History Distribusi
                                        </a>
                                    @else
                                        <a href="{{ route('distribusi', ['id'=>Main::encrypt($r->id)]) }}"
                                           class="m-btn btn btn-info btn-sm m-btn--pill m-btn--air akses-distribusi_qty_produk">
                                            <i class="la la-truck"></i> Distribusi Qty Produk
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection