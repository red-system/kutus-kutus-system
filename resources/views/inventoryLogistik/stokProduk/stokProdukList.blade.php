@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('inventoryLogistik/stokProduk/stokProdukCreate')
    @include('inventoryLogistik/stokProduk/stokProdukEdit')

        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                    <div>
                        <a href="{{ route("produkPage") }}"
                           class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Produk</span>
                            </span>
                        </a>
                        {!! "&nbsp" !!}
                        <a href="#"
                           class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                           data-toggle="modal"
                           data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
{{--                                <th>Kode Produk</th>
                                <th>Nama Produk</th>--}}
                                <th>Lokasi</th>
                                <th>No Seri Produk</th>
                                <th>HPP</th>
                                <th>Qty</th>
                                {{--<th>Last Stok</th>--}}
                                {{--<th>Keterangan</th>--}}
                                <th width="150">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $r)
                                @php($total += $r->qty)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
{{--                                <td>{{ $r->produk->kode_produk }}</td>
                                <td>{{ $r->produk->nama_produk }}</td>--}}
                                <td>{{ $r->lokasi->lokasi }}</td>
                                <td>{{ $r->no_seri_produk }}</td>
                                <td>{{ Main::format_money($r->hpp) }}</td>
                                <td class="text-right">{{ Main::format_number($r->qty) }}</td>
                                {{--<td>{{ Main::format_number($r->last_stok) }}</td>--}}
                                {{--<td>{{ $r->keterangan }}</td>--}}
                                <td align="center">
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                        <button type="button"
                                                class="m-btn btn btn-success btn-edit akses-edit"
                                                data-route="{{ route('stokUpdate', ['id'=>$r->id]) }}">
                                            <i class="la la-edit"></i> Edit
                                        </button>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-hapus akses-delete"
                                                data-route='{{ route('stokDelete', ['id'=>$r->id]) }}'>
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="text-center">
                                        <strong>TOTAL</strong>
                                    </td>
                                    <td class="text-right"><strong>{{ Main::format_number($total) }}</strong></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

        </div>
@endsection