<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailProduksi extends Model
{

    protected $table = 'tb_detail_produksi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produksi',
        'id_produk',
        'month',
        'year',
        'qty',
        'keterangan',
        'qty_progress'
    ];

    function produksi() {
        return $this->belongsTo(mProduksi::class, 'id_produksi');
    }

    function produk() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }

    function pengemas_produk() {
        return $this->hasMany(mPengemasProduk::class, 'id_produk');
    }
}
