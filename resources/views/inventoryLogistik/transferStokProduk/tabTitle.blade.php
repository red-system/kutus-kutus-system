<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item akses-list_transfer_stok_produk">
                <a class="nav-link m-tabs__link {{ $tab == 'transfer' ? 'active':'' }}" href="{{ route('transferStokProdukPage') }}">
                    <i class="la la-truck"></i> Transfer Stok Produk
                </a>
            </li>
            <li class="nav-item m-tabs__item akses-history_transfer">
                <a class="nav-link m-tabs__link {{ $tab == 'history' ? 'active':'' }}" href="{{ route('historyTransferStokProdukPage') }}">
                    <i class="la la-history"></i> History Transfer
                </a>
            </li>
        </ul>
    </div>
</div>