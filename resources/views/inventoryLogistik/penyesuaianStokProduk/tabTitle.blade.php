<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item akses-penyesuaian_stok_produk">
                <a class="nav-link m-tabs__link {{ $tab == 'penyesuaian' ? 'active':'' }}" href="{{ route('penyesuaianStokProdukPage') }}">
                    <i class="la la-truck"></i> Penyesuaian Stok Produk
                </a>
            </li>
            <li class="nav-item m-tabs__item akses-history">
                <a class="nav-link m-tabs__link {{ $tab == 'history' ? 'active':'' }}" href="{{ route('historyPenyesuaianStokProdukPage') }}">
                    <i class="la la-history"></i> History Penyesuaian Stok Produk
                </a>
            </li>
        </ul>
    </div>
</div>