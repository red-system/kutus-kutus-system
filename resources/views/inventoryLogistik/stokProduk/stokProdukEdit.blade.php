<form action="" method="post" class="m-form form-send m-form--label-align-right">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">

        <input type="hidden" name="urutan">

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label">Kode Produk </label>
                                <div class="col-lg-9 ">
                                    {{ $produk->kode_produk }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label">Nama Produk </label>
                                <div class="col-lg-9 ">
                                    {{ $produk->nama_produk }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">No Seri Produk </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control m-input" name="no_seri_produk" autofocus>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Qty </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control m-input format-number touchspin-number" name="qty">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Harga Pokok Penjualan (HPP) </label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control m-input touchspin-rp-decimal" name="hpp" value="1">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label required">Lokasi </label>
                                <div class="col-lg-9">
                                    <select class="form-control m-input m-select2 stok-produk-nomer-seri"
                                            name="id_lokasi"
                                            style="width: 100% !important;">
                                        @foreach($lokasi as $r)
                                            <option value="{{ $r->id}}">{{ $r->lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 form-control-label">Keterangan </label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" name="keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>