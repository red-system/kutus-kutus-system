<?php

namespace app\Http\Controllers\produksi;

use app\Helpers\Main;
use app\Models\mBahan;
use app\Models\mKomposisiProduk;
use app\Models\mStokBahan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Models\mTargetProduksi;
use app\Models\mBahanProduksi;
use app\Models\mProduk;
use DB;

class AnalisaSaranProduksi extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['produksi_4'];
        $this->breadcrumb = [
            [
                'label' => $cons['produksi'],
                'route' => ''
            ],
            [
                'label' => $cons['produksi_4'],
                'route' => ''
            ]
        ];
    }

    function perkiraanJumlahProduksi()
    {
        $dataMain = Main::data($this->breadcrumb);
        $perkiraanProduksi = [];
        $produk = mProduk
            ::withCount([
                'komposisi_produk AS bahan_perlu' => function ($query) {
                    $query->select(DB::raw('sum(qty)'));
                }
            ])
            ->with('komposisi_produk')
            ->orderBy('kode_produk', 'ASC')
            ->get();
        $no = 0;
        foreach ($produk as $r) {
            $bahan_tersedia = 0;
            foreach ($r->komposisi_produk as $r_komposisi_produk) {
                $total_bahan = mStokBahan::where('id_bahan', $r_komposisi_produk->id_bahan)->sum('qty');
                $bahan_tersedia += $total_bahan;
            }

            $perkiraanProduksi[$no] = $r;
            $perkiraanProduksi[$no]['bahan_tersedia'] = $bahan_tersedia;
            $perkiraanProduksi[$no]['kira_produksi'] = $bahan_tersedia != 0 ? floor($bahan_tersedia / $r->bahan_perlu) : 0;

            $no++;
        }

        $data = array_merge($dataMain, [
            'perkiraan_produksi' => $perkiraanProduksi,
            'tab' => 'perkiraanJumlahProduksi'
        ]);


        return view('produksi/analisaSaranProduksi/perkiraanJumlahProduksi', $data);
    }

    function jumlahBahanTerpakai()
    {
        $dataMain = Main::data($this->breadcrumb, $this->menuActive);
        $bahanTerpakai = [];

        $id_bahan = mBahanProduksi
            ::distinct()
            ->with('bahan:id,kode_bahan,nama_bahan')
            ->orderBy('id_bahan', 'ASC')
            ->get(['id_bahan']);
        $no = 0;
        foreach ($id_bahan as $r) {
            $totalBahanTerpakai = mBahanProduksi::where('id_bahan', $r->id_bahan)->sum('qty_diperlukan');
            $totalBahanSisa = mStokBahan::where('id_bahan', $r->id_bahan)->sum('qty');
            $bahanTerpakai[$no] = $r;
            $bahanTerpakai[$no]['totalBahanTerpakai'] = $totalBahanTerpakai;
            $bahanTerpakai[$no]['totalBahanSisa'] = $totalBahanSisa;

            $no++;
        }

        $data = array_merge($dataMain, [
            'bahan_terpakai' => $bahanTerpakai,
            'tab' => 'jumlahBahanTerpakai'
        ]);


        return view('produksi/analisaSaranProduksi/jumlahBahanTerpakai', $data);
    }

    function keperluanBahanTarget()
    {
        $dataMain = Main::data($this->breadcrumb, $this->menuActive);
        $list = mTargetProduksi::distinct()->orderBy('tahun_target', 'DESC')->get(['tahun_target']);
        $listArr = [];
        $no = 0;
        foreach ($list as $r) {
            $totalBahanProduk = 0;
            $totalTargetTahun = mTargetProduksi::where(['tahun_target' => $r->tahun_target])->sum('qty');
            $produk = mTargetProduksi::where(['tahun_target' => $r->tahun_target])->get();
            foreach ($produk as $r2) {
                $totalKomposisiProduk = mKomposisiProduk::where(['id_produk' => $r2->id_produk])->sum('qty');
                $totalBahanProduk += $totalKomposisiProduk;
            }

            //echo $r->tahun_target;

            $listArr[$no]['tahun_target'] = $r->tahun_target;
            $listArr[$no]['total_target'] = $totalTargetTahun;
            $listArr[$no]['total_bahan_produk'] = $totalBahanProduk;
            $listArr[$no]['total_bahan'] = $totalBahanProduk * $totalTargetTahun;

            $no++;
        }

        $data = array_merge($dataMain, [
            'list' => $listArr,
            'tab' => 'keperluanBahanTarget'
        ]);


        return view('produksi/analisaSaranProduksi/keperluanBahanTarget', $data);
    }

    function keperluanBahanTahun($tahun)
    {
        $dataMain = Main::data($this->breadcrumb, $this->menuActive);
        $tahun = Main::decrypt($tahun);
        $pageTitle = 'Tahun ' . $tahun;
        $bahan = mBahan
            ::orderBy('kode_bahan', 'ASC')
            ->get([
                'id',
                'kode_bahan',
                'nama_bahan'
            ]);
        $bahanArr = [];
        $no = 0;
        foreach ($bahan as $r) {

            $total_bahan = 0;
            $produk_list = mTargetProduksi::where(['tahun_target' => $tahun])->get();
            $produk_target_total = mTargetProduksi::where(['tahun_target' => $tahun])->sum('qty');

            foreach ($produk_list as $r_produk) {
                $total_bahan += mKomposisiProduk
                    ::where([
                        'id_produk' => $r_produk->id_produk,
                        'id_bahan' => $r->id
                    ])
                    ->sum('qty');
            }


            $bahanArr[$no] = $r;
            $bahanArr[$no]['total_bahan_diperlukan'] = $total_bahan * $produk_target_total;

            $no++;
        }

        $data = array_merge($dataMain, [
            'list' => $bahanArr,
            'pageTitle' => $pageTitle,
            'tab' => 'keperluanBahanTarget',
            'total_bahan' => 0
        ]);

        return view('produksi/analisaSaranProduksi/keperluanBahanTahun', $data);
    }

}
