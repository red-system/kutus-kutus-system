<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use app\Models\mDistributor;
use app\Models\mKaryawan;
use app\Models\mAcMaster;
use app\Models\mPiutangLain;
use app\Models\mAcJurnalUmum;
use app\Models\mAcTransaksi;
use app\Models\mPiutangLainPembayaran;

use Illuminate\Support\Facades\Session;

class PiutangLain extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['hutang_4'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['hutangPiutang'],
                'route' => ''
            ],
            [
                'label' => $cons['hutang_4'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $list = mPiutangLain
            ::with([
                'karyawan:id,kode_karyawan,nama_karyawan',
                'distributor:id,kode_distributor,nama_distributor',
                'transaksi' => function ($query) {
                    $query
                        ->where([
                            'trs_jenis_transaksi' => 'kredit'
                        ])
                        ->orderBy('transaksi_id', 'ASC');
                }
            ])
            ->where('pl_status', 'belum_bayar')
            ->orderBy('id', 'DESC')
            ->get();
        $kode = Config::get('constants.kodePiutangLain');
        $no_transaksi = hAkunting::getNoTransaksiPayment($kode);
        $karyawan = mKaryawan::orderBy('nama_karyawan')->get();
        $distributor = mDistributor::orderBy('nama_distributor')->get();
        $master = mAcMaster
            ::where('mst_pembayaran', 'yes')
            ->get();
        $akun_debet = mAcMaster::where('mst_nama_rekening', 'like', '%piutang%')->orderBy('mst_kode_rekening')->get();


        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'list',
            'no_transaksi' => $no_transaksi,
            'master' => $master,
            'karyawan' => $karyawan,
            'distributor' => $distributor,
            'akun_debet' => $akun_debet
        ]);

        return view('hutangPiutang/piutangLain/piutangLainList', $data);
    }

    function recycle()
    {
        $data = mPiutangLain::all();
        foreach ($data as $r) {
            if ($r->table_name == 'KYW') {
                $table_name = 'tb_karyawan';
                $table_id = 5;
            } else {
                $table_name = 'tb_distributor';
                $table_id = 1;
            }

            $data_update = [
                'table_id' => $table_id,
                'table_name' => $table_name
            ];


            //mPiutangLain::where('id', $r->id)->update($data_update);
        }
    }

    function insert(Request $request)
    {
        $rules = [
            'pl_invoice' => 'required',
            'pl_tanggal' => 'required',
            'pl_jatuh_tempo' => 'required',
            'pl_amount' => 'required',
            'pl_keterangan' => 'required',
            //'master_id_debet' => 'required',
            'master_id_kredit' => 'required',
            'master_id_kredit.*' => 'required',
            'kredit' => 'required',
            'kredit.*' => 'required',
        ];

        $attributes = [
            'pl_invoice' => 'No Piutang',
            'pl_tanggal' => 'Tangggal Piutang Lain-Lain',
            'pl_jatuh_tempo' => 'Tanggal Jatuh Tempo',
            'pl_amount' => 'Jumlah Piutang Lain-Lain',
            //'master_id_debet' => 'Akun Debet',
            'pl_keterangan' => 'Judul Piutang Lain - Lain',
            'master_id_kredit' => 'Kode Perkiraan Kredit',
            'kredit' => 'Nominal Kredit',
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id_kredit.' . $i] = 'Kode Perkiraan ke-' . $next;
            $attr['kredit.' . $i] = 'Nominal Kredit ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        $pl_invoice = $request->input('pl_invoice');
        $pl_tanggal = $request->input('pl_tanggal');
        $pl_jatuh_tempo = $request->input('pl_jatuh_tempo');
        $table_name = $request->input('table_name');
        $table_id_karyawan = $request->input('table_id_karyawan');
        $table_id_distributor = $request->input('table_id_distributor');
        $table_id = $table_name == 'tb_distributor' ? $table_id_distributor : $table_id_karyawan;

        if ($table_name == 'tb_distributor') {
            $id_distributor = $table_id_distributor;
            $id_karyawan = NULL;
        } else {
            $id_distributor = NULL;
            $id_karyawan = $table_id_karyawan;
        }


        $pl_amount = $request->input('pl_amount');
        $master_id_debet = $request->input('master_id_debet');
        $master_id_debet_arr = explode('|', $master_id_debet);
        $master_id_debet = $master_id_debet_arr[0];
        //$master_id_debet_arr = [17];
        //$kode_perkiraan = $master_id[1];
        $pl_keterangan = $request->input('pl_keterangan');
        $master_id_kredit_arr = $request->input('master_id_kredit');
        $kredit_arr = $request->input('kredit');
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $jmu_no = $this->jmu_no($year, $month, $day);
        $nama_penerima_piutang = $this->nama_penerima_piutang($table_id, $table_name);
        $jmu_keterangan = 'Pembuatan piutang lain lain dari ' . $nama_penerima_piutang;

        $check_balance = $this->check_balance($pl_amount, $kredit_arr);

        if (!$check_balance) {
            return response([
                'message' => 'Nominal Piutang Lain - lain <strong>Tidak Balance</strong> dengan Total Kredit',
                'errors' => [
                    'pl_amount' => [
                        'Nominal Piutang Lain - lain <strong>Tidak Balance</strong> dengan Total Kredit'
                    ]
                ]
            ], 422);
        }

        DB::beginTransaction();
        try {

            /**
             * Insert data ke piutang lain
             */
            $data_piutang_lain = [
                'pl_invoice' => $pl_invoice,
                'pl_jatuh_tempo' => Main::format_date_db($pl_jatuh_tempo),
                'pl_tanggal' => Main::format_date_db($pl_tanggal),
                'table_id' => $table_id,
                'table_name' => $table_name,
                'id_distributor' => $id_distributor,
                'id_karyawan' => $id_karyawan,
                'pl_amount' => $pl_amount,
                'pl_sisa_amount' => $pl_amount,
                'pl_keterangan' => $pl_keterangan,
                'pl_status' => 'belum_bayar',
                //'master_id_kode_perkiraan' => $master_id_kode_perkiraan,
                //'kode_perkiraan' => $kode_perkiraan,
            ];

            $response = mPiutangLain::create($data_piutang_lain);
            $id_piutang_lain = $response->id;

            /**
             * Insert data ke jurnal umum
             */
            $data_jurnal_umum = [
                /*                'table_id' => $id_piutang_lain,
                                'table_name' => 'tb_piutang_lain',*/
                /*                'pelaku_id' => $table_id,*/
                /*'pelaku_table' => $table_name,*/
                'id_distributor' => $id_distributor,
                'id_karyawan' => $id_karyawan,
                'id_piutang_lain' => $id_piutang_lain,
                'no_invoice' => $pl_invoice,
                'jmu_tanggal' => Main::format_date_db($pl_tanggal),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
                'reference_number' => $pl_invoice
            ];

            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            /**
             * Insert data ke transaksi Debet [Hard Code]
             */
            //foreach ($master_id_debet_arr as $key => $master_id) {
            $master = mAcMaster::where('master_id', $master_id_debet)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master->mst_kode_rekening;
            $trs_nama_rekening = $master->mst_nama_rekening;
            $trs_debet = $pl_amount;

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id_debet,
                'id_piutang_lain' => $id_piutang_lain,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $trs_debet,
                'trs_kredit' => 0,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $pl_keterangan,
                'trs_tgl_pencairan' => $this->datetime,
                'tgl_transaksi' => Main::format_date_db($pl_tanggal)
            ];

            mAcTransaksi::create($data_transaksi);
            //}

            /**
             * Insert data ke Transaksi Kredit
             */
            foreach ($master_id_kredit_arr as $index => $master_id_kredit) {
                $master = mAcMaster
                    ::where('master_id', $master_id_kredit)
                    ->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_kredit = $kredit_arr[$index];

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id_kredit,
                    'id_piutang_lain' => $id_piutang_lain,
                    /*                    'table_id' => $id_piutang_lain,
                                        'table_name' => 'tb_piutang_lain',*/
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet' => 0,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => "Pembuatan Piutang Lain Lain",
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => date('Y-m-d'),
                    'tgl_transaksi' => Main::format_date_db($pl_tanggal)
                ];

                mAcTransaksi::create($data_transaksi);
            }

            mAcTransaksi
                ::where([
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'trs_debet' => 0,
                    'trs_kredit' => 0
                ])
                ->delete();

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function edit_kredit(Request $request)
    {
        $id_piutang_lain = $request->input('id_piutang_lain');
        $kredit = mAcTransaksi
            ::where([
                'id_piutang_lain' => $id_piutang_lain,
                'trs_jenis_transaksi' => 'kredit'
            ])
            ->orderBy('transaksi_id', 'ASC')
            ->get();
        $master = mAcMaster::where('mst_pembayaran', 'yes')->get();

        $data = [
            'kredit' => $kredit,
            'master' => $master
        ];

        return view('hutangPiutang/piutangLain/trKreditEdit', $data);
    }


    function update(Request $request, $id)
    {
        $rules = [
            'pl_invoice' => 'required',
            'pl_tanggal' => 'required',
            'pl_jatuh_tempo' => 'required',
            'pl_amount' => 'required',
            'pl_keterangan' => 'required',
            //'master_id_debet' => 'required',
            'master_id_kredit' => 'required',
            'master_id_kredit.*' => 'required',
            'kredit' => 'required',
            'kredit.*' => 'required',
        ];

        $attributes = [
            'pl_invoice' => 'No Piutang',
            'pl_tanggal' => 'Tangggal Piutang Lain-Lain',
            'pl_jatuh_tempo' => 'Tanggal Jatuh Tempo',
            'pl_amount' => 'Jumlah Piutang Lain-Lain',
            'pl_keterangan' => 'Judul Piutang Lain - Lain',
            //'master_id_debet' => 'Akun Debet'
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id_kredit.' . $i] = 'Kode Perkiraan ke-' . $next;
            $attr['kredit.' . $i] = 'Nominal Kredit ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $id_piutang_lain = $id;
        $pl_invoice = $request->input('pl_invoice');
        $pl_tanggal = $request->input('pl_tanggal');
        $pl_jatuh_tempo = $request->input('pl_jatuh_tempo');
        $table_name = $request->input('table_name');
        $table_id_karyawan = $request->input('table_id_karyawan');
        $table_id_distributor = $request->input('table_id_distributor');
        $table_id = $table_name == 'tb_distributor' ? $table_id_distributor : $table_id_karyawan;
        if ($table_name == 'tb_distributor') {
            $id_distributor = $table_id_distributor;
            $id_karyawan = NULL;
        } else {
            $id_distributor = NULL;
            $id_karyawan = $table_id_karyawan;
        }
        $pl_amount = $request->input('pl_amount');
        $master_id_debet = $request->input('master_id_debet');
        $master_id_debet_arr = explode('|', $master_id_debet);
        $master_id_debet = $master_id_debet_arr[0];
        $kode_perkiraan = $master_id_debet_arr[1];
        //$master_id_debet_arr = [17];
        $pl_keterangan = $request->input('pl_keterangan');
        $master_id_kredit_arr = $request->input('master_id_kredit');
        $kredit_arr = $request->input('kredit');
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $jmu_no = $this->jmu_no($year, $month, $day);
        $nama_penerima_piutang = $this->nama_penerima_piutang($table_id, $table_name);
        $jmu_keterangan = 'Pembaruan piutang lain lain dari ' . $nama_penerima_piutang;
        $user = Session::get('user');

        $check_balance = $this->check_balance($pl_amount, $kredit_arr);

        if (!$check_balance) {
            return response([
                'message' => 'Nominal Piutang Lain - lain <strong>Tidak Balance</strong> dengan Total Kredit',
                'errors' => [
                    'pl_amount' => [
                        'Nominal Piutang Lain - lain <strong>Tidak Balance</strong> dengan Total Kredit'
                    ]
                ]
            ], 422);
        }

        DB::beginTransaction();
        try {

            /**
             * Insert data ke piutang lain
             */
            $data_piutang_lain = [
                'pl_invoice' => $pl_invoice,
                'pl_jatuh_tempo' => Main::format_date_db($pl_jatuh_tempo),
                'pl_tanggal' => Main::format_date_db($pl_tanggal),
                'id_distributor' => $id_distributor,
                'id_karyawan' => $id_karyawan,
                /*                'table_id' => $table_id,
                                'table_name' => $table_name,*/
                'pl_amount' => $pl_amount,
                'pl_sisa_amount' => $pl_amount,
                'pl_keterangan' => $pl_keterangan,
                'pl_status' => 'belum_bayar',
                //'master_id_kode_perkiraan' => $master_id_kode_perkiraan,
                //'kode_perkiraan' => $kode_perkiraan,
                'updated_by' => $user->id,
                'updated_by_name' => $user->karyawan->nama_karyawan
            ];

            mPiutangLain::where('id', $id_piutang_lain)->update($data_piutang_lain);
            mAcJurnalUmum::where('id_piutang_lain')->delete();
            mAcTransaksi
                ::where('id_piutang_lain', $id_piutang_lain)
                ->delete();

            /**
             * Insert data ke jurnal umum
             */
            $data_jurnal_umum = [
                /*                'table_id' => $id_piutang_lain,
                                'table_name' => 'tb_piutang_lain',*/
                'id_piutang_lain' => $id_piutang_lain,
                'id_distributor' => $id_distributor,
                'id_karyawan' => $id_karyawan,
                /*                'pelaku_id' => $table_id,
                                'pelaku_table' => $table_name,*/
                'no_invoice' => $pl_invoice,
                'jmu_tanggal' => Main::format_date_db($pl_tanggal),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
                'reference_number' => $pl_invoice
            ];

            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            /**
             * Insert data ke transaksi debet
             */


            $master = mAcMaster
                ::where('master_id', $master_id_debet)
                ->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master->mst_kode_rekening;
            $trs_nama_rekening = $master->mst_nama_rekening;
            $trs_debet = $pl_amount;

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id_debet,
                'id_piutang_lain' => $id_piutang_lain,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $trs_debet,
                'trs_kredit' => 0,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $pl_keterangan,
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => date('Y-m-d'),
                'trs_setor' => 0,
                'tgl_transaksi' => Main::format_date_db($pl_tanggal)
            ];

            mAcTransaksi::create($data_transaksi);


            /**
             * Insert Transaksi Kredit
             */
            foreach ($master_id_kredit_arr as $index => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_kredit = $kredit_arr[$index];

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'id_piutang_lain' => $id_piutang_lain,
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet' => 0,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => 'Operasi',
                    'trs_catatan' => "Pembuatan Piutang lain lain",
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => date('Y-m-d'),
                    'tgl_transaksi' => Main::format_date_db($pl_tanggal)
                ];

                mAcTransaksi::create($data_transaksi);
            }

            mAcTransaksi
                ::where([
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'trs_debet' => 0,
                    'trs_kredit' => 0
                ])
                ->delete();

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }


    function delete($id_piutang_lain)
    {
        DB::beginTransaction();
        try {

            $check = mPiutangLainPembayaran::where('id_piutang_lain', $id_piutang_lain)->count();

            if ($check > 0) {
                return response([
                    'message' => 'Piutang ini <strong>sudah</strong> melakukan pembayaran sebelumnya, sehingga tidak bisa dihapus.'
                ], 422);
            } else {
                $jurnal_umum_id = mAcJurnalUmum
                    ::where('id_piutang_lain', $id_piutang_lain)
                    ->value('jurnal_umum_id');
                mPiutangLain::where('id', $id_piutang_lain)->delete();
                mAcJurnalUmum
                    ::where('jurnal_umum_id', $jurnal_umum_id)
                    ->delete();
                mAcTransaksi
                    ::where('jurnal_umum_id', $jurnal_umum_id)
                    ->delete();
            }

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function pembayaran_insert(Request $request, $id)
    {

        $rules = [
            'master_id' => 'required',
            'master_id.*' => 'required'
        ];

        $attributes = [
            'master_id' => 'Kode Perkiraan',
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        DB::beginTransaction();
        try {

            // DUMMY VAARIABLE
            $total = $request->input('total');
            $terbayar = $request->input('terbayar');
            $sisa_pembayaran = $request->input('sisa_pembayaran');
            $kembalian = $request->input('kembalian');
            //$kode_supplier = $request->input('kode_supplier');
            //$nama_supplier = $request->input('nama_supplier');
            //$master_id_kredit = $request->input('master_id_kredit');
            //$master_id_debet = $request->input('master_id_debet');
            $master_id_kode_perkiraan = $request->input('master_id_kode_perkiraan');
            //$master = mAcMaster::find($master_id_kode_perkiraan);
            $kode_perkiraan = $request->input('kode_perkiraan');
            /*            $kode_perkiraan = $request->input('kode_perkiraan');
                        $ps_no_faktur = $request->input('ps_no_faktur');*/
            /*            $hs_amount = $request->input('hs_amount');
                        $hs_sisa_amount = $request->input('hs_sisa_amount');*/



            $id_piutang_lain = $id;
            $no_transaksi = $request->input('no_transaksi');
            $pl_invoice = $request->input('pl_invoice');
            $id_supplier = $request->input('id_supplier');
            $master_id_kredit = mAcTransaksi
                ::where([
                    'id_piutang_lain' => $id_piutang_lain,
                    'trs_jenis_transaksi' => 'debet'
                ])
                ->value('master_id');



            $tanggal_transaksi = $request->input('tanggal_transaksi');

            $master_id_arr = $request->input('master_id');
            $jumlah_arr = $request->input('jumlah');
            $no_bg_arr = $request->input('no_bg');
            $tanggal_pencairan_arr = $request->input('tanggal_pencairan');
            $tipe_arus_kas = 'Operasi';
            $nama_bank_arr = $request->input('nama_bank');
            $keterangan_arr = $request->input('keterangan');
            $year = date('Y', strtotime($tanggal_transaksi));
            $month = date('m', strtotime($tanggal_transaksi));
            $day = date('d', strtotime($tanggal_transaksi));
            $jmu_no = $this->jmu_no($year, $month, $day);
            $jmu_keterangan = 'Penerimaan : Piutang Lain Lain - No Invoice: ' . $pl_invoice;
            $total_payment = 0;
            $user = Session::get('user');
            $user_id = $user->id;
            $data_piutang_lain_pembayaran = [];

            /**
             * Data Jurnal Umum
             */

            $data_jurnal = [
                'no_invoice' => $no_transaksi,
                'table_id' => $id_piutang_lain,
                'table_name' => 'tb_piutang_lain',
                'id_piutang_lain' => $id_piutang_lain,
                'id_supplier' => $id_supplier,
                'jmu_tanggal' => date('Y-m-d', strtotime($tanggal_transaksi)),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
                'reference_number' => $pl_invoice
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            foreach ($master_id_arr as $key => $master_id) {

                $jumlah = $jumlah_arr[$key];
                $keterangan = $keterangan_arr[$key];
                $no_bg = $no_bg_arr[$key];
                $tanggal_pencairan = $tanggal_pencairan_arr[$key];
                $nama_bank = $nama_bank_arr[$key];

                /**
                 * Insert Data Transaksi
                 */
                $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'id_piutang_lain' => $id_piutang_lain,
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet' => $jumlah,
                    'trs_kredit' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $keterangan . ' No Invoice : ' . $pl_invoice,
                    'trs_no_check_bg' => $no_bg,
                    'trs_setor' => $jumlah,
                    'trs_tgl_pencairan' => Main::format_date_db($tanggal_pencairan),
                    'tgl_transaksi' => Main::format_date_db($tanggal_transaksi)
                ];

                mAcTransaksi::create($data_transaksi);

                /**
                 * Update Piutang Supplier sudah lunas atau belum
                 */

                $sisa_amount = mPiutangLain::where('id', $id_piutang_lain)->value('pl_sisa_amount');
                $sisa_amount -= $jumlah;
                $total_payment += $jumlah;
                if ($sisa_amount == 0) {
                    mPiutangLain
                        ::where('id', $id_piutang_lain)
                        ->update([
                            'pl_sisa_amount' => $sisa_amount,
                            'pl_status' => 'lunas'
                        ]);
                } else {
                    mPiutangLain
                        ::where('id', $id_piutang_lain)
                        ->update([
                            'pl_sisa_amount' => $sisa_amount
                        ]);
                }


                /**
                 * Memasukkan Piutang Lain Pembyaran
                 */

                $data_piutang_lain_pembayaran[] = [
                    'id_piutang_lain' => $id_piutang_lain,
                    'master_id' => $master_id,
                    'jumlah' => $jumlah,
                    'no_check_bg' => $no_bg,
                    'tanggal_pencairan' => Main::format_date_db($tanggal_pencairan),
                    'nama_bank' => $nama_bank,
                    'keterangan' => $keterangan,
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];

            }

            mPiutangLainPembayaran::insert($data_piutang_lain_pembayaran);

            $master = mAcMaster::where('master_id', $master_id_kredit)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening_hutang = $master->mst_kode_rekening;
            $trs_nama_rekening_hutang = $master->mst_nama_rekening;
            $trs_kredit = $total_payment;

            $data_transaksi_hutang = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id,
                'id_piutang_lain' => $id_piutang_lain,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet' => 0,
                'trs_kredit' => $trs_kredit,
                'user_id' => $user_id,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening_hutang,
                'trs_nama_rekening' => $trs_nama_rekening_hutang,
                'trs_tipe_arus_kas' => $tipe_arus_kas,
                'trs_catatan' => "Penerimaan Piutang",
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => $this->datetime,
                'trs_setor' => $total_payment,
                'tgl_transaksi' => Main::format_date_db($tanggal_transaksi)
            ];

            mAcTransaksi::create($data_transaksi_hutang);

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function jmu_no($year, $month, $day)
    {
        $where = [
            'jmu_year' => $year,
            'jmu_month' => $month,
            'jmu_day' => $day
        ];
        $count = mAcJurnalUmum::where($where)->count();
        if ($count == 0) {
            return 1;
        } else {
            $jmu_no = mAcJurnalUmum::where($where)->orderBy('jmu_no', 'DESC')->first(['jmu_no']);
            return $jmu_no->jmu_no + 1;
        }
    }

    function nama_penerima_piutang($table_id, $table_name)
    {
        if ($table_name == 'tb_distributor') {
            return mDistributor::where('id', $table_id)->value('nama_distributor');
        } else {
            return mKaryawan::where('id', $table_id)->value('nama_karyawan');
        }
    }

    function    check_balance($pl_amount, $kredit_arr)
    {
        $total = 0;
        foreach ($kredit_arr as $kredit) {
            $total += $kredit;
        }

        return $total == $pl_amount ? TRUE : FALSE;
    }
}
