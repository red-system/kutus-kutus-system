<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mTargetProduksi extends Model
{
    protected $table = 'tb_target_produksi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produk',
        'tgl_target',
        'bulan_target',
        'tahun_target',
        'qty',
        'status'
    ];

    function produk() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }
}
