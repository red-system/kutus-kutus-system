<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * Digunakan untuk check piutang pelanggan
 *
 * Class mCheques
 * @package app\Models
 */
class mCheques extends Model
{
    protected $table = 'tb_cheques';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'id_distributor',
        'no_bg_cek',
        'no_invoice',
        'tgl_cek',
        'tgl_pencairan',
        'bg_jatuh_tempo',
        'cek_amount',
        'sisa',
        'cek_dari',
        'cek_dari_table',
        'cek_keterangan',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name'
    ];

    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }
}
