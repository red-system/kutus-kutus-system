<table class="row-kredit m--hide">
    <tbody>
    <tr>
        <td class="td-master-id">
            <select class="form-control select2-kredit" name="master_id_kredit[]" style="width: 100%">
                <option value="">Pilih Pembayaran</option>
                @foreach($master as $r)
                    <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                @endforeach
            </select>
        </td>
        <td class="td-jumlah">
            <input type="text" name="kredit[]" class="form-control touchspin-kredit" value="0"
                   style="width: 80px">
        </td>
        <td>
            <button type="button" class="btn-delete-row-kredit btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
    </tbody>
</table>