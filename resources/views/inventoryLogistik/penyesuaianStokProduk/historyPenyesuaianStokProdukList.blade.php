@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tabs">
                @include('inventoryLogistik.penyesuaianStokProduk.tabTitle')
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ $date_start }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end" value="{{ $date_end }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('historyPenyesuaianStokProdukPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('historyPenyesuaianStokProdukExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body akses-history">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tanggal</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Kategori Produk</th>
                            <th>Lokasi</th>
                            <th>Qty Awal</th>
                            <th>Qty Akhir</th>
                            <th>Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ Main::format_date($r->tgl) }}</td>
                                <td>{{ $r->kode_produk }}</td>
                                <td>{{ $r->nama_produk }}</td>
                                <td>{{ $r->kategori_produk }}</td>
                                <td>{{ $r->lokasi }}</td>
                                <td align="right">{{ Main::format_number($r->qty_awal) }}</td>
                                <td align="right">{{ Main::format_number($r->qty_akhir) }}</td>
                                <td>{{ $r->keterangan }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection