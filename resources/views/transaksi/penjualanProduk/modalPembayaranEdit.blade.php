<div class="modal" id="modal-pembayaran" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xxlg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <h3 class="modal-title m--font-light" id="exampleModalLabel">PEMBAYARAN</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">No Faktur</label>
                            <div class="col-10 col-form-label">
                                {{ $no_faktur }}
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>
                <button type="button" class="btn-add-row-pembayaran btn btn-accent btn-sm m-btn--pill">
                    <i class="la la-plus"></i> Tambah Pembayaran
                </button>
                <br /><br />
                <table class="table-pembayaran table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Kode Perkiraan</th>
                        <th>Jumlah</th>
                        <th>Jatuh Tempo</th>
                        <th>No. Check / BG</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($penjualan_pembayaran as $r_pembayaran)
                            <tr>
                                <td class="td-master-id">
                                    <select class="form-control m-select2 master_id" name="master_id[{{ $r_pembayaran->id }}]" style="width: 250px">
                                        <option value="">Pilih Pembayaran</option>
                                        @foreach($master as $r)
                                            <option value="{{ $r->master_id }}" {{ $r->master_id == $r_pembayaran->master_id ? 'selected' : '' }}>{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class="td-jumlah">
                                    <input type="text" name="jumlah[{{ $r_pembayaran->id }}]" class="form-control touchspin-number jumlah" value="{{ $r_pembayaran->jumlah }}"
                                           style="width: 80px">
                                </td>
                                <td class="td-jatuh-tempo">
                                    <div class="input-group date" style="width: 150px">
                                        <input type="text" class="form-control m-input m_datepicker jatuh_tempo" name="jatuh_tempo[{{ $r_pembayaran->id }}]"
                                               readonly=""
                                               value="{{ date('d-m-Y', strtotime($r_pembayaran->jatuh_tempo)) }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </td>
                                <td class="td-no-bg">
                                    <input class="form-control m-input no_bg" type="text" name="no_bg[{{ $r_pembayaran->id }}]" style="width: 120px" value="{{ $r_pembayaran->no_check_bg }}">
                                </td>
                                <td class="td-keterangan">
                                    <textarea class="form-control m-input keterangan" name="keterangan[{{ $r_pembayaran->id }}]" style="width: 120px">{{ $r_pembayaran->keterangan }}</textarea>
                                </td>
                                <td>
                                    <button type="button" class="btn-delete-row-pembayaran btn m-btn--pill btn-danger btn-sm"
                                            data-confirm="false">
                                        <i class="la la-remove"></i> Hapus
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <br/>
                <hr/>
                <div class="form-no-padding">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Total</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="grand-total">{{ Main::format_number($penjualan->grand_total) }}</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Terbayar</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="terbayar">{{ Main::format_number($penjualan->terbayar) }}</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Sisa Pembayaran</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="sisa-pembayaran">{{ Main::format_number($penjualan->sisa_pembayaran) }}</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Kembalian</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="kembalian">{{ Main::format_number($penjualan->kembalian) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>