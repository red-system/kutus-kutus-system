@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
@endsection

@section('body')


    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">
                @include('inventoryLogistik.penyesuaianStokProduk.tabTitle')
                <div class="m-portlet__body akses-penyesuaian_stok_produk">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Kategori Produk</th>
{{--                            <th>Lokasi</th>--}}
                            <th>Minimal Stok</th>
{{--                            <th>Harga Eceran</th>
                            <th>Harga Grosir</th>
                            <th>Harga Kosinyasi</th>--}}
                            <th>Diskon Persen</th>
                            <th>Diskon Nominal</th>
                            <th>Total Stok</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                        <tr>
                            <td align="center">{{ $no++ }}.</td>
                            <td>{{ $r->kode_produk }}</td>
                            <td>{{ $r->nama_produk }}</td>
                            <td>{{ $r->kategori_produk->kategori_produk }}</td>
{{--                            <td>{{ $r->lokasi->lokasi }}</td>--}}
                            <td>{{ Main::format_number($r->minimal_stok) }}</td>
{{--                            <td>{{ Main::format_money($r->harga_eceran) }}</td>
                            <td>{{ Main::format_money($r->harga_grosir) }}</td>
                            <td>{{ Main::format_money($r->harga_kosinyasi) }}</td>--}}
                            <td>{{ Main::format_discount($r->disc_persen) }}</td>
                            <td>{{ $r->disc_nominal }}</td>
                            <td>{{ Main::format_number($r->total_stok) }}</td>
                            <td align="center">
                                <a href="{{ route('stokPenyesuaianProduk', ['idProduk'=>Main::encrypt($r->id)]) }}"
                                    class="m-btn btn btn-primary m-btn--pill btn-sm akses-stok_produk">
                                    <i class="la la-dropbox"></i> Stok Produk
                                </a>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection