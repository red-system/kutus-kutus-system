<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mDistributorOrderProduk;
use app\Models\mAcJurnalUmum;
use app\Models\mPenjualan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\mDistributor;
use Illuminate\Support\Facades\Config;

use app\Rules\UsernameDistributor;
use app\Rules\UsernameDistributorUpdate;
use Illuminate\Support\Facades\Session;

class Distributor extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_7'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        //return Session::all();
        $data = Main::data($this->breadcrumb);
        $data['list'] = mDistributor::orderBy('id', 'DESC')->get();

        return view('masterData/distributor/distributorList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'username' => ['required', new UsernameDistributor()],
            'password' => 'required',
            'ulangi_password' => 'required|same:password',
            'kode_distributor' => 'required',
            'nama_distributor' => 'required',
            'alamat_distributor' => 'required',
            'telp_distributor' => 'required'
        ]);

        $data = $request->except(['_token', 'foto_distributor', 'ulangi_password']);
        $data['password'] = Hash::make($data['password']);

        if ($request->hasFile('foto_distributor')) {
            $file = $request->file('foto_distributor');
            $file->move('upload', $file->getClientOriginalName());

            $data['foto_distributor'] = $file->getClientOriginalName();
        } else {
            $data['foto_distributor'] = 'empty.png';
        }

        mDistributor::create($data);
    }

    function delete($id)
    {
        $id_distributor = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $list_table = [
            'distributor_order_produk',
            'jurnal_umum',
            'penjualan'
        ];

        $distributor_order_produk = mDistributorOrderProduk::where(['id_distributor_order' => $id_distributor])->count();
        $jurnal_umum = mAcJurnalUmum::where(['id_distributor' => $id_distributor])->count();
        $penjualan = mPenjualan::where(['id_distributor' => $id_distributor])->count();

        foreach($list_table as $table) {
            if($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch($check_table_problem) {
            case "distributor_order_produk":
                $message = 'Tidak bisa dihapus, karena Distributor ini digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.transaksi_9')).'</strong>';
                break;
            case "jurnal_umum":
                $message = 'Tidak bisa dihapus, karena Distributor ini digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.laporanA_1')).'</strong>';
                break;
            case "penjualan":
                $message = 'Tidak bisa dihapus, karena Distributor ini digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.transaksi_1')).'</strong>';
                break;
        }

        if($check_status) {
            mDistributor::where('id', $id)->delete();
        } else {
            $response = [
                'title'=>'Perhatian ...',
                'message'=>$message
            ];

            return response($response, 422);
        }

    }

    function update(Request $request, $id)
    {
        $request->validate([
            'username' => ['required', new UsernameDistributorUpdate($id)],
            'ulangi_password' => 'same:password',
            'kode_distributor' => 'required',
            'nama_distributor' => 'required',
            'alamat_distributor' => 'required',
            'telp_distributor' => 'required',
        ]);

        $data = $request->except(["_token", "foto_distributor", "ulangi_password"]);
        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        if ($request->hasFile('foto_distributor')) {
            $file = $request->file('foto_distributor');
            $file->move('upload', $file->getClientOriginalName());
            $data['foto_distributor'] = $file->getClientOriginalName();
        }

        mDistributor::where(['id' => $id])->update($data);
    }
}
