<?php

namespace app\Http\Controllers\LaporanOperasional;

use app\Helpers\Main;
use app\Http\Controllers\Controller;
use app\Models\mBahan;
use app\Models\mBahanProduksi;
use app\Models\mDetailProduksi;
use app\Models\mProduk;
use app\Models\mProduksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use app\Models\mPenjualan;
use app\Models\mPenjualanProduk;
use PDF;

class LaporanPenggunaanBahan extends Controller
{

    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['distribusi'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporanOperasional'],
                'route' => ''
            ],
            [
                'label' => $cons['laporanO_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');
        $date_end_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $where_date_produksi = [
            Main::format_date_db($date_start).' 00:00:00',
            Main::format_date_db($date_end).' 23:59:59'
        ];

        $produk = mProduk
            ::withCount([
                'detail_produksi AS total_qty_produksi' => function ($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty)'))
                        ->leftJoin('tb_produksi', 'tb_produksi.id', '=', 'tb_detail_produksi.id_produksi')
                        ->whereBetween('tb_produksi.tgl_mulai_produksi', $where_date);
                },
                'komposisi_produk AS total_qty_bahan' => function ($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->orderBy('kode_produk')
            ->get();

        $bahan = mBahan::orderby('nama_bahan', 'ASC')->get();
        $produksi = mProduksi
            ::where([
                'publish' => 'yes',
                'status' => 'finish'
            ])
            ->whereBetween('status_finish_date', $where_date_produksi)
            ->orderBy('urutan', 'ASC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'produk' => $produk,
            'bahan' => $bahan,
            'produksi' => $produksi,
            'total' => 0
        ]);

        return view('laporanOperasional/laporanPenggunaanBahan/laporanPenggunaanBahanList', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');
        $date_end_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $where_date_produksi = [
            Main::format_date_db($date_start).' 00:00:00',
            Main::format_date_db($date_end).' 23:59:59'
        ];

        $produk = mProduk
            ::withCount([
                'detail_produksi AS total_qty_produksi' => function ($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty)'))
                        ->leftJoin('tb_produksi', 'tb_produksi.id', '=', 'tb_detail_produksi.id_produksi')
                        ->whereBetween('tb_produksi.tgl_mulai_produksi', $where_date);
                },
                'komposisi_produk AS total_qty_bahan' => function ($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->orderBy('kode_produk')
            ->get();

        $bahan = mBahan::orderby('nama_bahan', 'ASC')->get();
        $produksi = mProduksi
            ::where([
                'publish' => 'yes',
                'status' => 'finish'
            ])
            ->whereBetween('status_finish_date', $where_date_produksi)
            ->orderBy('urutan', 'ASC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'produk' => $produk,
            'bahan' => $bahan,
            'produksi' => $produksi,
            'total' => 0,
            'company'=>Main::companyInfo()
        ]);

        $pdf = PDF::loadView('laporanOperasional/laporanPenggunaanBahan/laporanPenggunaanBahanPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('Laporan Penggunaan Bahan ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');
        $date_end_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $where_date_produksi = [
            Main::format_date_db($date_start).' 00:00:00',
            Main::format_date_db($date_end).' 23:59:59'
        ];

        $produk = mProduk
            ::withCount([
                'detail_produksi AS total_qty_produksi' => function ($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty)'))
                        ->leftJoin('tb_produksi', 'tb_produksi.id', '=', 'tb_detail_produksi.id_produksi')
                        ->whereBetween('tb_produksi.tgl_mulai_produksi', $where_date);
                },
                'komposisi_produk AS total_qty_bahan' => function ($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty)'));
                }
            ])
            ->orderBy('kode_produk')
            ->get();

        $bahan = mBahan::orderby('nama_bahan', 'ASC')->get();
        $produksi = mProduksi
            ::where([
                'publish' => 'yes',
                'status' => 'finish'
            ])
            ->whereBetween('status_finish_date', $where_date_produksi)
            ->orderBy('urutan', 'ASC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'produk' => $produk,
            'bahan' => $bahan,
            'produksi' => $produksi,
            'total' => 0,
            'company'=>Main::companyInfo()
        ]);

        return view('laporanOperasional/laporanPenggunaanBahan/laporanPenggunaanBahanExcel', $data);
    }

}
