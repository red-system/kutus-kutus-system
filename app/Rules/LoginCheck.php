<?php

namespace app\Rules;

use app\Models\mUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use app\Models\mDistributor;
use Session;

class LoginCheck implements Rule
{

    protected $username;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($username)
    {
        $this->username = $username;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $username = $this->username;
        $password = $value;
        $userPassword = mUser::where('username', $username)->value('password');
        $level = '';
        $status = FALSE;

        /**
         * Login user
         */
        if (Hash::check($password, $userPassword)) {
            $level = 'administrator';
            $status = TRUE;
        } else {
            /**
             * Login Distributor
             */
            $distributorPassword = mDistributor::where('username', $username)->value('password');
            if (Hash::check($password, $distributorPassword)) {
                $level = 'distributor';
                $status = TRUE;
            } else {
                $status = FALSE;
            }
        }

        if ($status) {
            if ($level == 'administrator') {

                $user = mUser
                    ::with([
                        'karyawan',
                        'user_role'
                    ])
                    ->where([
                        'username' => $username,
                    ])
                    ->first();


                if ($user->karyawan->foto_karyawan == '') {
                    $user->karyawan->foto_karyawan = 'empty.png';
                }

                $session = [
                    'login' => TRUE,
                    'level' => $level,
                    'user' => $user
                ];


            } elseif ($level == 'distributor') {
                $user = mDistributor::where('username', $username)->first();
                if ($user->foto_distributor == '') {
                    $user->foto_distributor = 'empty.png';
                }

                $session = [
                    'login' => TRUE,
                    'level' => $level,
                    'user' => $user
                ];
            }

            //echo json_encode($session);

            Session::put($session);
        }


        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Username atau Password tidak benar';
    }
}
