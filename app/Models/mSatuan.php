<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mSatuan extends Model
{
    protected $table = 'tb_satuan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_satuan',
        'satuan',
    ];
}
