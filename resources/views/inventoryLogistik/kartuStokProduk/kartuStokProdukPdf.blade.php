<style type="text/css">
    .table-data {
        border-left: 0.01em solid #ccc;
        border-right: 0;
        border-top: 0.01em solid #ccc;
        border-bottom: 0;
        border-collapse: collapse;
        width: 100%;
    }

    .table-data td,
    .table-data th {
        border-left: 0;
        border-right: 0.01em solid #ccc;
        border-top: 0;
        border-bottom: 0.01em solid #ccc;
        padding: 2px 4px;
        text-align: center !important;
        font-size: 12px;
    }
</style>

<h1 align="center">KARTU STOK PRODUK</h1>
<table width="100%">
    <tbody>
    <tr>
        <td width="25%" align="center">Nama Kategori Produk : {{ $nama_kategori_produk }}</td>
        <td width="25%" align="center">Nama Produk : {{ $nama_produk }}</td>
        <td width="25%" align="center">Tanggal : {{ $date_start }} s/d {{ $date_end }}</td>
        <td width="25%" align="center">Lokasi : {{ $nama_lokasi }}</td>
    </tr>
    </tbody>
</table>
<br/>
<table width="100%" border="1" class="table-data">
    <caption>

    </caption>
    <thead>
    <tr>
        <th colspan="<?php echo $stok_masuk_col ?>" class="text-center" width="50%">STOK MASUK</th>
        <th colspan="<?php echo $stok_keluar_col ?>" class="text-center" width="50%">STOK KELUAR</th>
    </tr>
    <tr>
        <th>Tanggal</th>
        @if($view_produk == 'all')
            <th>Produk</th>
        @endif
        <th>Kode Produksi</th>
        <th>Masuk</th>
        @if($view_lokasi == 'all')
            <th>Lokasi</th>
        @endif
        <th>Keterangan</th>
        <th>Tanggal</th>
        @if($view_produk == 'all')
            <th>Produk</th>
        @endif
        <th>Kode Produksi</th>
        <th>Keluar</th>
        @if($view_lokasi == 'all')
            <th>Lokasi</th>
        @endif
        <th>Penerima/Keterangan</th>
        <th>Stok Akhir</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $tgl => $value)

        @foreach($value as $r_arus)
            @php
                $status = $r_arus->stok_in == 0 ? 'out' : 'in';
                    $stok_akhir = $r_arus->last_stok;
            @endphp

            @if($status == 'in')
                <tr>
                    <td>{{ Main::format_date($tgl) }}</td>
                    @if($view_produk == 'all')
                        <td>{{ $r_arus->kode_produk.' '.$r_arus->nama_produk }}</td>
                    @endif
                    <td>{{ $r_arus->no_seri_produk }}</td>
                    <td>{{ Main::format_number($r_arus->stok_in) }}</td>
                    @if($view_lokasi == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    <td>{{ $r_arus->arus_stok_produk_keterangan }}</td>
                    <td></td>
                    @if($view_produk == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    @if($view_lokasi == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td></td>
                    <td>{{ Main::format_number($stok_akhir) }}</td>
                </tr>
            @else
                <tr>
                    <td></td>
                    @if($view_produk == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td></td>
                    @if($view_lokasi == 'all')
                        <td></td>
                    @endif
                    <td></td>
                    <td>{{ Main::format_date($tgl) }}</td>
                    @if($view_produk == 'all')
                        <td>{{ $r_arus->kode_produk.' '.$r_arus->nama_produk }}</td>
                    @endif
                    <td>{{ $r_arus->no_seri_produk }}</td>
                    <td>{{ Main::format_number($r_arus->stok_out) }}</td>
                    @if($view_lokasi == 'all')
                        <td>{{ $r_arus->lokasi }}</td>
                    @endif
                    <td>{{ $r_arus->arus_stok_produk_keterangan }}</td>
                    <td>{{ Main::format_number($stok_akhir) }}</td>
                </tr>
            @endif
        @endforeach
    @endforeach

    </tbody>
</table>