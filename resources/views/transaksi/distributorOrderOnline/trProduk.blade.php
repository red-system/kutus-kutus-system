<table class="row-produk m--hide">
    <tr>
        <td class="m--hide data">
            <input type="hidden" name="id_produk[]">
            <input type="hidden" name="harga[]">
            <input type="hidden" name="ppn_nominal[]">
            <input type="hidden" name="harga_net[]">
            <input type="hidden" name="sub_total[]">
        </td>
        <td>
            <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                    type="button">
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td class="produk-nama">-</td>
        <td>
            <select class="form-control select2-penjualan" name="id_lokasi[]" disabled style="width: 140px">
                <option value="">Pilih Gudang</option>
                @foreach($gudang as $r)
                    <option value="{{ $r->id }}">{{ $r->kode_lokasi.' '.$r->lokasi }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <select class="form-control select2-penjualan" name="id_stok_produk[]" style="width: 140px">

            </select>
        </td>
        <td class="td-qty">
            <input type="text" name="qty[]" class="touchspin-penjualan" value="1" style="width: 70px">
        </td>
        <td class="td-harga">
            -
        </td>
        <td class="td-potongan">
            <input type="text" name="potongan[]" class="touchspin-penjualan" value="0" style="width: 60px">
        </td>
        <td class="td-ppn">
            -
        </td>
        <td class="td-harga-net">
            -
        </td>
        <td class="td-sub-total">
            -
        </td>
        <td>
            <button type="button" class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i>
            </button>
        </td>
    </tr>
</table>