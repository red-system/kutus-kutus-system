<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHargaProduk extends Model
{

    protected $table = 'tb_harga_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produk',
        'rentang_awal',
        'rentang_akhir',
        'harga'
    ];

    function lokasi() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }
}
