<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mBahan extends Model
{

    protected $table = 'tb_bahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_lokasi',
        'id_satuan',
        'id_supplier',
        'id_kategori_bahan',
        'kode_bahan',
        'nama_bahan',
        'minimal_stok',
        'qty',
        'last_stok',
        'harga',
        'keterangan'
    ];

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function kategori_bahan() {
        return $this->belongsTo(mKategoriBahan::class, 'id_kategori_bahan');
    }

    function satuan() {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    function supplier() {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    function stok_bahan() {
        return $this->hasMany(mStokBahan::class, 'id_bahan');
    }

    function bahan_produksi() {
        return $this->hasMany(mBahanProduksi::class, 'id_bahan');
    }
}
