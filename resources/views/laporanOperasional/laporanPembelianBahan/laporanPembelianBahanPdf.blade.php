<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>Laporan Pembelian Bahan</h3></td>
                    </tr>
                    <tr>
                        <td>{{ $date_start.' s/d '.$date_end }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <br/>
            <div id="table">
                @foreach($po_bahan as $r_po_bahan)


                    @php
                        $id_bahan_arr = [];
                        foreach($r_po_bahan->po_bahan_detail as $r_po_bahan_detail) {
                            $id_bahan_arr[] = $r_po_bahan_detail->id_bahan;
                        }

                        if($id_bahan == 'all') {
                            $bahan_exist = TRUE;
                        } else {
                            $bahan_exist = FALSE;
                            $bahan_exist = in_array($id_bahan, $id_bahan_arr) ? TRUE: FALSE;
                        }

                    @endphp

                    @if($bahan_exist)

                    <table>
                        <tr>
                            <td style="border: 1px solid black; padding: 2px;">
                                <h4>{{ $no_card++ }}.</h4>
                                <table width="100%" class="table-header">
                                    <tbody>
                                    <tr>
                                        <td width="20%">No Pembelian</td>
                                        <td width="30%">{{ $r_po_bahan->no_faktur }}</td>
                                        <td width="20%">Total</td>
                                        <td width="30%">{{ Main::format_money($r_po_bahan->total) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>{{ $r_po_bahan->tanggal }}</td>
                                        <td>Biaya Tambahan</td>
                                        <td>{{ Main::format_money($r_po_bahan->biaya_tambahan) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Pembayaran</td>
                                        <td>{{ ucwords($r_po_bahan->pembayaran) }}</td>
                                        <td>Potongan</td>
                                        <td>{{ Main::format_money($r_po_bahan->potongan) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pengiriman</td>
                                        <td>{{ ucwords($r_po_bahan->pengiriman) }}</td>
                                        <td>Grand Total</td>
                                        <td>{{ Main::format_money($r_po_bahan->grand_total) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Supplier</td>
                                        <td>{{ '('.$r_po_bahan->supplier->kode_supplier.') '.$r_po_bahan->supplier->nama_supplier }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr class="tabletitle">
                                        <th class="item" width="10">No</th>
                                        <th class="item">Kode Bahan</th>
                                        <th class="item">Nama Bahan</th>
                                        <th class="item">Harga</th>
                                        <th class="item">PPN</th>
                                        <th class="item">Harga Net</th>
                                        <th class="item">Qty</th>
                                        <th class="item">Sub Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $no = 1;
                                        $harga = 0;
                                        $ppn_nominal = 0;
                                        $harga_net = 0;
                                        $qty = 0;
                                        $sub_total = 0;
                                    @endphp
                                    @foreach($r_po_bahan->po_bahan_detail as $r_po_bahan_detail)
                                        @php
                                            $harga += $r_po_bahan_detail->harga;
                                            $ppn_nominal += $r_po_bahan_detail->ppn_nominal;
                                            $harga_net += $r_po_bahan_detail->harga_net;
                                            $qty += $r_po_bahan_detail->qty;
                                            $sub_total += $r_po_bahan_detail->sub_total;
                                        @endphp
                                        <tr class="service">
                                            <td class="tableitem">
                                                <p class="itemtext">{{ $no++ }}.</p>
                                            </td>
                                            <td class="tableitem">
                                                <p class="itemtext">{{ $r_po_bahan_detail->bahan->kode_bahan }}</p>
                                            </td>
                                            <td class="tableitem">
                                                <p class="itemtext">{{ $r_po_bahan_detail->bahan->nama_bahan }}</p>
                                            </td>
                                            <td class="tableitem" align="right">
                                                <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->harga) }}</p>
                                            </td>
                                            <td class="tableitem" align="right">
                                                <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->ppn_nominal) }}</p>
                                            </td>
                                            <td class="tableitem" align="right">
                                                <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->harga_net) }}</p>
                                            </td>
                                            <td class="tableitem" align="right">
                                                <p class="itemtext">{{ Main::format_number($r_po_bahan_detail->qty) }}</p>
                                            </td>
                                            <td class="tableitem" align="right">
                                                <p class="itemtext">{{ Main::format_money($r_po_bahan_detail->sub_total) }}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td class="tableitem" colspan="3" align="right">
                                            <p class="itemtext">
                                                <strong>Total</strong>
                                            </p>
                                        </td>
                                        <td class="tableitem" align="right">
                                            <p class="itemtext">
                                                <strong>{{ Main::format_money($harga) }}</strong>
                                            </p>
                                        </td>
                                        <td class="tableitem" align="right">
                                            <p class="itemtext">
                                                <strong>{{ Main::format_money($ppn_nominal) }}</strong>
                                            </p>
                                        </td>
                                        <td class="tableitem" align="right">
                                            <p class="itemtext">
                                                <strong>{{ Main::format_money($harga_net) }}</strong>
                                            </p>
                                        </td>
                                        <td class="tableitem" align="right">
                                            <p class="itemtext">
                                                <strong>{{ Main::format_number($qty) }}</strong>
                                            </p>
                                        </td>
                                        <td class="tableitem" align="right">
                                            <p class="itemtext">
                                                <strong>{{ Main::format_money($sub_total) }}</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <br />

                    @endif

                @endforeach
            </div>

        </div>
    </div>
</div>