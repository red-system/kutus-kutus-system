<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=History Transfer Stok Bahan " . date('d-m-Y') . ".xls");
?>

<h3 align="center">History Transfer Stok Bahan</h3>
Tanggal History: {{ $date_start }} s/d {{ $date_end }}
<br />
<table width="100%" border="1">
    <thead>
    <tr class="tabletitle">
        <th class="item" width="20">No</th>
        <th class="item">Tanggal</th>
        <th class="item">Kode Bahan</th>
        <th class="item">Nama Bahan</th>
        <th class="item">Qty Transfer</th>
        <th class="item">Dari Lokasi</th>
        <th class="item">Lokasi Tujuan</th>
        <th class="item">Last Stok</th>
        <th class="item">Penerima</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
        $total = 0;
    @endphp
    @foreach($list as $r)

        @php
            $total += $r->total_diperlukan;
        @endphp

        <tr class="service">
            <td class="tableitem"><p class="itemtext">{{ $no++ }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ Main::format_date($r->tgl) }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->kode_bahan }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->nama_bahan }}</p></td>
            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->qty_transfer) }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->dari }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->tujuan }}</p></td>
            <td class="tableitem" align="right"><p class="itemtext">{{ Main::format_number($r->last_stok) }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ $r->keterangan }}</p></td>

        </tr>
    @endforeach
    </tbody>
</table>