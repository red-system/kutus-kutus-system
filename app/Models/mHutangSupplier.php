<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHutangSupplier extends Model
{
    protected $table = 'tb_hutang_supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'urutan',
        'id_supplier',
        'id_po_bahan',
        'id_faktur_hutang',
        'tipe_faktur_hutang',
        'no_faktur_hutang',
        'tanggal_hutang',
        'js_jatuh_tempo',
        'ps_no_faktur',
        'hs_amount',
        'sisa_amount',
        'hs_keterangan',
        'hs_status',
        'master_id_kode_perkiraan',
        'kode_perkiraan',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name'
    ];

    function supplier() {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }

}
