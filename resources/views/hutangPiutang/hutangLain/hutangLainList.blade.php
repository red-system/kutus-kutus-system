@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/hutang_lain.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('hutangPiutang.hutangLain.hutangLainCreate')
    @include('hutangPiutang.hutangLain.hutangLainEdit')
    @include('hutangPiutang.hutangLain.modalPembayaranCreate')
    @include('hutangPiutang.hutangLain.trPembayaran')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Hutang Lain - Lain</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>No Hutang Lain</th>
                            <th>Jatuh Tempo</th>
                            <th>Supplier</th>
                            <th>Total</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total = 0;
                        @endphp
                        @foreach($list as $r)
                            @php
                                $r['fullname_supplier'] = $r->supplier->kode_supplier.' - '.$r->supplier->nama_supplier;
                                $r['kode_supplier'] = $r->supplier->kode_supplier;
                                $r['nama_supplier'] = $r->supplier->nama_supplier;
                                $r['hl_tanggal'] = Main::format_date($r->hl_tanggal);
                                $r['hl_jatuh_tempo'] = Main::format_date($r->hl_jatuh_tempo);

                                $r['total'] = $r->hl_sisa_amount;
                                $r['sisa_pembayaran'] = $r->hl_sisa_amount;
                                $r['hl_amount'] = $r->hl_amount;
                                $r['hl_sisa_amount'] = $r->hl_sisa_amount;
                                $r['grand-total'] = Main::format_number($r->hl_sisa_amount);
                                $r['sisa-pembayaran'] = Main::format_number($r->hl_sisa_amount);

                                $total += $r->hl_sisa_amount;
                            @endphp
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $r->no_hutang_lain }}</td>
                                <td>{{ Main::format_date($r->hl_jatuh_tempo) }}</td>
                                <td>{{ $r->supplier->kode_supplier.' - '.$r->supplier->nama_supplier }}</td>
                                <td align="right">{{ Main::format_money($r->hl_sisa_amount) }}</td>
                                <td>{{ $r->hl_keterangan }}</td>
                                <td>{!! Main::hutang_lain_status($r->hl_status) !!}</td>
                                <td>
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="dropdown">
                                        <button class="btn btn-accent btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i> Menu
                                        </button>
                                        <div class="dropdown-menu"
                                             aria-labelledby="dropdownMenuButton"
                                             x-placement="bottom-start">
                                            @if($r->hl_amount == $r->sisa_amount)
                                            <a class="dropdown-item btn-edit akses-edit"
                                               data-route="{{ route('hutangLainUpdate', ['id'=>$r->id]) }}"
                                               href="#">
                                                <i class="la la-edit"></i> Edit
                                            </a>
                                            @endif
                                            <a class="dropdown-item btn-pembayaran akses-pembayaran"
                                               data-route="{{ route('hutangLainPembayaran', ['id'=>$r->id]) }}"
                                               href="#">
                                                <i class="la la-money"></i> Pembayaran
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item btn-hapus akses-delete  m--font-danger"
                                               data-route='{{ route('hutangLainDelete', ['id'=>$r->id]) }}'
                                               href="#">
                                                <i class="la la-remove m--font-danger"></i> Hapus
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" align="right">
                                    <strong>Total</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_money($total) }}</strong>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection