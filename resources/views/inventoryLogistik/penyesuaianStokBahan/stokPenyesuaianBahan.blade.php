@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('inventoryLogistik/penyesuaianStokBahan/penyesuaianBahanForm')

        <!-- END: Left Aside -->
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                    <div>
                        <a href="{{ route("penyesuaianStokBahanPage") }}"
                           class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Daftar Bahan</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="m-content">
                <div class="m-portlet m-portlet--tabs">
                    @include('inventoryLogistik/penyesuaianStokBahan/tabTitle')
                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Bahan</th>
                                <th>Nama Bahan</th>
                                <th>Lokasi</th>
                                <th>Qty</th>
                                <th>Keterangan</th>
                                <th width="150">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $r)

                                @php(
                                    $row = [
                                        'id_bahan'=>$r->id_bahan,
                                        'id_kategori_bahan'=>$bahan->id_kategori_bahan,
                                        'id_lokasi'=>$r->id_lokasi,
                                        'kode_bahan'=>$bahan->kode_bahan,
                                        'nama_bahan'=>$bahan->nama_bahan,
                                        'kategori_bahan'=>$bahan->kategori_bahan->kategori_bahan,
                                        'lokasi'=>$r->lokasi->lokasi,
                                        'keterangan'=>$r->keterangan,
                                        'qty_awal'=>$r->qty,
                                        'qty'=>$r->qty
                                    ]
                                )
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->bahan->kode_bahan }}</td>
                                <td>{{ $r->bahan->nama_bahan }}</td>
                                <td>{{ $r->lokasi->lokasi }}</td>
                                <td>{{ Main::format_number($r->qty) }}</td>
                                <td>{{ $r->keterangan }}</td>
                                <td align="center">
                                    <textarea class="row-data hidden">@json($row)</textarea>
                                    <div>
                                        <button type="button"
                                                data-route="{{ route('penyesuaianStokBahanUpdate', ['id'=>$r->id]) }}"
                                                class="m-btn btn btn-primary m-btn--pill btn-sm btn-edit">
                                            <i class="la la-dropbox"></i> Sesuaikan Stok Bahan
                                        </button>
                                    </div>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

        </div>
@endsection