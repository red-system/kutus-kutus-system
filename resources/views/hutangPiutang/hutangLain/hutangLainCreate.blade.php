<form action="{{ route('hutangLainInsert') }}"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}

    <input type="hidden" name="no_hutang_lain" value="{{ $no_transaksi }}">

    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header btn-success">
                    <h5 class="modal-title m--font-light" id="exampleModalLabel">Tambah Hutang Lain - Lain</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <h4>Data Hutang Lain - Lain</h4>
                        <hr />
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Judul Hutang Lain Lain</label>
                            <div class="col-lg-8">
                                <textarea class="form-control m-input" name="hl_keterangan"></textarea>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Tanggal Hutang Lain - Lain</label>
                            <div class="col-lg-8">
                                <input type="text"
                                       class="form-control m-input m_datepicker"
                                       name="hl_tanggal"
                                       autocomplete="off"
                                       value="{{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No Faktur Hutang</label>
                            <div class="col-lg-8 col-form-label">
                                <span class="no_faktur_hutang">{{ $no_transaksi }}</span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Tanggal Jatuh Tempo</label>
                            <div class="col-lg-8">
                                <input type="text"
                                       class="form-control m-input m_datepicker"
                                       name="hl_jatuh_tempo"
                                       autocomplete="off"
                                       value="{{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Nama Supplier</label>
                            <div class="col-lg-8 col-form-label">
                                <select class="form-control m-select2" name="id_supplier" style="width: 100%">
                                    @foreach($supplier as $r)
                                        <option value="{{ $r->id }}">{{ $r->kode_supplier.' - '.$r->nama_supplier }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Nominal Hutang Lain - Lain</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control m-input touchspin-number-decimal" name="hl_amount" value="0">
                            </div>
                        </div>
                        <br />
                        <h4>Data Akunting</h4>
                        <hr />

                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Akun Debet</label>
                            <div class="col-lg-8 col-form-label">
                                <select class="form-control m-select2" name="master_id_debet" style="width: 100%">
                                    @foreach($master_kredit as $r)
                                        <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label required">Akun Kredit</label>
                            <div class="col-lg-8 col-form-label">
                                <select class="form-control m-select2" name="master_id_kredit" style="width: 100%">
                                    @foreach($master_debet as $r)
                                        <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>