<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mDistributor;
use app\Models\mPenjualan;
use app\Models\mPiutangPelanggan;
use app\Models\mPiutangPelangganPembayaran;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use app\Models\mAcMaster;

use app\Models\mHutangSupplier;
use app\Models\mAcJurnalUmum;
use app\Models\mAcTransaksi;
use app\Models\mHutangCek;
use app\Models\mCheques;
use PDF;

use Illuminate\Support\Facades\Session;

class PiutangPelanggan extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['hutang_1'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['hutangPiutang'],
                'route' => ''
            ],
            [
                'label' => $cons['hutang_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $kode = Config::get('constants.kodePiutangPelanggan');
        $no_transaksi = hAkunting::getNoTransaksiPayment($kode);
        $master = mAcMaster
            ::where('mst_pembayaran', 'yes')
            ->get();
        $list = mPiutangPelanggan
            ::with('distributor')
            ->where([
                'pp_status' => 'belum_bayar'
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'tab' => 'piutang_langsung',
            'no_transaksi' => $no_transaksi,
            'master' => $master,
            'list' => $list,
        ]);

        return view('hutangPiutang/piutangPelanggan/piutangPelangganList', $data);
    }

    function recycle()
    {
        $data = mPiutangPelanggan::all();
        foreach ($data as $r) {
            $master_id = mAcMaster
                ::where('mst_kode_rekening', $r->kode_perkiraan)
                ->value('master_id');
            $data_update = [
                'master_id_kode_perkiraan' => $master_id
            ];

            //mPiutangPelanggan::where('id', $r->id)->update($data_update);
        }
    }

    function update(Request $request, $id)
    {

        $request->validate([
            'tanggal_hutang' => 'required',
            'js_jatuh_tempo' => 'required',
            'hs_amount' => 'required'
        ]);

        $data = $request->except(['_token']);
        $data['tanggal_hutang'] = date('Y-m-d', strtotime($data['tanggal_hutang']));
        $data['js_jatuh_tempo'] = date('Y-m-d', strtotime($data['js_jatuh_tempo']));

        mHutangSupplier::where('id', $id)->update($data);
    }

    function pembayaran_insert(Request $request, $id)
    {
        $rules = [
            'master_id' => 'required',
            'master_id.*' => 'required',
            'no_bg' => 'required',
            'no_bg.*' => 'required'
        ];

        $attributes = [
            'master_id' => 'Kode Perkiraan',
            'no_bg' => 'No. Check / BG',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
            $attr['no_bg.' . $i] = 'No. Check / BG ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        DB::beginTransaction();
        try {

            $id_piutang_pelanggan = $id;
            $total = $request->input('total');
            $terbayar = $request->input('terbayar');
            $sisa_pembayaran = $request->input('sisa_pembayaran');
            $kembalian = $request->input('kembalian');
            $no_transaksi = $request->input('no_transaksi');
            $no_piutang_pelanggan = $request->input('no_piutang_pelanggan');
            $id_distributor = $request->input('id_distributor');
            $kode_distributor = $request->input('kode_distributor');
            $nama_distributor = $request->input('nama_distributor');
            /*            $master_id_kode_perkiraan = $request->input('master_id_kode_perkiraan');
                        $kode_perkiraan = $request->input('kode_perkiraan');*/
            $pp_no_faktur = $request->input('pp_no_faktur');
            /*            $hs_amount = $request->input('hs_amount');
                        $hs_sisa_amount = $request->input('hs_sisa_amount');*/
            $tanggal_transaksi = $request->input('tanggal_transaksi');
            $master_id_arr = $request->input('master_id');
            $master_id_debet_arr = $master_id_arr;
            $master_id_kredit_arr = [36];
            $jumlah_arr = $request->input('jumlah');
            $no_bg_arr = $request->input('no_bg');
            $tanggal_pencairan_arr = $request->input('tanggal_pencairan');
            $tipe_arus_kas = 'Operasi';
            $nama_bank_arr = $request->input('nama_bank');
            $keterangan_arr = $request->input('keterangan');
            $year = date('Y', strtotime($tanggal_transaksi));
            $month = date('m', strtotime($tanggal_transaksi));
            $day = date('d', strtotime($tanggal_transaksi));
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $jmu_keterangan = 'Penerimaan Piutang Pelanggan : ' . $kode_distributor . '-' . $nama_distributor . ' No Invoice Piutang : ' . $no_piutang_pelanggan;
            $trs_kredit = 0;


            /**
             * Untuk update apakah piutang pelanggan sudah lunas atau belum
             */
            foreach ($master_id_arr as $key => $master_id) {
                $jumlah = $jumlah_arr[$key];
                $no_bg = $no_bg_arr[$key];
                $nama_bank = $nama_bank_arr[$key];
                $tanggal_pencairan = $tanggal_pencairan_arr[$key];
                $keterangan = $keterangan_arr[$key];

                $master = mAcMaster::where('master_id', $master_id)->first();
                $trs_kode_rekening = $master->mst_kode_rekening;

                /**
                 * Update Hutang Supplier sudah lunas atau belum
                 */

                $sisa_amount = mPiutangPelanggan::where('id', $id_piutang_pelanggan)->value('pp_sisa_amount');
                $sisa_amount -= $jumlah_arr[$key];
                if ($sisa_amount == 0) {
                    mPiutangPelanggan
                        ::where('id', $id_piutang_pelanggan)
                        ->update([
                            'pp_sisa_amount' => $sisa_amount,
                            'pp_status' => 'lunas'
                        ]);
                } else {
                    mPiutangPelanggan
                        ::where('id', $id_piutang_pelanggan)
                        ->update([
                            'pp_sisa_amount' => $sisa_amount
                        ]);
                }

                if ($trs_kode_rekening == '1303') {
                    $data_cheques = [
                        'no_bg_cek' => $no_bg,
                        'no_invoice' => $no_piutang_pelanggan,
                        'tgl_cek' => Main::format_date_db($tanggal_transaksi),
                        'tgl_pencairan' => Main::format_date_db($tanggal_pencairan),
                        'cek_amount' => $jumlah,
                        'sisa' => $jumlah,
                        'cek_dari' => $id_distributor,
                        'cek_dari_table' => 'tb_distributor',
                        'cek_keterangan' => $keterangan
                    ];

                    mCheques::create($data_cheques);
                }

                $data_piutang_pelanggan_pembayaran = [
                    'id_piutang_pelanggan' => $id_piutang_pelanggan,
                    'master_id' => $master_id,
                    'jumlah' => $jumlah,
                    'no_check_bg' => $no_bg,
                    'tanggal_pencairan' => Main::format_date_db($tanggal_pencairan),
                    'nama_bank' => $nama_bank,
                    'keterangan' => $keterangan
                ];

                mPiutangPelangganPembayaran::create($data_piutang_pelanggan_pembayaran);

            }


            /**
             * Begin Akunting
             */


            /**
             * Create Jurnal Umum
             */
            $data_jurnal = [
                'no_invoice' => $no_transaksi,
                'id_distributor' => $id_distributor,
                'id_piutang_pelanggan' => $id_piutang_pelanggan,
                'jmu_tanggal' => date('Y-m-d', strtotime($tanggal_transaksi)),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
                'reference_number' => $no_piutang_pelanggan
            ];

            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            /**
             * Create Transaksi Debet
             */
            foreach ($master_id_debet_arr as $key => $master_id) {
                $master = mAcMaster::where('master_id', $master_id)->first();
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_debet = $jumlah_arr[$key];
                $trs_catatan = $keterangan_arr[$key];
                $trs_no_check_bg = $no_bg_arr[$key];
                $trs_setor = $jumlah_arr[$key];
                $trs_kredit += $jumlah_arr[$key];


                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    /*                    'table_id' => $jurnal_umum_id,
                                        'table_name' => 'tb_ac_jurnal_umum',*/
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'trs_no_check_bg' => $trs_no_check_bg,
                    'trs_setor' => $trs_setor,
                    'trs_tgl_pencairan' => Main::format_date_db($tanggal_pencairan_arr[$key]),
                    'tgl_transaksi' => Main::format_date_db($tanggal_transaksi)
                ];

                mAcTransaksi::create($data_transaksi);
            }

            /**
             * Create Transaksi Kredit
             */
            foreach ($master_id_kredit_arr as $key => $master_id) {
                $master = mAcMaster::where('master_id', $master_id)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                $data_transaksi_hutang = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    /*                'table_id' => $jurnal_umum_id,
                                    'table_name' => 'tb_ac_jurnal_umum',*/
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet' => 0,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => "Penerimaan Piutang",
                    'trs_charge' => 0,
                    'trs_no_check_bg' => 0,
                    'trs_tgl_pencairan' => date('Y-m-d'),
                    'trs_setor' => $trs_kredit,
                    'tgl_transaksi' => date('Y-m-d', strtotime($tanggal_transaksi))
                ];

                mAcTransaksi::create($data_transaksi_hutang);
            }

            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }


    function pdf($id)
    {
        $id = Main::decrypt($id);

        $company = Main::companyInfo();
        $piutang_pelanggan = mPiutangPelanggan::where('id', $id)->first();
        $penjualan = mPenjualan
            ::with([
                'penjualan_produk.produk',
                'distributor'
            ])
            ->where('id', $piutang_pelanggan->id_penjualan)
            ->first();

        $data = [
            'company' => $company,
            'piutang_pelanggan' => $piutang_pelanggan,
            'penjualan' => $penjualan,
            'no' => 1
        ];

        $pdf = PDF::loadView('hutangPiutang.piutangPelanggan.piutangPelangganPdf', $data);
        //return $pdf->stream();
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Piutang Pelanggan ' . $piutang_pelanggan->no_piutang_pelanggan . '.pdf');
    }

    function delete($id_piutang_pelanggan)
    {
        DB::beginTransaction();
        try {

            $piutang_pelanggan = mPiutangPelanggan
                ::where('id', $id_piutang_pelanggan)
                ->first(['pp_amount', 'pp_sisa_amount', 'id_penjualan']);
            $pp_amount = $piutang_pelanggan->pp_amount;
            $pp_sisa_amount = $piutang_pelanggan->pp_sisa_amount;
            $id_penjualan = $piutang_pelanggan->id_penjualan;

            if ($pp_amount != $pp_sisa_amount) {
                return response([
                    'message' => 'Piutang Pelanggan ini tidak bisa dihapus.
                                  <br />
                                  Karena sudah melakukan Pembayaran/Penerimaan sebelumnya.'
                ], 422);
            } else {
                /**
                 * Check, apakah penjualan produk yang berkaitan dengan piutang pelanggan ini, sudah dihapus atau belum
                 */
                $check_penjualan_produk = mPenjualan::where('id', $id_penjualan)->count();
                if ($check_penjualan_produk > 0) {
                    return response([
                        'message' => 'Piutang Pelanggan ini tidak bisa dihapus.
                                 <br />
                                 Hapus terlebih dahulu Penjualan atau Transaksi lain yang berkaitan dengan Piutang Pelanggan ini.'
                    ], 422);
                } else {
                    /**
                     * Delete data piutang dan jurnal umum
                     */
                    $jurnal_umum_id = mAcJurnalUmum
                        ::where('id_piutang_pelanggan', $id_piutang_pelanggan)
                        ->value('jurnal_umum');
                    mPiutangPelanggan
                        ::where('id', $id_piutang_pelanggan)
                        ->delete();
                    mPiutangPelangganPembayaran
                        ::where('id_piutang_pelanggan', $id_piutang_pelanggan)
                        ->delete();
                    mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();
                    mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->delete();

                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }
}
