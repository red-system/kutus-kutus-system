<?php

namespace app\Http\Controllers\produksi;

use app\Helpers\Main;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Rules\TargetProduksiInsert;
use app\Rules\TargetProduksiUpdate;


use app\Models\mTargetProduksi;
use app\Models\mProduk;

class TargetProduksi extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $const;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['produksi_2'];
        $this->cons = $cons;
        $this->breadcrumb = [
            [
                'label' => $cons['produksi'],
                'route' => ''
            ],
            [
                'label' => $cons['produksi_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['produk'] = mProduk::orderBy('kode_produk', 'ASC')->get();
        $data['list'] = mTargetProduksi::distinct()->orderBy('tahun_target','DESC')->get(['tahun_target']);

        return view('produksi/targetProduksi/targetProduksiList', $data);
    }

    function tahun($tahun)
    {
        $tahun = Main::decrypt($tahun);
        $list = mTargetProduksi
            ::with('produk')
            ->where('tahun_target', $tahun)
            ->orderBy('id', 'DESC')
            ->get();

        $dataMain = Main::data($this->breadcrumb, $this->menuActive);
        $produk = mProduk::orderBy('kode_produk', 'ASC')->get();
        $pageTitle = $this->cons['produksi_2'].' Tahun '.$tahun;

        $data = [
            'list' => $list,
            'produk' => $produk,
            'tahun' => $tahun,
            'pageTitle'=>$pageTitle
        ];

        $data = array_merge($dataMain, $data);

        return view('produksi/targetProduksi/targetProduksiTahun', $data);
    }

    function insertOne(Request $request)
    {

        $tgl_target = $request->tgl_target;
        $date = explode('-', $tgl_target);
        $bulan_target = $date[0];
        $tahun_target = $date[1];


        $request->validate([
            'id_produk' => ['required', new TargetProduksiInsert($bulan_target, $tahun_target)],
            'tgl_target' => 'required',
            'qty' => 'required'
        ]);

        $data = $request->except('_token');
        $data['bulan_target'] = $bulan_target;
        $data['tahun_target'] = $tahun_target;

        mTargetProduksi::create($data);
    }

    function insertMonth(Request $request)
    {
        $bulan_target = $request->bulan_target;
        $tahun_target = $request->tahun_target;

        $request->validate([
            'id_produk' => ['required', new TargetProduksiInsert($bulan_target, $tahun_target)],
            'bulan_target' => 'required',
            'qty' => 'required'
        ]);

        $data = $request->except('_token');
        $data['bulan_target'] = $bulan_target;
        $data['tahun_target'] = $tahun_target;

        mTargetProduksi::create($data);
    }
    function update(Request $request, $id) {
        $bulan_target = $request->bulan_target;
        $tahun_target = $request->tahun_target;

        $request->validate([
            'id_produk' => ['required', new TargetProduksiUpdate($id, $bulan_target, $tahun_target)],
            'bulan_target' => 'required',
            'qty' => 'required'
        ]);

        $data = $request->except('_token');

        mTargetProduksi::where('id',$id)->update($data);
    }

    function delete($id) {
        mTargetProduksi::where('id', $id)->delete();
    }





    function insertMany(Request $request)
    {
        $tahun_target = $request->tahun_target;
        foreach ($request->bulan_target as $id_produk => $bulan_target) {
            $check = mTargetProduksi::where([
                'id_produk' => $id_produk,
                'bulan_target' => $bulan_target
            ])->count();

            if ($check == 0) {
                $data_insert = [
                    'id_produk' => $id_produk,
                    'tgl_target' => $bulan_target . '-' . $tahun_target,
                    'bulan_target' => $bulan_target,
                    'tahun_target' => $tahun_target,
                    'qty' => $request->qty[$id_produk]
                ];
                mTargetProduksi::create($data_insert);
            } else {
                $data_update = [
                    'tgl_target' => $bulan_target . '-' . $tahun_target,
                    'bulan_target' => $bulan_target,
                    'tahun_target' => $tahun_target,
                    'qty' => $request->qty[$id_produk]
                ];

                mTargetProduksi::where([
                    'id_produk' => $id_produk,
                    'bulan_target' => $bulan_target,
                    'tahun_target' => $tahun_target
                ])
                    ->update($data_update);
            }
        }

    }

}
