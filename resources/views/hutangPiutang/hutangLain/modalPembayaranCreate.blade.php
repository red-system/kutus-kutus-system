<div class="modal" id="modal-pembayaran" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form action=""
          method="post"
          class="form-send"
          data-alert-show="true"
          data-alert-field-message="true">

        {{ csrf_field() }}

        <input type="hidden" name="total">
        <input type="hidden" name="terbayar" value="0">
        <input type="hidden" name="sisa_pembayaran" value="">
        <input type="hidden" name="kembalian">
        <input type="hidden" name="no_transaksi" value="{{ $no_transaksi }}">
        <input type="hidden" name="no_hutang_lain">
        <input type="hidden" name="id_supplier">
        <input type="hidden" name="kode_supplier">
        <input type="hidden" name="nama_supplier">
        <input type="hidden" name="master_id_kredit">
        <input type="hidden" name="master_id_debet">
{{--        <input type="hidden" name="ps_no_faktur">--}}
{{--        <input type="hidden" name="hs_amount">
        <input type="hidden" name="hs_sisa_amount">--}}

        <div class="modal-dialog modal-xxlg" role="document">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <h3 class="modal-title m--font-light" id="exampleModalLabel">PEMBAYARAN</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
                                <div class="col-9">
                                    <input type="text" value="{{ date('d-m-Y') }}" class="form-control m-input m_datepicker" name="tanggal_transaksi">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Transaksi</label>
                                <div class="col-9 col-form-label">
                                    <span class="no_transaksi">{{ $no_transaksi }}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Hutang</label>
                                <div class="col-9 col-form-label">
                                    <span class="no_hutang_lain"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Supplier</label>
                                <div class="col-9 col-form-label">
                                    <span class="fullname_supplier"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr/>
                    <button type="button" class="btn-add-row-pembayaran btn btn-accent btn-sm m-btn--pill">
                        <i class="la la-plus"></i> Tambah Pembayaran
                    </button>
                    <br/><br/>
                    <table class="table-pembayaran table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Kode Perkiraan</th>
                            <th>Jumlah Pembayaran</th>
                            <th>No. Check / BG</th>
                            <th>Tanggal Pencairan</th>
                            <th>Nama Bank</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br/>
                    <hr/>
                    <div class="form-no-padding">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">
                                <h3>Total</h3>
                            </label>
                            <div class="col-9">
                                <h3 class="grand-total">0</h3>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">
                                <h3>Terbayar</h3>
                            </label>
                            <div class="col-9">
                                <h3 class="terbayar">0</h3>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">
                                <h3>Sisa Pembayaran</h3>
                            </label>
                            <div class="col-9">
                                <h3 class="sisa-pembayaran">0</h3>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">
                                <h3>Kembalian</h3>
                            </label>
                            <div class="col-9">
                                <h3 class="kembalian">0</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="la la-check"></i> Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </form>
</div>