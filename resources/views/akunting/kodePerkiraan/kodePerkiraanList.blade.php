@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('akunting/kodePerkiraan/modalPerkiraanCreate')
    @include('akunting/kodePerkiraan/modalPerkiraanEdit')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <button data-toggle="modal"
                            href="#modal-create"
                            type="button"
                            class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data Perkiraan</span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-print"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Cetak Data
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot text-center">
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                        <a href="{{ route('perkiraanPdf') }}"
                           class="btn btn-danger akses-pdf">
                            <i class="la la-file-pdf-o"></i> Print PDF
                        </a>
                        <a href="{{ route('perkiraanExcel') }}"
                           class="btn btn-success akses-excel">
                            <i class="la la-file-excel-o"></i> Print Excel
                        </a>
                    </div>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-header-fixed datatable-no-pagination">
                        <thead>
                        <tr class="">
                            <th width="150"> Kode Rekening</th>
                            <th> Nama Rekening</th>
                            <th> Posisi</th>
                            <th> Normal</th>
                            <th width="40"> Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($perkiraan as $row)
                            <tr>
                                <td class="font-weight-bold">{{ $row['mst_kode_rekening'] }}</td>
                                <td class="font-weight-bold">{{ $row['mst_nama_rekening'] }}</td>
                                <td> {{ Main::menuAction($row['mst_posisi']) }} </td>
                                <td> {{ Main::menuAction($row['mst_normal']) }} </td>
                                <td>
                                    <textarea class="row-data hidden">@json($row)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm akses-edit">
                                        <button class="btn btn-sm btn-success btn-edit"
                                                data-route="{{ route('perkiraanEdit', ['kode'=>$row->master_id]) }}">
                                            <i class="la la-edit"></i> Edit
                                        </button>
                                        <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                data-route="{{ route('perkiraanDelete', ['kode'=>$row->master_id]) }}">
                                            <i class="la la-remove"></i> Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @foreach($row['sub1'] as $row1)
                                <tr>
                                    <td {{ count($row1['sub2']) > 0 ? 'class=font-weight-bold':'' }}>
                                        {{ $row1['mst_kode_rekening'] }}
                                    </td>
                                    <td {{ count($row1['sub2']) > 0 ? 'class=font-weight-bold':'' }}>
                                        {!! $space1.$row1['mst_nama_rekening'] !!}
                                    </td>
                                    <td> {{ Main::menuAction($row1['mst_posisi']) }} </td>
                                    <td> {{ Main::menuAction($row1['mst_normal']) }} </td>
                                    <td>
                                        <textarea class="row-data hidden">@json($row1)</textarea>
                                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                            <button class="btn btn-sm btn-success btn-edit akses-edit"
                                                    data-route="{{ route('perkiraanEdit', ['kode'=>$row1->master_id]) }}">
                                                <i class="la la-edit"></i> Edit
                                            </button>
                                            <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                    data-route="{{ route('perkiraanDelete', ['kode'=>$row1->master_id]) }}">
                                                <i class="la la-remove"></i> Delete
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @foreach($row1['sub2'] as $row2)
                                    <tr>
                                        <td {{ count($row2['sub3']) > 0 ? 'class=font-weight-bold':'' }}>
                                            {{ $row2['mst_kode_rekening'] }}
                                        </td>
                                        <td {{ count($row2['sub3']) > 0 ? 'class=font-weight-bold':'' }}>
                                            {!! $space2.$row2['mst_nama_rekening'] !!}
                                        </td>
                                        <td> {{ Main::menuAction($row2['mst_posisi']) }} </td>
                                        <td> {{ Main::menuAction($row2['mst_normal']) }} </td>
                                        <td>
                                            <textarea class="row-data hidden">@json($row2)</textarea>
                                            <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                                <button class="btn btn-sm btn-success btn-edit akses-edit"
                                                        data-route="{{ route('perkiraanEdit', ['kode'=>$row2->master_id]) }}">
                                                    <i class="la la-edit"></i> Edit
                                                </button>
                                                <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                        data-route="{{ route('perkiraanDelete', ['kode'=>$row2->master_id]) }}">
                                                    <i class="la la-remove"></i> Delete
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @foreach($row2['sub3'] as $row3)
                                        <tr>
                                            <td> {{ $row3['mst_kode_rekening'] }} </td>
                                            <td> {!! $space3.$row3['mst_nama_rekening'] !!} </td>
                                            <td> {{ Main::menuAction($row3['mst_posisi']) }} </td>
                                            <td> {{ Main::menuAction($row3['mst_normal']) }} </td>
                                            <td>
                                                <textarea class="row-data hidden">@json($row3)</textarea>
                                                <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                                    <button class="btn btn-sm btn-success btn-edit akses-edit"
                                                            data-route="{{ route('perkiraanEdit', ['kode'=>$row3->master_id]) }}">
                                                        <i class="la la-edit"></i> Edit
                                                    </button>
                                                    <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                            data-route="{{ route('perkiraanDelete', ['kode'=>$row3->master_id]) }}">
                                                        <i class="la la-remove"></i> Delete
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection