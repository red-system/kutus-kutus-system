<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mEkspedisi extends Model
{
    protected $table = 'tb_ekspedisi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_ekspedisi',
        'nama_ekspedisi'
    ];
}
