<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            @if($penjualan->status_distribusi == 'not_yet')
                <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link {{ $tab == 'distribusi' ? 'active':''  }}"
                       href="{{ route('distribusi', ['id'=>Main::encrypt($id_penjualan)]) }}">
                        <i class="la la-refresh"></i> Distribusi Produk
                    </a>
                </li>
            @endif
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'history' ? 'active':''  }}"
                   href="{{ route('distribusiHistory', ['id'=>Main::encrypt($id_penjualan)]) }}">
                    <i class="la la-history"></i> Distribusi History
                </a>
            </li>
        </ul>
    </div>
</div>