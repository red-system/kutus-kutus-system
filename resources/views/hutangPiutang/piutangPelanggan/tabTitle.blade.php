<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'piutang_langsung' ? 'active':''  }}" href="{{ route('piutangLangsungPage') }}">
                    <i class="la la-angle-double-right"></i> Piutang Langsung
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'piutang_titipan' ? 'active':''  }}" href="{{ route('piutangTitipanPage') }}">
                    <i class="la la-angle-double-down"></i> Piutang Titipan
                </a>
            </li>
        </ul>
    </div>
</div>