<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'list' ? 'active':''  }}" href="{{ route('orderOnlinePage') }}">
                    <i class="la la-plus"></i> Order Baru
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'konfirmasi' ? 'active':''  }}" href="{{ route('orderOnlineKonfirmasi') }}">
                    <i class="la la-check"></i> Order Terkonfirmasi
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'history' ? 'active':''  }}" href="{{ route('orderOnlineHistory') }}">
                    <i class="la la-history"></i> History Order
                </a>
            </li>
        </ul>
    </div>
</div>