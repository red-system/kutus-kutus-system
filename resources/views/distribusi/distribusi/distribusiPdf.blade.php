<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
                <p>{{ $company->companyEmail }}</p>
            </div><!--End Info-->
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td><h1>SURAT JALAN</h1></td>
                    </tr>
                </table>
            </div>
        </div>
        <br /><br /><br /><br /><br /><br />
        <div id="invoice-bot">
            <table width="100%" class="table-header">
                <tbody>
                <tr>
                    <td width="50%">Tanggal : {{ Main::format_date($distribusi->tanggal_distribusi) }}</td>
                    <td width="50%" align="center">DIKIRIM KEPADA</td>
                </tr>
                <tr>
                    <td>No. Surat Jalan : {{ $distribusi->no_distribusi }}</td>
                    <td rowspan="3">
                        {{ '('.$distribusi->penjualan->distributor->kode_distributor.') '.$distribusi->penjualan->distributor->nama_distributor }}<br />
                        @if($distribusi->alamat_lain)
                            {{ $distribusi->alamat_lain }}
                        @else
                            {{ $distribusi->penjualan->distributor->alamat_distributor }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>No. PO/Invoice : {{ $distribusi->penjualan->no_faktur }}</td>
                </tr>
                <tr>
                    <td>No. Mobil : {{ $distribusi->no_polisi }}</td>
                </tr>
                </tbody>
            </table>
            <br/>
            <div id="table">
                <table>
                    <tr class="tabletitle">
                        <td class="no"><h2>No</h2></td>
                        <td class="item"><h2>Item</h2></td>
                        <td class="item"><h2>Unit</h2></td>
                        <td class="item"><h2>Qty</h2></td>
                        <td class="item"><h2>Net Weight</h2></td>
                        <td class="item"><h2>U/Price (IDR)</h2></td>
                        <td class="item"><h2>Remark</h2></td>
                    </tr>
                    @foreach($distribusi_detail as $r)
                        <tr class="service">
                            <td class="tableitem"><p class="itemtext">{{ $no++ }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->penjualan_produk->produk->nama_produk }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->penjualan_produk->produk->kategori_produk->kategori_produk }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_number($r->qty_dikirim) }}</p></td>
                            <td class="tableitem"><p class="itemtext"></p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_number($r->penjualan_produk->sub_total) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->remark }}</p></td>
                        </tr>
                    @endforeach
                </table>

            </div>
            <br/><br/>

            <table width="100%" class="table-ttd">
                <tbody>
                <tr>
                    <td rowspan="2" align="center">
                        Dikirim Oleh<br/><br/><br/><br/><br/>

                        (..............................)
                        <br/>
                        <i>Driver</i>
                    </td>
                    <td colspan="2" align="center">Diperiksa Oleh</td>
                    <td rowspan="2" align="center">
                        Management<br/><br/><br/><br/><br/>

                        (..............................)
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <br/><br/><br/><br/>

                        (..............................)<br/>
                        <i>Receiving/Stockis</i>
                    </td>
                    <td align="center">
                        <br/><br/><br/><br/>

                        (..............................)<br/>
                        <span style="color:white">1</span>
                    </td>
                </tr>
                </tbody>
            </table>

        </div><!--End InvoiceBot-->
    </div><!--End Invoice-->
</div><!-- End Invoice Holder-->