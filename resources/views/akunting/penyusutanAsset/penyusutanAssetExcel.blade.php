<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Penyusutan Asset " . $tahun . ".xls");
?>

<h3 align="center">PENYUSUTAN ASSET {{ $tahun }}</h3>
<br/>
<table width="100%" border="1">
    <thead>
    <tr class="success">
        <th rowspan="2" align="center"> No</th>
        <th rowspan="2"> Keterangan</th>
        <th rowspan="2" width="5%" class="text-center">Jumlah</th>
        <th rowspan="2" width="5%"> Tahun Perolehan</th>
        <th rowspan="2" width="5%"> Nilai Perolehan</th>
        <th rowspan="2" width="5%"> Nilai Penyusutan</th>
        <th colspan="13" class="text-center"> Penyusutan {{ $tahun }}</th>
        <th rowspan="2" width="5%"> Akumulasi Penyusutan</th>
        <th rowspan="2" width="5%"> Nilai Buku</th>
    </tr>
    <tr class="info">
        <th>Thn Lalu</th>
        <th>Jan</th>
        <th>Feb</th>
        <th>Mar</th>
        <th>Apr</th>
        <th>Mei</th>
        <th>Jun</th>
        <th>Jul</th>
        <th>Ags</th>
        <th>Sep</th>
        <th>Okt</th>
        <th>Nov</th>
        <th>Des</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $ktgAsset)
        <tr>
            <td><strong>{{$no++}}</strong></td>
            <td colspan="20"><strong>{{$ktgAsset->nama}}</strong></td>
        </tr>
        @foreach($ktgAsset->asset as $asset)
            @php
                $sum_penyusutan_perbulan = $asset
                ->penyusutanAsset
                ->where('tahun', '<', $tahun)
                ->sum('penyusutan_perbulan')
            @endphp
            <tr>
                <td></td>
                <td>{{$asset->nama}}</td>
                <td class="text-center">{{ Main::format_number($asset->qty) }}</td>
                <td class="text-right">{{ date('M Y', strtotime($asset->tanggal_beli)) }}</td>
                <td class="text-right">{{ Main::format_number($asset->harga_beli) }}</td>
                <td class="text-right">{{ Main::format_number($asset->beban_perbulan) }} </td>
                <td class="text-right">{{ Main::format_number($sum_penyusutan_perbulan) }}</td>
                @for($i=1;$i<=12;$i++)
                    @php
                        $sum_penyusutan_perbulan = $asset
                        ->penyusutanAsset
                        ->where('bulan', '=', $i)
                        ->where('tahun', '=', $tahun)
                        ->sum('penyusutan_perbulan');
                    @endphp
                    <td class="text-right">{{ Main::format_number($sum_penyusutan_perbulan) }}</td>
                @endfor
                <td class="text-right"> {{ Main::format_number($asset->akumulasi_beban) }} </td>
                <td class="text-right"> {{ Main::format_number($asset->nilai_buku) }} </td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>