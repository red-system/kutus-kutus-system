@extends('../general/index')

@section('js')
    <script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-daterangepicker.js') }}"
            type="text/javascript"></script>

    <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"
            type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>



    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var amChartsChartsDemo = function () {
                return {
                    init: function () {
                        AmCharts.makeChart("m_amcharts_4", {
                                theme: "light",
                                type: "serial",
                                dataProvider: [
                                        @foreach($data_penjualan as $r)
                                    {
                                        month: "{{ $r['month'] }}",
                                        total_penjualan: "{{ $r['total_penjualan'] }}",
                                    },
                                    @endforeach
                                ], valueAxes: [{
                                    stackType: "3d", unit: "", position: "left", title: "Total Penjualan Produk"
                                }
                                ], startDuration: 1, graphs: [{
                                    balloonText: "GDP grow in [[category]] (2004): <b>[[value]]</b>",
                                    fillAlphas: .9,
                                    lineAlpha: .2,
                                    title: "2004",
                                    type: "column",
                                    valueField: "total_penjualan"
                                }
                                ], plotAreaFillAlphas: .1, depth3D: 60, angle: 30, categoryField: "month", categoryAxis: {
                                    gridPosition: "start"
                                }
                                , export: {
                                    enabled: !0
                                }
                            }
                        )
                    }
                }
            }

            ();
            jQuery(document).ready(function () {
                    amChartsChartsDemo.init()
                }
            );
        });
    </script>
@endsection

@section('css')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css"/>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-3 col-sm-12">Rentang Tanggal Data</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control"
                                       id="m_daterangepicker_1"
                                       readonly
                                       placeholder="Select time"
                                       type="text"
                                       name="date_range"
                                       value="{{ $date_start.' - '.$date_end }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <button type="submit" class="btn btn-accent m-btn--pill btn-sm">
                            <i class="la la-search"></i> View Data
                        </button>
                    </div>
                </form>
            </div>

            <div class="row">
                <div class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height  m-portlet--rounded-force">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light">
                                        Total Nominal Transaksi
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('assets/app/media/img//bg/bg-1.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span><span>Rp.</span>{{ $total_nominal_transaksi }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height  m-portlet--rounded-force">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light">
                                        Gross Profit
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('assets/app/media/img//bg/bg-2.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span><span>Rp.</span>{{ $gross_profit }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height  m-portlet--rounded-force">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light">
                                        Net Profit
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('assets/app/media/img//bg/bg-7.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span><span>Rp.</span>{{ $net_profit }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height  m-portlet--rounded-force">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light">
                                        Total Data Transaksi
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('assets/app/media/img//bg/bg-4.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span><span></span>{{ $total_data_transaksi }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Daftar Produk Terjual dan Produksi
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <table class="table m-table table-bordered m-table--head-bg-success datatable-general">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Total Penjualan</th>
                                    <th>Total Produksi</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($produk as $r)

                                    @php
                                        $penjualan_produk_count += $r->total_penjualan;
                                        $detail_produksi_count += $r->total_produksi;
                                    @endphp
                                    <tr>
                                        <th scope="row">{{ $no++ }}.</th>
                                        <td>{{ $r->kode_produk }}</td>
                                        <td>{{ $r->nama_produk }}</td>
                                        <td class="text-right">{{ Main::format_number($r->total_penjualan) }}</td>
                                        <td class="text-right">{{ Main::format_number($r->total_produksi) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="3" class="text-right">
                                        <strong>Total</strong>
                                    </th>
                                    <th class="text-right">{{ Main::format_number($penjualan_produk_count) }}</th>
                                    <th class="text-right">{{ Main::format_number($detail_produksi_count) }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Daftar Penggunaan Bahan
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <table class="table m-table table-bordered m-table--head-bg-success datatable-general">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode Bahan</th>
                                    <th>Nama Bahan</th>
                                    <th>Satuan</th>
                                    <th>Total Penggunaan</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($bahan as $r)

                                    @php
                                        $total_penggunaan_bahan += $r->total_bahan;
                                    @endphp
                                    <tr>
                                        <th scope="row">{{ $no++ }}.</th>
                                        <td>{{ $r->kode_bahan }}</td>
                                        <td>{{ $r->nama_bahan }}</td>
                                        <td>{{ $r->satuan['satuan'] }}</td>
                                        <td class="text-right">{{ Main::format_number($r->total_bahan) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="4" class="text-right">
                                        <strong>Total</strong>
                                    </th>
                                    <th class="text-right">{{ Main::format_number($total_penggunaan_bahan) }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="m-portlet__foot m--hide">
                            <div class="row">
                                <div class="col-lg-6">
                                    Portlet footer:
                                </div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <span class="m--margin-left-10">or <a href="#"
                                                                          class="m-link m--font-bold">Cancel</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet  m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Grafik Penjualan Produk
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div id="m_amcharts_4" style="height: 500px;"></div>
                </div>
            </div>

        </div>
    </div>
@endsection
