<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div><!--End Info-->
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td><h1>INVOICE</h1></td>
                    </tr>
                    <tr>
                        <td width="50">No</td>
                        <td>{{ $penjualan->no_faktur }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>{{ $penjualan->tanggal }}</td>
                    </tr>
                    <tr>
                        <td>PO No</td>
                        <td>{{ $penjualan->no_faktur }}</td>
                    </tr>
                    <tr>
                        <td>Quo No</td>
                        <td>{{ '-' }}</td>
                    </tr>
                </table>
            </div><!--End Title-->
        </div><!--End InvoiceTop-->

        <div style="clear: both"></div>
        <br/><br/><br/><br/>
        <hr />

        <div id="invoice-mid">
            <div class="info">
                <h2>Ditujukan Kepada</h2>
                <p>{{ $penjualan->distributor->nama_distributor }}</p>
                <p>{{ $penjualan->distributor->alamat_distributor }}</p>
            </div>

            <div style="clear: both;"></div>

        </div><!--End Invoice Mid-->

        <div id="invoice-bot">

            <div id="table">
                <table>
                    <tr class="tabletitle">
                        <td class="no" width="20"><h2>No</h2></td>
                        <td class="item"><h2>Deskripsi</h2></td>
                        <td class="Hours"><h2>Jumlah</h2></td>
                        <td class="Rate"><h2>Harga Satuan</h2></td>
                        <td class="Hours"><h2>PPN</h2></td>
                        <td class="Hours"><h2>PPN Nominal</h2></td>
                        <td class="Hours"><h2>Potongan</h2></td>
                        <td class="Hours"><h2>Sub Total</h2></td>
                        <td class="subtotal"><h2>Total</h2></td>
                    </tr>
                    @foreach($penjualan_produk as $r)
                    <tr class="service">
                        <td class="tableitem"><p class="itemtext">{{ $no++ }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ $r->produk->nama_produk }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::format_number($r->qty) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->harga) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ ($r->ppn_persen*100).'% ' }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->ppn_nominal) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->potongan) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->harga_net) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->sub_total) }}</p></td>
                    </tr>
                    @endforeach
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Total</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($penjualan->total) }}</h2></td>
                    </tr>
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Biaya Tambahan</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($penjualan->biaya_tambahan) }}</h2></td>
                    </tr>
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Potongan</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($penjualan->potongan_akhir) }}</h2></td>
                    </tr>
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Grand Total</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($penjualan->grand_total) }}</h2></td>
                    </tr>

                </table>
            </div><!--End Table-->

            <br /><br />

            <div id="legalcopy">
                <p class="legal"><strong>Dipesan oleh.</strong>
                    <br />
                    Nama Bank : {{ $company->bankType }}<br />
                    Nomer Rekening : {{ $company->bankRekening }}<br />
                    Atas Nama : {{ $company->bankAtasNama }}<br /><br />

                    <span style="color: red;">*</span> <i>Batas Pembayaran Tujuh Hari Setelah Invoice Diterima</i>
                </p>
            </div>

            <div id="ttd">
                Bali, {{ date('d F Y') }}<br />
                TAMBA WARAS<br />
                <br /><br /><br /><br />
                {{ $company->companyBendahara }}
            </div>

            <div style="clear: both"></div>

        </div><!--End InvoiceBot-->
    </div><!--End Invoice-->
</div><!-- End Invoice Holder-->