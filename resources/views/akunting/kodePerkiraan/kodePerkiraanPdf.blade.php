<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice.css') }}">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ asset('images/logo.png') }}" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2">
                            <h3>Kode Perkiraan</h3>
                            Tanggal : {{  date('d F Y H:i') }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <div id="table">
                <table>
                    <thead>
                    <tr class="tabletitle">
                        <th class="item">Kode Rekening</th>
                        <th class="item">Nama Rekening</th>
                        <th class="item">Posisi</th>
                        <th class="item">Normal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($perkiraan as $row)
                        <tr>
                            <td class="tableitem"><p class="itemtext"><strong>{{ $row['mst_kode_rekening'] }}</strong></p></td>
                            <td class="tableitem"><p class="itemtext"><strong>{{ $row['mst_nama_rekening'] }}</strong></p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row['mst_posisi']) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row['mst_normal']) }}</p></td>
                        </tr>
                        @foreach($row['sub1'] as $row1)
                            <tr>
                                <td class="tableitem"><p class="itemtext">{{ $row1['mst_kode_rekening'] }}</p></td>
                                <td class="tableitem"><p class="itemtext">{!! $space1.$row1['mst_nama_rekening'] !!}</p></td>
                                <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row1['mst_posisi']) }}</p></td>
                                <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row1['mst_normal']) }} </td>
                            </tr>
                            @foreach($row1['sub2'] as $row2)
                                <tr>
                                    <td class="tableitem"><p class="itemtext">{{ $row2['mst_kode_rekening'] }}</p></td>
                                    <td class="tableitem"><p class="itemtext">{!! $space2.$row2['mst_nama_rekening'] !!}</p></td>
                                    <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row2['mst_posisi']) }}</p></td>
                                    <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row2['mst_normal']) }}</p></td>
                                </tr>
                                @foreach($row2['sub3'] as $row3)
                                    <tr>
                                        <td class="tableitem"><p class="itemtext">{{ $row3['mst_kode_rekening'] }}</p></td>
                                        <td class="tableitem"><p class="itemtext">{!! $space3.$row3['mst_nama_rekening'] !!}</p></td>
                                        <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row3['mst_posisi']) }}</p></td>
                                        <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row3['mst_normal']) }}</p></td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
