<?php

namespace app\Http\Controllers\LaporanOperasional;

use app\Helpers\Main;
use app\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use app\Models\mPenjualan;
use app\Models\mPenjualanProduk;
use PDF;

class LaporanPenjualanDetail extends Controller
{

    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['distribusi'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporanOperasional'],
                'route' => ''
            ],
            [
                'label' => $cons['laporanO_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];
        $list = mPenjualanProduk
            ::with([
                'produk:id,kode_produk,nama_produk',
                'lokasi:id,kode_lokasi,lokasi',
                'stok_produk:id,no_seri_produk'
            ])
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_penjualan_produk.id_penjualan')
            ->leftJoin('tb_distributor', 'tb_distributor.id', '=', 'tb_penjualan.id_distributor')
            ->whereBetween('tb_penjualan.tanggal', $where_date)
            ->orderBy('tb_penjualan_produk.id', 'DESC')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'total' => 0
        ]);

        return view('laporanOperasional/laporanPenjualanDetail/laporanPenjualanDetailList', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mPenjualanProduk
            ::with([
                'produk:id,kode_produk,nama_produk',
                'lokasi:id,kode_lokasi,lokasi',
                'stok_produk:id,no_seri_produk'
            ])
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_penjualan_produk.id_penjualan')
            ->leftJoin('tb_distributor', 'tb_distributor.id', '=', 'tb_penjualan.id_distributor')
            ->whereBetween('tb_penjualan.tanggal', $where_date)
            ->orderBy('tb_penjualan_produk.id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'date_start' => Main::format_date($date_start),
            'date_end' => Main::format_date($date_end),
            'company' => Main::companyInfo(),
            'total' => 0
        ]);

        $pdf = PDF::loadView('laporanOperasional/laporanPenjualanDetail/laporanPenjualanDetailPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('Laporan Penjualan Detail ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');
        $date_end_db = mPenjualan
            ::select('tanggal')
            ->orderBy('tanggal', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tanggal');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $list = mPenjualanProduk
            ::with([
                'produk:id,kode_produk,nama_produk',
                'lokasi:id,kode_lokasi,lokasi',
                'stok_produk:id,no_seri_produk'
            ])
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_penjualan_produk.id_penjualan')
            ->leftJoin('tb_distributor', 'tb_distributor.id', '=', 'tb_penjualan.id_distributor')
            ->whereBetween('tb_penjualan.tanggal', $where_date)
            ->orderBy('tb_penjualan_produk.id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'date_start' => Main::format_date($date_start),
            'date_end' => Main::format_date($date_end),
            'company' => Main::companyInfo(),
            'total' => 0
        ]);

        return view('laporanOperasional/laporanPenjualanDetail/laporanPenjualanDetailExcel', $data);
    }

}
