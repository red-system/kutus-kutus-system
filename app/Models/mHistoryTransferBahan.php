<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHistoryTransferBahan extends Model
{

    protected $table = 'tb_history_transfer_bahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tgl',
        'id_bahan',
        'id_stok_bahan',
        'kode_bahan',
        'nama_bahan',
        'qty_transfer',
        'id_lokasi_dari',
        'id_lokasi_tujuan',
        'dari',
        'tujuan',
        'last_stok'
    ];
}
