<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mProgressProduksiPengemas extends Model
{
    protected $table = 'tb_progress_produksi_pengemas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produksi',
        'id_progress_produksi',
        'id_detail_produksi',
        'id_bahan',
        'id_stok_bahan',
        'qty'
    ];

    function produksi()
    {
        return $this->belongsTo(mProduksi::class, 'id_produksi');
    }

    function detail_produksi()
    {
        return $this->belongsTo(mDetailProduksi::class, 'id_detail_produksi');
    }

    function bahan()
    {
        return $this->belongsTo(mBahan::class, 'id_bahan');
    }

    function stok_bahan()
    {
        return $this->belongsTo(mStokBahan::class, 'id_stok_bahan');
    }
}
