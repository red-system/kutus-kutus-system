@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    <div class="m--hide">
        {!! $download_surat_jalan !!}
    </div>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tabs">
                @include('distribusi/distribusi/tabTitle')
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>No Distribusi</th>
                            <th>Tanggal Distribusi</th>
                            <th>No Faktur</th>
                            <th>Qty Dikirim</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($distribusi as $r)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $r->no_distribusi }}</td>
                                <td>{{ Main::format_date($r->tanggal_distribusi) }}</td>
                                <td>{{ $r->penjualan->no_faktur }}</td>
                                <td>{{ Main::format_number($r->total_dikirim) }}</td>
                                <td>
                                    <textarea class="row-data hidden">@json([])</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                        <a href="{{ route('distribusiCetak', ['id'=>Main::encrypt($r->id)]) }}"
                                           class="m-btn btn btn-success">
                                            <i class="la la-print"></i> Cetak Surat Jalan
                                        </a>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-hapus"
                                                data-route="{{ route('distribusiDelete', ['id'=>$r->id]) }}"
                                                data-reload="true">
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection