@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('inventoryLogistik/stokBahan/stokBahanCreate')
    @include('inventoryLogistik/stokBahan/stokBahanEdit')

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                    <div>
                        <a href="{{ route("bahanPage") }}"
                           class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Bahan</span>
                            </span>
                        </a>
                        {!! "&nbsp" !!}
                        <a href="#"
                           class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                           data-toggle="modal"
                           data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
{{--                                <th>Kode Bahan</th>
                                <th>Nama Bahan</th>--}}
                                <th>Gudang</th>
                                <th>No Seri Bahan</th>
                                <th>Penerima</th>
                                <th>Qty</th>
                                <th width="150">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $r)

                                @php
                                    $total_stok_bahan += $r->qty;
                                @endphp
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
{{--                                <td>{{ $r->bahan->kode_bahan }}</td>
                                <td>{{ $r->bahan->nama_bahan }}</td>--}}
                                <td>{{ $r->lokasi->lokasi }}</td>
                                <td>{{ $r->no_seri_bahan }} </td>
                                <td>{{ $r->keterangan }}</td>
                                <td class="text-right">{{ Main::format_number($r->qty) }}</td>
                                <td align="center">
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                        <button type="button"
                                                class="m-btn btn btn-success btn-edit akses-edit"
                                                data-route="{{ route('stokBahanUpdate', ['id'=>$r->id]) }}">
                                            <i class="la la-edit"></i> Edit
                                        </button>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-hapus akses-delete"
                                                data-route='{{ route('stokBahanDelete', ['id'=>$r->id]) }}'
                                                data-message="Yakin hapus data ini ?<br /> Menghapus data akan mengurangi Stok Bahan">
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-center">
                                        <strong>TOTAL</strong>
                                    </th>
                                    <th class="text-right">
                                        <strong>{{ Main::format_number($total_stok_bahan) }}</strong>
                                    </th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

        </div>
@endsection