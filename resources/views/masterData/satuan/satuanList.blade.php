@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('masterData/satuan/satuanCreate')
    @include('masterData/satuan/satuanEdit')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Satuan</th>
                            <th>Nama Satuan</th>
                            <th width="200">Waktu Dibuat</th>
                            <th width="200">Terakhir Diperbarui</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                        <tr>
                            <td align="center">{{ $no++ }}.</td>
                            <td>{{ $r->kode_satuan }}</td>
                            <td>{{ $r->satuan }}</td>
                            <td>{{ $r->created_at }}</td>
                            <td>{{ $r->updated_at }}</td>
                            <td align="center">
                                <textarea class="row-data hidden">@json($r)</textarea>
                                <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                    <button type="button"
                                            class="m-btn btn btn-success btn-edit akses-edit"
                                            data-route="{{ route('satuanUpdate', ['id'=>$r->id]) }}"
                                            data-redirect="?table_value={{ $no-1 }}.&table_index=0">
                                        <i class="la la-edit"></i> Edit
                                    </button>
                                    <button type="button"
                                            class="m-btn btn btn-danger btn-hapus akses-delete"
                                            data-route='{{ route('satuanDelete', ['id'=>$r->id]) }}'>
                                        <i class="la la-remove"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

@endsection