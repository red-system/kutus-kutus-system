@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <form action="{{ route('distribusiInsert') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('distribusiHistory', ['id'=>Main::encrypt($id_penjualan)]) }}"
          data-alert-show="true"
          style="width: 100%">

        {{ csrf_field() }}

        <input type="hidden" name="id_penjualan" value="{{ $id_penjualan }}">
        <input type="hidden" name="urutan" value="{{ $urutan }}">
        <input type="hidden" name="no_distribusi" value="{{ $no_distribusi }}">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--tabs">
                    @include('distribusi/distribusi/tabTitle')
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">No Faktur</label>
                                <div class="col-8 col-form-label">
                                    {{ $penjualan->no_faktur }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Tanggal Penjualan</label>
                                <div class="col-8 col-form-label">
                                    {{ Main::format_date($penjualan->tanggal) }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Catatan</label>
                                <div class="col-8">
                                    <textarea class="form-control" rows="3" name="catatan"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">No Distribusi</label>
                                <div class="col-8 col-form-label">{{ $no_distribusi }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pelanggan</label>
                                <div class="col-8 col-form-label">
                                    {{ $penjualan->distributor->kode_distributor.' '.$penjualan->distributor->nama_distributor }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">No Telepon</label>
                                <div class="col-8 col-form-label">
                                    {{ $penjualan->distributor->telp_distributor }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">No Polisi</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" name="no_polisi">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Alamat</label>
                                <div class="col-8 col-form-label">
                                    {{ $penjualan->distributor->alamat_distributor }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Alamat Lain</label>
                                <div class="col-8">
                                    <span class="m--font-info"><i class="flaticon-info"></i> <strong>Perhatian.</strong>
                                        <br />
                                        Jika Kolom Alamat Lain diisikan, maka alamat ini yang akan digunakan pada <strong>Surat Jalan.</strong></span>
                                    <textarea class="form-control" rows="3" name="alamat_lain"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>No Batch</th>
                                <th>Lokasi</th>
                                <th>Stok Produk</th>
                                <th>Qty Distribusi</th>
                                <th>Terkirim</th>
                                <th>Sisa Pengiriman</th>
                                <th width="140">Dikirim</th>
                                <td>Remark</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($penjualan_produk as $r)
                                @php
                                    $qty_terkirim = \app\Models\mDistribusiDetail
                                    ::where([
                                        'id_penjualan_produk'=> $r->id
                                    ])
                                    ->sum('qty_dikirim');
                                $qty_sisa = $r->qty - $qty_terkirim;
                                @endphp
                                <tr data-id="{{ $r->id }}">
                                    <td class="m--hide">
                                        <input type="hidden" name="id_penjualan_produk[]" value="{{ $r->id }}">
                                        <input type="hidden" name="kode_produk[]" value="{{ $r->produk->kode_produk }}">
                                        <input type="hidden" name="nama_produk[]" value="{{ $r->produk->nama_produk }}">
                                        <input type="hidden" name="no_seri_produk[]" value="{{ $r->stok_produk['no_seri_produk'] }}">
                                        <input type="hidden" name="kode_lokasi[]" value="{{ $r->lokasi['kode_lokasi'] }}">
                                        <input type="hidden" name="lokasi[]" value="{{ $r->lokasi['lokasi'] }}">
                                        <input type="hidden" name="id_stok_produk[]" value="{{ $r->id_stok_produk }}">
                                        <input type="hidden" name="id_produk[]" value="{{ $r->id_produk }}">
                                        <input type="hidden" name="qty_minta[]" value="{{ $r->qty }}">
                                        <input type="hidden" name="qty_terkirim[]" value="{{ $qty_terkirim }}">
                                    </td>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $r->produk->kode_produk }}</td>
                                    <td>{{ $r->produk->nama_produk }}</td>
                                    <td>{{ $r->stok_produk['no_seri_produk'] }}</td>
                                    <td>{{ $r->lokasi['kode_lokasi'].' '.$r->lokasi['lokasi'] }}</td>
                                    <td>{{ Main::format_number($r->stok_produk['qty']) }}</td>
                                    <td>{{ Main::format_number($r->qty) }}</td>
                                    <td>{{ Main::format_number($qty_terkirim) }}</td>
                                    <td>{{ Main::format_number($qty_sisa) }}</td>
                                    <td>
                                        <input type="text"
                                               class="form-control touchspin-number"
                                               value="{{ $qty_sisa }}"
                                               name="qty_dikirim[]">
                                    </td>
                                    <td>
                                        <textarea name="remark" class="form-control"></textarea>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
            <span>
                <i class="la la-check"></i>
                <span>Simpan Data</span>
            </span>
            </button>

            <a href="{{ route("distribusiPage") }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
            <span>
                <i class="la la-angle-double-left"></i>
                <span>Kembali ke Daftar Data</span>
            </span>
            </a>
        </div>

    </form>
@endsection