<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPenjualanPembayaran extends Model
{

    protected $table = 'tb_penjualan_pembayaran';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'master_id',
        'jumlah',
        'jatuh_tempo',
        'no_check_bg',
        'keterangan'
    ];

    function master() {
        return $this->belongsTo(mAcMaster::class, 'master_id');
    }

    function penjualan() {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }
}
