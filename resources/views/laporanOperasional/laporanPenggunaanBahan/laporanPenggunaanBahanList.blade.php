@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('produksi.produksi.produksiDetailModal')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ $date_start }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end" value="{{ $date_end }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('laporanPenggunaanBahanPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('laporanPenggunaanBahanExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--tabs akses-list">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_tabs_6_1"
                                   role="tab"
                                   aria-selected="false">
                                    <i class="la la-refresh"></i> Rangkuman Penggunaan Bahan
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_2"
                                   role="tab" aria-selected="false">
                                    <i class="la la-info-circle"></i> Detail Penggunaan Bahan
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="m_tabs_6_1" role="tabpanel">
                            <table class="table table-striped- table-bordered table-hover table-checkable datatable-no-order">
                                <thead>
                                <tr>
                                    <th width="10">No</th>
                                    <th>Kode Produk</th>
                                    <th>Nama Produk</th>
                                    <th>Jumlah Produksi</th>
                                    <th>Total Bahan Digunakan</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php
                                    $total_qty_produksi = 0;
                                    $total_qty_bahan = 0;
                                @endphp

                                @foreach($produk as $r)
                                    @php
                                        $total_qty_produksi += $r->total_qty_produksi;
                                        $total_qty_bahan += ($r->total_qty_produksi * $r->total_qty_bahan)
                                    @endphp
                                    <tr>
                                        <td width="10">{{ $no++ }}.</td>
                                        <td>{{ $r->kode_produk }}</td>
                                        <td>{{ $r->nama_produk }}</td>
                                        <td align="right">{{ Main::format_number($r->total_qty_produksi) }}</td>
                                        <td align="right">{{ Main::format_number($r->total_qty_produksi*$r->total_qty_bahan) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="3" align="center">
                                        <strong>Total</strong>
                                    </td>
                                    <td align="right">
                                        <strong>{{ Main::format_number($total_qty_produksi) }}</strong>
                                    </td>
                                    <td align="right">
                                        <strong>{{ Main::format_number($total_qty_bahan) }}</strong>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="tab-pane" id="m_tabs_6_2" role="tabpanel">

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover datatable-no-order">
                                    <thead>
                                    <tr>
                                        <th width="30">No</th>
                                        <th width="210">Nama Bahan</th>
                                        @foreach($produksi as $r)
                                            <th class="text-center btn-detail"
                                                data-route="{{ route('produksiDetail', ['id'=>$r->id]) }}"
                                                style="cursor: pointer">
                                                {{ $r->kode_produksi }}
                                                <i class="la la-link"></i>
                                            </th>
                                        @endforeach
                                        <th>Total Pemakaian</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach($bahan as $r1_bahan)
                                        @php
                                            $id_bahan = $r1_bahan->id;
                                            $nama_bahan = $r1_bahan->nama_bahan;
                                            $total_bahan = 0;
                                        @endphp

                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $nama_bahan }}</td>
                                            @foreach($produksi as $r2_produksi)
                                                @php
                                                    $id_produksi = $r2_produksi->id;

                                                    $qty_komposisi_bahan = \app\Models\mBahanProduksi
                                                        ::where([
                                                            'id_produksi'=>$id_produksi,
                                                            'id_bahan'=>$id_bahan
                                                        ])
                                                        ->sum('qty_diperlukan');


                                                    $qty_bahan_pengemas = \app\Models\mProgressProduksiPengemas
                                                        ::where([
                                                            'id_produksi'=>$id_produksi,
                                                            'id_bahan'=>$id_bahan
                                                        ])
                                                        ->sum('qty');

                                                    $total_qty = $qty_komposisi_bahan + $qty_bahan_pengemas;
                                                    $total_bahan += $total_qty;
                                                @endphp
                                                <td align="right">{{ Main::format_number($total_qty) }}</td>
                                            @endforeach
                                            <td align="right" class="font-weight-bold">{{ Main::format_number($total_bahan) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection