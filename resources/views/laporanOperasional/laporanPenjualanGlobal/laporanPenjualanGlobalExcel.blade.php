<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Penjualan Global " . date('d-m-Y') . ".xls");
?>

<h3 align="center">LAPORAN PENJUALAN GLOBAL</h3>
<table width="100%">
    <tbody>
    <tr>
        <td align="center">Tanggal : {{ $date_start }} s/d {{ $date_end }}</td>
    </tr>
    </tbody>
</table>
<br />
<table width="100%" border="1">
    <thead>
    <tr>
        <th>No Faktur</th>
        <th>Tanggal</th>
        <th>Jenis Transaksi</th>
        <th>Distributor</th>
        <th>Total</th>
        <th>Biaya Tambahan</th>
        <th>Potongan</th>
        <th>PPN</th>
        <th>Grand Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        @php
            $total_grand_total += $r->grand_total;
            $total_biaya_tambahan += $r->biaya_tambahan;
            $total_potongan += $r->total_potongan;
            $total_ppn_nominal += $r->total_ppn_nominal;
        @endphp
        <tr>
            <td>{{ $r->no_faktur }}</td>
            <td>{{ Main::format_date($r->tanggal) }}</td>
            <td>{{ ucwords($r->jenis_transaksi) }}</td>
            <td>{{ '('.$r->distributor->kode_distributor.') '.$r->distributor->nama_distributor }}</td>
            <td>{{ Main::format_number($r->total) }}</td>
            <td>{{ Main::format_number($r->biaya_tambahan) }}</td>
            <td>{{ Main::format_number($r->total_potongan) }}</td>
            <td>{{ Main::format_number($r->total_ppn_potongan) }}</td>
            <td>{{ Main::format_number($r->grand_total) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="5" align="center" class="font-weight-bold">
            Total
        </td>
        <td align="right" class="font-weight-bold">{{ Main::format_number($total_biaya_tambahan) }}</td>
        <td align="right" class="font-weight-bold">{{ Main::format_number($total_potongan) }}</td>
        <td align="right" class="font-weight-bold">{{ Main::format_number($total_ppn_nominal) }}</td>
        <td align="right" class="font-weight-bold">{{ Main::format_number($total_grand_total) }}</td>
    </tr>
    </tfoot>
</table>