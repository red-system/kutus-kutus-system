<?php

namespace app\Http\Controllers\produksi;

use app\Helpers\Main;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Models\mTargetProduksi;
use app\Models\mDetailProduksi;
use app\Models\mProduk;

class RealisasiTargetProduksi extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->menuActive = $cons['produksi_3'];
        $this->breadcrumb = [
            [
                'label' => $cons['produksi'],
                'route' => ''
            ],
            [
                'label' => $cons['produksi_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data['produk'] = mProduk::orderBy('kode_produk', 'ASC')->get();
        $data['list'] = mTargetProduksi::distinct()->orderBy('tahun_target', 'DESC')->get(['tahun_target']);

        return view('produksi/realisasiTargetProduksi/realisasiList', $data);
    }

    function tahun($tahun)
    {
        $tahun = Main::decrypt($tahun);

        $dataMain = Main::data($this->breadcrumb, $this->menuActive);
        $produk = mProduk::orderBy('kode_produk', 'ASC')->get();
        $pageTitle = $this->cons['produksi_2'] . ' Tahun ' . $tahun;
        $bulan = isset($_GET['bulan']) ? $_GET['bulan'] : date('m') . '-' . $tahun;
        $id_produk = isset($_GET['id_produk']) ? $_GET['id_produk'] : 'all';

        $bulan_where = explode('-', $bulan)[0];


        $list = mTargetProduksi
            ::with('produk')
            ->where('tahun_target', $tahun)
            ->where('bulan_target', $bulan_where);
        if ($id_produk != 'all') {
            $list = $list->where('id_produk', $id_produk);
        }
        $list = $list->orderBy('id', 'DESC')->get();

        $data = [
            'list' => $list,
            'produk' => $produk,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'id_produk' => $id_produk,
            'pageTitle' => $pageTitle,
            'totalTarget' => 0,
            'totalRealisasi' => 0
        ];

        $data = array_merge($dataMain, $data);

        return view('produksi/realisasiTargetProduksi/realisasiTahun', $data);
    }

    function index1()
    {
        $data = Main::data($this->breadcrumb);
        $list = mTargetProduksi::with('produk')->orderBy('id', 'ASC')->get();
        $dataList = [];
        $no = 0;
        foreach ($list as $r) {
            $sum = 0;
            $detailProduksi = mDetailProduksi::select('qty')->where('id_produk', $r->id_produk)->get();
            foreach ($detailProduksi as $r2) {
                $sum += $r2->qty;
            }

            $dataList[$no] = $r;
            $dataList[$no]->qtySekarang = $sum;

            $no++;
        }

        $data['list'] = $dataList;

        return view('produksi/realisasiTargetProduksi/realisasiTargetProduksiList', $data);
    }
}
