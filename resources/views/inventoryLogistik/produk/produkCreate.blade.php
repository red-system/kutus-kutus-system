<form action="{{ route('produkInsert') }}" method="post" class="m-form form-send m-form--label-align-right">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Kode Produk </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="kode_produk" autofocus>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Nama Produk </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control m-input" name="nama_produk">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Kategori Produk </label>
                            <div class="col-lg-9">
                                <select class="form-control m-input  m-select2" name="id_kategori_produk" style="width: 100% !important;">
                                    @foreach($kategoriProduk as $r)
                                        <option value="{{ $r->id}}">{{ $r->kategori_produk }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
{{--                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Lokasi </label>
                            <div class="col-lg-9">
                                <select class="form-control m-input  m-select2" name="id_lokasi" style="width: 100% !important;">
                                    @foreach($lokasi as $r)
                                        <option value="{{ $r->id }}">{{ $r->lokasi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>--}}
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Minimal Stok </label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control m-input touchspin-number" name="minimal_stok" value="1">
                            </div>
                        </div>
{{--                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Harga Eceran</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control m-input touchspin-rp" name="harga_eceran" value="0">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Harga Grosir</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control m-input touchspin-rp" name="harga_grosir" value="0">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Harga Kosinyasi</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control m-input touchspin-rp" name="harga_kosinyasi" value="0">
                            </div>
                        </div>--}}
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 form-control-label required">Diskon (%) </label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control m-input touchspin-persen" name="disc_persen" value="0">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>