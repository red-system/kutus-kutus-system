<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mKategoriBahan extends Model
{

    protected $table = 'tb_kategori_bahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_kategori_bahan',
        'kategori_bahan'
    ];
}
