<?php

namespace app\Http\Controllers\InventoryLogistik;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;

use app\Models\mBahan;
use app\Models\mKategoriProduk;
use app\Models\mLokasi;
use app\Models\mProduk;

class StokAlert extends Controller
{

    private $breadcrumb;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;

        $this->breadcrumb = [
            [
                'label'=>$cons['inventory'],
                'route'=>''
            ]
        ];
    }


    function bahan() {
        $breadcrumb = [
            [
                'label'=>$this->cons['inventory_10'],
                'route'=>''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb);
        $listData = [];
        $no = 0;
        $list = mBahan
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_bahan:id,kode_kategori_bahan,kategori_bahan',
                'satuan:id,kode_satuan,satuan',
                'supplier:id,kode_supplier,nama_supplier',
                'stok_bahan:id_bahan,qty'
            )
            ->orderBy('id', 'DESC')
            ->get();

        foreach($list as $r) {

            $countQty = 0;
            foreach($r->stok_bahan as $r2) {
                $countQty += $r2->qty;
            }

            if($r->minimal_stok > $countQty) {
                $listData[$no] = $r;
                $listData[$no]->total_stok = $countQty;

                $no++;
            }
        }

        $data['list'] = $listData;

        return view('inventoryLogistik/stokAlert/alertBahan', $data);
    }

    function produk() {
        $breadcrumb = [
            [
                'label'=>$this->cons['inventory_11'],
                'route'=>''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb);
        $listData = [];
        $no = 0;
        $list = mProduk
            ::with(
                'lokasi:id,kode_lokasi,lokasi',
                'kategori_produk:id,kode_kategori_produk,kategori_produk',
                'stok_produk:id_produk,qty'
            )
            ->orderBy('id', 'DESC')
            ->get();

        foreach($list as $r) {
            $countQty = 0;
            foreach($r->stok_produk as $r2) {
                $countQty += $r2->qty;
            }

            if($r->minimal_stok > $countQty) {
                $listData[$no] = $r;
                $listData[$no]->total_stok = $countQty;

                $no++;
            }
        }

        $data['list'] = $listData;

        return view('inventoryLogistik/stokAlert/alertProduk', $data);
    }
}
