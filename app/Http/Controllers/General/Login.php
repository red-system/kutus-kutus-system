<?php

namespace app\Http\Controllers\General;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Rules\LoginCheck;
use Illuminate\Support\Facades\Log;
use app\Models\mUser;
use Illuminate\Support\Facades\Session;

class Login extends Controller
{
    function index() {
        $data = [
            'pageTitle'=>'Login | '.env('APP_NAME'),
        ];
        return view('general/login', $data);
    }

    function do(Request $request) {

        $request->validate([
            'username'=>'required',
            'password'=>['required', new LoginCheck($request->username) ]
        ]);

    }
}
