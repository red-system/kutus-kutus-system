<input type="hidden" name="id_distributor_order" value="{{ $id_distributor_order }}">

<h3>Detail Distributor Order</h3>
<div class="m-form">
    <div class="m-portlet__body">
        <div class="m-form__section m-form__section--first row data-detail-section">
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">No Order</label>
                    <div class="col-lg-6 col-form-label">
                        {{ $distributor_order->no_order }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">Tanggal</label>
                    <div class="col-lg-6 col-form-label">
                        {{ Main::format_date($distributor_order->tanggal) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">Pengiriman</label>
                    <div class="col-lg-6 col-form-label">
                        {!! Main::distributor_pengiriman($distributor_order->pengiriman) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Pelanggan</label>
                    <div class="col-lg-9 col-form-label">
                        {{ $distributor_order->distributor->kode_distributor.' '.$distributor_order->distributor->nama_distributor }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Alamat</label>
                    <div class="col-lg-9 col-form-label">
                        {{ $distributor_order->distributor->alamat_distributor }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">No Telepon</label>
                    <div class="col-lg-9 col-form-label">
                        {{ $distributor_order->distributor->telp_distributor }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr/>
<br/>
<h3>Daftar Produk</h3>
<table class="table m-table m-table--head-separator-primary">
    <thead>
    <tr>
        <th>No</th>
        <th>Kode Produk</th>
        <th>Nama Produk</th>
        <th>Qty Order</th>
        <th>Qty Konfirmasi</th>
    </tr>
    </thead>
    <tbody>
    @php(
        $no = 1
    )
    @foreach($distributor_order_produk as $r)
        <tr>
            <td>{{ $no++ }}.</td>
            <td>{{ $r->produk->kode_produk }}</td>
            <td>{{ $r->produk->nama_produk }}</td>
            <td>{{ Main::format_number($r->qty_order) }}</td>
            <td>{{ Main::format_number($r->qty_konfirm) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>