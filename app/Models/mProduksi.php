<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mProduksi extends Model
{
    protected $table = 'tb_produksi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'urutan',
        'id_lokasi',
        'kode_produksi',
        'tgl_mulai_produksi',
        'tgl_selesai_produksi',
        'catatan',
        'status',
        'status_finish_date',
        'produksi',
        'publish',
        'publish_date',
        'finish'
    ];

    function detail_produksi()
    {
        return $this->hasMany(mDetailProduksi::class, 'id_produksi');
    }

    function bahan_produksi()
    {
        return $this->hasMany(mBahanProduksi::class, 'id_produksi');
    }

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function progress_produksi() {
        return $this->hasMany(mProgressProduksi::class, 'id_produksi');
    }

    function bahan_pengemas() {
        return $this->hasMany(mProgressProduksiPengemas::class, 'id_produksi');
    }
}
