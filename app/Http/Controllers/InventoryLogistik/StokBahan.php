<?php

namespace app\Http\Controllers\InventoryLogistik;

use app\Models\mHistoryPenyesuaianBahan;
use app\Models\mProgressProduksiPengemas;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;

use app\Rules\StokBahanLokasiInsert;
use app\Rules\StokBahanLokasiUpdate;

use app\Models\mKategoriStok;
use app\Models\mStokBahan;
use app\Models\mArusStokBahan;
use app\Models\mLokasi;
use app\Models\mBahan;

use DB;

class StokBahan extends Controller
{
    private $breadcrumb;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => route('bahanPage')
            ],
            [
                'label' => $cons['inventory_1'],
                'route' => route('bahanPage')
            ],
            [
                'label' => $cons['inventory_14'],
                'route' => ''
            ]
        ];
    }


    function index($idBahan)
    {

        $idBahan = Main::decrypt($idBahan);
        $menuActive = $this->cons['inventory_1'];
        $data = Main::data($this->breadcrumb, $menuActive);
        $list = mStokBahan
            ::with(
            //'bahan:id,kode_bahan,nama_bahan',
                'lokasi:id,kode_lokasi,lokasi'
            )
            ->where('qty', '>', 0)
            ->where('id_bahan', $idBahan)
            ->orderBy('id', 'DESC')
            ->get();
        $bahan = mBahan
            ::with('satuan')
            ->select(['kode_bahan', 'nama_bahan', 'id_satuan'])
            ->find($idBahan);
        $pageTitle = $bahan->kode_bahan . ' ' . $bahan->nama_bahan;
        $lokasi = mLokasi::where('tipe', 'gudang')->orderBy('lokasi', 'ASC')->get();
        $total_stok_bahan = 0;

        $data['idBahan'] = $idBahan;
        $data['list'] = $list;
        $data['pageTitle'] = $pageTitle;
        $data['lokasi'] = $lokasi;
        $data['bahan'] = $bahan;
        $data['total_stok_bahan'] = $total_stok_bahan;

        return view('inventoryLogistik/stokBahan/stokBahanList', $data);
    }

    function insert(Request $request, $idBahan)
    {
        $request->validate([
            'qty' => 'required',
            //'id_lokasi'=>['required', new StokBahanLokasiInsert($idBahan, $request->input('id_lokasi'))]
            'id_lokasi' => 'required',
            'no_seri_bahan' => 'required',
        ]);

        $data_stok = $request->except(['_token']);
        $data_stok['id_bahan'] = $idBahan;
        $response = mStokBahan::create($data_stok);

        $id_stok_bahan = $response->id;

        $last_stok = mStokBahan::where('id_bahan', $idBahan)->sum('qty');
        $last_stok_total = mStokBahan::sum('qty');

        $data_arus = [
            'tgl' => date('Y-m-d H:i:s'),
            'table_id' => $id_stok_bahan,
            'table_name' => 'tb_stok_bahan',
            'id_stok_bahan' => $id_stok_bahan,
            'id_bahan' => $idBahan,
            'stok_in' => $data_stok['qty'],
            'stok_out' => 0,
            'last_stok' => $last_stok,
            'last_stok_total' => $last_stok_total,
            'keterangan' => 'Insert Stok : '.$data_stok['keterangan'],
            'method' => 'insert'
        ];
        mArusStokBahan::create($data_arus);
    }

    function delete($id)
    {
        DB::beginTransaction();
        try {

            $allow_delete = TRUE;
            $table_delete = '';
            $message = '';
            $tables = [
                'arus_stok_bahan',
                'progress_produksi_pengemas',
                'history_penyesusaian_bahan',
                'history_transfer_bahan',
                'history_penyesuaian_bahan',
            ];

            $arus_stok_bahan = mArusStokBahan::where('id_stok_bahan', $id)->count();
            $progress_produksi_pengemas = mProgressProduksiPengemas::where('id_stok_bahan', $id)->count();
            $history_penyesuaian_bahan = mHistoryPenyesuaianBahan::where('id_stok_bahan', $id)->count();
            $history_transfer_bahan = mHistoryTransferBahan::where('id_stok_bahan', $id)->count();
            $history_penyesuaian_bahan = mHistoryPenyesuaianBahan::where('id_stok_bahan', $id)->count();

            foreach ($tables as $table) {
                if (${$table} > 0) {
                    $allow_delete = FALSE;
                    $table_delete = $table;
                }
            }

            switch($table_delete) {
                case 'arus_stok_bahan':
                    $message = 'Stok Bahan ini sudah ada di Arus Stok Produk. Sehingga tidak bisa dihapus.';
                    break;
                case 'progress_produksi_pengemas':
                    $message = 'Stok Bahan ini sudah ada di Progress Produksi Pengemas. Sehingga tidak bisa dihapus.';
                    break;
                case 'progress_penyesuaian_bahan':
                    $message = 'Stok Bahan ini sudah ada di Progress Penyesuaian Bahan. Sehingga tidak bisa dihapus.';
                    break;
                case 'history_transfer_bahan':
                    $message = 'Stok Bahan ini sudah ada di History Transfer Bahan. Sehingga tidak bisa dihapus';
                    break;
                case 'history_penyesuaian_bahan':
                    $message = 'Stok Bahan ini sudah ada di History Penyesuaian Bahan. Sehingga tidak bisa dihapus';
                    break;
            }

            if (!$allow_delete) {
                return response([
                    'message' => $message
                ], 422);
            }

            $stok = mStokBahan::find($id);
            $id_bahan = $stok->id_bahan;
            mStokBahan::where('id', $id)->delete();

            $last_stok = mStokBahan::where('id_bahan', $id_bahan)->sum('qty');
            $last_stok_total = mStokBahan::sum('qty');

            $data_arus = [
                'tgl' => date('Y-m-d H:i:s'),
                'table_id' => $id,
                'table_name' => 'tb_stok_bahan',
                'id_stok_bahan' => $id,
                'id_bahan' => $id_bahan,
                'stok_in' => 0,
                'stok_out' => $stok->qty,
                'last_stok' => $last_stok,
                'last_stok_total' => $last_stok_total,
                'keterangan' => 'Dihapus dari stok bahan / Inventory/Logistik',
                'method' => 'delete'
            ];
            mArusStokBahan::create($data_arus);

            DB::commit();

        } catch(\Exception $exception) {
            throw $exception;

            DB::rollback();
        }


    }

    function update(Request $request, $id_stok_bahan)
    {
        $request->validate([
            'qty' => 'required',
            //'id_lokasi'=>['required', new StokBahanLokasiUpdate($id_stok_bahan, $request->input('id_lokasi'))]
            'id_lokasi' => 'required',
            'no_seri_bahan' => 'required',
        ]);


        DB::beginTransaction();
        try {

            $data_stok = $request->except("_token");

            $stok_bahan_before = mStokBahan::find($id_stok_bahan);
            mStokBahan::where(['id' => $id_stok_bahan])->update($data_stok);

            //return response($response, 422);

            if($stok_bahan_before->qty > $data_stok['qty']) {
                $stok_in = 0;
                $stok_out = $stok_bahan_before->qty - $data_stok['qty'];
            } else {
                $stok_in = $data_stok['qty'] - $stok_bahan_before->qty;
                $stok_out = 0;
            }

            $id_bahan = mStokBahan::where('id', $id_stok_bahan)->value('id_bahan');
            $last_stok = mStokBahan::where('id_bahan', $id_bahan)->sum('qty');
            $last_stok_total = mStokBahan::sum('qty');

            $data_arus = [
                'tgl' => date('Y-m-d H:i:s'),
                'table_id' => $id_stok_bahan,
                'table_name' => 'tb_stok_bahan',
                'id_stok_bahan' => $id_stok_bahan,
                'id_bahan' => $id_bahan,
                'stok_in' => $stok_in,
                'stok_out' => $stok_out,
                'last_stok' => $last_stok,
                'last_stok_total' => $last_stok_total,
                'keterangan' => 'Update Stok : '.$data_stok['keterangan'],
                'method' => 'update'
            ];
            mArusStokbahan::create($data_arus);

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

    }
}
