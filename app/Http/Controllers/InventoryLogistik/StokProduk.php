<?php

namespace app\Http\Controllers\InventoryLogistik;

use app\Models\mHistoryPenyesuaianBahan;
use app\Models\mHistoryPenyesuaianProduk;
use app\Models\mHistoryTransferProduk;
use app\Models\mPenjualanProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use app\Helpers\hProduk;

use app\Rules\StokProdukLokasiInsert;
use app\Rules\StokProdukLokasiUpdate;

use app\Models\mKategoriProduk;
use app\Models\mStokProduk;
use app\Models\mArusStokProduk;
use app\Models\mLokasi;
use app\Models\mProduk;

use DB;

class StokProduk extends Controller
{
    private $breadcrumb;
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => route('produkPage')
            ],
            [
                'label' => $cons['inventory_2'],
                'route' => route('produkPage')
            ],
            [
                'label' => $cons['inventory_14'],
                'route' => ''
            ]
        ];
    }


    function index($idProduk)
    {
        $idProduk = Main::decrypt($idProduk);
        $menuActive = $this->cons['inventory_2'];
        $data = Main::data($this->breadcrumb, $menuActive);
        $list = mStokProduk
            ::where([
                'id_produk' => $idProduk,
            ])
            ->where('qty', '>', 0)
            ->orderBy('id', 'DESC')
            ->get();
        $produk = mProduk::find($idProduk);
        $pageTitle = $produk->kode_produk . ' ' . $produk->nama_produk;
        $lokasi = mLokasi::where('tipe', 'gudang')->orderBy('lokasi', 'ASC')->get();

        $data['idProduk'] = $idProduk;
        $data['list'] = $list;
        $data['pageTitle'] = $pageTitle;
        $data['lokasi'] = $lokasi;
        $data['produk'] = $produk;
        $data['total'] = 0;

        return view('inventoryLogistik/stokProduk/stokProdukList', $data);
    }

    function insert(Request $request, $idProduk)
    {
        $request->validate([
            'no_seri_produk' => 'required',
            'qty' => 'required',
            'hpp' => 'required',
            'id_lokasi' => ['required']
        ]);

        DB::beginTransaction();
        try {

            $month = date('m');
            $year = date('y');
            $id_produk = $idProduk;
            $id_lokasi = $request->input('id_lokasi');
            $no_seri_produk = $request->input('no_seri_produk');
            $urutan = hProduk::urutan_produk($month, $year, $id_produk, $id_lokasi);
            $qty = $request->input('qty');
            $last_stok = $qty;

            $data_stok = $request->except(['_token']);
            $data_stok = array_merge($data_stok, [
                'id_produk' => $id_produk,
                'qty' => $qty,
                'last_stok' => $last_stok,
                'no_seri_produk' => $no_seri_produk,
                'urutan' => $urutan,
                'month' => $year,
                'year' => $month
            ]);

            $response = mStokProduk::create($data_stok);
            $id_stok = $response->id;
            $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
            $last_stok_total = mStokProduk::sum('qty');

            $data_arus_produk = [
                'tgl' => date('Y-m-d'),
                'table_id' => $id_stok,
                'table_name' => 'tb_stok_produk',
                'id_stok_produk' => $id_stok,
                'id_produk' => $id_produk,
                'stok_in' => $data_stok['qty'],
                'stok_out' => 0,
                'last_stok' => $last_stok,
                'last_stok_total' => $last_stok_total,
                'keterangan' => 'Insert Stok : ' . $data_stok['keterangan'],
                'method' => 'insert'
            ];

            mArusStokProduk::create($data_arus_produk);

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();

        }

    }

    function delete($id)
    {
        DB::beginTransaction();
        try {

            $allow_delete = TRUE;
            $table_delete = '';
            $message = '';
            $tables = [
                'arus_stok_produk',
                'penjualan_produk',
                'history_transfer_produk',
                'history_penyesuaian_produk'
            ];

            $arus_stok_produk = mArusStokProduk::where('id_stok_produk', $id)->count();
            $penjualan_produk = mPenjualanProduk::where('id_stok_produk', $id)->count();
            $history_transfer_produk = mHistoryTransferProduk::where('id_stok_produk', $id)->count();
            $history_penyesuaian_produk = mHistoryPenyesuaianProduk::where('id_stok_produk', $id)->count();

            foreach ($tables as $table) {
                if (${$table} > 0) {
                    $allow_delete = FALSE;
                    $table_delete = $table;
                }
            }

            switch($table_delete) {
                case 'arus_stok_produk':
                    $message = 'Stok Produk ini sudah ada di Arus Stok Produk. Sehingga tidak bisa dihapus.';
                    break;
                case 'penjualan_produk':
                    $message = 'Stok Produk ini sudah ada di Penjualan Produk. Sehingga tidak bisa dihapus.';
                    break;
                case 'history_transfer_produk':
                    $message = 'Stok Produk ini sudah ada di History Transfer Produk. Sehingga tidak bisa dihapus';
                    break;
                case 'history_penyesuaian_produk':
                    $message = 'Stok Produk ini sudah ada di History Penyesuaian Produk. Sehingga tidak bisa dihapus';
                    break;
            }

            if (!$allow_delete) {
                return response([
                    'message' => $message
                ], 422);
            }


            $stok = mStokProduk::find($id);
            mStokProduk::where('id', $id)->delete();
            $last_stok = mStokProduk::where('id_produk', $stok->id_produk)->sum('qty');
            $last_stok_total = mStokProduk::sum('qty');
            $data_arus = [
                'tgl' => date('Y-m-d'),
                'table_id' => $id,
                'table_name' => 'tb_stok_produk',
                'id_stok_produk' => $id,
                'id_produk' => $stok->id_produk,
                'stok_in' => 0,
                'stok_out' => $stok->qty,
                'last_stok' => $last_stok,
                'last_stok_total' => $last_stok_total,
                'keterangan' => 'Dihapus dari stok produk / Inventory/Logistik',
                'method' => 'delete'
            ];
            mArusStokProduk::create($data_arus);


            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }


    }

    function update(Request $request, $id_stok)
    {
        $request->validate([
            'no_seri_produk' => 'required',
            'qty' => 'required',
            'hpp' => 'required',
            'id_lokasi' => ['required']
        ]);

        DB::beginTransaction();
        try {

            $id_produk = mStokProduk::where('id', $id_stok)->value('id_produk');
            $stok_produk_before = mStokProduk::where('id', $id_stok)->first();
            $data_stok = $request->except(["_token", 'urutan']);
            $month = date('m');
            $year = date('Y');
            $urutan = hProduk::urutan_produk($month, $year, $id_produk, $data_stok['id_lokasi']);

            if ($stok_produk_before->qty > $data_stok['qty']) {
                $stok_in = 0;
                $stok_out = $stok_produk_before->qty - $data_stok['qty'];
            } else {
                $stok_in = $data_stok['qty'] - $stok_produk_before->qty;
                $stok_out = 0;
            }

            $data_stok = array_merge($data_stok, [
                'urutan' => $urutan,
                'month' => $month,
                'year' => $year
            ]);

            mStokProduk::where(['id' => $id_stok])->update($data_stok);

            if ($stok_in != 0 && $stok_out != 0) {
                $last_stok = mStokProduk::where('id_produk', $id_produk)->sum('qty');
                $last_stok_total = mStokProduk::sum('qty');

                $data_arus = [
                    'tgl' => date('Y-m-d'),
                    'table_id' => $id_stok,
                    'table_name' => 'tb_stok_produk',
                    'id_stok_produk' => $id_stok,
                    'id_produk' => $id_produk,
                    'stok_in' => $stok_in,
                    'stok_out' => $stok_out,
                    'last_stok' => $last_stok,
                    'last_stok_total' => $last_stok_total,
                    'keterangan' => 'Update Stok : ' . $data_stok['keterangan'],
                    'method' => 'update'
                ];

                mArusStokProduk::create($data_arus);
            }

            //return response($response, 422);

            DB::commit();

        } catch (\Exception $exception) {
            throw  $exception;

            DB::rollback();
        }


    }

    /**
     * Jika user mengupdate stok produk yang ada,
     * Jika lokasi / gudang sama, maka tidak mengganti no seri produk
     * Jika beda, makan akan generate no seri produk baru
     *
     * @param $id_produk
     * @param $id_lokasi
     * @param $id_stok
     * @return array
     */
    function data_seri_produk_update($id_produk, $id_lokasi, $id_stok)
    {
        $stok = mStokProduk::find($id_stok);

        if ($stok->id_lokasi == $id_lokasi) {
            $month = $stok->month;
            $year = $stok->year;
            $urutan = $stok->urutan;
            $no_seri_produk = $stok->no_seri_produk;
        } else {
            $month = date('m');
            $year = date('y');
            $urutan = hProduk::urutan_produk($month, $year, $id_produk, $id_lokasi);
            $no_seri_produk = hProduk::no_seri_produk($month, $year, $id_produk, $id_lokasi);
        }

        return [
            'month' => $month,
            'year' => $year,
            'urutan' => $urutan,
            'no_seri_produk' => $no_seri_produk,
        ];
    }
}
