$(document).ready(function () {

    select_ekspedisi();
    proses_ekspedisi();
    change_jenis_transaksi();

    kondisi_tombol_submit();

    select_distributor();
    add_row_produk();
    produk_stok();
    change_biaya_tambahan();
    change_potongan_akhir();

    add_row_pembayaran();


    ////// Saat edit ////////

    btn_search_produk();
    delete_row_produk();

    change_gudang();
    change_qty();
    change_potongan();
    change_ppn_nominal();
    change_jumlah();

    delete_row_pembayaran();

    footer_hide();
});

var base_url = $('#base-value').data('base-url');
var routePenjualanProdukStok = $('#base-value').data('route-penjualan-produk-stok');
var routePenjualanProdukGudang = $('#base-value').data('route-penjualan-produk-gudang');
var routePenjualanNoSeriProduk = $('#base-value').data('route-penjualan-no-seri-produk');
var routePenjualanProdukHarga = $('#base-value').data('route-penjualan-produk-harga');
var routePenjualanNoFaktur = $('#base-value').data('route-penjualan-no-faktur');
var _token = $('#base-value').data('csrf-token');
var method_proses = $('#base-value').data('method-proses');

function select_ekspedisi() {
    $('[name="pengiriman"]').change(function() {
        proses_ekspedisi();
    });
}

function proses_ekspedisi() {
    var pengiriman = $('[name="pengiriman"]:checked').val();
    console.log(pengiriman);
    if(pengiriman == 'yes') {
        $('.wrapper-ekspedisi').show();
        $('.wrapper-ekspedisi select').prop('disabled', false);
    } else {
        $('.wrapper-ekspedisi').hide();
        $('.wrapper-ekspedisi select').prop('disabled', true);
    }
}

function select_distributor() {
    $('.datatable-general').on('click', '.select-distributor', function () {

        var id = $(this).data('id');
        var kode_distributor = $(this).data('kode-distributor');
        var nama_distributor = $(this).data('nama-distributor');
        var telepon_distributor = $(this).data('telepon-distributor');
        var alamat_distributor = $(this).data('alamat-distributor');

        $('[name="id_distributor"]').val(id);
        $('[name="kode_distributor"]').val(kode_distributor);
        $('[name="nama_distributor"]').val('('+kode_distributor + ') ' + nama_distributor);
        $('.alamat-distributor').html(alamat_distributor);
        $('.telepon-distributor').html(telepon_distributor);

        $('#modal-distributor').modal('hide');

        proses_no_faktur();
    });
}

function change_jenis_transaksi() {
    $('[name="jenis_transaksi"]').change(function() {
        proses_no_faktur();
    });
}

function proses_no_faktur() {
    var tanggal_transaksi = $('[name="tanggal_transaksi"]').val();
    var id_distributor = $('[name="id_distributor"]').val();
    var kode_distributor = $('[name="kode_distributor"]').val();
    var jenis_transaksi = $('[name="jenis_transaksi"]').val();
    var data_send = {
        tanggal_transaksi: tanggal_transaksi,
        id_distributor: id_distributor,
        kode_distributor: kode_distributor,
        _token: _token
    };

    $('[name="no_faktur"]').val('-');
    $('.no_faktur').html('-');
    $('[name="urutan"]').val('');

    if(jenis_transaksi == 'distributor') {
        $.ajax({
            beforeSend: loading_start(),
            url: routePenjualanNoFaktur,
            type: 'post',
            data: data_send,
            success: function(data) {

                var no_faktur = data.no_faktur;
                var urutan = data.urutan;

                $('[name="no_faktur"]').val(no_faktur);
                $('.no_faktur').html(no_faktur);
                $('[name="urutan"]').val(urutan);

                loading_finish();
            }
        })
    }
}

function add_row_produk() {
    $('.btn-add-row-produk').click(function () {
        var row_produk = $('.row-produk tbody').html();
        $('.table-produk tbody').append(row_produk);

        var index = $('.table-produk tbody tr').length;

        $('.table-produk tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-produk tbody tr:nth-child(' + index + ') .touchspin-penjualan').TouchSpin(touchspin_number);
        $('.table-produk tbody tr:nth-child(' + index + ') .select2-penjualan').select2();

        btn_search_produk();
        delete_row_produk();
        footer_hide();
    });
}

function delete_row_produk() {
    $('.btn-delete-row-produk').click(function () {
        $(this).parents('tr').remove();
        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
        proses_kembalian();

        if(method_proses == 'edit') {
            var id_penjualan_produk = $(this).parents('tr').data('id-penjualan-produk');
            var id_penjualan_produk_delete = $('[name="id_penjualan_produk_delete"]').val();
            var id_penjualan_produk_val = id_penjualan_produk+','+id_penjualan_produk_delete;

            $('[name="id_penjualan_produk_delete"]').val(id_penjualan_produk_val);
        }
    });
}

function produk_stok() {
    $('.btn-produk-stok').click(function () {
        var id_produk = $(this).parents('tr').data('id');
        $.ajax({
            url: routePenjualanProdukStok,
            type: 'POST',
            data: {
                id_produk: id_produk,
                _token: _token
            },
            success: function (view) {
                $('#modal-produk-stok .modal-body').html(view);
                $('#modal-produk-stok').modal('show');
            }
        });
    });
}

function btn_search_produk() {
    $('.btn-search-produk').click(function () {
        $('#modal-produk').modal('show');
        var index = $(this).parents('tr').data('index');
        $('#base-value').data('index', index);
        btn_select_produk();
    });
}

function btn_select_produk() {
    $('.datatable-general').on('click', '.btn-select-produk', function () {
        var index = $('#base-value').data('index');

        var id_produk = $(this).parents('tr').data('id');
        var kode_produk = $(this).parents('tr').data('kode-produk');
        var nama_produk = $(this).parents('tr').data('nama-produk');


        $('.table-produk tbody tr:nth-child(' + index + ') td.produk-nama').html(kode_produk + ' ' + nama_produk);
        $('.table-produk tbody tr:nth-child(' + index + ') .id_produk').val(id_produk);

        enabled_select_gudang(index);

        proses_gudang(index, id_produk);
        proses_no_seri_produk(index);
        proses_harga(index);
        proses_ppn(index);
        proses_harga_net(index);
        proses_sub_total(index);

        proses_total();
        proses_grand_total();

        change_gudang();
        change_qty();
        //change_no_seri_produk();
        change_potongan();
        change_ppn_nominal();

        proses_sisa_pembayaran();
        proses_kembalian();

        //self.parents('td').siblings('td.produk-nama').html(kode_produk+' '+nama_produk);
        //self.parents('td').siblings('td.data').children('[name="id_produk[]"]').val(id_produk);

        $('#modal-produk').modal('hide');

    });
}

function enabled_select_gudang(index) {
    $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').removeAttr('disabled');
    $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').val("").trigger('change');
}

function change_gudang() {
    $('.id_lokasi').change(function () {
        var index = $(this).parents('tr').data('index');
        //alert(index);
        proses_no_seri_produk(index);
/*        proses_harga(index);
        proses_ppn(index);
        proses_harga_net(index);
        proses_sub_total(index);*/
    });
}

function change_no_seri_produk(index) {
    $('.id_stok_produk').change(function () {
/*        proses_harga(index);
        proses_ppn(index);
        proses_harga_net(index);
        proses_sub_total(index);*/
    });
}

function change_qty() {
    $('.qty').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');

        proses_harga(index);
        proses_ppn(index);
        proses_harga_net(index);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_potongan() {
    $('.potongan').on('change keyup', function () {

        var index = $(this).parents('tr').data('index');

        proses_ppn(index);
        proses_harga_net(index);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_ppn_nominal() {
    $('.ppn_nominal').on('change keyup', function () {

        var index = $(this).parents('tr').data('index');

        proses_harga_net(index);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_biaya_tambahan() {
    $('[name="biaya_tambahan"]').on('change keyup', function() {
        proses_grand_total();
    });
}

function change_potongan_akhir() {
    $('[name="potongan_akhir"]').on('change keyup', function() {
        proses_grand_total();
    });
}

function proses_gudang(index, id_produk) {
    var data_send = {
        id_produk: id_produk,
        _token: _token
    };

    console.log(data_send);

    $.ajax({
        url: routePenjualanProdukGudang,
        type: 'post',
        data: data_send,
        success: function(data) {
            $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').html('');
            $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi')
                .append($("<option></option>")
                    .attr("value", "")
                    .text('Pilih Gudang'));
            $.each(data, function (key, value) {
                $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi')
                    .append($("<option></option>")
                        .attr("value", value.id_lokasi)
                        .text(value.lokasi.lokasi));
            });
        }
    });
}

function proses_no_seri_produk(index) {
    var id_lokasi = $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').val();
    var id_produk = $('.table-produk tbody tr:nth-child(' + index + ') .id_produk').val();

    $.ajax({
        url: routePenjualanNoSeriProduk,
        type: 'post',
        data: {
            _token: _token,
            id_lokasi: id_lokasi,
            id_produk: id_produk
        },
        success: function (data) {
            $('.table-produk tbody tr:nth-child(' + index + ') .id_stok_produk').html('');
            $.each(data, function (key, value) {
                $('.table-produk tbody tr:nth-child(' + index + ') .id_stok_produk')
                    .append($("<option></option>")
                        .attr("value", value.id)
                        .text(value.no_seri_produk+' : Jumlah stok = '+format_number(value.qty)));
            });
        }
    });
}

function proses_harga(index) {
    var id_lokasi = $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').val();
    var id_produk = $('.table-produk tbody tr:nth-child(' + index + ') .id_produk').val();
    var qty = $('.table-produk tbody tr:nth-child(' + index + ') .qty').val();
    var data_send = {
        _token: _token,
        id_lokasi: id_lokasi,
        id_produk: id_produk,
        qty: qty
    };

    $.ajax({
        url: routePenjualanProdukHarga,
        type: "post",
        data: data_send,
        success: function (data) {
            $('.table-produk tbody tr:nth-child(' + index + ') .harga').val(data.number_raw);
            $('.table-produk tbody tr:nth-child(' + index + ') .td-harga').html(data.number_format);

            proses_ppn(index);
            proses_harga_net(index);
            proses_sub_total(index);
            proses_total();
            proses_grand_total();
        }
    });
}

function proses_ppn(index) {
    var ppn_persen = $('#base-value').data('ppn-persen');
    var harga = $('.table-produk tbody tr:nth-child(' + index + ') .harga').val();
    var potongan = $('.table-produk tbody tr:nth-child(' + index + ') .potongan').val();

    var ppn = (parseFloat(harga) - parseFloat(potongan)) * parseFloat(ppn_persen);
    //ppn = ppn.toFixed(0);
    //var ppn_format = format_number(ppn);


    $('.table-produk tbody tr:nth-child(' + index + ') .ppn_nominal').val(ppn);
    //$('.table-produk tbody tr:nth-child(' + index + ') .td-ppn').html(ppn_format);
}

function proses_harga_net(index) {
    var harga = $('.table-produk tbody tr:nth-child(' + index + ') .harga').val();
    var potongan = $('.table-produk tbody tr:nth-child(' + index + ') .potongan').val();
    var ppn_nominal = $('.table-produk tbody tr:nth-child(' + index + ') .ppn_nominal').val();

    var harga_net = parseFloat(harga) - parseFloat(potongan) + parseFloat(ppn_nominal);

    console.log(harga, potongan, ppn_nominal, harga_net);


    var harga_net_format = format_number(harga_net);

    $('.table-produk tbody tr:nth-child(' + index + ') .harga_net').val(harga_net);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-harga-net').html(harga_net_format);

}

function proses_sub_total(index) {
    var harga_net = $('.table-produk tbody tr:nth-child(' + index + ') .harga_net').val();
    var qty = $('.table-produk tbody tr:nth-child(' + index + ') .qty').val();
    var sub_total = parseFloat(harga_net) * parseFloat(qty);
    var sub_total_format = format_number(sub_total);

    $('.table-produk tbody tr:nth-child(' + index + ') .sub_total').val(sub_total);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-sub-total').html(sub_total_format);
}

function proses_total() {
    var total = 0;
    $('.table-produk .sub_total').each(function () {
        var sub_total = $(this).val();
        total = parseFloat(total) + parseFloat(sub_total);
    });

    $('[name="total"]').val(total);
    $('.total').html(format_number(total));
}

function proses_grand_total() {
    var total = $('[name="total"]').val();
    var biaya_tambahan = $('[name="biaya_tambahan"]').val();
    var potongan_akhir = $('[name="potongan_akhir"]').val();
    var grand_total = parseFloat(total) + parseFloat(biaya_tambahan) - parseFloat(potongan_akhir);

    //console.log(total+' '+biaya_tambahan+' '+potongan_akhir+' '+grand_total);

    if(!isNaN(total)) {
        $('[name="grand_total"]').val(grand_total);
        $('.grand-total').html(format_number(grand_total));
    }
}

function footer_hide() {
    $('footer').hide();
}

function kondisi_tombol_submit() {
    $('.btn-simpan').hide();
    $('.btn-selanjutnya').click(function() {
        $(this).hide();
        $('.btn-simpan').show();
    });
    $('.btn-modal-pembayaran-tutup').click(function() {
        $('.btn-simpan').hide();
        $('.btn-selanjutnya').show();
    });

    $('.btn-kembali').click(function(e) {
        e.preventDefault();
        if($('#modal-pembayaran').hasClass('show')) {
            $('#modal-pembayaran').modal('hide');
            $('.btn-simpan').hide();
            $('.btn-selanjutnya').show();
        } else {
            var href = $(this).attr('href');
            window.location.href = href;
        }

        return false;
    });
}

////////////////////// Modal Pembayaran //////////////////////


function add_row_pembayaran() {
    $('.btn-add-row-pembayaran').click(function () {
        var row_pembayaran = $('.row-pembayaran tbody').html();
        $('.table-pembayaran tbody').append(row_pembayaran);

        var index = $('.table-pembayaran tbody tr').length;

        $('.table-pembayaran tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .touchspin-pembayaran').TouchSpin(touchspin_number);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .datepicker-pembayaran').datepicker({
            rtl: mUtil.isRTL(),
            todayHighlight: !0,
            orientation: "bottom left",
            format: 'dd-mm-yyyy'
        });
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .select2-pembayaran').select2();

        change_pembayaran();
        change_jumlah();
        delete_row_pembayaran();
    });
}

function delete_row_pembayaran() {
    $('.btn-delete-row-pembayaran').click(function () {
        $(this).parents('tr').remove();

        proses_terbayar();
        proses_sisa_pembayaran();
        proses_kembalian();
    });
}

function change_pembayaran() {
    $('.master_id').change(function() {
        proses_jumlah($(this));
        proses_terbayar();
        proses_sisa_pembayaran();
    });
}

function change_jumlah() {
    $('.jumlah').on('change keyup', function() {
        proses_jumlah($(this));
    });
}

function proses_jumlah(self) {
    var grand_total = $('[name="grand_total"]').val();
    var jumlah_all = 0;
    var jumlah = grand_total;
    $('.jumlah').each(function() {
        jumlah_all = parseFloat(jumlah_all) + parseFloat($(this).val());
    });

    //console.log(jumlah_all+' '+grand_total);

    if(jumlah_all <= grand_total) {
        jumlah = grand_total - jumlah_all;
    }

    self.parents('td').siblings('td.td-jumlah').children('div').children('.jumlah').val(jumlah);



    proses_terbayar();
}

function proses_terbayar() {
    var terbayar = 0;
    $('.jumlah').each(function() {
        var jumlah = $(this).val();
        terbayar = parseFloat(terbayar) + parseFloat(jumlah);
    });

    $('.terbayar').html(format_number(terbayar));
    $('[name="terbayar"]').val(terbayar);

    proses_sisa_pembayaran();
    proses_kembalian();
}

function proses_sisa_pembayaran() {
    var grand_total = $('[name="grand_total"]').val();
    var terbayar = $('[name="terbayar"]').val();

    grand_total = parseFloat(grand_total);
    terbayar = parseFloat(terbayar);

    var sisa = grand_total - terbayar;

    if(terbayar > grand_total) {
        sisa = 0;
    }


    $('.sisa-pembayaran').html(format_number(sisa));
    $('[name="sisa_pemabayaran"]').val(sisa);
}

function proses_kembalian() {
    var grand_total = $('[name="grand_total"]').val();
    var terbayar = $('[name="terbayar"]').val();

    grand_total = parseFloat(grand_total);
    terbayar = parseFloat(terbayar);

    var kembalian = terbayar - grand_total;

    if(terbayar <=  grand_total) {
        kembalian = 0;
    }

    $('.kembalian').html(format_number(kembalian));
    $('[name="kembalian"]').val(kembalian);
}