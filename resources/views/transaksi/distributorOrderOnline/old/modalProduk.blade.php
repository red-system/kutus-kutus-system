<div class="modal" id="modal-produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-accent">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_4">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Total Stok</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php($no = 1)
                        @foreach($produk as $r)
                            <tr data-id="{{ $r->id }}"
                                data-kode-produk="{{ $r->kode_produk }}"
                                data-nama-produk="{{ $r->nama_produk }}">
                                <td width="10">{{ $no++ }}.</td>
                                <td>{{ $r->kode_produk }}</td>
                                <td>{{ $r->nama_produk }}</td>
                                <td>{{ Main::format_number($r->stok_produk) }}</td>
                                <td width="100">
                                    <div class="btn-group m-btn-group m-btn-group--pill">
                                        <button type="button"
                                                class="btn-produk-stok btn btn-info btn-sm">
                                            <i class="la la-check"></i> Stok
                                        </button>
                                        <button type="button"
                                                class="btn-select-produk btn btn-success btn-sm">
                                            <i class="la la-check"></i> Pilih
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>