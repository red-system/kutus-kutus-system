<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mStokProduk extends Model
{

    protected $table = 'tb_stok_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produk',
        'id_lokasi',
        'id_produksi',
        'id_progress_produksi',
        'month',
        'year',
        'urutan',
        'no_seri_produk',
        'qty',
        'hpp',
        'last_stok',
        'keterangan'
    ];

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function produk() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }
}
