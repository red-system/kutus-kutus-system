<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;

use app\Models\mBahan;
use app\Models\mStokBahan;
use app\Models\mLokasi;

class StokBahanLokasiInsert implements Rule
{
    private $id_bahan;
    private $id_lokasi;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_bahan, $id_lokasi)
    {
        $this->id_bahan = $id_bahan;
        $this->id_lokasi = $id_lokasi;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id_lokasi = $value;
        $check = mStokBahan
            ::where([
                'id_lokasi'=>$id_lokasi,
                'id_bahan'=>$this->id_bahan
            ])
            ->count();

        if($check == 0) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $bahan = mBahan::find($this->id_bahan);
        $lokasi = mLokasi::find($this->id_lokasi);
        return 'Bahan <strong>'.$bahan->nama_bahan.'</strong> sudah ada pada Gudang <strong>'.$lokasi->lokasi.'</strong>';
    }
}
