$(document).ready(function() {

    modal_pembayaran();
    add_row_pembayaran();

});

function modal_pembayaran() {
    $('.datatable-general').on('click', '.btn-pembayaran',function() {
        var route = $(this).data('route');
        var row = $(this).parents('div').siblings('textarea').val();
        var json = JSON.parse(row);

        $('#modal-pembayaran').modal('show');
        $('#modal-pembayaran form').attr('action', route);

        $.each(json, function (key, val) {
            var type = $('[name="' + key + '"]').attr('type');
            if (type == 'file') {
                $('#modal-pembayaran .img-preview').attr('src', base_url + '/upload/' + val);
            } else if (type == 'radio') {
                $('#modal-pembayaran [name="' + key + '"][value=' + val + ']').attr('checked', 'checked');
            } else {
                $('#modal-pembayaran [name="' + key + '"]').val(val);
                $('#modal-pembayaran [name="' + key + '"]').trigger('change');
                $('#modal-pembayaran .' + key).html(val);
            }
        });
    });
}

function add_row_pembayaran() {
    $('.btn-add-row-pembayaran').click(function () {
        var row_pembayaran = $('.row-pembayaran tbody').html();
        $('.table-pembayaran tbody').append(row_pembayaran);

        var index = $('.table-pembayaran tbody tr').length;

        $('.table-pembayaran tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .touchspin-pembayaran').TouchSpin(touchspin_number);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .datepicker-pembayaran').datepicker({
            rtl: mUtil.isRTL(),
            todayHighlight: !0,
            orientation: "bottom left",
            format: 'dd-mm-yyyy'
        });
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .select2-pembayaran').select2();

        change_pembayaran();
        change_jumlah();
        delete_row_pembayaran();
    });
}

function delete_row_pembayaran() {
    $('.btn-delete-row-pembayaran').click(function () {
        $(this).parents('tr').remove();

        proses_terbayar();
        proses_sisa_pembayaran();
        proses_kembalian();
    });
}

function change_pembayaran() {
    $('[name="master_id[]"]').change(function() {
        proses_jumlah($(this));
        proses_terbayar();
        proses_sisa_pembayaran();
    });
}

function change_jumlah() {
    $('[name="jumlah[]"]').on('change keyup', function() {
        proses_jumlah($(this));
        proses_sisa_pembayaran();
    });
}

function proses_jumlah(self) {
    var total = $('[name="total"]').val();
    var jumlah_all = 0;
    var jumlah = total;
    $('[name="jumlah[]"]').each(function() {
        jumlah_all = parseInt(jumlah_all) + parseInt($(this).val());
    });

    //console.log(jumlah_all+' '+grand_total);

    if(jumlah_all <= total) {
        jumlah = total - jumlah_all;
    }

    self.parents('td').siblings('td.td-jumlah').children('div').children('[name="jumlah[]"]').val(jumlah);

    proses_terbayar();
}

function proses_terbayar() {
    var terbayar = 0;
    $('[name="jumlah[]"]').each(function() {
        var jumlah = $(this).val();
        terbayar = parseInt(terbayar) + parseInt(jumlah);
    });

    $('.terbayar').html(format_number(terbayar));
    $('[name="terbayar"]').val(terbayar);

    proses_sisa_pembayaran();
    proses_kembalian();
}

function proses_sisa_pembayaran() {
    var total = $('[name="total"]').val();
    var terbayar = $('[name="terbayar"]').val();

    total = parseInt(total);
    terbayar = parseInt(terbayar);

    var sisa = total - terbayar;

    if(terbayar > total) {
        sisa = 0;
    }


    $('.sisa-pembayaran').html(format_number(sisa));
    $('[name="sisa_pemabayaran"]').val(sisa);
}

function proses_kembalian() {
    var total = $('[name="total"]').val();
    var terbayar = $('[name="terbayar"]').val();

    total = parseInt(total);
    terbayar = parseInt(terbayar);

    var kembalian = terbayar - total;

    if(terbayar <=  total) {
        kembalian = 0;
    }


    $('.kembalian').html(format_number(kembalian));
    $('[name="kembalian"]').val(kembalian);
}