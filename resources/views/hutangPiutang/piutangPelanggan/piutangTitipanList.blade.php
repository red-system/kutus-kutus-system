@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/piutang_pelanggan.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('hutangPiutang.piutangPelanggan.modalPembayaranCreate')
    @include('hutangPiutang.piutangPelanggan.trPembayaran')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">
                @include('hutangPiutang.piutangPelanggan.tabTitle')
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20"> No</th>
                            <th> No Invoice Piutang</th>
                            <th> Jatuh Tempo</th>
                            <th> No Faktur Penjualan</th>
                            <th> Tanggal Faktur Penjualan</th>
                            <th> Nama Distributor</th>
                            <th> Sisa</th>
                            <th> Keterangan</th>
                            <th> Status</th>
                            <th width="100"> Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total = 0;
                        @endphp
                        @foreach($piutang_titipan as $r)


                            @php
                                $r['fullname_distributor'] = $r->distributor->kode_distributor.' - '.$r->distributor->nama_distributor;
                                $r['kode_distributor'] = $r->distributor->kode_distributor;
                                $r['nama_distributor'] = $r->distributor->nama_distributor;
                                $r['tanggal_piutang'] = Main::format_date($r->tanggal_piutang);
                                $r['pp_jatuh_tempo'] = Main::format_date($r->pp_jatuh_tempo);

                                $r['total'] = round($r->pp_sisa_amount);
                                $r['sisa_pembayaran'] = round($r->pp_sisa_amount);
                                $r['pp_amount'] = round($r->pp_amount);
                                $r['pp_sisa_amount'] = round($r->pp_sisa_amount);
                                $r['grand-total'] = Main::format_number($r->pp_sisa_amount);
                                $r['sisa-pembayaran'] = Main::format_number($r->pp_sisa_amount);

                                $total += $r->pp_sisa_amount;
                            @endphp

                            <tr>
                                <td> {{ $no++ }}.</td>
                                <td> {{ $r->no_piutang_pelanggan }} </td>
                                <td> {{ Main::format_date($r->pp_jatuh_tempo) }} </td>
                                <td> {{ $r->pp_no_faktur }} </td>
                                <td> {{ Main::format_date($r->tanggal_piutang) }} </td>
                                <td> {{ $r->distributor->nama_distributor}}</td>
                                <td> {{ Main::format_money($r->pp_sisa_amount) }} </td>
                                <td> {{ $r->pp_keterangan }} </td>
                                <td> {!! Main::hutang_lain_status($r->pp_status) !!} </td>
                                <td>
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                        <button type="button"
                                                name="btn-cart"
                                                class="m-btn btn btn-success"
                                                href="{{ route('piutangPelangganPrint', ['kode'=>$r->pp_invoice]) }}">
                                            <i class="la la-print"></i> Print
                                        </button>
                                        <button type="button"
                                                class="m-btn btn btn-primary btn-pembayaran"
                                                data-route="{{ route('piutangPelangganPembayaran', ['id'=>$r->id]) }}">
                                            <i class="la la-money"></i> Pembayaran
                                        </button>
                                    </div>
                                    <!-- </div> -->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="8" align="right">
                                    <strong>Total</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_money($total) }}</strong>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection