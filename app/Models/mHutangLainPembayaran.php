<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mHutangLainPembayaran extends Model
{
    protected $table = 'tb_hutang_lain_pembayaran';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_hutang_lain',
        'master_id',
        'jumlah',
        'no_check_bg',
        'tanggal_pencairan',
        'nama_bank',
        'keterangan',
    ];

    function hutang_lain()
    {
        return $this->belongsTo(mHutangLain::class, 'id_hutang_lain');
    }

    function master()
    {
        return $this->belongsTo(mAcMaster::class, 'master_id');
    }

    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }
}
