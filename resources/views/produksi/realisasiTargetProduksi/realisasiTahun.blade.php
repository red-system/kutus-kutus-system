@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("realisasiTargetProduksiPage") }}"
                       class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Daftar Tahun</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-lg-1 col-form-label">Bulan</label>
                            <div class="col-lg-2">
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input m_datepicker-year-month"
                                           name="bulan" readonly value="{{ $bulan }}"/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <label for="example-text-input" class="col-lg-1 col-form-label">Produk</label>
                            <div class="col-lg-3">
                                <select class="form-control m-input m-select2" name="id_produk">
                                    <option value="all" {{ $id_produk == 'all' ? 'selected' : '' }}><S>Semua Produk</S></option>
                                    @foreach($produk as $r)
                                        <option value="{{ $r->id }}" {{ $r->id == $id_produk ? 'selected' : '' }}>{{ $r->kode_produk.' '.$r->nama_produk }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-1">
                                <button type="submit" class="btn btn-success">Filter Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <input type="hidden" name="tahun_target" value="{{ $tahun }}">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Bulan</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Target Produksi</th>
                            <th>Realisasi Produksi</th>
                            <th>Persentase</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)

                            @php
                                $total_produksi = \app\Models\mProgressProduksi
                                ::whereYear('tgl_selesai', $r->tahun_target)
                                ->where('id_produk', $r->id_produk)
                                ->sum('qty_progress');

                                $persentase = round(($total_produksi / $r->qty) * 100, $roundDecimal);

                                $totalTarget += $r->qty;
                                $totalRealisasi += $total_produksi;
                            @endphp
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->bulan_target }}</td>
                                <td>{{ $r->produk->kode_produk }}</td>
                                <td>{{ $r->produk->nama_produk }}</td>
                                <td align="right">{{ Main::format_number($r->qty) }}</td>
                                <td align="right">{{ Main::format_number($total_produksi) }}</td>
                                <td align="right">{{ $persentase }} %</td>
                            </tr>
                        @endforeach

                        @php
                            $persentase = $totalRealisasi == 0 ? 0 : round(($totalRealisasi / $totalTarget) * 100, 2);
                        @endphp
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4" align="center">
                                <strong>TOTAL</strong>
                            </td>
                            <td align="right">
                                <strong>{{ Main::format_number($totalTarget) }}</strong>
                            </td>
                            <td align="right">
                                <strong>{{ Main::format_number($totalRealisasi) }}</strong>
                            </td>
                            <td align="right">
                                <strong>{{ $persentase }}%</strong>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection