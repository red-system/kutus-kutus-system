<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mUser extends Model
{
    protected $table = 'tb_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_karyawan',
        'id_user_role',
        'username',
        'password',
        'inisial_karyawan',
    ];
    protected $hidden = [
        'password'
    ];

    function karyawan() {
        return $this->belongsTo(mKaryawan::class, 'id_karyawan');
    }

    function user_role() {
        return $this->belongsTo(mUserRole::class, 'id_user_role');
    }
}
