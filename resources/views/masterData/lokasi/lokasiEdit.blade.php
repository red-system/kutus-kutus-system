<form action="" method="post" class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Tipe Lokasi</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="tipe" value="gudang"> Gudang
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="tipe" value="pabrik"> Pabrik
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Lokasi</label>
                            <input type="text" class="form-control m-input" name="kode_lokasi" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Lokasi </label>
                            <input type="text" class="form-control m-input" name="lokasi" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">PIC </label>
                            <input type="text" class="form-control m-input" name="pic">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Telepon </label>
                            <input type="text" class="form-control m-input" name="telp">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Potongan (%)</label>
                            <input type="text" class="form-control touchspin-persen" value="0" name="potongan">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat </label>
                            <textarea class="form-control m-input" name="alamat"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>