$(document).ready(function () {

    add_row_produk();
    produk_stok();

    btn_batal();


    // Edit //


    btn_search_produk();
    delete_row_produk();
    footer_hide();

});

var base_url = $('#base-value').data('base-url');
var routeDistributorOrderProdukStok = $('#base-value').data('route-distributor-order-produk-stok');
var _token = $('#base-value').data('csrf-token');

function add_row_produk() {
    $('.btn-add-row-produk').click(function () {
        var row_produk = $('.row-produk tbody').html();
        $('.table-produk tbody').append(row_produk);

        var index = $('.table-produk tbody tr').length;

        $('.table-produk tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-produk tbody tr:nth-child(' + index + ') .touchspin-penjualan').TouchSpin(touchspin_number);
        $('.table-produk tbody tr:nth-child(' + index + ') .select2-penjualan').select2();

        btn_search_produk();
        delete_row_produk();
        footer_hide();
    });
}

function delete_row_produk() {
    $('.btn-delete-row-produk').click(function () {
        $(this).parents('tr').remove();
    });
}

function produk_stok() {
    $('.btn-produk-stok').click(function () {
        var id_produk = $(this).parents('tr').data('id');
        $.ajax({
            url: routeDistributorOrderProdukStok,
            type: 'POST',
            data: {
                id_produk: id_produk,
                _token: _token
            },
            success: function (view) {
                $('#modal-produk-stok .modal-body').html(view);
                $('#modal-produk-stok').modal('show');
            }
        });
    });
}

function btn_search_produk() {
    $('.btn-search-produk').click(function () {
        $('#modal-produk').modal('show');
        var index = $(this).parents('tr').data('index');
        $('#base-value').data('index', index);

        btn_select_produk();
    });
}

function btn_select_produk() {
    $('.btn-select-produk').click(function () {
        var index = $('#base-value').data('index');

        var id_produk = $(this).parents('tr').data('id');
        var kode_produk = $(this).parents('tr').data('kode-produk');
        var nama_produk = $(this).parents('tr').data('nama-produk');

        console.log(kode_produk+' '+nama_produk);

        $('.table-produk tbody tr:nth-child(' + index + ') td.produk-nama').html('(' + kode_produk + ') ' + nama_produk);
        $('.table-produk tbody tr:nth-child(' + index + ') [name="id_produk[]"]').val(id_produk);

        $('#modal-produk').modal('hide');

    });
}

function btn_batal() {
    $('.btn-batal').click(function (e) {
        var route = $(this).data('route');

        e.preventDefault();
        var self = $(this);

        swal({
            title: "Perhatian ...",
            text: "Batalkan Order Produk ini ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: route,
                    type: "get",
                    data: {
                        _token: _token
                    },
                    success: function () {
                        window.location.reload();
                    }
                });
            }
        });

        return false;
    });
}

function footer_hide() {
    $('footer').hide();
}