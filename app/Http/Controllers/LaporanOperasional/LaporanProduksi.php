<?php

namespace app\Http\Controllers\LaporanOperasional;

use app\Helpers\Main;
use app\Http\Controllers\Controller;
use app\Models\mProduk;
use app\Models\mProduksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use app\Models\mPenjualan;
use app\Models\mPenjualanProduk;
use PDF;

class LaporanProduksi extends Controller
{

    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['distribusi'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporanOperasional'],
                'route' => ''
            ],
            [
                'label' => $cons['laporanO_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');
        $date_end_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $produk = mProduk
            ::withCount([
                'progress_produksi AS total_qty' => function($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty_progress)'))
                        ->whereBetween('tgl_selesai', $where_date);
                }
            ])
            ->orderBy('kode_produk')
            ->get();
        $produksi = mProduksi::whereBetween('tgl_mulai_produksi', $where_date)->orderBy('tgl_mulai_produksi', 'ASC')->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'produk' => $produk,
            'produksi' => $produksi,
            'total' => 0
        ]);

        return view('laporanOperasional/laporanProduksi/laporanProduksiList', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');
        $date_end_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $produk = mProduk
            ::withCount([
                'progress_produksi AS total_qty' => function($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty_progress)'))
                        ->whereBetween('tgl_selesai', $where_date);
                }
            ])
            ->orderBy('kode_produk')
            ->get();
        $produksi = mProduksi::whereBetween('tgl_mulai_produksi', $where_date)->orderBy('tgl_mulai_produksi', 'ASC')->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'produk' => $produk,
            'produksi' => $produksi,
            'total' => 0,
            'company'=> Main::companyInfo()
        ]);

        $pdf = PDF::loadView('laporanOperasional/laporanProduksi/laporanProduksiPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->download('Laporan Produksi ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start_url = $request->input('date_start');
        $date_end_url = $request->input('date_end');
        $date_start_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'ASC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');
        $date_end_db = mProduksi
            ::select('tgl_mulai_produksi')
            ->orderBy('tgl_mulai_produksi', 'DESC')
            ->offset(0)
            ->limit(1)
            ->value('tgl_mulai_produksi');


        $date_start = $date_start_url ? $date_start_url : Main::format_date($date_start_db);
        $date_end = $date_end_url ? $date_end_url : Main::format_date($date_end_db);
        $where_date = [
            Main::format_date_db($date_start),
            Main::format_date_db($date_end)
        ];

        $produk = mProduk
            ::withCount([
                'progress_produksi AS total_qty' => function($query) use ($where_date) {
                    $query->select(DB::raw('SUM(qty_progress)'))
                        ->whereBetween('tgl_selesai', $where_date);
                }
            ])
            ->orderBy('kode_produk')
            ->get();
        $produksi = mProduksi::whereBetween('tgl_mulai_produksi', $where_date)->orderBy('tgl_mulai_produksi', 'ASC')->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'produk' => $produk,
            'produksi' => $produksi,
            'total' => 0,
            'company'=> Main::companyInfo()
        ]);

        return view('laporanOperasional/laporanProduksi/laporanProduksiExcel', $data);
    }

}
