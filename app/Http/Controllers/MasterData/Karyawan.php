<?php

namespace app\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class Karyawan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label'=>$cons['masterData'],
                'route'=>''
            ],
            [
                'label'=>$cons['master_4'],
                'route'=>''
            ]
        ];
    }

    function index() {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mKaryawan::orderBy('nama_karyawan', 'ASC')->get();

        //return $data['userRole'];
        //return Route::currentRouteName();
        //return $data['user_role'];

        return view('masterData/karyawan/karyawanList', $data);
    }

    function insert(Request $request) {
        $request->validate([
            'kode_karyawan' => 'required',
            'nama_karyawan' => 'required',
            'posisi_karyawan' => 'required',
            'telp_karyawan' => 'required',
            'alamat_karyawan' => 'required',
            'email_karyawan' => 'required|email',
            'foto_karyawan'=>'required|image'
        ]);

        $file = $request->file('foto_karyawan');
        $file->move('upload', $file->getClientOriginalName());

        $data = $request->except(['_token','foto_karyawan']);
        $data['foto_karyawan'] = $file->getClientOriginalName();

        mKaryawan::create($data);
    }

    function delete($id) {
        mKaryawan::where('id', $id)->delete();
    }

    function update(Request $request, $id) {
        $request->validate([
            'kode_karyawan' => 'required',
            'nama_karyawan' => 'required',
            'posisi_karyawan' => 'required',
            'telp_karyawan' => 'required',
            'alamat_karyawan' => 'required',
            'email_karyawan' => 'required|email'
        ]);

        $data = $request->except(["_token", "foto_karyawan"]);

        if($request->hasFile('foto_karyawan')) {
            $file = $request->file('foto_karyawan');
            $file->move('upload', $file->getClientOriginalName());
            $data['foto_karyawan'] = $file->getClientOriginalName();
        }

        mKaryawan::where(['id'=>$id])->update($data);
    }
}
