<?php

namespace app\Http\Controllers\Transaksi;

use app\Models\mDistributor;
use DB;

use app\Helpers\Main;
use app\Models\mProduk;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Models\mDistributorOrder;
use app\Models\mDistributorOrderProduk;
use Illuminate\Support\Facades\Session;
use app\Models\mPenjualan;

use app\Models\mStokProduk;

class OrderOnline extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['transaksi_3'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $id_distributor = Session::get('user.id');
        $list = mDistributorOrder
            ::where([
                'id_distributor' => $id_distributor
            ])
            ->whereIn('status', ['order', 'process'])
            ->withCount([
                'distributor_order_produk AS total_qty' => function ($query) {
                    $query->select(DB::raw("SUM(qty_order) AS qty_sum"));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'list'
        ]);

        return view('transaksi/orderOnline/orderOnlineList', $data);
    }

    function detail_order_baru($id)
    {
        $id_distributor_order = Main::decrypt($id);
        $distributor_order = mDistributorOrder
            ::with('distributor')
            ->where('id', $id_distributor_order)
            ->first();
        $distributor_order_produk = mDistributorOrderProduk
            ::with('produk')
            ->where('id_distributor_order', $id_distributor_order)
            ->get();

        $data = [
            'distributor_order' => $distributor_order,
            'distributor_order_produk' => $distributor_order_produk
        ];

        return view('transaksi/orderOnline/modalBodyDetailBaru', $data);
    }

    function detail_order_konfirmasi($id)
    {
        $id_distributor_order = Main::decrypt($id);
        $distributor_order = mDistributorOrder
            ::with('distributor')
            ->where('id', $id_distributor_order)
            ->first();
        $distributor_order_produk = mDistributorOrderProduk
            ::with('produk')
            ->where('id_distributor_order', $id_distributor_order)
            ->get();

        $data = [
            'distributor_order' => $distributor_order,
            'distributor_order_produk' => $distributor_order_produk,
            'id_distributor_order' => $id_distributor_order
        ];

        return view('transaksi/orderOnline/modalBodyDetailKonfirmasi', $data);
    }

    function batal($id)
    {
        $id_distributor_order = Main::decrypt($id);
        $data = [
            'status' => 'reject',
            'rejected_at' => $this->datetime
        ];
        mDistributorOrder
            ::where('id', $id_distributor_order)
            ->update($data);
    }

    function konfirmasi()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $id_distributor = Session::get('user.id');
        $list = mDistributorOrder
            ::where([
                'id_distributor' => $id_distributor
            ])
            ->whereIn('status', ['konfirm_admin', 'konfirm_distributor'])
            ->withCount([
                'distributor_order_produk AS total_qty' => function ($query) {
                    $query->select(DB::raw("SUM(qty_order) AS qty_sum"));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'konfirmasi'
        ]);

        return view('transaksi/orderOnline/orderOnlineKonfirm', $data);
    }

    function history()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $id_distributor = Session::get('user.id');
        $list = mDistributorOrder
            ::where([
                'id_distributor' => $id_distributor
            ])
            ->whereIn('status', ['done'])
            ->withCount([
                'distributor_order_produk AS total_qty_konfirm' => function ($query) {
                    $query->select(DB::raw("SUM(qty_konfirm) AS qty_sum"));
                },
                'distributor_order_produk AS total_qty_order' => function ($query) {
                    $query->select(DB::raw("SUM(qty_order) AS qty_sum"));
                }
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, [
            'list' => $list,
            'tab' => 'history'
        ]);

        return view('transaksi/orderOnline/orderOnlineHistory', $data);
    }

    function konfirm_distributor(Request $request)
    {
        $id_distributor_order = $request->input('id_distributor_order');
        $data = [
            'status' => 'konfirm_distributor',
            'konfirm_distributor_at' => $this->datetime
        ];

        mDistributorOrder::where('id', $id_distributor_order)->update($data);
    }

    function create()
    {
        $breadcrumb = [
            [
                'label' => 'Order Online Baru',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $distributor = Session::get('user');
        $urutan = $this->urutan();
        $no_order = $this->no_faktur();
        $produk = mProduk
            ::withCount([
                'stok_produk AS stok_produk' => function ($query) {
                    //$query->where('qty', '<', 400);
                    $query->select(DB::raw("SUM(qty) AS qty_sum"));
                }
            ])
            ->orderBy('kode_produk', 'ASC')
            ->get();

        $data = array_merge($data, [
            'produk' => $produk,
            'distributor' => $distributor,
            'tab' => 'list',
            'urutan' => $urutan,
            'no_order' => $no_order,
            'pageTitle' => 'Order Online Baru'
        ]);

        return view('transaksi/orderOnline/orderOnlineCreate', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_produk.*' => 'required',
            'qty_order.*' => 'required'
        ]);

        $data_distributor_order_produk = [];
        $id_distributor = Session::get('user.id');
        $urutan = $request->input('urutan');
        $no_order = $request->input('no_order');
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $pengiriman = $request->input('pengiriman');
        $status = 'order';

        $id_produk_arr = $request->input('id_produk');
        $qty_order_arr = $request->input('qty_order');

        $data_distributor_order = [
            'id_distributor' => $id_distributor,
            'urutan' => $urutan,
            'no_order' => $no_order,
            'tanggal' => $tanggal,
            'pengiriman' => $pengiriman,
            'status' => $status,
            'ordered_at' => $this->datetime
        ];

        DB::beginTransaction();

        try {

            $response = mDistributorOrder::create($data_distributor_order);
            $id_distributor_order = $response->id;

            foreach ($id_produk_arr as $index => $id_produk) {
                $data_distributor_order_produk[] = [
                    'id_distributor_order' => $id_distributor_order,
                    'id_produk' => $id_produk,
                    'qty_order' => $qty_order_arr[$index],
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];
            }

            mDistributorOrderProduk::insert($data_distributor_order_produk);

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }

    function edit($id_distribusi_order)
    {
        $id_distribusi_order = Main::decrypt($id_distribusi_order);
        $breadcrumb = [
            [
                'label' => 'Order Online Edit',
                'route' => ''
            ]
        ];
        $breadcrumb = array_merge($this->breadcrumb, $breadcrumb);
        $data = Main::data($breadcrumb, $this->menuActive);
        $distributor = Session::get('user');
        $distributor_order = mDistributorOrder::where('id', $id_distribusi_order)->first();
        $distributor_order_produk = mDistributorOrderProduk
            ::with(['produk'])
            ->where('id_distributor_order', $id_distribusi_order)
            ->get();
        $produk = mProduk
            ::withCount([
                'stok_produk AS stok_produk' => function ($query) {
                    //$query->where('qty', '<', 400);
                    $query->select(DB::raw("SUM(qty) AS qty_sum"));
                }
            ])
            ->orderBy('kode_produk', 'ASC')
            ->get();

        $data = array_merge($data, [
            'produk' => $produk,
            'distributor' => $distributor,
            'distributor_order' => $distributor_order,
            'distributor_order_produk' => $distributor_order_produk,
            'tab' => 'list',
            'pageTitle' => 'Order Online Edit',
            'no' => 1
        ]);

        return view('transaksi/orderOnline/orderOnlineEdit', $data);
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'id_produk.*' => 'required',
            'qty_order.*' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $id_distributor_order = Main::decrypt($id);
            $tanggal = $request->input('tanggal');
            $tanggal = Main::format_date_db($tanggal);
            $pengiriman = $request->input('pengiriman');
            $id_produk_arr = $request->input('id_produk');
            $qty_order_arr = $request->input('qty_order');

            $data_distributor_order = [
                'tanggal' => $tanggal,
                'pengiriman' => $pengiriman,
            ];

            mDistributorOrder::where('id', $id_distributor_order)->update($data_distributor_order);
            mDistributorOrderProduk::where('id_distributor_order', $id_distributor_order)->delete();

            foreach ($id_produk_arr as $index => $id_produk) {
                $data_distributor_order_produk[] = [
                    'id_distributor_order' => $id_distributor_order,
                    'id_produk' => $id_produk,
                    'qty_order' => $qty_order_arr[$index],
                    'created_at' => $this->datetime,
                    'updated_at' => $this->datetime
                ];
            }

            mDistributorOrderProduk::insert($data_distributor_order_produk);

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }


    }

    function urutan()
    {
        $count = mDistributorOrder::count();
        if ($count == 0) {
            return 1;
        } else {
            $urutan = mDistributorOrder::orderBy('urutan', 'DESC')->first(['urutan']);
            return $urutan->urutan + 1;
        }
    }


    function produk_stok(Request $request)
    {
        $id_produk = $request->input('id_produk');
        $produkStok = mStokProduk::with('lokasi')->where(['id_produk' => $id_produk])->get();

        $data = [
            'produkStok' => $produkStok,
            'no' => 1
        ];

        return view('transaksi.orderOnline.tbodyProdukStok', $data);
    }

    function no_faktur()
    {
        $id_distributor = Session::get('user.id');
        $kode_distributor = mDistributor::where('id', $id_distributor)->value('kode_distributor');
        $year = date('Y');
        $month = date('m');

        $urutan = mPenjualan
            ::where('id_distributor', $id_distributor)
            ->whereYear('tanggal', $year)
            ->whereMonth('tanggal', $month);
        $urutan_next = $urutan->count() > 0 ? $urutan->first(['urutan'])->urutan + 1 : 1;

        $no_faktur = $kode_distributor
            . '.' . str_pad($urutan_next, 2, "0", STR_PAD_LEFT)
            . '.' . $month
            . '.' . $year;

        return $no_faktur;

    }

}
