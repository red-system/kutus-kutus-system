@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("produkPage") }}"
                       class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Produk</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">

                    <form action="{{ route('hargaProdukInsert', ['idProduk'=>$idProduk]) }}"
                          method="post"
                          class="form-send"
                          data-alert-show="true">
                        {{ csrf_field() }}

                        <table class="table-produk-harga table table-striped- table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Jumlah Awal Produk</th>
                                <th>Jumlah Akhir Produk</th>
                                <th>Harga Produk</th>
                                <th width="150">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($list as $r)
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="text" class="form-control m-input touchspin-number" name="rentang_awal[]" value="{{ $r->rentang_awal }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control m-input touchspin-number" name="rentang_akhir[]" value="{{ $r->rentang_akhir }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control m-input touchspin-rp" name="harga[]" value="{{ $r->harga }}">
                                        </td>
                                        <td align="center">
                                            <button type="button"
                                                    class="btn-rentang-hapus m-btn btn btn-danger m-btn--pill btn-sm">
                                                <i class="la la-remove"></i> Hapus
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="text-center">

                            <a href="#"
                               class="btn-rentang-tambah btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Tambah Rentang Harga</span>
                                </span>
                            </a>

                            <button type="submit"
                               class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                                <span>
                                    <i class="la la-check"></i>
                                    <span>Simpan Data</span>
                                </span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>

        </div>

    </div>

    <table id="tr-rentang-tambah" class="hidden">
        <tr>
            <td></td>
            <td>
                <input type="text" class="form-control m-input touchspin-number" name="rentang_awal[]" value="1">
            </td>
            <td>
                <input type="text" class="form-control m-input touchspin-number" name="rentang_akhir[]" value="1">
            </td>
            <td>
                <input type="text" class="form-control m-input touchspin-rp" name="harga[]" value="1">
            </td>
            <td align="center">
                <button type="button"
                        class="btn-rentang-hapus m-btn btn btn-danger m-btn--pill btn-sm">
                    <i class="la la-remove"></i> Hapus
                </button>
            </td>
        </tr>
    </table>
@endsection