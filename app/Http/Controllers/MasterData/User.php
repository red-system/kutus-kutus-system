<?php

namespace app\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Hash;
use app\Rules\UsernameChecker;
use app\Rules\UsernameCheckerUpdate;
use Illuminate\Support\Facades\Config;

use app\Models\mKaryawan;
use app\Models\mUser;
use app\Models\mUserRole;

class User extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label'=>$cons['masterData'],
                'route'=>''
            ],
            [
                'label'=>$cons['master_5'],
                'route'=>''
            ]
        ];
    }

    function index() {
        $data = Main::data($this->breadcrumb);
        $data['karyawan'] = mKaryawan
            ::select(['id','nama_karyawan'])
            ->orderBy('nama_karyawan', 'ASC')
            ->get();
        $data['user_role'] = mUserRole::orderBy('role_name', 'ASC')->get();
        $data['list'] = mUser
            ::with([
                'karyawan:id,kode_karyawan,nama_karyawan,foto_karyawan,posisi_karyawan',
                'user_role:id,role_name'
            ])
            ->get();

        return view('masterData/user/userList', $data);
    }

    function insert(Request $request) {
        $request->validate([
            'id_karyawan' => 'required',
            'id_user_role' => 'required',
            'username' => ['required', new UsernameChecker],
            'password' => 'required'
        ]);

        $data = $request->except('_token');
        $data['password'] = Hash::make($data['password']);
        mUser::create($data);
    }

    function delete($id) {
        mUser::where('id', $id)->delete();
    }

    function update(Request $request, $id) {
        $request->validate([
            'id_karyawan' => 'required',
            'id_user_role' => 'required',
            'username' => ['required', new UsernameCheckerUpdate($id)],
            //'password' => 'required'
        ]);

        $data = $request->except("_token");
        if($request->input('password')) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        mUser::where(['id'=>$id])->update($data);
    }
}
