@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tahun Produksi</th>
                            <th>Realisasi (%)</th>
                            <th width="200">Menu</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)

                            @php
                                $total_target = \app\Models\mTargetProduksi::where('tahun_target', $r->tahun_target)->sum('qty');
                                $total_produksi = \app\Models\mProgressProduksi::whereYear('tgl_selesai', $r->tahun_target)->sum('qty_progress');
                                $persentase = round(( $total_produksi / $total_target ) * 100, $roundDecimal);
                            @endphp
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->tahun_target }}</td>
                                <td>{{ $persentase }}%</td>
                                <td>
                                    <a class="m-btn btn btn-success m-btn--pill m-btn--sm akses-detail_transaksi"
                                       href="{{ route('realisasiTargetProduksiTahun', ['tahun'=>Main::encrypt($r->tahun_target)]) }}">
                                        <i class="la la-calendar"></i> Detail Realisasi
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection