<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPiutangPelanggan extends Model
{
    protected $table = 'tb_piutang_pelanggan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_distributor',
        'id_penjualan',
        'table_id',
        'table_name',
        'no_piutang_pelanggan',
        'pp_jatuh_tempo',
        'pp_no_faktur',
        'pp_amount',
        'master_id_kode_perkiraan',
        'kode_perkiraan',
        'pp_sisa_amount',
        'tanggal_piutang',
        'pp_keterangan',
        'pp_status',
        'tipe_penjualan',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name'
    ];

    function distributor()
    {
        return $this->belongsTo(mDistributor::class, 'id_distributor');
    }

    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }
}
