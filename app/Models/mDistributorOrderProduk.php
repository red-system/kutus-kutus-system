<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mDistributorOrderProduk extends Model
{
    protected $table = 'tb_distributor_order_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_distributor_order',
        'id_produk',
        'qty_order',
        'qty_konfirm'
    ];

    function distributor_order() {
        return $this->belongsTo(mDistributorOrder::class, 'id_distributor_order');
    }

    function produk() {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }
}
