<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mDistribusiDetail extends Model
{
    protected $table = 'tb_distribusi_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'id_distribusi',
        'id_penjualan_produk',
        'id_stok_produk',
        'qty_dikirim',
        'remark'
    ];

    function penjualan() {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    function distribusi() {
        return $this->belongsTo(mDistribusi::class, 'id_distribusi');
    }

    function penjualan_produk() {
        return $this->belongsTo(mPenjualanProduk::class, 'id_penjualan_produk');
    }

    function stok_produk() {
        return $this->belongsTo(mStokProduk::class, 'id_stok_produk');
    }
}
