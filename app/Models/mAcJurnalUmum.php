<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mAcJurnalUmum extends Model
{
    // public $incrementing = false;
    protected $table = 'tb_ac_jurnal_umum';
    protected $primaryKey = 'jurnal_umum_id';
    protected $fillable = [
        'no_invoice',
        'table_id',
        'table_name',
        'id_supplier',
        'id_history_penyesuaian_bahan',
        'id_history_penyesuaian_produk',
        'id_penjualan',
        'id_distributor',
        'id_karyawan',
        'id_asset',
        'id_penyusutan_asset',
        'id_hutang_supplier',
        'id_hutang_lain',
        'id_piutang_lain',
        'id_piutang_pelanggan',
        'id_distribusi',
        'id_po_bahan',
        'id_produksi',
        'id_progress_produksi',
        'pelaku_id',
        'pelaku_table',
        'jmu_tanggal',
        'jmu_no',
        'jmu_keterangan',
        'jmu_year',
        'jmu_month',
        'jmu_day',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name',
        'edit',
        'reference_number'
    ];

    function supplier()
    {
        return $this->belongsTo(mAcJurnalUmum::class, 'id_supplier');
    }

    function transaksi()
    {
        return $this->hasMany(mAcTransaksi::class, 'jurnal_umum_id');
    }

    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }
}
