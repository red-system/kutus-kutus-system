<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use DB,
    PDF;

class LabaRugi extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_2'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_4'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);

        $where_date = [
            $date_start,
            $date_end
        ];

        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);


        //transaksi pendapatan
        $data['kode_pendapatan'] = mAcMaster
            ::where('mst_kode_rekening', '3100')
            ->get();
        $biaya = 0;
        foreach ($data['kode_pendapatan'] as $pendapatan) {
            $total_pendapatanUsaha = 0;
            foreach ($pendapatan->childs as $pendapatanUsaha) {
                $total_penjualan = 0;
                foreach ($pendapatanUsaha->childs as $penjualan) {
                    $total_penjualanChilds = 0;
                    foreach ($penjualan->childs as $penjualanChilds) {
                        if ($penjualanChilds->mst_normal == 'kredit') {
                            $data['biaya'][$penjualanChilds->mst_kode_rekening] =
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_kredit')
                                -
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_debet');
                            $total_penjualanChilds =
                                $total_penjualanChilds + $data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                        if ($penjualanChilds->mst_normal == 'debet') {
                            $data['biaya'][$penjualanChilds->mst_kode_rekening] =
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_debet')
                                -
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_kredit');
                            $total_penjualanChilds =
                                $total_penjualanChilds - $data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                    }
                    if ($penjualan->mst_normal == 'kredit') {
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening] =
                            $total_childs
                            +
                            $penjualan
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                                ->sum('trs_kredit');
                        $total_penjualan = $total_penjualan + $data['biaya'][$penjualan->mst_kode_rekening];
                    }
                    if ($penjualan->mst_normal == 'debet') {
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening] =
                            $total_childs
                            -
                            $penjualan
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                                ->sum('trs_debet');
                        $total_penjualan = $total_penjualan - $data['biaya'][$penjualan->mst_kode_rekening];
                    }
                }
                if ($pendapatanUsaha->mst_normal == 'kredit') {
                    $penjualan_total = $total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening] =
                        $penjualan_total
                        +
                        $pendapatanUsaha
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_kredit');
                    $total_pendapatanUsaha = $total_pendapatanUsaha + $data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
                if ($pendapatanUsaha->mst_normal == 'debet') {
                    $penjualan_total = $total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening] =
                        $penjualan_total
                        -
                        $pendapatanUsaha
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_debet');
                    $total_pendapatanUsaha = $total_pendapatanUsaha - $data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
            }
            if ($pendapatan->mst_normal == 'kredit') {
                $total_pendapatan = $total_pendapatanUsaha;
                $data['biaya'][$pendapatan->mst_kode_rekening] =
                    $total_pendapatanUsaha
                    +
                    $pendapatan
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_kredit');
            }
            if ($pendapatan->mst_normal == 'debet') {
                $total_pendapatan = $total_pendapatanUsaha;
                $data['biaya'][$pendapatan->mst_kode_rekening] =
                    $total_pendapatanUsaha
                    -
                    $pendapatan
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_debet');
            }
        }
        //transaksi pendapatan

        //transaksi pendapatan diluar usaha
        $data['kode_pendapatan_diluar_usaha'] = mAcMaster
            ::where('mst_kode_rekening', '3120')
            ->get();
        foreach ($data['kode_pendapatan_diluar_usaha'] as $pendapatanDiluarUsaha) {
            $total_pendapatanDiluarUsahaChilds = 0;
            foreach ($pendapatanDiluarUsaha->childs as $pendapatanDiluarUsahaChilds) {
                if ($pendapatanDiluarUsahaChilds->mst_normal == 'kredit') {

                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening] =
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit')
                        -
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_pendapatanDiluarUsahaChilds =
                        $total_pendapatanDiluarUsahaChilds
                        +
                        $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
                if ($pendapatanDiluarUsahaChilds->mst_normal == 'debet') {

                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening] =
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet')
                        -
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_pendapatanDiluarUsahaChilds =
                        $total_pendapatanDiluarUsahaChilds
                        -
                        $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
            }
            if ($pendapatanDiluarUsaha->mst_normal == 'kredit') {
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening] =
                    $total_pendapatanDiluarUsaha
                    +
                    $pendapatanDiluarUsaha
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');
            }
            if ($pendapatanDiluarUsaha->mst_normal == 'debet') {
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening] =
                    $total_pendapatanDiluarUsaha
                    -
                    $pendapatanDiluarUsaha
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');
            }
        }
        //transaksi pendapatan diluar usaha

        //transaksi hpp
        $data['kode_hpp'] = mAcMaster
            ::where('mst_kode_rekening', '4100')
            ->get();
        foreach ($data['kode_hpp'] as $hpp) {
            $total_penjualan = 0;
            foreach ($hpp->childs as $hppChilds) {
                $total_hppChild = 0;
                foreach ($hppChilds->childs as $hppChild) {
                    if ($hppChild->mst_normal == 'kredit') {
                        $data['biaya'][$hppChild->mst_kode_rekening] =
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit')
                            -
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet');
                        $total_hppChild = $total_hppChild - $data['biaya'][$hppChild->mst_kode_rekening];
                    }
                    if ($hppChild->mst_normal == 'debet') {
                        $data['biaya'][$hppChild->mst_kode_rekening] =
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet')
                            -
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit');
                        $total_hppChild = $total_hppChild + $data['biaya'][$hppChild->mst_kode_rekening];
                    }
                }
                if ($hppChilds->mst_normal == 'kredit') {
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening] =
                        $total_hppChilds
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_hppChilds = $total_hppChilds - $data['biaya'][$hppChilds->mst_kode_rekening];
                }
                if ($hppChilds->mst_normal == 'debet') {
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening] =
                        $total_hppChilds
                        +
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_hppChilds = $data['biaya'][$hppChilds->mst_kode_rekening];
                }
            }
            if ($hpp->mst_normal == 'kredit') {
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening] =
                    $total_hpp
                    -
                    $hpp
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');
                // $total_pendapatanUsaha = $total_pendapatanUsaha+$data['biaya'][$hpp->mst_kode_rekening];
            }
            if ($hpp->mst_normal == 'debet') {
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening] =
                    $total_hpp
                    +
                    $hpp
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');
                // $total_hpp = $total_hpp-$data['biaya'][$hpp->mst_kode_rekening];
            }
        }
        //transaksi hpp

        //transaksi biaya-biaya
        $data['kode_biaya'] = mAcMaster
            ::where('mst_kode_rekening', '5100')
            ->get();
        foreach ($data['kode_biaya'] as $biaya) {
            $total_biayaChild = 0;
            foreach ($biaya->childs as $biayaChild) {
                $total_biayaChild2 = 0;
                foreach ($biayaChild->childs as $biayaChild2) {
                    if ($biayaChild2->mst_normal == 'kredit') {
                        $data['biaya'][$biayaChild2->mst_kode_rekening] =
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit')
                            -
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet');
                        $total_biayaChild2 = $total_biayaChild2 - $data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                    if ($biayaChild2->mst_normal == 'debet') {
                        $data['biaya'][$biayaChild2->mst_kode_rekening] =
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet')
                            -
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit');
                        $total_biayaChild2 = $total_biayaChild2 + $data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                }

                if ($biayaChild->mst_normal == 'kredit') {
                    $data['biaya'][$biayaChild->mst_kode_rekening] =
                        $total_biayaChild
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit')
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_biayaChild = $total_biayaChild - $data['biaya'][$biayaChild->mst_kode_rekening];
                }
                if ($biayaChild->mst_normal == 'debet') {
                    $data['biaya'][$biayaChild->mst_kode_rekening] =
                        $total_biayaChild2
                        +
                        $biayaChild
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet')
                        -
                        $biayaChild
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_biayaChild = $total_biayaChild + $data['biaya'][$biayaChild->mst_kode_rekening];
                }
            }
            if ($biaya->mst_normal == 'kredit') {
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening] =
                    $total_hpp
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit')
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');

            }
            if ($hpp->mst_normal == 'debet') {
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening] =
                    $total_hpp
                    +
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet')
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');

            }
        }

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
        ]);

        return view('akunting/labaRugi/labaRugiList', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);

        $where_date = [
            $date_start,
            $date_end
        ];

        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);


        //transaksi pendapatan
        $data['kode_pendapatan'] = mAcMaster
            ::where('mst_kode_rekening', '3100')
            ->get();
        $biaya = 0;
        foreach ($data['kode_pendapatan'] as $pendapatan) {
            $total_pendapatanUsaha = 0;
            foreach ($pendapatan->childs as $pendapatanUsaha) {
                $total_penjualan = 0;
                foreach ($pendapatanUsaha->childs as $penjualan) {
                    $total_penjualanChilds = 0;
                    foreach ($penjualan->childs as $penjualanChilds) {
                        if ($penjualanChilds->mst_normal == 'kredit') {
                            $data['biaya'][$penjualanChilds->mst_kode_rekening] =
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_kredit')
                                -
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_debet');
                            $total_penjualanChilds =
                                $total_penjualanChilds + $data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                        if ($penjualanChilds->mst_normal == 'debet') {
                            $data['biaya'][$penjualanChilds->mst_kode_rekening] =
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_debet')
                                -
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_kredit');
                            $total_penjualanChilds =
                                $total_penjualanChilds - $data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                    }
                    if ($penjualan->mst_normal == 'kredit') {
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening] =
                            $total_childs
                            +
                            $penjualan
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                                ->sum('trs_kredit');
                        $total_penjualan = $total_penjualan + $data['biaya'][$penjualan->mst_kode_rekening];
                    }
                    if ($penjualan->mst_normal == 'debet') {
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening] =
                            $total_childs
                            -
                            $penjualan
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                                ->sum('trs_debet');
                        $total_penjualan = $total_penjualan - $data['biaya'][$penjualan->mst_kode_rekening];
                    }
                }
                if ($pendapatanUsaha->mst_normal == 'kredit') {
                    $penjualan_total = $total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening] =
                        $penjualan_total
                        +
                        $pendapatanUsaha
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_kredit');
                    $total_pendapatanUsaha = $total_pendapatanUsaha + $data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
                if ($pendapatanUsaha->mst_normal == 'debet') {
                    $penjualan_total = $total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening] =
                        $penjualan_total
                        -
                        $pendapatanUsaha
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_debet');
                    $total_pendapatanUsaha = $total_pendapatanUsaha - $data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
            }
            if ($pendapatan->mst_normal == 'kredit') {
                $total_pendapatan = $total_pendapatanUsaha;
                $data['biaya'][$pendapatan->mst_kode_rekening] =
                    $total_pendapatanUsaha
                    +
                    $pendapatan
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_kredit');
            }
            if ($pendapatan->mst_normal == 'debet') {
                $total_pendapatan = $total_pendapatanUsaha;
                $data['biaya'][$pendapatan->mst_kode_rekening] =
                    $total_pendapatanUsaha
                    -
                    $pendapatan
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_debet');
            }
        }
        //transaksi pendapatan

        //transaksi pendapatan diluar usaha
        $data['kode_pendapatan_diluar_usaha'] = mAcMaster
            ::where('mst_kode_rekening', '3120')
            ->get();
        foreach ($data['kode_pendapatan_diluar_usaha'] as $pendapatanDiluarUsaha) {
            $total_pendapatanDiluarUsahaChilds = 0;
            foreach ($pendapatanDiluarUsaha->childs as $pendapatanDiluarUsahaChilds) {
                if ($pendapatanDiluarUsahaChilds->mst_normal == 'kredit') {

                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening] =
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit')
                        -
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_pendapatanDiluarUsahaChilds =
                        $total_pendapatanDiluarUsahaChilds
                        +
                        $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
                if ($pendapatanDiluarUsahaChilds->mst_normal == 'debet') {

                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening] =
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet')
                        -
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_pendapatanDiluarUsahaChilds =
                        $total_pendapatanDiluarUsahaChilds
                        -
                        $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
            }
            if ($pendapatanDiluarUsaha->mst_normal == 'kredit') {
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening] =
                    $total_pendapatanDiluarUsaha
                    +
                    $pendapatanDiluarUsaha
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');
            }
            if ($pendapatanDiluarUsaha->mst_normal == 'debet') {
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening] =
                    $total_pendapatanDiluarUsaha
                    -
                    $pendapatanDiluarUsaha
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');
            }
        }
        //transaksi pendapatan diluar usaha

        //transaksi hpp
        $data['kode_hpp'] = mAcMaster
            ::where('mst_kode_rekening', '4100')
            ->get();
        foreach ($data['kode_hpp'] as $hpp) {
            $total_penjualan = 0;
            foreach ($hpp->childs as $hppChilds) {
                $total_hppChild = 0;
                foreach ($hppChilds->childs as $hppChild) {
                    if ($hppChild->mst_normal == 'kredit') {
                        $data['biaya'][$hppChild->mst_kode_rekening] =
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit')
                            -
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet');
                        $total_hppChild = $total_hppChild - $data['biaya'][$hppChild->mst_kode_rekening];
                    }
                    if ($hppChild->mst_normal == 'debet') {
                        $data['biaya'][$hppChild->mst_kode_rekening] =
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet')
                            -
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit');
                        $total_hppChild = $total_hppChild + $data['biaya'][$hppChild->mst_kode_rekening];
                    }
                }
                if ($hppChilds->mst_normal == 'kredit') {
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening] =
                        $total_hppChilds
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_hppChilds = $total_hppChilds - $data['biaya'][$hppChilds->mst_kode_rekening];
                }
                if ($hppChilds->mst_normal == 'debet') {
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening] =
                        $total_hppChilds
                        +
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_hppChilds = $data['biaya'][$hppChilds->mst_kode_rekening];
                }
            }
            if ($hpp->mst_normal == 'kredit') {
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening] =
                    $total_hpp
                    -
                    $hpp
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');
                // $total_pendapatanUsaha = $total_pendapatanUsaha+$data['biaya'][$hpp->mst_kode_rekening];
            }
            if ($hpp->mst_normal == 'debet') {
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening] =
                    $total_hpp
                    +
                    $hpp
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');
                // $total_hpp = $total_hpp-$data['biaya'][$hpp->mst_kode_rekening];
            }
        }
        //transaksi hpp

        //transaksi biaya-biaya
        $data['kode_biaya'] = mAcMaster
            ::where('mst_kode_rekening', '5100')
            ->get();
        foreach ($data['kode_biaya'] as $biaya) {
            $total_biayaChild = 0;
            foreach ($biaya->childs as $biayaChild) {
                $total_biayaChild2 = 0;
                foreach ($biayaChild->childs as $biayaChild2) {
                    if ($biayaChild2->mst_normal == 'kredit') {
                        $data['biaya'][$biayaChild2->mst_kode_rekening] =
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit')
                            -
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet');
                        $total_biayaChild2 = $total_biayaChild2 - $data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                    if ($biayaChild2->mst_normal == 'debet') {
                        $data['biaya'][$biayaChild2->mst_kode_rekening] =
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet')
                            -
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit');
                        $total_biayaChild2 = $total_biayaChild2 + $data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                }

                if ($biayaChild->mst_normal == 'kredit') {
                    $data['biaya'][$biayaChild->mst_kode_rekening] =
                        $total_biayaChild
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit')
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_biayaChild = $total_biayaChild - $data['biaya'][$biayaChild->mst_kode_rekening];
                }
                if ($biayaChild->mst_normal == 'debet') {
                    $data['biaya'][$biayaChild->mst_kode_rekening] =
                        $total_biayaChild2
                        +
                        $biayaChild
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet')
                        -
                        $biayaChild
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_biayaChild = $total_biayaChild + $data['biaya'][$biayaChild->mst_kode_rekening];
                }
            }
            if ($biaya->mst_normal == 'kredit') {
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening] =
                    $total_hpp
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit')
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');

            }
            if ($hpp->mst_normal == 'debet') {
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening] =
                    $total_hpp
                    +
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet')
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');

            }
        }

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
            'company' => Main::companyInfo()
        ]);


        $pdf = PDF::loadView('akunting/labaRugi/labaRugiPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Laba Rugi ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);

        $where_date = [
            $date_start,
            $date_end
        ];

        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);


        //transaksi pendapatan
        $data['kode_pendapatan'] = mAcMaster
            ::where('mst_kode_rekening', '3100')
            ->get();
        $biaya = 0;
        foreach ($data['kode_pendapatan'] as $pendapatan) {
            $total_pendapatanUsaha = 0;
            foreach ($pendapatan->childs as $pendapatanUsaha) {
                $total_penjualan = 0;
                foreach ($pendapatanUsaha->childs as $penjualan) {
                    $total_penjualanChilds = 0;
                    foreach ($penjualan->childs as $penjualanChilds) {
                        if ($penjualanChilds->mst_normal == 'kredit') {
                            $data['biaya'][$penjualanChilds->mst_kode_rekening] =
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_kredit')
                                -
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_debet');
                            $total_penjualanChilds =
                                $total_penjualanChilds + $data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                        if ($penjualanChilds->mst_normal == 'debet') {
                            $data['biaya'][$penjualanChilds->mst_kode_rekening] =
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_debet')
                                -
                                $penjualanChilds
                                    ->transaksi
                                    ->whereBetween('tgl_transaksi', $where_date)
                                    ->sum('trs_kredit');
                            $total_penjualanChilds =
                                $total_penjualanChilds - $data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                    }
                    if ($penjualan->mst_normal == 'kredit') {
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening] =
                            $total_childs
                            +
                            $penjualan
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                                ->sum('trs_kredit');
                        $total_penjualan = $total_penjualan + $data['biaya'][$penjualan->mst_kode_rekening];
                    }
                    if ($penjualan->mst_normal == 'debet') {
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening] =
                            $total_childs
                            -
                            $penjualan
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                                ->sum('trs_debet');
                        $total_penjualan = $total_penjualan - $data['biaya'][$penjualan->mst_kode_rekening];
                    }
                }
                if ($pendapatanUsaha->mst_normal == 'kredit') {
                    $penjualan_total = $total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening] =
                        $penjualan_total
                        +
                        $pendapatanUsaha
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_kredit');
                    $total_pendapatanUsaha = $total_pendapatanUsaha + $data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
                if ($pendapatanUsaha->mst_normal == 'debet') {
                    $penjualan_total = $total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening] =
                        $penjualan_total
                        -
                        $pendapatanUsaha
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_debet');
                    $total_pendapatanUsaha = $total_pendapatanUsaha - $data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
            }
            if ($pendapatan->mst_normal == 'kredit') {
                $total_pendapatan = $total_pendapatanUsaha;
                $data['biaya'][$pendapatan->mst_kode_rekening] =
                    $total_pendapatanUsaha
                    +
                    $pendapatan
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_kredit');
            }
            if ($pendapatan->mst_normal == 'debet') {
                $total_pendapatan = $total_pendapatanUsaha;
                $data['biaya'][$pendapatan->mst_kode_rekening] =
                    $total_pendapatanUsaha
                    -
                    $pendapatan
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_debet');
            }
        }
        //transaksi pendapatan

        //transaksi pendapatan diluar usaha
        $data['kode_pendapatan_diluar_usaha'] = mAcMaster
            ::where('mst_kode_rekening', '3120')
            ->get();
        foreach ($data['kode_pendapatan_diluar_usaha'] as $pendapatanDiluarUsaha) {
            $total_pendapatanDiluarUsahaChilds = 0;
            foreach ($pendapatanDiluarUsaha->childs as $pendapatanDiluarUsahaChilds) {
                if ($pendapatanDiluarUsahaChilds->mst_normal == 'kredit') {

                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening] =
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit')
                        -
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_pendapatanDiluarUsahaChilds =
                        $total_pendapatanDiluarUsahaChilds
                        +
                        $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
                if ($pendapatanDiluarUsahaChilds->mst_normal == 'debet') {

                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening] =
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet')
                        -
                        $pendapatanDiluarUsahaChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_pendapatanDiluarUsahaChilds =
                        $total_pendapatanDiluarUsahaChilds
                        -
                        $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
            }
            if ($pendapatanDiluarUsaha->mst_normal == 'kredit') {
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening] =
                    $total_pendapatanDiluarUsaha
                    +
                    $pendapatanDiluarUsaha
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');
            }
            if ($pendapatanDiluarUsaha->mst_normal == 'debet') {
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening] =
                    $total_pendapatanDiluarUsaha
                    -
                    $pendapatanDiluarUsaha
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');
            }
        }
        //transaksi pendapatan diluar usaha

        //transaksi hpp
        $data['kode_hpp'] = mAcMaster
            ::where('mst_kode_rekening', '4100')
            ->get();
        foreach ($data['kode_hpp'] as $hpp) {
            $total_penjualan = 0;
            foreach ($hpp->childs as $hppChilds) {
                $total_hppChild = 0;
                foreach ($hppChilds->childs as $hppChild) {
                    if ($hppChild->mst_normal == 'kredit') {
                        $data['biaya'][$hppChild->mst_kode_rekening] =
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit')
                            -
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet');
                        $total_hppChild = $total_hppChild - $data['biaya'][$hppChild->mst_kode_rekening];
                    }
                    if ($hppChild->mst_normal == 'debet') {
                        $data['biaya'][$hppChild->mst_kode_rekening] =
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet')
                            -
                            $hppChild
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit');
                        $total_hppChild = $total_hppChild + $data['biaya'][$hppChild->mst_kode_rekening];
                    }
                }
                if ($hppChilds->mst_normal == 'kredit') {
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening] =
                        $total_hppChilds
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_hppChilds = $total_hppChilds - $data['biaya'][$hppChilds->mst_kode_rekening];
                }
                if ($hppChilds->mst_normal == 'debet') {
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening] =
                        $total_hppChilds
                        +
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_hppChilds = $data['biaya'][$hppChilds->mst_kode_rekening];
                }
            }
            if ($hpp->mst_normal == 'kredit') {
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening] =
                    $total_hpp
                    -
                    $hpp
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');
                // $total_pendapatanUsaha = $total_pendapatanUsaha+$data['biaya'][$hpp->mst_kode_rekening];
            }
            if ($hpp->mst_normal == 'debet') {
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening] =
                    $total_hpp
                    +
                    $hpp
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');
                // $total_hpp = $total_hpp-$data['biaya'][$hpp->mst_kode_rekening];
            }
        }
        //transaksi hpp

        //transaksi biaya-biaya
        $data['kode_biaya'] = mAcMaster
            ::where('mst_kode_rekening', '5100')
            ->get();
        foreach ($data['kode_biaya'] as $biaya) {
            $total_biayaChild = 0;
            foreach ($biaya->childs as $biayaChild) {
                $total_biayaChild2 = 0;
                foreach ($biayaChild->childs as $biayaChild2) {
                    if ($biayaChild2->mst_normal == 'kredit') {
                        $data['biaya'][$biayaChild2->mst_kode_rekening] =
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit')
                            -
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet');
                        $total_biayaChild2 = $total_biayaChild2 - $data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                    if ($biayaChild2->mst_normal == 'debet') {
                        $data['biaya'][$biayaChild2->mst_kode_rekening] =
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_debet')
                            -
                            $biayaChild2
                                ->transaksi
                                ->whereBetween('tgl_transaksi', $where_date)
                                ->sum('trs_kredit');
                        $total_biayaChild2 = $total_biayaChild2 + $data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                }

                if ($biayaChild->mst_normal == 'kredit') {
                    $data['biaya'][$biayaChild->mst_kode_rekening] =
                        $total_biayaChild
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit')
                        -
                        $hppChilds
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet');
                    $total_biayaChild = $total_biayaChild - $data['biaya'][$biayaChild->mst_kode_rekening];
                }
                if ($biayaChild->mst_normal == 'debet') {
                    $data['biaya'][$biayaChild->mst_kode_rekening] =
                        $total_biayaChild2
                        +
                        $biayaChild
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_debet')
                        -
                        $biayaChild
                            ->transaksi
                            ->whereBetween('tgl_transaksi', $where_date)
                            ->sum('trs_kredit');
                    $total_biayaChild = $total_biayaChild + $data['biaya'][$biayaChild->mst_kode_rekening];
                }
            }
            if ($biaya->mst_normal == 'kredit') {
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening] =
                    $total_hpp
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit')
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet');

            }
            if ($hpp->mst_normal == 'debet') {
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening] =
                    $total_hpp
                    +
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_debet')
                    -
                    $biaya
                        ->transaksi
                        ->whereBetween('tgl_transaksi', $where_date)
                        ->sum('trs_kredit');

            }
        }

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
            'company' => Main::companyInfo()
        ]);


        return view('akunting/labaRugi/labaRugiExcel', $data);
    }

}