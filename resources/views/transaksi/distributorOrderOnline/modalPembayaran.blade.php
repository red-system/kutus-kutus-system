<div class="modal" id="modal-pembayaran" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xxlg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <h3 class="modal-title m--font-light" id="exampleModalLabel">PEMBAYARAN</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">No Faktur</label>
                            <div class="col-10 col-form-label">
                                {{ $no_faktur }}
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>
                <button type="button" class="btn-add-row-pembayaran btn btn-accent btn-sm m-btn--pill">
                    <i class="la la-plus"></i> Tambah Pembayaran
                </button>
                <br /><br />
                <table class="table-pembayaran table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Pembayaran</th>
                        <th>Jumlah</th>
                        <th>Jatuh Tempo</th>
                        <th>No. Check / BG</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <br/>
                <hr/>
                <div class="form-no-padding">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Total</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="grand-total">{{ Main::format_number($grand_total) }}</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Terbayar</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="terbayar">{{ Main::format_number($terbayar) }}</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Sisa Pembayaran</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="sisa-pembayaran">{{ Main::format_number($sisa_pembayaran) }}</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">
                            <h3>Kembalian</h3>
                        </label>
                        <div class="col-9">
                            <h3 class="kembalian">{{ Main::format_number($kembalian) }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>