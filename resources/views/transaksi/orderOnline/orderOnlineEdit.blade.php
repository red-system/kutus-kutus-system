@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>

    <script src="{{ asset('js/distributor_order.js') }}"></script>
@endsection

@section('body')

    @include('transaksi.orderOnline.trProduk')
    @include('transaksi.orderOnline.modalProduk')
    @include('transaksi.orderOnline.modalProdukStok')

    <form action="{{ route('orderOnlineUpdate', ['id'=>Main::encrypt($distributor_order->id)]) }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('orderOnlinePage') }}"
          data-alert-show="true"
          style="width: 100%;">

            {{ csrf_field() }}

        <div class="m-grid__item m-grid__item--fluid m-wrapper">


            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--tabs">
                    @include('transaksi.orderOnline.tabTitle')
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">No Order</label>
                                    <div class="col-9 col-form-label">{{ $distributor_order->no_order }}</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Tanggal Order</label>
                                    <div class="col-9">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker"
                                                   name="tanggal" readonly value="{{ Main::format_date($distributor_order->tanggal) }}"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Pengiriman</label>
                                    <div class="col-9">
                                        <div class="m-radio-inline">
                                            <label class="m-radio">
                                                <input type="radio" name="pengiriman" value="yes" {{ $distributor_order->pengiriman == 'yes' ? 'checked':'' }}> Ya
                                                <span></span>
                                            </label>
                                            <label class="m-radio">
                                                <input type="radio" name="pengiriman" value="no" {{ $distributor_order->pengiriman == 'no' ? 'checked':'' }}> Tidak
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
                                    <div class="col-9 col-form-label">{{ $distributor->kode_distributor.' '.$distributor->nama_distributor }}</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                    <div class="col-9 col-form-label">{{ $distributor->alamat_distributor }}</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Kontak</label>
                                    <div class="col-9 col-form-label">{{ $distributor->telp_distributor }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-produk btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Produk
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">

                                <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                                    <thead>
                                    <tr>
                                        <th width="200">Kode Produk</th>
                                        <th>Nama Produk</th>
                                        <th>Qty</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($distributor_order_produk as $r)
                                            <tr data-index="{{ $no++ }}"
                                                data-kode-produk="{{ $r->produk->kode_produk }}"
                                                data-nama-produk="{{ $r->produk->nama_produk }}">
                                                <td class="m--hide data">
                                                    <input type="hidden" name="id_produk[]" value="{{ $r->id_produk }}">
                                                </td>
                                                <td>
                                                    <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                                                            type="button">
                                                        <i class="la la-search"></i> Cari
                                                    </button>
                                                </td>
                                                <td class="produk-nama">{{ '('.$r->produk->kode_produk.') '.$r->produk->nama_produk }}</td>
                                                <td class="td-qty">
                                                    <input type="text" name="qty_order[]" class="touchspin-number" value="{{ $r->qty_order }}" style="width: 70px">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                                                            data-confirm="false">
                                                        <i class="la la-remove"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Update Order</span>
                        </span>
            </button>

            <a href="{{ route("orderOnlinePage") }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>
    </form>



@endsection