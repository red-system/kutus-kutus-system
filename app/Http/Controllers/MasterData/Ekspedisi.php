<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\mEkspedisi;
use app\Models\mPenjualan;
use Illuminate\Support\Facades\Config;

class Ekspedisi extends Controller
{

    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label'=>$cons['masterData'],
                'route'=>''
            ],
            [
                'label'=>$cons['master_9'],
                'route'=>''
            ]
        ];
    }

    function index() {
        $data = Main::data($this->breadcrumb);
        $data['list'] = mEkspedisi::orderBy('nama_ekspedisi', 'ASC')->get();

        return view('masterData/ekspedisi/ekspedisiList', $data);
    }

    function insert(Request $request) {
        $request->validate([
            'kode_ekspedisi' => 'required',
            'nama_ekspedisi' => 'required',
        ]);

        $data = $request->except('_token');
        mEkspedisi::create($data);
    }

    function delete($id) {
        $id_ekspedisi = $id;
        $check_table_problem = '';
        $check_status = TRUE;
        $message = '';
        $list_table = [
            'penjualan'
        ];

        $penjualan = mPenjualan::where(['id_ekspedisi' => $id_ekspedisi])->count();

        foreach($list_table as $table) {
            if($$table > 0) {
                $check_table_problem = $table;
                $check_status = FALSE;
                break;
            }
        }

        switch($check_table_problem) {
            case "penjualan":
                $message = 'Tidak bisa dihapus, karena Data Ekspedisi ini digunakan di <strong>Menu '.Main::menuAction(Config::get('constants.topMenu.transaksi_1')).'</strong>';
                break;
        }

        if($check_status) {
            mEkspedisi::where('id', $id)->delete();
        } else {
            $response = [
                'title'=>'Perhatian ...',
                'message'=>$message
            ];

            return response($response, 422);
        }
    }

    function update(Request $request, $id) {
        $request->validate([
            'kode_ekspedisi' => 'required',
            'nama_ekspedisi' => 'required',
        ]);

        $data = $request->except("_token");
        mEkspedisi::where(['id'=>$id])->update($data);
    }
}
