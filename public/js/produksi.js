$(document).ready(function() {
    var pageMethod = $('#base-value').data('page-method');

    produk_add();
    produk_qty();
    hapus_produk();
    produksi_detail();
    produksi_publish();

    if(pageMethod === 'edit') {
        bahan_list();
    }
});


var _token = $('#base-value').data('csrf-token');
var route_produksi_no_seri_produk = $('#base-value').data('route-produksi-no-seri-produk');
var route_produksi_bahan = $('#base-value').data('route-produksi-bahan');
var route_detail_produksi_delete = $('#base-value').data('route-detail-produksi-delete');
var base_url = $('#base-value').data('base-url');

function hapus_produk() {
    $('.btn-hapus-produk').click(function(e) {
        e.preventDefault();
        var confirm = $(this).data('confirm');
        var id_detail_produksi = $(this).parents('tr').data('id-detail-produksi');
        var self = $(this);

        if(confirm) {
            swal({
                title: "Perhatian ...",
                text: "Yakin hapus data ini ?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function(e) {
                if(e.value) {
                    $.ajax({
                        url: route_detail_produksi_delete,
                        type: "POST",
                        data: {
                            _token: _token,
                            id_detail_produksi: id_detail_produksi
                        },
                        beforeSend: function() {
                            loading_start();
                        },
                        success: function() {
                            self.parents('tr').remove();
                            bahan_list();
                            loading_finish();
                        }
                    });
                }
            });
        } else {
            $(this).parents('tr').remove();
            bahan_list();
        }

    });
}

function select_produk() {
    $('[name="id_produk[]"]').change(function() {

        var id_produk = $(this).val();
        var id_lokasi = $('[name="id_lokasi"]').val();
        var kode_produk = $('[name="id_produk[]"] option[value="'+id_produk+'"]').data('kode-produk');
        var nama_produk = $('[name="id_produk[]"] option[value="'+id_produk+'"]').data('nama-produk');
        //var no_seri_produk = no_seri_produk_process(id_produk, id_lokasi);

        $(this).parents('td').siblings('.nama-produk').html(nama_produk);
        // $(this).parents('td').siblings('.no-seri-produk').children('input').val(no_seri_produk);

        bahan_list();
    });
}

function no_seri_produk_process(id_produk, id_lokasi) {
    var no_seri_produk = null;
    $.ajax({
        async: false,
        global: false,
        url: route_produksi_no_seri_produk,
        type: 'POST',
        data: {
            id_produk: id_produk,
            id_lokasi: id_lokasi,
            _token: _token
        },
        success: function (data) {
            no_seri_produk = data.no_seri_produk;
        }
    });

    return no_seri_produk;
}

function bahan_list() {
    loading_start();
    var produk_bahan_list = [];
    $('.table-produk select.id-produk').each(function(key, select) {
        var id_produk = $(this).val();
        var qty = $(this).parents('td').siblings('td.qty').children('div').children('input').val();

        /**
         * Jika touchspin tidak dibaca terlebih dahulu, karena masih proses render
         */
        if(typeof qty == 'undefined') {
            qty = $(this).parents('td').siblings('td.qty').children('input').val();
        }

        var data = {
            id_produk: id_produk,
            qty: qty
        }

        produk_bahan_list.push(data);
    });

    $.ajax({
        url: route_produksi_bahan,
        type: 'POST',
        data: {
            produk_bahan_list: produk_bahan_list,
            _token: _token
        },
        success: function(data) {
            $('.table-bahan tbody').html(data);
            $('.table-bahan tbody .touchspin-number-decimal-js').TouchSpin(touchspin_number_decimal);
            loading_finish();
        }
    });

}

function produk_add() {
    $('.btn-produk-add').click(function() {
        var row_produk = $('.row-produk table tbody').html();
        $('.table-produk tbody').append(row_produk);

        var index = $('.table-produk tbody tr').length;

        $('.table-produk tbody tr:nth-child('+index+') .detail-produksi-qty').TouchSpin(touchspin_number);
        $('.table-produk tbody tr:nth-child('+index+') select').select2();

        $('footer').hide();

        hapus_produk();
        select_produk();
        produk_qty();
    });
}

function produk_qty() {
    $('.table-produk .detil-produksi-qty, .table-produk [name="qty[]"]').on('change keyup', function() {
        bahan_list();
    });
}

function produksi_detail() {
    $('.btn-produksi-detail').click(function() {
        loading_start();
        var route = $(this).data('route');
        $.ajax({
            url: route,
            success: function(table_view) {
                $('#modal-detail .modal-body').html(table_view);
                $('#modal-detail').modal('show');
                loading_finish();
            }
        })
    });
}

function produksi_publish() {
    $('.btn-publish').click(function() {
        var self = $(this);
        swal({
            title: "Perhatian ...",
            html: "Yakin <strong>Publish</strong> produksi ini ?<br /> Setelah di-Publish, akan <strong class='m--font-danger'>Mengurangi</strong> stok bahan.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function(e) {
            if(e.value) {
                $.ajax({
                    url: self.data('route'),
                    success: function(data) {
                        window.location.reload();
                    },
                    error: function(request, error) {
                        swal({
                            title: "Perhatian",
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });
    });
}
