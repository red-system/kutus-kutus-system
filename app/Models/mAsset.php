<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mAsset extends Model
{

    protected $table = 'tb_asset';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_kategori_asset',
        'kode_asset',
        'nama',
        'tanggal_beli',
        'tanggal_beli_2',
        'qty',
        'harga_beli',
        'provisi',
        'total_beli',
        'nilai_residu',
        'umur_ekonomis',
        'lokasi',
        'akumulasi_beban',
        'terhitung_tanggal',
        'nilai_buku',
        'beban_perbulan',
        'metode',
        'tanggal_pensiun',
        'master_id_asset',
        'master_id_akumulasi',
        'master_id_depresiasi',
        'master_id_jenis_pembayaran',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name',
    ];

    function kategori_asset() {
        return $this->belongsTo(mKategoriAsset::class, 'id_kategori_asset');
    }

    public function kodePerkiraanAsset(){
        return $this->belongsTo(mAcMaster::class,'master_id_asset');
    }

    public function kodePerkiraanAkumulasi(){
        return $this->belongsTo(mAcMaster::class,'master_id_akumulasi');
    }

    public function kodePerkiraanDepresiasi(){
        return $this->belongsTo(mAcMaster::class,'master_id_depresiasi');
    }

    public function kodePerkiraanJenisPembayaran(){
        return $this->belongsTo(mAcMaster::class,'master_id_jenis_pembayaran');
    }

    public function penyusutanAsset(){
        return $this->hasMany(mPenyusutanAsset::class,'id_asset');
    }

    public static function create(array $data = [])
    {
        $user = \Illuminate\Support\Facades\Session::get('user');

        $data['created_by'] = $user->id;
        $data['created_by_name'] = $user->karyawan->nama_karyawan;
        $data['updated_by'] = $user->id;
        $data['updated_by_name'] = $user->karyawan->nama_karyawan;

        $model = static::query()->create($data);
        return $model;
    }

}
