<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mPoBahan extends Model
{

    protected $table = 'tb_po_bahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_supplier',
        'urutan',
        'no_faktur',
        'tanggal',
        'pembayaran',
        'pengiriman',
        'total',
        'biaya_tambahan',
        'potongan',
        'grand_total',
        'status_pembelian',
        'status_bahan_datang',
        'tanggal_bahan_datang',
    ];

    function supplier() {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    function po_bahan_detail() {
        return $this->hasMany(mPoBahanDetail::class, 'id_po_bahan');
    }
}
