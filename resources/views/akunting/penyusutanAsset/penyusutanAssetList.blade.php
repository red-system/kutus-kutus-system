@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('akunting.asset.modalPenyusutan')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">


            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 offset-2 col-sm-12">Tahun Penyusutan</label>
                            <div class="col-lg-2 col-md-2 col-sm-12">
                                <div class="input-group date">
                                    <input type="text"
                                           class="form-control m-input m_datepicker-year"
                                           readonly placeholder="Select date"
                                           name="tahun"
                                           value="{{ $tahun }}"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-list">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('penyusutanAssetPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('penyusutanAssetExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table m-table table-bordered table-striped table-hover m-table--head-bg-success m-table--border-success">
                        <thead>
                        <tr class="success">
                            <th rowspan="2" align="center"> No</th>
                            <th rowspan="2"> Keterangan</th>
                            <th rowspan="2" width="5%" class="text-center">Jumlah</th>
                            <th rowspan="2" width="5%"> Tahun Perolehan</th>
                            <th rowspan="2" width="5%"> Nilai Perolehan</th>
                            <th rowspan="2" width="5%"> Nilai Penyusutan</th>
                            <th colspan="13" class="text-center"> Penyusutan {{ $tahun }}</th>
                            <th rowspan="2" width="5%"> Akumulasi Penyusutan</th>
                            <th rowspan="2" width="5%"> Nilai Buku</th>
                        </tr>
                        <tr class="info">
                            <th>Thn Lalu</th>
                            <th>Jan</th>
                            <th>Feb</th>
                            <th>Mar</th>
                            <th>Apr</th>
                            <th>Mei</th>
                            <th>Jun</th>
                            <th>Jul</th>
                            <th>Ags</th>
                            <th>Sep</th>
                            <th>Okt</th>
                            <th>Nov</th>
                            <th>Des</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $ktgAsset)
                            <tr>
                                <td><strong>{{$no++}}</strong></td>
                                <td colspan="20"><strong>{{$ktgAsset->nama}}</strong></td>
                            </tr>
                            @foreach($ktgAsset->asset as $asset)
                                @php
                                    $sum_penyusutan_perbulan = $asset
                                    ->penyusutanAsset
                                    ->where('tahun', '<', $tahun)
                                    ->sum('penyusutan_perbulan')
                                @endphp
                                <tr>
                                    <td></td>
                                    <td>{{$asset->nama}}</td>
                                    <td class="text-center">{{ Main::format_number($asset->qty) }}</td>
                                    <td class="text-right">{{ date('M Y', strtotime($asset->tanggal_beli)) }}</td>
                                    <td class="text-right">{{ Main::format_number($asset->harga_beli) }}</td>
                                    <td class="text-right">{{ Main::format_number($asset->beban_perbulan) }} </td>
                                    <td class="text-right">{{ Main::format_number($sum_penyusutan_perbulan) }}</td>
                                    @for($i=1;$i<=12;$i++)
                                        @php
                                            $sum_penyusutan_perbulan = $asset
                                            ->penyusutanAsset
                                            ->where('bulan', '=', $i)
                                            ->where('tahun', '=', $tahun)
                                            ->sum('penyusutan_perbulan');
                                        @endphp
                                        <td class="text-right">{{ Main::format_number($sum_penyusutan_perbulan) }}</td>
                                    @endfor
                                    <td class="text-right"> {{ Main::format_number($asset->akumulasi_beban) }} </td>
                                    <td class="text-right"> {{ Main::format_number($asset->nilai_buku) }} </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection