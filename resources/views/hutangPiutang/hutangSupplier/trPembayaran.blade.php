<table class="row-pembayaran m--hide">
    <tbody>
    <tr>
        <td class="td-master-id">
            <select class="form-control select2-pembayaran" name="master_id[]" style="width: 250px">
                <option value="">Pilih Pembayaran</option>
                @foreach($master as $r)
                    <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                @endforeach
            </select>
        </td>
        <td class="td-jumlah">
            <input type="text" name="jumlah[]" class="form-control touchspin-number-decimal-js" value="0"
                   style="width: 80px">
        </td>
        <td class="td-no-bg">
            <input class="form-control m-input" type="text" name="no_bg[]" style="width: 120px">
        </td>
        <td class="td-tanggal-pencairan">
            <div class="input-group date" style="width: 150px">
                <input type="text" class="form-control m-input datepicker-pembayaran" name="tanggal_pencairan[]"
                       readonly=""
                       value="{{ date('d-m-Y') }}">
                <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                </div>
            </div>
        </td>
        <td class="td-nama-bank">
            <input class="form-control m-input" type="text" name="nama_bank[]" style="width: 120px">
        </td>
        <td class="td-keterangan">
            <textarea class="form-control m-input" name="keterangan[]" style="width: 120px"></textarea>
        </td>
        <td>
            <button type="button" class="btn-delete-row-pembayaran btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
    </tbody>
</table>