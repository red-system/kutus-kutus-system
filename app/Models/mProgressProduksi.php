<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mProgressProduksi extends Model
{
    protected $table = 'tb_progress_produksi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produksi',
        'id_detail_produksi',
        'id_produk',
        'id_lokasi',
        'tgl_selesai',
        'deskripsi',
        'qty_produksi',
        'qty_progress'
    ];

    function produksi()
    {
        return $this->belongsTo(mProduksi::class, 'id_produksi');
    }


    function detail_produksi()
    {
        return $this->belongsTo(mDetailProduksi::class, 'id_detail_produksi');
    }

    function produk()
    {
        return $this->belongsTo(mProduk::class, 'id_produk');
    }

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function stok_produk() {
        return $this->belongsTo(mStokProduk::class, 'id','id_progress_produksi');
    }
}
